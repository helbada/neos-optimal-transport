#ifndef __ADVECTOR_HPP__
#define __ADVECTOR_HPP__

/**
 * @file Gradient.hpp
 *
 * @author Matias Hastaran
 * @version 0.1
 * @date 2017-07-14
 * @copyright Inria
 */

#include<vector>
#include<utility>
class AdVector: private std::vector<std::pair<long,double>>
{
    typedef double T;
    typedef std::vector<std::pair<long,double>> vector;
public:
    using vector::push_back;
    using vector::operator[];
    using vector::begin;
    using vector::end;
    using vector::size;
    AdVector operator*(const AdVector & ) const;
    AdVector operator+(const AdVector & ) const;
    AdVector();
    virtual ~AdVector();
};

#endif /* __GRADIENT_HPP__ */
