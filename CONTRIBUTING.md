# Contributing to `Neos`

Create an account on https://gitlab.inria.fr.

Fork the project by clicking on the `Fork` item on the webpage
https://gitlab.inria.fr/memphis/neos. You must now have a copy of the
project `neos` in your space. Clone the project.

```
git clone git@gitlab.inria.fr:<user>/neos.git
```
or
```
git clone https://gitlab.inria.fr/<user>/neos.git
```

From now on, we assume that you are in the directory `neos`.

### Adding a contribution

Eventually, you can create a branch where you put all your changes.

Modify files. Add the files, commit them and push them.

Records contain an array authors which contains concise name.

### Sending a merge request

Look for the icon `+`, click on it and select `New merge request`.

Select the branch you want to merge to `memphis/neos`, and continue.

Describe your changes and submit the request.

## Keeping your fork up to date

### Adding remote from original repository

This must be done once.

```
git remote add upstream <what/the/way/you/want>memphis/neos.git
```

```
git fetch upstream
```

### Uptading your fork

Now, you can perform, at any time, an update of your fork.

```
git pull upstream master
```

## More about Gitlab

Additional help can be found by following [Gitlab
help](https://gitlab.inria.fr/help).
