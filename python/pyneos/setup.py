# -------------------------------------------------------------------------
#
#  NEOS
#
#  -------------------------------------------------------------------------
#  License
#  This file is part of Neos.
#
#  Neos is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License v3 (LGPL)
#  as published by the Free Software Foundation.
#
#  Neos is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
#  License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with Neos. If not, see <http://www.gnu.org/licenses/>.
#
#---------------------------------------------------------------------------

from setuptools import setup, Extension
import sys
import setuptools

__version__ = '0.0.1'


setup(
    name='pyneos',
    version=__version__,
    author='Philippe Depouilly',
    author_email='philippe.depouilly@math.u-bordeaux.fr',
    url='https://gitlab.inria.fr/neos',
    description='A Neos API using pybind11',
    long_description='',
    install_requires=['pybind11>=2.2'],
    packages=['pyneos'],
    package_data={'pyneos':['pyneos.so']},
    zip_safe=False,
)
