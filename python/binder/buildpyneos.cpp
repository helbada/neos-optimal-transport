#include <BoundaryConditions.hpp>
#include <Geometry.hpp>
#include <Grid.hpp>
#include <ILevelSet.hpp>
#include <NeosPiercedVector.hpp>
#include <PabloUniform.hpp>
#include <VTK.hpp>
#include <adaption.hpp>
#include <array>
#include <cell.hpp>
#include <common.hpp>
#include <element_type.hpp>
#include <fstream>
#include <functional>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <locale>
#include <map>
#include <memory>
#include <ostream>
#include <patch_kernel.hpp>
#include <piercedSync.hpp>
#include <piercedVector.hpp>
#include <piercedVectorKernel.hpp>
#include <piercedVectorStorage.hpp>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
#include <vertex.hpp>
#include <voloctree.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// neos::Grid file:Grid.hpp line:31
struct PyCallBack_neos_Grid : public neos::Grid {
	using neos::Grid::Grid;

	void reset() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "reset");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::reset();
	}
	void setDimension(int a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "setDimension");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::setDimension(a0);
	}
	void settleAdaptionMarkers() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "settleAdaptionMarkers");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::settleAdaptionMarkers();
	}
	double evalCellVolume(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "evalCellVolume");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return VolOctree::evalCellVolume(a0);
	}
	double evalCellSize(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "evalCellSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return VolOctree::evalCellSize(a0);
	}
	using _binder_ret_0 = struct std::array<double, 3>;
	_binder_ret_0 evalCellCentroid(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "evalCellCentroid");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return VolOctree::evalCellCentroid(a0);
	}
	void simulateCellUpdate(long a0, enum bitpit::adaption::Marker a1, class std::vector<class bitpit::Cell, class std::allocator<class bitpit::Cell> > * a2, class bitpit::PiercedVector<class bitpit::Vertex> * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "simulateCellUpdate");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::simulateCellUpdate(a0, a1, a2, a3);
	}
	void evalCellBoundingBox(long a0, struct std::array<double, 3> * a1, struct std::array<double, 3> * a2) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "evalCellBoundingBox");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::evalCellBoundingBox(a0, a1, a2);
	}
	double evalInterfaceArea(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "evalInterfaceArea");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return VolOctree::evalInterfaceArea(a0);
	}
	using _binder_ret_1 = struct std::array<double, 3>;
	_binder_ret_1 evalInterfaceNormal(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "evalInterfaceNormal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_1>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_1> caster;
				return pybind11::detail::cast_ref<_binder_ret_1>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_1>(std::move(o));
		}
		return VolOctree::evalInterfaceNormal(a0);
	}
	bool isPointInside(const struct std::array<double, 3> & a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "isPointInside");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::isPointInside(a0);
	}
	bool isPointInside(long a0, const struct std::array<double, 3> & a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "isPointInside");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::isPointInside(a0, a1);
	}
	long locatePoint(const struct std::array<double, 3> & a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "locatePoint");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<long>::value) {
				static pybind11::detail::override_caster_t<long> caster;
				return pybind11::detail::cast_ref<long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<long>(std::move(o));
		}
		return VolOctree::locatePoint(a0);
	}
	void translate(const struct std::array<double, 3> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "translate");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::translate(a0);
	}
	void scale(const struct std::array<double, 3> & a0, const struct std::array<double, 3> & a1) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "scale");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::scale(a0, a1);
	}
	int getCellHaloLayer(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "getCellHaloLayer");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<int>::value) {
				static pybind11::detail::override_caster_t<int> caster;
				return pybind11::detail::cast_ref<int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<int>(std::move(o));
		}
		return VolOctree::getCellHaloLayer(a0);
	}
	using _binder_ret_2 = class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> >;
	_binder_ret_2 _spawn(bool a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_spawn");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_2>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_2> caster;
				return pybind11::detail::cast_ref<_binder_ret_2>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_2>(std::move(o));
		}
		return VolOctree::_spawn(a0);
	}
	void _updateAdjacencies() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_updateAdjacencies");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_updateAdjacencies();
	}
	using _binder_ret_3 = class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> >;
	_binder_ret_3 _adaptionPrepare(bool a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_adaptionPrepare");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_3>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_3> caster;
				return pybind11::detail::cast_ref<_binder_ret_3>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_3>(std::move(o));
		}
		return VolOctree::_adaptionPrepare(a0);
	}
	using _binder_ret_4 = class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> >;
	_binder_ret_4 _adaptionAlter(bool a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_adaptionAlter");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_4>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_4> caster;
				return pybind11::detail::cast_ref<_binder_ret_4>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_4>(std::move(o));
		}
		return VolOctree::_adaptionAlter(a0);
	}
	void _adaptionCleanup() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_adaptionCleanup");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_adaptionCleanup();
	}
	bool _markCellForRefinement(long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_markCellForRefinement");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::_markCellForRefinement(a0);
	}
	bool _markCellForCoarsening(long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_markCellForCoarsening");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::_markCellForCoarsening(a0);
	}
	bool _resetCellAdaptionMarker(long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_resetCellAdaptionMarker");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::_resetCellAdaptionMarker(a0);
	}
	enum bitpit::adaption::Marker _getCellAdaptionMarker(long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_getCellAdaptionMarker");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<enum bitpit::adaption::Marker>::value) {
				static pybind11::detail::override_caster_t<enum bitpit::adaption::Marker> caster;
				return pybind11::detail::cast_ref<enum bitpit::adaption::Marker>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<enum bitpit::adaption::Marker>(std::move(o));
		}
		return VolOctree::_getCellAdaptionMarker(a0);
	}
	bool _enableCellBalancing(long a0, bool a1) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_enableCellBalancing");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::_enableCellBalancing(a0, a1);
	}
	void _setTol(double a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_setTol");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_setTol(a0);
	}
	void _resetTol() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_resetTol");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_resetTol();
	}
	int _getDumpVersion() const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_getDumpVersion");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<int>::value) {
				static pybind11::detail::override_caster_t<int> caster;
				return pybind11::detail::cast_ref<int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<int>(std::move(o));
		}
		return VolOctree::_getDumpVersion();
	}
	void _dump(class std::basic_ostream<char> & a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_dump");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_dump(a0);
	}
	void _restore(class std::basic_istream<char> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_restore");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_restore(a0);
	}
	long _getCellNativeIndex(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_getCellNativeIndex");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<long>::value) {
				static pybind11::detail::override_caster_t<long> caster;
				return pybind11::detail::cast_ref<long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<long>(std::move(o));
		}
		return VolOctree::_getCellNativeIndex(a0);
	}
	void _findCellNeighs(long a0, const class std::vector<long, class std::allocator<long> > & a1, class std::vector<long, class std::allocator<long> > * a2) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_findCellNeighs");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_findCellNeighs(a0, a1, a2);
	}
	void _findCellEdgeNeighs(long a0, int a1, const class std::vector<long, class std::allocator<long> > & a2, class std::vector<long, class std::allocator<long> > * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_findCellEdgeNeighs");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_findCellEdgeNeighs(a0, a1, a2, a3);
	}
	void _findCellVertexNeighs(long a0, int a1, const class std::vector<long, class std::allocator<long> > & a2, class std::vector<long, class std::allocator<long> > * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_findCellVertexNeighs");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_findCellVertexNeighs(a0, a1, a2, a3);
	}
	unsigned long _getMaxHaloSize() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_getMaxHaloSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned long>::value) {
				static pybind11::detail::override_caster_t<unsigned long> caster;
				return pybind11::detail::cast_ref<unsigned long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned long>(std::move(o));
		}
		return VolOctree::_getMaxHaloSize();
	}
	void _setHaloSize(unsigned long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_setHaloSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_setHaloSize(a0);
	}
	using _binder_ret_5 = class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> >;
	_binder_ret_5 _partitioningAlter(bool a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_partitioningAlter");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_5>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_5> caster;
				return pybind11::detail::cast_ref<_binder_ret_5>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_5>(std::move(o));
		}
		return VolOctree::_partitioningAlter(a0);
	}
	void _partitioningCleanup() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_partitioningCleanup");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_partitioningCleanup();
	}
	using _binder_ret_6 = class std::vector<long, class std::allocator<long> >;
	_binder_ret_6 _findGhostCellExchangeSources(int a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_findGhostCellExchangeSources");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_6>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_6> caster;
				return pybind11::detail::cast_ref<_binder_ret_6>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_6>(std::move(o));
		}
		return VolOctree::_findGhostCellExchangeSources(a0);
	}
	void resetVertices() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "resetVertices");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::resetVertices();
	}
	void resetCells() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "resetCells");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::resetCells();
	}
	void resetInterfaces() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "resetInterfaces");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::resetInterfaces();
	}
	long getVertexCount() const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "getVertexCount");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<long>::value) {
				static pybind11::detail::override_caster_t<long> caster;
				return pybind11::detail::cast_ref<long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<long>(std::move(o));
		}
		return PatchKernel::getVertexCount();
	}
	long getCellCount() const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "getCellCount");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<long>::value) {
				static pybind11::detail::override_caster_t<long> caster;
				return pybind11::detail::cast_ref<long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<long>(std::move(o));
		}
		return PatchKernel::getCellCount();
	}
	enum bitpit::ElementType getCellType(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "getCellType");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<enum bitpit::ElementType>::value) {
				static pybind11::detail::override_caster_t<enum bitpit::ElementType> caster;
				return pybind11::detail::cast_ref<enum bitpit::ElementType>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<enum bitpit::ElementType>(std::move(o));
		}
		return PatchKernel::getCellType(a0);
	}
	long getInterfaceCount() const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "getInterfaceCount");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<long>::value) {
				static pybind11::detail::override_caster_t<long> caster;
				return pybind11::detail::cast_ref<long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<long>(std::move(o));
		}
		return PatchKernel::getInterfaceCount();
	}
	enum bitpit::ElementType getInterfaceType(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "getInterfaceType");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<enum bitpit::ElementType>::value) {
				static pybind11::detail::override_caster_t<enum bitpit::ElementType> caster;
				return pybind11::detail::cast_ref<enum bitpit::ElementType>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<enum bitpit::ElementType>(std::move(o));
		}
		return PatchKernel::getInterfaceType(a0);
	}
	using _binder_ret_7 = struct std::array<double, 3>;
	_binder_ret_7 evalInterfaceCentroid(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "evalInterfaceCentroid");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_7>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_7> caster;
				return pybind11::detail::cast_ref<_binder_ret_7>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_7>(std::move(o));
		}
		return PatchKernel::evalInterfaceCentroid(a0);
	}
	void evalInterfaceBoundingBox(long a0, struct std::array<double, 3> * a1, struct std::array<double, 3> * a2) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "evalInterfaceBoundingBox");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::evalInterfaceBoundingBox(a0, a1, a2);
	}
	void flushData(class std::basic_fstream<char> & a0, const class std::__cxx11::basic_string<char> & a1, enum bitpit::VTKFormat a2) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "flushData");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::flushData(a0, a1, a2);
	}
	void _resetAdjacencies() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_resetAdjacencies");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::_resetAdjacencies();
	}
	void _resetInterfaces() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_resetInterfaces");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::_resetInterfaces();
	}
	void _updateInterfaces() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_updateInterfaces");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::_updateInterfaces();
	}
	void _findCellFaceNeighs(long a0, int a1, const class std::vector<long, class std::allocator<long> > & a2, class std::vector<long, class std::allocator<long> > * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Grid *>(this), "_findCellFaceNeighs");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::_findCellFaceNeighs(a0, a1, a2, a3);
	}
};

// neos::Geometry file:Geometry.hpp line:31
struct PyCallBack_neos_Geometry : public neos::Geometry {
	using neos::Geometry::Geometry;

	double getLevelSet(const struct std::array<double, 3> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Geometry *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Geometry::getLevelSet\"");
	}
	double getLevelSet(const long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Geometry *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Geometry::getLevelSet\"");
	}
	void computeLevelSet(class neos::Grid * a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Geometry *>(this), "computeLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Geometry::computeLevelSet\"");
	}
	using _binder_ret_0 = class neos::PiercedVector<double, long> &;
	_binder_ret_0 getPhi() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::Geometry *>(this), "getPhi");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Geometry::getPhi\"");
	}
};

void bind_BoundaryConditions(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B612_[neos::BoundaryConditions] ";
	{ // neos::BoundaryConditions file:BoundaryConditions.hpp line:32
		pybind11::class_<neos::BoundaryConditions, std::shared_ptr<neos::BoundaryConditions>> cl(M("neos"), "BoundaryConditions", "Class to handle boundary conditions");
		cl.def( pybind11::init( [](){ return new neos::BoundaryConditions(); } ) );
		cl.def( pybind11::init( [](neos::BoundaryConditions const &o){ return new neos::BoundaryConditions(o); } ) );
		cl.def("addCondition", [](neos::BoundaryConditions &o, int const & a0, class std::__cxx11::basic_string<char> const & a1, class std::function<double (struct std::array<double, 3>, double)> const & a2) -> void { return o.addCondition(a0, a1, a2); }, "", pybind11::arg("border"), pybind11::arg("cond"), pybind11::arg("f"));
		cl.def("addCondition", (void (neos::BoundaryConditions::*)(int, std::string, class std::function<double (struct std::array<double, 3>, double)>, enum neos::Var)) &neos::BoundaryConditions::addCondition, "Add a boundary condition on border\n\n \n Local number of the border\n \n\n \"Dirichlet\" or \"Neumann\"\n \n\n Function used as boundary conditions\n\nC++: neos::BoundaryConditions::addCondition(int, std::string, class std::function<double (struct std::array<double, 3>, double)>, enum neos::Var) --> void", pybind11::arg("border"), pybind11::arg("cond"), pybind11::arg("f"), pybind11::arg("type"));
		cl.def("getBCConditions", (class std::map<struct std::pair<enum neos::Var, int>, struct std::pair<std::string, class std::function<double (struct std::array<double, 3>, double)> >, struct std::less<struct std::pair<enum neos::Var, int> >, class std::allocator<struct std::pair<const struct std::pair<enum neos::Var, int>, struct std::pair<std::string, class std::function<double (struct std::array<double, 3>, double)> > > > > & (neos::BoundaryConditions::*)()) &neos::BoundaryConditions::getBCConditions, "Get boundary conditions function\n\n \n Boundary conditions in form <Condition type, function>\n\nC++: neos::BoundaryConditions::getBCConditions() --> class std::map<struct std::pair<enum neos::Var, int>, struct std::pair<std::string, class std::function<double (struct std::array<double, 3>, double)> >, struct std::less<struct std::pair<enum neos::Var, int> >, class std::allocator<struct std::pair<const struct std::pair<enum neos::Var, int>, struct std::pair<std::string, class std::function<double (struct std::array<double, 3>, double)> > > > > &", pybind11::return_value_policy::automatic);
		cl.def("getBCType", [](neos::BoundaryConditions &o, const int & a0) -> std::string { return o.getBCType(a0); }, "", pybind11::arg("boundId"));
		cl.def("getBCType", (std::string (neos::BoundaryConditions::*)(const int, enum neos::Var)) &neos::BoundaryConditions::getBCType, "C++: neos::BoundaryConditions::getBCType(const int, enum neos::Var) --> std::string", pybind11::arg("boundId"), pybind11::arg("type"));
		cl.def("getBCFunction", [](neos::BoundaryConditions &o, const int & a0) -> std::function<double (struct std::array<double, 3>, double)> { return o.getBCFunction(a0); }, "", pybind11::arg("boundId"));
		cl.def("getBCFunction", (class std::function<double (struct std::array<double, 3>, double)> (neos::BoundaryConditions::*)(const int, enum neos::Var)) &neos::BoundaryConditions::getBCFunction, "C++: neos::BoundaryConditions::getBCFunction(const int, enum neos::Var) --> class std::function<double (struct std::array<double, 3>, double)>", pybind11::arg("boundId"), pybind11::arg("type"));
	}
	std::cout << "B613_[neos::Grid] ";
	{ // neos::Grid file:Grid.hpp line:31
		pybind11::class_<neos::Grid, std::shared_ptr<neos::Grid>, PyCallBack_neos_Grid, bitpit::VolOctree> cl(M("neos"), "Grid", "Abstraction class to use bitpit grid class\n Change-it for an Absctraction for bitpit and a non-z-order cartesian grid ?");
		cl.def( pybind11::init( [](double const & a0, double const & a1, double const & a2, double const & a3, double const & a4){ return new neos::Grid(a0, a1, a2, a3, a4); }, [](double const & a0, double const & a1, double const & a2, double const & a3, double const & a4){ return new PyCallBack_neos_Grid(a0, a1, a2, a3, a4); } ), "doc");
		cl.def( pybind11::init<double, double, double, double, double, unsigned char>(), pybind11::arg("X"), pybind11::arg("Y"), pybind11::arg("Z"), pybind11::arg("L"), pybind11::arg("dh"), pybind11::arg("dim") );

		cl.def( pybind11::init( [](double const & a0, double const & a1, double const & a2, double const & a3, double const & a4, class neos::BoundaryConditions * a5){ return new neos::Grid(a0, a1, a2, a3, a4, a5); }, [](double const & a0, double const & a1, double const & a2, double const & a3, double const & a4, class neos::BoundaryConditions * a5){ return new PyCallBack_neos_Grid(a0, a1, a2, a3, a4, a5); } ), "doc");
		cl.def( pybind11::init<double, double, double, double, double, class neos::BoundaryConditions *, unsigned char>(), pybind11::arg("X"), pybind11::arg("Y"), pybind11::arg("Z"), pybind11::arg("L"), pybind11::arg("dh"), pybind11::arg("BC"), pybind11::arg("dim") );

		cl.def( pybind11::init( [](PyCallBack_neos_Grid const &o){ return new PyCallBack_neos_Grid(o); } ) );
		cl.def( pybind11::init( [](neos::Grid const &o){ return new neos::Grid(o); } ) );
		cl.def("getDim", (int (neos::Grid::*)()) &neos::Grid::getDim, "Get the dimension\n\nC++: neos::Grid::getDim() --> int");
		cl.def("getNFaceVertex", (int (neos::Grid::*)()) &neos::Grid::getNFaceVertex, "Get the number of Vertices per Face\n\nC++: neos::Grid::getNFaceVertex() --> int");
		cl.def("nbCells", (long (neos::Grid::*)()) &neos::Grid::nbCells, "gets the number of cells\n\n \n file name\n\nC++: neos::Grid::nbCells() --> long");
		cl.def("refineFromRangeVector", (long (neos::Grid::*)(class std::vector<double, class std::allocator<double> >, double, double)) &neos::Grid::refineFromRangeVector, "Return elements size Refine from a Vector Range Values\n\nC++: neos::Grid::refineFromRangeVector(class std::vector<double, class std::allocator<double> >, double, double) --> long", pybind11::arg("v"), pybind11::arg("min"), pybind11::arg("max"));
		cl.def("getMapper", (class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > (neos::Grid::*)()) &neos::Grid::getMapper, "C++: neos::Grid::getMapper() --> class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> >");
		cl.def("update", [](neos::Grid &o) -> void { return o.update(); }, "");
		cl.def("update", [](neos::Grid &o, bool const & a0) -> void { return o.update(a0); }, "", pybind11::arg("trackAdaption"));
		cl.def("update", (void (neos::Grid::*)(bool, bool)) &neos::Grid::update, "Update Mesh for Refinement and store Mapper for Levelset Updates\n\nC++: neos::Grid::update(bool, bool) --> void", pybind11::arg("trackAdaption"), pybind11::arg("squeezeStorage"));
		cl.def("findQuadrantNeighs", (class std::vector<long, class std::allocator<long> > (neos::Grid::*)(const struct std::array<double, 3> &, const long)) &neos::Grid::findQuadrantNeighs, "Return a vector of the neighbours id\n\n \n Coordinate of the point\n \n\n Id of the cell\n\nC++: neos::Grid::findQuadrantNeighs(const struct std::array<double, 3> &, const long) --> class std::vector<long, class std::allocator<long> >", pybind11::arg("x0"), pybind11::arg("id"));
		cl.def("locatePoint", (long (neos::Grid::*)(const struct std::array<double, 3> &)) &neos::Grid::locatePoint, "Return the id of the cell\n\n \n Coordinate of the point\n\nC++: neos::Grid::locatePoint(const struct std::array<double, 3> &) --> long", pybind11::arg("point"));
		cl.def("setExportName", (void (neos::Grid::*)(std::string)) &neos::Grid::setExportName, "set name for export files\n\n \n file name\n\nC++: neos::Grid::setExportName(std::string) --> void", pybind11::arg("name"));
		cl.def("setCounter", [](neos::Grid &o) -> void { return o.setCounter(); }, "");
		cl.def("setCounter", (void (neos::Grid::*)(int)) &neos::Grid::setCounter, "set name for export files\n\n \n file name\n\nC++: neos::Grid::setCounter(int) --> void", pybind11::arg("c"));
		cl.def("addData", (void (neos::Grid::*)(std::string, class std::vector<double, class std::allocator<double> > &)) &neos::Grid::addData, "C++: neos::Grid::addData(std::string, class std::vector<double, class std::allocator<double> > &) --> void", pybind11::arg("tag"), pybind11::arg("data"));
		cl.def("addData", (void (neos::Grid::*)(std::string, class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > > &)) &neos::Grid::addData, "C++: neos::Grid::addData(std::string, class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > > &) --> void", pybind11::arg("tag"), pybind11::arg("data"));
		cl.def("write", (void (neos::Grid::*)()) &neos::Grid::write, "C++: neos::Grid::write() --> void");
		cl.def("getMinSize", (double (neos::Grid::*)()) &neos::Grid::getMinSize, "Get the smallest edge size\n\n \n Smallest edge size\n\nC++: neos::Grid::getMinSize() --> double");
		cl.def("getFaceVertexLocalIds", (class std::vector<int, class std::allocator<int> > (neos::Grid::*)(int)) &neos::Grid::getFaceVertexLocalIds, "C++: neos::Grid::getFaceVertexLocalIds(int) --> class std::vector<int, class std::allocator<int> >", pybind11::arg("face"));
		cl.def("hasVertexOnBorder", (bool (neos::Grid::*)(const int &, int &)) &neos::Grid::hasVertexOnBorder, "C++: neos::Grid::hasVertexOnBorder(const int &, int &) --> bool", pybind11::arg("interId"), pybind11::arg("interOnBorder"));
		cl.def("isBorder", (bool (neos::Grid::*)(const long &)) &neos::Grid::isBorder, "Is the cell \"cellId\" is a boundary cell ?\n\n \n Global id of the cell\n\n \n True if the cell is on boundary\n\nC++: neos::Grid::isBorder(const long &) --> bool", pybind11::arg("cellId"));
		cl.def("nbBorder", (unsigned long (neos::Grid::*)(const long &)) &neos::Grid::nbBorder, "Return number of faces which are on boundary\n\n \n Global id of the cell\n\n \n Number of boundary faces\n\nC++: neos::Grid::nbBorder(const long &) --> unsigned long", pybind11::arg("cellId"));
		cl.def("getBoundFaceId", (int (neos::Grid::*)(const long &)) &neos::Grid::getBoundFaceId, "Return the local id of the boundary face.\n\n \n Global id of the cell\n\n \n local id of the boundary face\n\nC++: neos::Grid::getBoundFaceId(const long &) --> int", pybind11::arg("cellId"));
		cl.def("getBoundFacesId", (class std::vector<int, class std::allocator<int> > (neos::Grid::*)(const long &)) &neos::Grid::getBoundFacesId, "Return the local ids of the boundary faces.\n\n \n Global id of the cell\n\n \n vector of boundary faces local id\n\nC++: neos::Grid::getBoundFacesId(const long &) --> class std::vector<int, class std::allocator<int> >", pybind11::arg("cellId"));
		cl.def("computeBoundaryValue", [](neos::Grid const &o, const struct std::array<double, 3> & a0, const double & a1, const int & a2) -> double { return o.computeBoundaryValue(a0, a1, a2); }, "", pybind11::arg("coord"), pybind11::arg("val"), pybind11::arg("boundaryFace"));
		cl.def("computeBoundaryValue", [](neos::Grid const &o, const struct std::array<double, 3> & a0, const double & a1, const int & a2, enum neos::Var const & a3) -> double { return o.computeBoundaryValue(a0, a1, a2, a3); }, "", pybind11::arg("coord"), pybind11::arg("val"), pybind11::arg("boundaryFace"), pybind11::arg("type"));
		cl.def("computeBoundaryValue", (double (neos::Grid::*)(const struct std::array<double, 3> &, const double &, const int &, enum neos::Var, double) const) &neos::Grid::computeBoundaryValue, "compute boundary value depending on boundary conditions (Neumann\n     or Dirichlet).\n\n \n Coordinate where we want to compute boundary value\n \n\n value at cell center\n \n\n local id of the boundary face\n \n\n Type of function Var::P, Var::Ux, Var::Uy or Var::LS\n\n \n Value used for boundary ghost cell.\n\nC++: neos::Grid::computeBoundaryValue(const struct std::array<double, 3> &, const double &, const int &, enum neos::Var, double) const --> double", pybind11::arg("coord"), pybind11::arg("val"), pybind11::arg("boundaryFace"), pybind11::arg("type"), pybind11::arg("t"));
		cl.def("getBoundaryConditions", (class neos::BoundaryConditions * (neos::Grid::*)()) &neos::Grid::getBoundaryConditions, "Return boundary conditions\n\n \n BoundaryConditions for the grid\n\nC++: neos::Grid::getBoundaryConditions() --> class neos::BoundaryConditions *", pybind11::return_value_policy::automatic);
		cl.def("haveBoundaryConditions", (bool (neos::Grid::*)()) &neos::Grid::haveBoundaryConditions, "Does the user provides boundary conditions in constructor ?\n\n \n True if user provides boundary conditions. False if not\n\nC++: neos::Grid::haveBoundaryConditions() --> bool");
		cl.def("getCellGlobalMap", (class std::unordered_map<long, long, struct std::hash<long>, struct std::equal_to<long>, class std::allocator<struct std::pair<const long, long> > > (neos::Grid::*)()) &neos::Grid::getCellGlobalMap, "C++: neos::Grid::getCellGlobalMap() --> class std::unordered_map<long, long, struct std::hash<long>, struct std::equal_to<long>, class std::allocator<struct std::pair<const long, long> > >");
		cl.def("getCells", (const class bitpit::PiercedVector<class bitpit::Cell> & (neos::Grid::*)() const) &neos::Grid::getCells, "C++: neos::Grid::getCells() const --> const class bitpit::PiercedVector<class bitpit::Cell> &", pybind11::return_value_policy::automatic);
		cl.def("getCell", (const class bitpit::Cell & (neos::Grid::*)(const long &) const) &neos::Grid::getCell, "C++: neos::Grid::getCell(const long &) const --> const class bitpit::Cell &", pybind11::return_value_policy::automatic, pybind11::arg("id"));
		cl.def("getCellLevel", (int (neos::Grid::*)(const long &) const) &neos::Grid::getCellLevel, "C++: neos::Grid::getCellLevel(const long &) const --> int", pybind11::arg("id"));
	}
	std::cout << "B614_[neos::Geometry] ";
	{ // neos::Geometry file:Geometry.hpp line:31
		pybind11::class_<neos::Geometry, std::shared_ptr<neos::Geometry>, PyCallBack_neos_Geometry> cl(M("neos"), "Geometry", "");
		cl.def( pybind11::init( [](){ return new PyCallBack_neos_Geometry(); } ) );
		cl.def(pybind11::init<PyCallBack_neos_Geometry const &>());
		cl.def("getLevelSet", (double (neos::Geometry::*)(const struct std::array<double, 3> &)) &neos::Geometry::getLevelSet, "Return the levelset value of the point\n\n \n coordinate array of the point { { x, y, z } }\n\nC++: neos::Geometry::getLevelSet(const struct std::array<double, 3> &) --> double", pybind11::arg("point"));
		cl.def("getLevelSet", (double (neos::Geometry::*)(const long)) &neos::Geometry::getLevelSet, "C++: neos::Geometry::getLevelSet(const long) --> double", pybind11::arg("id"));
		cl.def("computeLevelSet", (void (neos::Geometry::*)(class neos::Grid *)) &neos::Geometry::computeLevelSet, "C++: neos::Geometry::computeLevelSet(class neos::Grid *) --> void", pybind11::arg("grid"));
		cl.def("getPhi", (class neos::PiercedVector<double, long> & (neos::Geometry::*)()) &neos::Geometry::getPhi, "C++: neos::Geometry::getPhi() --> class neos::PiercedVector<double, long> &", pybind11::return_value_policy::automatic);
		cl.def("assign", (class neos::Geometry & (neos::Geometry::*)(const class neos::Geometry &)) &neos::Geometry::operator=, "C++: neos::Geometry::operator=(const class neos::Geometry &) --> class neos::Geometry &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B615_[neos::ILevelSet] ";
	{ // neos::ILevelSet file:ILevelSet.hpp line:23
		pybind11::class_<neos::ILevelSet, std::shared_ptr<neos::ILevelSet>> cl(M("neos"), "ILevelSet", "Class to apply the Levelset method to the Geometry objects");
		cl.def( pybind11::init<class neos::Grid *>(), pybind11::arg("grid") );

		cl.def( pybind11::init( [](neos::ILevelSet const &o){ return new neos::ILevelSet(o); } ) );
		cl.def("addGeometry", (void (neos::ILevelSet::*)(class neos::Geometry *)) &neos::ILevelSet::addGeometry, "Add a new Geometry in the vector\n\n \n Pointer to the Geometry\n\nC++: neos::ILevelSet::addGeometry(class neos::Geometry *) --> void", pybind11::arg("geo"));
		cl.def("getGeometry", (class neos::Geometry * (neos::ILevelSet::*)()) &neos::ILevelSet::getGeometry, "Return the first Geometry in the vector\n\nC++: neos::ILevelSet::getGeometry() --> class neos::Geometry *", pybind11::return_value_policy::automatic);
		cl.def("get", (class std::vector<double, class std::allocator<double> > (neos::ILevelSet::*)()) &neos::ILevelSet::get, "Return the levelset of the grid without update\n\nC++: neos::ILevelSet::get() --> class std::vector<double, class std::allocator<double> >");
		cl.def("getGeoNbr", (int (neos::ILevelSet::*)()) &neos::ILevelSet::getGeoNbr, "Return the number of geometry\n\nC++: neos::ILevelSet::getGeoNbr() --> int");
		cl.def("getLevelSet", (double (neos::ILevelSet::*)(const struct std::array<double, 3> &)) &neos::ILevelSet::getLevelSet, "Return the phi value of the point\n\n \n Position of the point\n\nC++: neos::ILevelSet::getLevelSet(const struct std::array<double, 3> &) --> double", pybind11::arg("cell"));
		cl.def("getLevelSet", (class std::vector<double, class std::allocator<double> > (neos::ILevelSet::*)()) &neos::ILevelSet::getLevelSet, "Update and return the levelset vector\n\nC++: neos::ILevelSet::getLevelSet() --> class std::vector<double, class std::allocator<double> >");
		cl.def("getLevelSet", (double (neos::ILevelSet::*)(const long &)) &neos::ILevelSet::getLevelSet, "C++: neos::ILevelSet::getLevelSet(const long &) --> double", pybind11::arg("id"));
	}
}
#include <DataLBInterface.hpp>
#include <binary_archive.hpp>
#include <binary_stream.hpp>
#include <communications.hpp>
#include <communications_buffers.hpp>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <memory>
#include <ostream>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_DataLBInterface(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B520_[bitpit::DataLBInterface<bitpit::DummyDataLBImpl>] ";
	{ // bitpit::DataLBInterface file:DataLBInterface.hpp line:70
		pybind11::class_<bitpit::DataLBInterface<bitpit::DummyDataLBImpl>, std::shared_ptr<bitpit::DataLBInterface<bitpit::DummyDataLBImpl>>> cl(M("bitpit"), "DataLBInterface_bitpit_DummyDataLBImpl_t", "");
		cl.def("size", (unsigned long (bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::*)(const unsigned int) const) &bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::size, "C++: bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::size(const unsigned int) const --> unsigned long", pybind11::arg("e"));
		cl.def("fixedSize", (unsigned long (bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::*)() const) &bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::fixedSize, "C++: bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::fixedSize() const --> unsigned long");
		cl.def("move", (void (bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::*)(const unsigned int, const unsigned int)) &bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::move, "C++: bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::move(const unsigned int, const unsigned int) --> void", pybind11::arg("from"), pybind11::arg("to"));
		cl.def("assign", (void (bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::*)(unsigned int, unsigned int)) &bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::assign, "C++: bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::assign(unsigned int, unsigned int) --> void", pybind11::arg("stride"), pybind11::arg("length"));
		cl.def("resize", (void (bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::*)(unsigned int)) &bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::resize, "C++: bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::resize(unsigned int) --> void", pybind11::arg("newSize"));
		cl.def("resizeGhost", (void (bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::*)(unsigned int)) &bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::resizeGhost, "C++: bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::resizeGhost(unsigned int) --> void", pybind11::arg("newSize"));
		cl.def("shrink", (void (bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::*)()) &bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::shrink, "C++: bitpit::DataLBInterface<bitpit::DummyDataLBImpl>::shrink() --> void");
	}
	std::cout << "B521_[bitpit::DummyDataLBImpl] ";
	{ // bitpit::DummyDataLBImpl file:DataLBInterface.hpp line:105
		pybind11::class_<bitpit::DummyDataLBImpl, std::shared_ptr<bitpit::DummyDataLBImpl>, bitpit::DataLBInterface<bitpit::DummyDataLBImpl>> cl(M("bitpit"), "DummyDataLBImpl", "Dummy class for data communications\n\n    This class is a dummy class for the user interface to data communications\n    for load balance. All the implemented methods are no-op.");
		cl.def( pybind11::init( [](){ return new bitpit::DummyDataLBImpl(); } ) );
		cl.def("size", (unsigned long (bitpit::DummyDataLBImpl::*)(const unsigned int) const) &bitpit::DummyDataLBImpl::size, "C++: bitpit::DummyDataLBImpl::size(const unsigned int) const --> unsigned long", pybind11::arg("e"));
		cl.def("fixedSize", (unsigned long (bitpit::DummyDataLBImpl::*)() const) &bitpit::DummyDataLBImpl::fixedSize, "C++: bitpit::DummyDataLBImpl::fixedSize() const --> unsigned long");
		cl.def("move", (void (bitpit::DummyDataLBImpl::*)(const unsigned int, const unsigned int)) &bitpit::DummyDataLBImpl::move, "C++: bitpit::DummyDataLBImpl::move(const unsigned int, const unsigned int) --> void", pybind11::arg("from"), pybind11::arg("to"));
		cl.def("assign", (void (bitpit::DummyDataLBImpl::*)(unsigned int, unsigned int)) &bitpit::DummyDataLBImpl::assign, "C++: bitpit::DummyDataLBImpl::assign(unsigned int, unsigned int) --> void", pybind11::arg("stride"), pybind11::arg("length"));
		cl.def("resize", (void (bitpit::DummyDataLBImpl::*)(unsigned int)) &bitpit::DummyDataLBImpl::resize, "C++: bitpit::DummyDataLBImpl::resize(unsigned int) --> void", pybind11::arg("newSize"));
		cl.def("resizeGhost", (void (bitpit::DummyDataLBImpl::*)(unsigned int)) &bitpit::DummyDataLBImpl::resizeGhost, "C++: bitpit::DummyDataLBImpl::resizeGhost(unsigned int) --> void", pybind11::arg("newSize"));
		cl.def("shrink", (void (bitpit::DummyDataLBImpl::*)()) &bitpit::DummyDataLBImpl::shrink, "C++: bitpit::DummyDataLBImpl::shrink() --> void");
	}
	std::cout << "B522_[bitpit::CommunicationBuffer<bitpit::OBinaryStream>] ";
	{ // bitpit::CommunicationBuffer file:communications_buffers.hpp line:60
		pybind11::class_<bitpit::CommunicationBuffer<bitpit::OBinaryStream>, std::shared_ptr<bitpit::CommunicationBuffer<bitpit::OBinaryStream>>> cl(M("bitpit"), "CommunicationBuffer_bitpit_OBinaryStream_t", "");
		cl.def( pybind11::init( [](){ return new bitpit::CommunicationBuffer<bitpit::OBinaryStream>(); } ), "doc" );
		cl.def( pybind11::init( [](unsigned long const & a0){ return new bitpit::CommunicationBuffer<bitpit::OBinaryStream>(a0); } ), "doc" , pybind11::arg("size"));
		cl.def( pybind11::init<unsigned long, bool>(), pybind11::arg("size"), pybind11::arg("doubleBuffer") );

		cl.def( pybind11::init( [](bitpit::CommunicationBuffer<bitpit::OBinaryStream> const &o){ return new bitpit::CommunicationBuffer<bitpit::OBinaryStream>(o); } ) );
		cl.def("getSize", (unsigned long (bitpit::CommunicationBuffer<bitpit::OBinaryStream>::*)() const) &bitpit::CommunicationBuffer<bitpit::OBinaryStream>::getSize, "C++: bitpit::CommunicationBuffer<bitpit::OBinaryStream>::getSize() const --> unsigned long");
		cl.def("setSize", (void (bitpit::CommunicationBuffer<bitpit::OBinaryStream>::*)(unsigned long)) &bitpit::CommunicationBuffer<bitpit::OBinaryStream>::setSize, "C++: bitpit::CommunicationBuffer<bitpit::OBinaryStream>::setSize(unsigned long) --> void", pybind11::arg("size"));
		cl.def("seekg", (bool (bitpit::CommunicationBuffer<bitpit::OBinaryStream>::*)(unsigned long)) &bitpit::CommunicationBuffer<bitpit::OBinaryStream>::seekg, "C++: bitpit::CommunicationBuffer<bitpit::OBinaryStream>::seekg(unsigned long) --> bool", pybind11::arg("pos"));
		cl.def("isDouble", (bool (bitpit::CommunicationBuffer<bitpit::OBinaryStream>::*)() const) &bitpit::CommunicationBuffer<bitpit::OBinaryStream>::isDouble, "C++: bitpit::CommunicationBuffer<bitpit::OBinaryStream>::isDouble() const --> bool");
	}
	std::cout << "B523_[bitpit::CommunicationBuffer<bitpit::IBinaryStream>] ";
	{ // bitpit::CommunicationBuffer file:communications_buffers.hpp line:60
		pybind11::class_<bitpit::CommunicationBuffer<bitpit::IBinaryStream>, std::shared_ptr<bitpit::CommunicationBuffer<bitpit::IBinaryStream>>> cl(M("bitpit"), "CommunicationBuffer_bitpit_IBinaryStream_t", "");
		cl.def( pybind11::init( [](){ return new bitpit::CommunicationBuffer<bitpit::IBinaryStream>(); } ), "doc" );
		cl.def( pybind11::init( [](unsigned long const & a0){ return new bitpit::CommunicationBuffer<bitpit::IBinaryStream>(a0); } ), "doc" , pybind11::arg("size"));
		cl.def( pybind11::init<unsigned long, bool>(), pybind11::arg("size"), pybind11::arg("doubleBuffer") );

		cl.def( pybind11::init( [](bitpit::CommunicationBuffer<bitpit::IBinaryStream> const &o){ return new bitpit::CommunicationBuffer<bitpit::IBinaryStream>(o); } ) );
		cl.def("getSize", (unsigned long (bitpit::CommunicationBuffer<bitpit::IBinaryStream>::*)() const) &bitpit::CommunicationBuffer<bitpit::IBinaryStream>::getSize, "C++: bitpit::CommunicationBuffer<bitpit::IBinaryStream>::getSize() const --> unsigned long");
		cl.def("setSize", (void (bitpit::CommunicationBuffer<bitpit::IBinaryStream>::*)(unsigned long)) &bitpit::CommunicationBuffer<bitpit::IBinaryStream>::setSize, "C++: bitpit::CommunicationBuffer<bitpit::IBinaryStream>::setSize(unsigned long) --> void", pybind11::arg("size"));
		cl.def("seekg", (bool (bitpit::CommunicationBuffer<bitpit::IBinaryStream>::*)(unsigned long)) &bitpit::CommunicationBuffer<bitpit::IBinaryStream>::seekg, "C++: bitpit::CommunicationBuffer<bitpit::IBinaryStream>::seekg(unsigned long) --> bool", pybind11::arg("pos"));
		cl.def("isDouble", (bool (bitpit::CommunicationBuffer<bitpit::IBinaryStream>::*)() const) &bitpit::CommunicationBuffer<bitpit::IBinaryStream>::isDouble, "C++: bitpit::CommunicationBuffer<bitpit::IBinaryStream>::isDouble() const --> bool");
	}
	std::cout << "B524_[bitpit::SendBuffer] ";
	{ // bitpit::SendBuffer file:communications_buffers.hpp line:90
		pybind11::class_<bitpit::SendBuffer, std::shared_ptr<bitpit::SendBuffer>, bitpit::CommunicationBuffer<bitpit::OBinaryStream>> cl(M("bitpit"), "SendBuffer", "");
		cl.def( pybind11::init( [](){ return new bitpit::SendBuffer(); } ), "doc" );
		cl.def( pybind11::init( [](unsigned long const & a0){ return new bitpit::SendBuffer(a0); } ), "doc" , pybind11::arg("size"));
		cl.def( pybind11::init<unsigned long, bool>(), pybind11::arg("size"), pybind11::arg("doubleBuffer") );

		cl.def("squeeze", (void (bitpit::SendBuffer::*)()) &bitpit::SendBuffer::squeeze, "C++: bitpit::SendBuffer::squeeze() --> void");
		cl.def("write", (void (bitpit::SendBuffer::*)(const char *, unsigned long)) &bitpit::SendBuffer::write, "C++: bitpit::SendBuffer::write(const char *, unsigned long) --> void", pybind11::arg("data"), pybind11::arg("size"));
	}
	std::cout << "B525_[bitpit::RecvBuffer] ";
	{ // bitpit::RecvBuffer file:communications_buffers.hpp line:106
		pybind11::class_<bitpit::RecvBuffer, std::shared_ptr<bitpit::RecvBuffer>, bitpit::CommunicationBuffer<bitpit::IBinaryStream>> cl(M("bitpit"), "RecvBuffer", "");
		cl.def( pybind11::init( [](){ return new bitpit::RecvBuffer(); } ), "doc" );
		cl.def( pybind11::init( [](unsigned long const & a0){ return new bitpit::RecvBuffer(a0); } ), "doc" , pybind11::arg("size"));
		cl.def( pybind11::init<unsigned long, bool>(), pybind11::arg("size"), pybind11::arg("doubleBuffer") );

		cl.def("read", (void (bitpit::RecvBuffer::*)(char *, unsigned long)) &bitpit::RecvBuffer::read, "C++: bitpit::RecvBuffer::read(char *, unsigned long) --> void", pybind11::arg("data"), pybind11::arg("size"));
	}
	std::cout << "B526_[bitpit::DataCommunicator] ";
	std::cout << "B527_[bitpit::BinaryArchive] ";
	{ // bitpit::BinaryArchive file:binary_archive.hpp line:34
		pybind11::class_<bitpit::BinaryArchive, std::shared_ptr<bitpit::BinaryArchive>> cl(M("bitpit"), "BinaryArchive", "");
		cl.def( pybind11::init( [](){ return new bitpit::BinaryArchive(); } ) );
		cl.def("close", [](bitpit::BinaryArchive &o) -> void { return o.close(); }, "");
		cl.def_static("generatePath", [](const class std::__cxx11::basic_string<char> & a0) -> std::string { return bitpit::BinaryArchive::generatePath(a0); }, "", pybind11::arg("name"));
		cl.def_static("generatePath", (std::string (*)(const std::string &, int)) &bitpit::BinaryArchive::generatePath, "C++: bitpit::BinaryArchive::generatePath(const std::string &, int) --> std::string", pybind11::arg("name"), pybind11::arg("block"));
		cl.def_static("generatePath", [](const class std::__cxx11::basic_string<char> & a0, const class std::__cxx11::basic_string<char> & a1) -> std::string { return bitpit::BinaryArchive::generatePath(a0, a1); }, "", pybind11::arg("name"), pybind11::arg("extension"));
		cl.def_static("generatePath", (std::string (*)(const std::string &, const std::string &, int)) &bitpit::BinaryArchive::generatePath, "C++: bitpit::BinaryArchive::generatePath(const std::string &, const std::string &, int) --> std::string", pybind11::arg("name"), pybind11::arg("extension"), pybind11::arg("block"));
		cl.def("getVersion", (int (bitpit::BinaryArchive::*)() const) &bitpit::BinaryArchive::getVersion, "C++: bitpit::BinaryArchive::getVersion() const --> int");
		cl.def("getHeader", (std::string (bitpit::BinaryArchive::*)() const) &bitpit::BinaryArchive::getHeader, "C++: bitpit::BinaryArchive::getHeader() const --> std::string");
		cl.def("getPath", (std::string (bitpit::BinaryArchive::*)() const) &bitpit::BinaryArchive::getPath, "C++: bitpit::BinaryArchive::getPath() const --> std::string");
	}
	std::cout << "B528_[bitpit::IBinaryArchive] ";
	{ // bitpit::IBinaryArchive file:binary_archive.hpp line:65
		pybind11::class_<bitpit::IBinaryArchive, std::shared_ptr<bitpit::IBinaryArchive>, bitpit::BinaryArchive> cl(M("bitpit"), "IBinaryArchive", "");
		cl.def( pybind11::init( [](const class std::__cxx11::basic_string<char> & a0){ return new bitpit::IBinaryArchive(a0); } ), "doc" , pybind11::arg("name"));

		cl.def( pybind11::init( [](const class std::__cxx11::basic_string<char> & a0, const class std::__cxx11::basic_string<char> & a1){ return new bitpit::IBinaryArchive(a0, a1); } ), "doc" , pybind11::arg("name"), pybind11::arg("extension"));

		cl.def("read", [](bitpit::IBinaryArchive &o, char * a0, long const & a1) -> std::istream & { return o.read(a0, a1); }, "", pybind11::return_value_policy::automatic, pybind11::arg("__s"), pybind11::arg("__n"));
		cl.def("open", [](bitpit::IBinaryArchive &o, const class std::__cxx11::basic_string<char> & a0) -> void { return o.open(a0); }, "", pybind11::arg("name"));
		cl.def("open", (void (bitpit::IBinaryArchive::*)(const std::string &, int)) &bitpit::IBinaryArchive::open, "C++: bitpit::IBinaryArchive::open(const std::string &, int) --> void", pybind11::arg("name"), pybind11::arg("block"));
		cl.def("open", [](bitpit::IBinaryArchive &o, const class std::__cxx11::basic_string<char> & a0, const class std::__cxx11::basic_string<char> & a1) -> void { return o.open(a0, a1); }, "", pybind11::arg("name"), pybind11::arg("extension"));
		cl.def("open", (void (bitpit::IBinaryArchive::*)(const std::string &, const std::string &, int)) &bitpit::IBinaryArchive::open, "C++: bitpit::IBinaryArchive::open(const std::string &, const std::string &, int) --> void", pybind11::arg("name"), pybind11::arg("extension"), pybind11::arg("block"));
		cl.def("getStream", (std::istream & (bitpit::IBinaryArchive::*)()) &bitpit::IBinaryArchive::getStream, "C++: bitpit::IBinaryArchive::getStream() --> std::istream &", pybind11::return_value_policy::automatic);
		cl.def("checkVersion", (bool (bitpit::IBinaryArchive::*)(int)) &bitpit::IBinaryArchive::checkVersion, "C++: bitpit::IBinaryArchive::checkVersion(int) --> bool", pybind11::arg("version"));
	}
	std::cout << "B529_[bitpit::OBinaryArchive] ";
	{ // bitpit::OBinaryArchive file:binary_archive.hpp line:84
		pybind11::class_<bitpit::OBinaryArchive, std::shared_ptr<bitpit::OBinaryArchive>, bitpit::BinaryArchive> cl(M("bitpit"), "OBinaryArchive", "");
		cl.def( pybind11::init( [](const class std::__cxx11::basic_string<char> & a0, int const & a1){ return new bitpit::OBinaryArchive(a0, a1); } ), "doc" , pybind11::arg("name"), pybind11::arg("version"));

		cl.def( pybind11::init( [](const class std::__cxx11::basic_string<char> & a0, int const & a1, const class std::__cxx11::basic_string<char> & a2){ return new bitpit::OBinaryArchive(a0, a1, a2); } ), "doc" , pybind11::arg("name"), pybind11::arg("version"), pybind11::arg("header"));

		cl.def( pybind11::init( [](const class std::__cxx11::basic_string<char> & a0, const class std::__cxx11::basic_string<char> & a1, int const & a2){ return new bitpit::OBinaryArchive(a0, a1, a2); } ), "doc" , pybind11::arg("name"), pybind11::arg("extension"), pybind11::arg("version"));

		cl.def( pybind11::init( [](const class std::__cxx11::basic_string<char> & a0, const class std::__cxx11::basic_string<char> & a1, int const & a2, const class std::__cxx11::basic_string<char> & a3){ return new bitpit::OBinaryArchive(a0, a1, a2, a3); } ), "doc" , pybind11::arg("name"), pybind11::arg("extension"), pybind11::arg("version"), pybind11::arg("header"));

		cl.def("write", [](bitpit::OBinaryArchive &o, const char * a0, long const & a1) -> std::ostream & { return o.write(a0, a1); }, "", pybind11::return_value_policy::automatic, pybind11::arg("__s"), pybind11::arg("__n"));
		cl.def("open", [](bitpit::OBinaryArchive &o, const class std::__cxx11::basic_string<char> & a0, const int & a1) -> void { return o.open(a0, a1); }, "", pybind11::arg("name"), pybind11::arg("version"));
		cl.def("open", (void (bitpit::OBinaryArchive::*)(const std::string &, const int, int)) &bitpit::OBinaryArchive::open, "C++: bitpit::OBinaryArchive::open(const std::string &, const int, int) --> void", pybind11::arg("name"), pybind11::arg("version"), pybind11::arg("block"));
		cl.def("open", [](bitpit::OBinaryArchive &o, const class std::__cxx11::basic_string<char> & a0, int const & a1, const class std::__cxx11::basic_string<char> & a2) -> void { return o.open(a0, a1, a2); }, "", pybind11::arg("name"), pybind11::arg("version"), pybind11::arg("header"));
		cl.def("open", (void (bitpit::OBinaryArchive::*)(const std::string &, int, const std::string &, int)) &bitpit::OBinaryArchive::open, "C++: bitpit::OBinaryArchive::open(const std::string &, int, const std::string &, int) --> void", pybind11::arg("name"), pybind11::arg("version"), pybind11::arg("header"), pybind11::arg("block"));
		cl.def("open", [](bitpit::OBinaryArchive &o, const class std::__cxx11::basic_string<char> & a0, const class std::__cxx11::basic_string<char> & a1, int const & a2) -> void { return o.open(a0, a1, a2); }, "", pybind11::arg("name"), pybind11::arg("extension"), pybind11::arg("version"));
		cl.def("open", (void (bitpit::OBinaryArchive::*)(const std::string &, const std::string &, int, int)) &bitpit::OBinaryArchive::open, "C++: bitpit::OBinaryArchive::open(const std::string &, const std::string &, int, int) --> void", pybind11::arg("name"), pybind11::arg("extension"), pybind11::arg("version"), pybind11::arg("block"));
		cl.def("open", [](bitpit::OBinaryArchive &o, const class std::__cxx11::basic_string<char> & a0, const class std::__cxx11::basic_string<char> & a1, int const & a2, const class std::__cxx11::basic_string<char> & a3) -> void { return o.open(a0, a1, a2, a3); }, "", pybind11::arg("name"), pybind11::arg("extension"), pybind11::arg("version"), pybind11::arg("header"));
		cl.def("open", (void (bitpit::OBinaryArchive::*)(const std::string &, const std::string &, int, const std::string &, int)) &bitpit::OBinaryArchive::open, "C++: bitpit::OBinaryArchive::open(const std::string &, const std::string &, int, const std::string &, int) --> void", pybind11::arg("name"), pybind11::arg("extension"), pybind11::arg("version"), pybind11::arg("header"), pybind11::arg("block"));
		cl.def("getStream", (std::ostream & (bitpit::OBinaryArchive::*)()) &bitpit::OBinaryArchive::getStream, "C++: bitpit::OBinaryArchive::getStream() --> std::ostream &", pybind11::return_value_policy::automatic);
	}
}
#include <ACylinder.hpp>
#include <ASphere.hpp>
#include <BoundaryConditions.hpp>
#include <Geometry.hpp>
#include <Grid.hpp>
#include <NGeometry.hpp>
#include <NeosLevelSet.hpp>
#include <NeosPiercedVector.hpp>
#include <PabloUniform.hpp>
#include <adaption.hpp>
#include <array>
#include <cell.hpp>
#include <common.hpp>
#include <element_type.hpp>
#include <functional>
#include <iostream>
#include <istream>
#include <iterator>
#include <map>
#include <memory>
#include <ostream>
#include <piercedVector.hpp>
#include <piercedVectorKernel.hpp>
#include <piercedVectorStorage.hpp>
#include <sstream> // __str__
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// neos::NGeometry file:NGeometry.hpp line:28
struct PyCallBack_neos_NGeometry : public neos::NGeometry {
	using neos::NGeometry::NGeometry;

	double getLevelSet(const struct std::array<double, 3> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::NGeometry *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"NGeometry::getLevelSet\"");
	}
	double getLevelSet(const long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::NGeometry *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return NGeometry::getLevelSet(a0);
	}
	using _binder_ret_0 = class neos::PiercedVector<double, long> &;
	_binder_ret_0 getPhi() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::NGeometry *>(this), "getPhi");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return NGeometry::getPhi();
	}
	void computeLevelSet(class neos::Grid * a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::NGeometry *>(this), "computeLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Geometry::computeLevelSet\"");
	}
};

// neos::AGeometry file: line:19
struct PyCallBack_neos_AGeometry : public neos::AGeometry {
	using neos::AGeometry::AGeometry;

	double getLevelSet(const long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::AGeometry *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return AGeometry::getLevelSet(a0);
	}
	void computeLevelSet(class neos::Grid * a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::AGeometry *>(this), "computeLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"AGeometry::computeLevelSet\"");
	}
	double getLevelSet(const struct std::array<double, 3> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::AGeometry *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"AGeometry::getLevelSet\"");
	}
	using _binder_ret_0 = class neos::PiercedVector<double, long> &;
	_binder_ret_0 getPhi() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::AGeometry *>(this), "getPhi");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return NGeometry::getPhi();
	}
};

// neos::ACylinder file:ACylinder.hpp line:20
struct PyCallBack_neos_ACylinder : public neos::ACylinder {
	using neos::ACylinder::ACylinder;

	double getLevelSet(const struct std::array<double, 3> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::ACylinder *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ACylinder::getLevelSet(a0);
	}
	void computeLevelSet(class neos::Grid * a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::ACylinder *>(this), "computeLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ACylinder::computeLevelSet(a0);
	}
	double getLevelSet(const long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::ACylinder *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return AGeometry::getLevelSet(a0);
	}
	using _binder_ret_0 = class neos::PiercedVector<double, long> &;
	_binder_ret_0 getPhi() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::ACylinder *>(this), "getPhi");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return NGeometry::getPhi();
	}
};

// neos::ASphere file:ASphere.hpp line:17
struct PyCallBack_neos_ASphere : public neos::ASphere {
	using neos::ASphere::ASphere;

	double getLevelSet(const struct std::array<double, 3> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::ASphere *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ASphere::getLevelSet(a0);
	}
	void computeLevelSet(class neos::Grid * a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::ASphere *>(this), "computeLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ASphere::computeLevelSet(a0);
	}
	double getLevelSet(const long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::ASphere *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return AGeometry::getLevelSet(a0);
	}
	using _binder_ret_0 = class neos::PiercedVector<double, long> &;
	_binder_ret_0 getPhi() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::ASphere *>(this), "getPhi");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return NGeometry::getPhi();
	}
};

void bind_NeosLevelSet(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B616_[neos::Utils] ";
	{ // neos::Utils file:NeosLevelSet.hpp line:27
		pybind11::class_<neos::Utils, std::shared_ptr<neos::Utils>> cl(M("neos"), "Utils", "Return an unique id");
		cl.def( pybind11::init( [](){ return new neos::Utils(); } ) );
		cl.def_static("getUid", (int (*)()) &neos::Utils::getUid, "Return an unique id\n\nC++: neos::Utils::getUid() --> int");
	}
	std::cout << "B617_[neos::LevelSet] ";
	{ // neos::LevelSet file:NeosLevelSet.hpp line:43
		pybind11::class_<neos::LevelSet, std::shared_ptr<neos::LevelSet>> cl(M("neos"), "LevelSet", "Class to apply the Levelset method to the Geometry objects");
		cl.def( pybind11::init<class neos::Grid *>(), pybind11::arg("grid") );

		cl.def( pybind11::init( [](neos::LevelSet const &o){ return new neos::LevelSet(o); } ) );
		cl.def("countGeometry", (int (neos::LevelSet::*)()) &neos::LevelSet::countGeometry, "Return the current nb of Geometry\n\nC++: neos::LevelSet::countGeometry() --> int");
		cl.def("addGeometry", (int (neos::LevelSet::*)(class neos::Geometry *)) &neos::LevelSet::addGeometry, "Add a Geometry\n\n \n Pointer to the Geometry\n\nC++: neos::LevelSet::addGeometry(class neos::Geometry *) --> int", pybind11::arg("geo"));
		cl.def("addGeometry", (int (neos::LevelSet::*)(class neos::Geometry *, std::string)) &neos::LevelSet::addGeometry, "Add a Geometry\n\n \n Pointer to the Geometry\n \n\n Tag of the geometry\n\nC++: neos::LevelSet::addGeometry(class neos::Geometry *, std::string) --> int", pybind11::arg("geo"), pybind11::arg("tag"));
		cl.def("delGeometry", (void (neos::LevelSet::*)(int)) &neos::LevelSet::delGeometry, "Delete a levelset\n\n \n Geometry id\n\nC++: neos::LevelSet::delGeometry(int) --> void", pybind11::arg("id"));
		cl.def("delGeometry", (void (neos::LevelSet::*)(std::string)) &neos::LevelSet::delGeometry, "Delete a group of levelset\n\n \n Group tag\n\nC++: neos::LevelSet::delGeometry(std::string) --> void", pybind11::arg("tag"));
		cl.def("getGeometry", (class neos::Geometry * (neos::LevelSet::*)(int)) &neos::LevelSet::getGeometry, "Return a Geometry\n\n \n id of the geometry\n\nC++: neos::LevelSet::getGeometry(int) --> class neos::Geometry *", pybind11::return_value_policy::automatic, pybind11::arg("idGeo"));
		cl.def("getId", (int (neos::LevelSet::*)(std::string)) &neos::LevelSet::getId, "Return the id of a given tag\n\n \n Geometry tag\n\nC++: neos::LevelSet::getId(std::string) --> int", pybind11::arg("tag"));
		cl.def("printMaps", (void (neos::LevelSet::*)()) &neos::LevelSet::printMaps, "Print the Geometry map (for debug)\n\nC++: neos::LevelSet::printMaps() --> void");
		cl.def("getLevelSet", (double (neos::LevelSet::*)(const struct std::array<double, 3> &)) &neos::LevelSet::getLevelSet, "Return the levelset value of an given point\n\n \n Coordinate of the point\n\nC++: neos::LevelSet::getLevelSet(const struct std::array<double, 3> &) --> double", pybind11::arg("cell"));
		cl.def("getLevelSet", (double (neos::LevelSet::*)(const struct std::array<double, 3> &, int)) &neos::LevelSet::getLevelSet, "Return the levelset value of an given point for a geometry\n\n \n Coordinate of the point\n \n\n Id of the Geometry\n\nC++: neos::LevelSet::getLevelSet(const struct std::array<double, 3> &, int) --> double", pybind11::arg("cell"), pybind11::arg("id"));
		cl.def("getLevelSet", (double (neos::LevelSet::*)(const struct std::array<double, 3> &, std::string)) &neos::LevelSet::getLevelSet, "Return the levelset value of an given point for a tag\n\n \n Coordinate of the point\n \n\n Tag of the set of geometry\n\nC++: neos::LevelSet::getLevelSet(const struct std::array<double, 3> &, std::string) --> double", pybind11::arg("cell"), pybind11::arg("tag"));
		cl.def("getLevelSet", (class std::vector<double, class std::allocator<double> > (neos::LevelSet::*)()) &neos::LevelSet::getLevelSet, "Return the levelset of the grid\n\nC++: neos::LevelSet::getLevelSet() --> class std::vector<double, class std::allocator<double> >");
		cl.def("getLevelSet", (class std::vector<double, class std::allocator<double> > (neos::LevelSet::*)(int)) &neos::LevelSet::getLevelSet, "Return the levelset of the grid for a given Geometry\n\n \n Id of the Geometry\n\nC++: neos::LevelSet::getLevelSet(int) --> class std::vector<double, class std::allocator<double> >", pybind11::arg("id"));
		cl.def("getLevelSet", (class std::vector<double, class std::allocator<double> > (neos::LevelSet::*)(std::string)) &neos::LevelSet::getLevelSet, "Return the levelset of the grid for a given set of Geometry\n\n \n Tag of the Geometrie's set\n\nC++: neos::LevelSet::getLevelSet(std::string) --> class std::vector<double, class std::allocator<double> >", pybind11::arg("tag"));
		cl.def("compute", (void (neos::LevelSet::*)(int)) &neos::LevelSet::compute, "Update (compute) the levelset of the grid for a given set of Geometry\n\n \n Id of the Geometry\n\nC++: neos::LevelSet::compute(int) --> void", pybind11::arg("idGeo"));
	}
	std::cout << "B618_[neos::NGeometry] ";
	{ // neos::NGeometry file:NGeometry.hpp line:28
		pybind11::class_<neos::NGeometry, std::shared_ptr<neos::NGeometry>, PyCallBack_neos_NGeometry, neos::Geometry> cl(M("neos"), "NGeometry", "");
		cl.def( pybind11::init<double, double, double>(), pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("radius") );

		cl.def( pybind11::init<double, double, double, double>(), pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"), pybind11::arg("radius") );

		cl.def(pybind11::init<PyCallBack_neos_NGeometry const &>());
		cl.def("getCenter", (struct std::array<double, 3> (neos::NGeometry::*)()) &neos::NGeometry::getCenter, "Return the center coordinate\n\n \n\nC++: neos::NGeometry::getCenter() --> struct std::array<double, 3>");
		cl.def("getDim", (int (neos::NGeometry::*)()) &neos::NGeometry::getDim, "Return the geometry dimension\n\n \n\nC++: neos::NGeometry::getDim() --> int");
		cl.def("getInitialCenter", (struct std::array<double, 3> (neos::NGeometry::*)()) &neos::NGeometry::getInitialCenter, "Return the center initial coordinate\n\n \n\nC++: neos::NGeometry::getInitialCenter() --> struct std::array<double, 3>");
		cl.def("getLevelSet", (double (neos::NGeometry::*)(const struct std::array<double, 3> &)) &neos::NGeometry::getLevelSet, "Return the levelset value of the point\n\n \n coordinate array of the point { { x, y, z } }\n\nC++: neos::NGeometry::getLevelSet(const struct std::array<double, 3> &) --> double", pybind11::arg("point"));
		cl.def("getLevelSet", (double (neos::NGeometry::*)(const long)) &neos::NGeometry::getLevelSet, "Return the levelset of cell\n\n \n id of the cell\n\n \n levelset value\n\nC++: neos::NGeometry::getLevelSet(const long) --> double", pybind11::arg("id"));
		cl.def("getPhi", (class neos::PiercedVector<double, long> & (neos::NGeometry::*)()) &neos::NGeometry::getPhi, "renommer en GetLevelSet ?\n\nC++: neos::NGeometry::getPhi() --> class neos::PiercedVector<double, long> &", pybind11::return_value_policy::automatic);
		cl.def("assign", (class neos::NGeometry & (neos::NGeometry::*)(const class neos::NGeometry &)) &neos::NGeometry::operator=, "C++: neos::NGeometry::operator=(const class neos::NGeometry &) --> class neos::NGeometry &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B619_[neos::AGeometry] ";
	{ // neos::AGeometry file: line:19
		pybind11::class_<neos::AGeometry, std::shared_ptr<neos::AGeometry>, PyCallBack_neos_AGeometry, neos::NGeometry> cl(M("neos"), "AGeometry", "AGeometry.hpp\n \n\n This file contains Analytic Geometry class declaration\n \n\n Matias Hastaran\n \n\n 0.1\n \n\n 2017-03-14\n \n\n Inria");
		cl.def( pybind11::init<double, double, double, double, int>(), pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"), pybind11::arg("radius"), pybind11::arg("dim") );

		cl.def(pybind11::init<PyCallBack_neos_AGeometry const &>());
		cl.def("updateCenter", (void (neos::AGeometry::*)(double, const class std::vector<double, class std::allocator<double> > &)) &neos::AGeometry::updateCenter, "Return the levelset value of the point\n\n \n coordinate array of the point { { x, y, z } }\n\nC++: neos::AGeometry::updateCenter(double, const class std::vector<double, class std::allocator<double> > &) --> void", pybind11::arg("step"), pybind11::arg("u"));
		cl.def("getLevelSet", (double (neos::AGeometry::*)(const long)) &neos::AGeometry::getLevelSet, "Return the levelset value of the point\n\n \n id of the cell (with bitpit)\n \n\n phivalue is returned\n\nC++: neos::AGeometry::getLevelSet(const long) --> double", pybind11::arg("id"));
		cl.def("computeLevelSet", (void (neos::AGeometry::*)(class neos::Grid *)) &neos::AGeometry::computeLevelSet, "Compute the levelset\n\n \n Grid to compute the levelset\n\nC++: neos::AGeometry::computeLevelSet(class neos::Grid *) --> void", pybind11::arg("grid"));
		cl.def("getLevelSet", (double (neos::AGeometry::*)(const struct std::array<double, 3> &)) &neos::AGeometry::getLevelSet, "Return the levelset value of the point\n\n \n coordinate array of the point { { x, y, z } }\n\n \n Levelset of the point\n\nC++: neos::AGeometry::getLevelSet(const struct std::array<double, 3> &) --> double", pybind11::arg("point"));
		cl.def("setLevelSet", (void (neos::AGeometry::*)(class neos::PiercedVector<double, long> &, class neos::Grid *)) &neos::AGeometry::setLevelSet, "Copy the levelset\n\n \n Vector of levelset\n \n\n Current grid\n\nC++: neos::AGeometry::setLevelSet(class neos::PiercedVector<double, long> &, class neos::Grid *) --> void", pybind11::arg("phistar"), pybind11::arg("grid"));
		cl.def("assign", (class neos::AGeometry & (neos::AGeometry::*)(const class neos::AGeometry &)) &neos::AGeometry::operator=, "C++: neos::AGeometry::operator=(const class neos::AGeometry &) --> class neos::AGeometry &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B620_[neos::ACylinder] ";
	{ // neos::ACylinder file:ACylinder.hpp line:20
		pybind11::class_<neos::ACylinder, std::shared_ptr<neos::ACylinder>, PyCallBack_neos_ACylinder, neos::AGeometry> cl(M("neos"), "ACylinder", "ACylinder.hpp\n \n\n This file contains Analytic Cylinder class declaration\n \n\n Matias Hastaran\n \n\n 0.1\n \n\n 2017-03-14\n \n\n Inria");
		cl.def( pybind11::init<double, double, double, double, int, int>(), pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"), pybind11::arg("radius"), pybind11::arg("dim"), pybind11::arg("axe") );

		cl.def( pybind11::init( [](PyCallBack_neos_ACylinder const &o){ return new PyCallBack_neos_ACylinder(o); } ) );
		cl.def( pybind11::init( [](neos::ACylinder const &o){ return new neos::ACylinder(o); } ) );
		cl.def("getLevelSet", [](neos::ACylinder &o, const long & a0) -> double { return o.getLevelSet(a0); }, "", pybind11::arg("id"));
		cl.def("getLevelSet", (double (neos::ACylinder::*)(const struct std::array<double, 3> &)) &neos::ACylinder::getLevelSet, "Return the levelset value of the point\n\n \n coordinate array of the point { { x, y, z } }\n\n \n Levelset of the point\n\nC++: neos::ACylinder::getLevelSet(const struct std::array<double, 3> &) --> double", pybind11::arg("cell"));
		cl.def("computeLevelSet", (void (neos::ACylinder::*)(class neos::Grid *)) &neos::ACylinder::computeLevelSet, "Compute the levelset\n\n \n Grid to compute the levelset\n\nC++: neos::ACylinder::computeLevelSet(class neos::Grid *) --> void", pybind11::arg("grid"));
		cl.def("setAxe", (void (neos::ACylinder::*)(int)) &neos::ACylinder::setAxe, "Set the axe\n\n \n Cylinder new axe\n\nC++: neos::ACylinder::setAxe(int) --> void", pybind11::arg("axe"));
		cl.def("assign", (class neos::ACylinder & (neos::ACylinder::*)(const class neos::ACylinder &)) &neos::ACylinder::operator=, "C++: neos::ACylinder::operator=(const class neos::ACylinder &) --> class neos::ACylinder &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B621_[neos::ASphere] ";
	{ // neos::ASphere file:ASphere.hpp line:17
		pybind11::class_<neos::ASphere, std::shared_ptr<neos::ASphere>, PyCallBack_neos_ASphere, neos::AGeometry> cl(M("neos"), "ASphere", "");
		cl.def( pybind11::init<double, double, double, double, int>(), pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"), pybind11::arg("radius"), pybind11::arg("dim") );

		cl.def( pybind11::init( [](PyCallBack_neos_ASphere const &o){ return new PyCallBack_neos_ASphere(o); } ) );
		cl.def( pybind11::init( [](neos::ASphere const &o){ return new neos::ASphere(o); } ) );
		cl.def("getLevelSet", [](neos::ASphere &o, const long & a0) -> double { return o.getLevelSet(a0); }, "", pybind11::arg("id"));
		cl.def("getLevelSet", (double (neos::ASphere::*)(const struct std::array<double, 3> &)) &neos::ASphere::getLevelSet, "Return the levelset value of the point\n\n \n coordinate array of the point { { x, y, z } }\n\n \n Levelset of the point\n\nC++: neos::ASphere::getLevelSet(const struct std::array<double, 3> &) --> double", pybind11::arg("cell"));
		cl.def("computeLevelSet", (void (neos::ASphere::*)(class neos::Grid *)) &neos::ASphere::computeLevelSet, "Compute the levelset\n\n \n Grid to compute the levelset\n\nC++: neos::ASphere::computeLevelSet(class neos::Grid *) --> void", pybind11::arg("grid"));
		cl.def("assign", (class neos::ASphere & (neos::ASphere::*)(const class neos::ASphere &)) &neos::ASphere::operator=, "C++: neos::ASphere::operator=(const class neos::ASphere &) --> class neos::ASphere &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
}
#include <NeosPiercedVector.hpp>
#include <iostream>
#include <istream>
#include <ostream>
#include <piercedVector.hpp>
#include <piercedVectorKernel.hpp>
#include <piercedVectorStorage.hpp>
#include <sstream> // __str__
#include <string>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// neos::PiercedVector file:NeosPiercedVector.hpp line:27
struct PyCallBack_neos_PiercedVector_double_long_t : public neos::PiercedVector<double,long> {
	using neos::PiercedVector<double,long>::PiercedVector;

};

void bind_NeosPiercedVector(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B371_[neos::PiercedVector<double,long>] ";
	{ // neos::PiercedVector file:NeosPiercedVector.hpp line:27
		pybind11::class_<neos::PiercedVector<double,long>, std::shared_ptr<neos::PiercedVector<double,long>>, PyCallBack_neos_PiercedVector_double_long_t, bitpit::PiercedVector<double,long>> cl(M("neos"), "PiercedVector_double_long_t", "");
		cl.def( pybind11::init<class bitpit::PiercedVector<double, long> &>(), pybind11::arg("v") );

		cl.def( pybind11::init( [](PyCallBack_neos_PiercedVector_double_long_t const &o){ return new PyCallBack_neos_PiercedVector_double_long_t(o); } ) );
		cl.def( pybind11::init( [](neos::PiercedVector<double,long> const &o){ return new neos::PiercedVector<double,long>(o); } ) );
		cl.def( pybind11::init( [](){ return new neos::PiercedVector<double,long>(); }, [](){ return new PyCallBack_neos_PiercedVector_double_long_t(); } ) );
		cl.def( pybind11::init<unsigned long>(), pybind11::arg("n") );

		cl.def( pybind11::init( [](){ return new neos::PiercedVector<double,long>(); }, [](){ return new PyCallBack_neos_PiercedVector_double_long_t(); } ) );
		cl.def("__add__", (class neos::PiercedVector<double, long> (neos::PiercedVector<double,long>::*)(const class neos::PiercedVector<double, long> &)) &neos::PiercedVector<double, long>::operator+, "C++: neos::PiercedVector<double, long>::operator+(const class neos::PiercedVector<double, long> &) --> class neos::PiercedVector<double, long>", pybind11::arg("x"));
		cl.def("__sub__", (class neos::PiercedVector<double, long> (neos::PiercedVector<double,long>::*)(const class neos::PiercedVector<double, long> &)) &neos::PiercedVector<double, long>::operator-, "C++: neos::PiercedVector<double, long>::operator-(const class neos::PiercedVector<double, long> &) --> class neos::PiercedVector<double, long>", pybind11::arg("x"));
		cl.def("__iadd__", (class neos::PiercedVector<double, long> & (neos::PiercedVector<double,long>::*)(const class neos::PiercedVector<double, long> &)) &neos::PiercedVector<double, long>::operator+=, "C++: neos::PiercedVector<double, long>::operator+=(const class neos::PiercedVector<double, long> &) --> class neos::PiercedVector<double, long> &", pybind11::return_value_policy::automatic, pybind11::arg("x"));
		cl.def("__isub__", (class neos::PiercedVector<double, long> & (neos::PiercedVector<double,long>::*)(const class neos::PiercedVector<double, long> &)) &neos::PiercedVector<double, long>::operator-=, "C++: neos::PiercedVector<double, long>::operator-=(const class neos::PiercedVector<double, long> &) --> class neos::PiercedVector<double, long> &", pybind11::return_value_policy::automatic, pybind11::arg("x"));
		cl.def("assign", (class neos::PiercedVector<double, long> & (neos::PiercedVector<double,long>::*)(const class neos::PiercedVector<double, long> &)) &neos::PiercedVector<double, long>::operator=, "C++: neos::PiercedVector<double, long>::operator=(const class neos::PiercedVector<double, long> &) --> class neos::PiercedVector<double, long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("assign", (class bitpit::PiercedVector<double, long> & (bitpit::PiercedVector<double,long>::*)(class bitpit::PiercedVector<double, long>)) &bitpit::PiercedVector<double, long>::operator=, "C++: bitpit::PiercedVector<double, long>::operator=(class bitpit::PiercedVector<double, long>) --> class bitpit::PiercedVector<double, long> &", pybind11::return_value_policy::automatic, pybind11::arg("x"));
		cl.def("popBack", (void (bitpit::PiercedVector<double,long>::*)()) &bitpit::PiercedVector<double, long>::popBack, "C++: bitpit::PiercedVector<double, long>::popBack() --> void");
		cl.def("swap", (void (bitpit::PiercedVector<double,long>::*)(long, long)) &bitpit::PiercedVector<double, long>::swap, "C++: bitpit::PiercedVector<double, long>::swap(long, long) --> void", pybind11::arg("id_first"), pybind11::arg("id_second"));
		cl.def("clear", [](bitpit::PiercedVector<double,long> &o) -> void { return o.clear(); }, "");
		cl.def("clear", (void (bitpit::PiercedVector<double,long>::*)(bool)) &bitpit::PiercedVector<double, long>::clear, "C++: bitpit::PiercedVector<double, long>::clear(bool) --> void", pybind11::arg("release"));
		cl.def("reserve", (void (bitpit::PiercedVector<double,long>::*)(unsigned long)) &bitpit::PiercedVector<double, long>::reserve, "C++: bitpit::PiercedVector<double, long>::reserve(unsigned long) --> void", pybind11::arg("n"));
		cl.def("resize", (void (bitpit::PiercedVector<double,long>::*)(unsigned long)) &bitpit::PiercedVector<double, long>::resize, "C++: bitpit::PiercedVector<double, long>::resize(unsigned long) --> void", pybind11::arg("n"));
		cl.def("sort", (void (bitpit::PiercedVector<double,long>::*)()) &bitpit::PiercedVector<double, long>::sort, "C++: bitpit::PiercedVector<double, long>::sort() --> void");
		cl.def("sortAfter", (void (bitpit::PiercedVector<double,long>::*)(long, bool)) &bitpit::PiercedVector<double, long>::sortAfter, "C++: bitpit::PiercedVector<double, long>::sortAfter(long, bool) --> void", pybind11::arg("referenceId"), pybind11::arg("inclusive"));
		cl.def("sortBefore", (void (bitpit::PiercedVector<double,long>::*)(long, bool)) &bitpit::PiercedVector<double, long>::sortBefore, "C++: bitpit::PiercedVector<double, long>::sortBefore(long, bool) --> void", pybind11::arg("referenceId"), pybind11::arg("inclusive"));
		cl.def("squeeze", (void (bitpit::PiercedVector<double,long>::*)()) &bitpit::PiercedVector<double, long>::squeeze, "C++: bitpit::PiercedVector<double, long>::squeeze() --> void");
		cl.def("shrinkToFit", (void (bitpit::PiercedVector<double,long>::*)()) &bitpit::PiercedVector<double, long>::shrinkToFit, "C++: bitpit::PiercedVector<double, long>::shrinkToFit() --> void");
		cl.def("swap", (void (bitpit::PiercedVector<double,long>::*)(class bitpit::PiercedVector<double, long> &)) &bitpit::PiercedVector<double, long>::swap, "C++: bitpit::PiercedVector<double, long>::swap(class bitpit::PiercedVector<double, long> &) --> void", pybind11::arg("x"));
		cl.def("getKernel", (const class bitpit::PiercedVectorKernel<long> & (bitpit::PiercedVector<double,long>::*)() const) &bitpit::PiercedVector<double, long>::getKernel, "C++: bitpit::PiercedVector<double, long>::getKernel() const --> const class bitpit::PiercedVectorKernel<long> &", pybind11::return_value_policy::automatic);
		cl.def("dump", (void (bitpit::PiercedVector<double,long>::*)() const) &bitpit::PiercedVector<double, long>::dump, "C++: bitpit::PiercedVector<double, long>::dump() const --> void");
		cl.def("restoreKernel", (void (bitpit::PiercedVector<double,long>::*)(std::istream &)) &bitpit::PiercedVector<double, long>::restoreKernel, "C++: bitpit::PiercedVector<double, long>::restoreKernel(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dumpKernel", (void (bitpit::PiercedVector<double,long>::*)(std::ostream &) const) &bitpit::PiercedVector<double, long>::dumpKernel, "C++: bitpit::PiercedVector<double, long>::dumpKernel(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("assign", (class bitpit::BasePiercedVector & (bitpit::BasePiercedVector::*)(const class bitpit::BasePiercedVector &)) &bitpit::BasePiercedVector::operator=, "C++: bitpit::BasePiercedVector::operator=(const class bitpit::BasePiercedVector &) --> class bitpit::BasePiercedVector &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("dump", [](bitpit::PiercedVectorKernel<long> const &o, class std::basic_ostream<char> & a0) -> void { return o.dump(a0); }, "", pybind11::arg("stream"));
		cl.def("dump", [](bitpit::PiercedVectorKernel<long> const &o) -> void { return o.dump(); }, "");
		cl.def("restore", [](bitpit::PiercedVectorKernel<long> &o, class std::basic_istream<char> & a0) -> void { return o.restore(a0); }, "", pybind11::arg("stream"));
		cl.def("rawIndex", (unsigned long (bitpit::PiercedVectorKernel<long>::*)(long) const) &bitpit::PiercedVectorKernel<long>::rawIndex, "C++: bitpit::PiercedVectorKernel<long>::rawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("exists", (bool (bitpit::PiercedVectorKernel<long>::*)(long) const) &bitpit::PiercedVectorKernel<long>::exists, "C++: bitpit::PiercedVectorKernel<long>::exists(long) const --> bool", pybind11::arg("id"));
		cl.def("assign", (class bitpit::PiercedVectorKernel<long> & (bitpit::PiercedVectorKernel<long>::*)(const class bitpit::PiercedVectorKernel<long> &)) &bitpit::PiercedVectorKernel<long>::operator=, "C++: bitpit::PiercedVectorKernel<long>::operator=(const class bitpit::PiercedVectorKernel<long> &) --> class bitpit::PiercedVectorKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("assign", (class bitpit::BasePiercedVectorKernel & (bitpit::BasePiercedVectorKernel::*)(const class bitpit::BasePiercedVectorKernel &)) &bitpit::BasePiercedVectorKernel::operator=, "C++: bitpit::BasePiercedVectorKernel::operator=(const class bitpit::BasePiercedVectorKernel &) --> class bitpit::BasePiercedVectorKernel &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("sync", [](bitpit::PiercedKernel<long> &o) -> void { return o.sync(); }, "");
		cl.def("isSynced", [](bitpit::PiercedKernel<long> const &o) -> bool { return o.isSynced(); }, "");
		cl.def("clear", [](bitpit::PiercedKernel<long> &o) -> bitpit::PiercedKernel<long>::ClearAction { return o.clear(); }, "");
		cl.def("clear", (class bitpit::PiercedKernel<long>::ClearAction (bitpit::PiercedKernel<long>::*)(bool)) &bitpit::PiercedKernel<long>::clear, "C++: bitpit::PiercedKernel<long>::clear(bool) --> class bitpit::PiercedKernel<long>::ClearAction", pybind11::arg("release"));
		cl.def("swap", (void (bitpit::PiercedKernel<long>::*)(class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::swap, "C++: bitpit::PiercedKernel<long>::swap(class bitpit::PiercedKernel<long> &) --> void", pybind11::arg("x"));
		cl.def("flush", (void (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::flush, "C++: bitpit::PiercedKernel<long>::flush() --> void");
		cl.def("contiguous", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::contiguous, "C++: bitpit::PiercedKernel<long>::contiguous() const --> bool");
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump() const --> void");
		cl.def("empty", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::empty, "C++: bitpit::PiercedKernel<long>::empty() const --> bool");
		cl.def("isIteratorSlow", (bool (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::isIteratorSlow, "C++: bitpit::PiercedKernel<long>::isIteratorSlow() --> bool");
		cl.def("maxSize", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::maxSize, "C++: bitpit::PiercedKernel<long>::maxSize() const --> unsigned long");
		cl.def("size", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::size, "C++: bitpit::PiercedKernel<long>::size() const --> unsigned long");
		cl.def("capacity", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::capacity, "C++: bitpit::PiercedKernel<long>::capacity() const --> unsigned long");
		cl.def("contains", (bool (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::contains, "C++: bitpit::PiercedKernel<long>::contains(long) const --> bool", pybind11::arg("id"));
		cl.def("count", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::count, "C++: bitpit::PiercedKernel<long>::count(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getRawIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::getRawIndex, "C++: bitpit::PiercedKernel<long>::getRawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("evalFlatIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::evalFlatIndex, "C++: bitpit::PiercedKernel<long>::evalFlatIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getIds", [](bitpit::PiercedKernel<long> const &o) -> std::vector<long, class std::allocator<long> > { return o.getIds(); }, "");
		cl.def("getIds", (class std::vector<long, class std::allocator<long> > (bitpit::PiercedKernel<long>::*)(bool) const) &bitpit::PiercedKernel<long>::getIds, "C++: bitpit::PiercedKernel<long>::getIds(bool) const --> class std::vector<long, class std::allocator<long> >", pybind11::arg("ordered"));
		cl.def("getSizeMarker", [](bitpit::PiercedKernel<long> &o, unsigned long const & a0) -> long { return o.getSizeMarker(a0); }, "", pybind11::arg("targetSize"));
		cl.def("getSizeMarker", (long (bitpit::PiercedKernel<long>::*)(unsigned long, const long &)) &bitpit::PiercedKernel<long>::getSizeMarker, "C++: bitpit::PiercedKernel<long>::getSizeMarker(unsigned long, const long &) --> long", pybind11::arg("targetSize"), pybind11::arg("fallback"));
		cl.def("find", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(const long &) const) &bitpit::PiercedKernel<long>::find, "C++: bitpit::PiercedKernel<long>::find(const long &) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("id"));
		cl.def("rawFind", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(unsigned long) const) &bitpit::PiercedKernel<long>::rawFind, "C++: bitpit::PiercedKernel<long>::rawFind(unsigned long) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("pos"));
		cl.def("begin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::begin, "C++: bitpit::PiercedKernel<long>::begin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("end", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::end, "C++: bitpit::PiercedKernel<long>::end() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cbegin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cbegin, "C++: bitpit::PiercedKernel<long>::cbegin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cend", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cend, "C++: bitpit::PiercedKernel<long>::cend() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("checkIntegrity", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::checkIntegrity, "C++: bitpit::PiercedKernel<long>::checkIntegrity() const --> void");
		cl.def("updateId", (void (bitpit::PiercedKernel<long>::*)(const long &, const long &)) &bitpit::PiercedKernel<long>::updateId, "C++: bitpit::PiercedKernel<long>::updateId(const long &, const long &) --> void", pybind11::arg("currentId"), pybind11::arg("updatedId"));
		cl.def("back", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::back, "C++: bitpit::PiercedKernel<long>::back() const --> unsigned long");
		cl.def("front", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::front, "C++: bitpit::PiercedKernel<long>::front() const --> unsigned long");
		cl.def("restore", (void (bitpit::PiercedKernel<long>::*)(std::istream &)) &bitpit::PiercedKernel<long>::restore, "C++: bitpit::PiercedKernel<long>::restore(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)(std::ostream &) const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("assign", (class bitpit::PiercedKernel<long> & (bitpit::PiercedKernel<long>::*)(const class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::operator=, "C++: bitpit::PiercedKernel<long>::operator=(const class bitpit::PiercedKernel<long> &) --> class bitpit::PiercedKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B372_[neos::PiercedVector<neos::Stencil,long>] ";
}
#include <Intersection.hpp>
#include <LocalTree.hpp>
#include <Map.hpp>
#include <Octant.hpp>
#include <ParaTree.hpp>
#include <array>
#include <fstream>
#include <functional>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <locale>
#include <logger.hpp>
#include <map>
#include <memory>
#include <ostream>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// bitpit::ParaTree file:ParaTree.hpp line:83
struct PyCallBack_bitpit_ParaTree : public bitpit::ParaTree {
	using bitpit::ParaTree::ParaTree;

	void reset() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ParaTree *>(this), "reset");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ParaTree::reset();
	}
	int getDumpVersion() const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ParaTree *>(this), "getDumpVersion");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<int>::value) {
				static pybind11::detail::override_caster_t<int> caster;
				return pybind11::detail::cast_ref<int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<int>(std::move(o));
		}
		return ParaTree::getDumpVersion();
	}
	void dump(class std::basic_ostream<char> & a0, bool a1) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ParaTree *>(this), "dump");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ParaTree::dump(a0, a1);
	}
	void restore(class std::basic_istream<char> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ParaTree *>(this), "restore");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ParaTree::restore(a0);
	}
};

void bind_Octant(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B577_[bitpit::Octant] ";
	{ // bitpit::Octant file:Octant.hpp line:91
		pybind11::class_<bitpit::Octant, std::shared_ptr<bitpit::Octant>> cl(M("bitpit"), "Octant", "10/dec/2015\n    \n\n        Edoardo Lombardi\n    \n\n        Marco Cisternino\n    \n\n        Copyright (C) 2015-2021 OPTIMAD engineering srl. All rights reserved.\n\n    \n Octant class definition\n\n    Octants are the grid elements of PABLO. In the logical domain octants are\n    squares or cubes, depending on dimension, with size function of their level.\n    Each octant has nodes and faces ordered with Z-order.\n     html PabloOctant.png\n     html PabloOctant3D.png\n    The main feature of each octant are:\n    - x,y,z        : coordinates of the node 0 of the octant;\n    - Morton index : classical Morton index defined anly by the coordinates\n    (info about level used additionally for equality operator);\n    - marker       : refinement marker can assume negative, positive or zero values, wich mean\n    a coarsening, refinement and none adaptation respectively;\n    - level        : octant level in the octree, zero for the first upper level.\n    - balance      : flag to fix if the octant has to 2:1 balanced with respect\n    to its face neighbours.\n\n ");
		cl.def( pybind11::init( [](){ return new bitpit::Octant(); } ) );
		cl.def_static("getBinarySize", (unsigned int (*)()) &bitpit::Octant::getBinarySize, "C++: bitpit::Octant::getBinarySize() --> unsigned int");
		cl.def("computeFatherMorton", (unsigned long (bitpit::Octant::*)() const) &bitpit::Octant::computeFatherMorton, "C++: bitpit::Octant::computeFatherMorton() const --> unsigned long");
		cl.def("getDim", (unsigned int (bitpit::Octant::*)() const) &bitpit::Octant::getDim, "C++: bitpit::Octant::getDim() const --> unsigned int");
		cl.def("getLogicalX", (unsigned int (bitpit::Octant::*)() const) &bitpit::Octant::getLogicalX, "C++: bitpit::Octant::getLogicalX() const --> unsigned int");
		cl.def("getLogicalY", (unsigned int (bitpit::Octant::*)() const) &bitpit::Octant::getLogicalY, "C++: bitpit::Octant::getLogicalY() const --> unsigned int");
		cl.def("getLogicalZ", (unsigned int (bitpit::Octant::*)() const) &bitpit::Octant::getLogicalZ, "C++: bitpit::Octant::getLogicalZ() const --> unsigned int");
		cl.def("getLevel", (unsigned char (bitpit::Octant::*)() const) &bitpit::Octant::getLevel, "C++: bitpit::Octant::getLevel() const --> unsigned char");
		cl.def("getMarker", (signed char (bitpit::Octant::*)() const) &bitpit::Octant::getMarker, "C++: bitpit::Octant::getMarker() const --> signed char");
		cl.def("getBound", (bool (bitpit::Octant::*)(unsigned char) const) &bitpit::Octant::getBound, "C++: bitpit::Octant::getBound(unsigned char) const --> bool", pybind11::arg("face"));
		cl.def("getEdgeBound", (bool (bitpit::Octant::*)(unsigned char) const) &bitpit::Octant::getEdgeBound, "C++: bitpit::Octant::getEdgeBound(unsigned char) const --> bool", pybind11::arg("edge"));
		cl.def("getNodeBound", (bool (bitpit::Octant::*)(unsigned char) const) &bitpit::Octant::getNodeBound, "C++: bitpit::Octant::getNodeBound(unsigned char) const --> bool", pybind11::arg("node"));
		cl.def("getBound", (bool (bitpit::Octant::*)() const) &bitpit::Octant::getBound, "C++: bitpit::Octant::getBound() const --> bool");
		cl.def("getPbound", (bool (bitpit::Octant::*)(unsigned char) const) &bitpit::Octant::getPbound, "C++: bitpit::Octant::getPbound(unsigned char) const --> bool", pybind11::arg("face"));
		cl.def("getPbound", (bool (bitpit::Octant::*)() const) &bitpit::Octant::getPbound, "C++: bitpit::Octant::getPbound() const --> bool");
		cl.def("getIsNewR", (bool (bitpit::Octant::*)() const) &bitpit::Octant::getIsNewR, "C++: bitpit::Octant::getIsNewR() const --> bool");
		cl.def("getIsNewC", (bool (bitpit::Octant::*)() const) &bitpit::Octant::getIsNewC, "C++: bitpit::Octant::getIsNewC() const --> bool");
		cl.def("getIsGhost", (bool (bitpit::Octant::*)() const) &bitpit::Octant::getIsGhost, "C++: bitpit::Octant::getIsGhost() const --> bool");
		cl.def("getGhostLayer", (int (bitpit::Octant::*)() const) &bitpit::Octant::getGhostLayer, "C++: bitpit::Octant::getGhostLayer() const --> int");
		cl.def("getBalance", (bool (bitpit::Octant::*)() const) &bitpit::Octant::getBalance, "C++: bitpit::Octant::getBalance() const --> bool");
		cl.def("getLogicalSize", (unsigned int (bitpit::Octant::*)() const) &bitpit::Octant::getLogicalSize, "C++: bitpit::Octant::getLogicalSize() const --> unsigned int");
		cl.def("getLogicalArea", (unsigned long (bitpit::Octant::*)() const) &bitpit::Octant::getLogicalArea, "C++: bitpit::Octant::getLogicalArea() const --> unsigned long");
		cl.def("getLogicalVolume", (unsigned long (bitpit::Octant::*)() const) &bitpit::Octant::getLogicalVolume, "C++: bitpit::Octant::getLogicalVolume() const --> unsigned long");
		cl.def("getLogicalCenter", (struct std::array<double, 3> (bitpit::Octant::*)() const) &bitpit::Octant::getLogicalCenter, "C++: bitpit::Octant::getLogicalCenter() const --> struct std::array<double, 3>");
		cl.def("getLogicalFaceCenter", (struct std::array<double, 3> (bitpit::Octant::*)(unsigned char) const) &bitpit::Octant::getLogicalFaceCenter, "C++: bitpit::Octant::getLogicalFaceCenter(unsigned char) const --> struct std::array<double, 3>", pybind11::arg("iface"));
		cl.def("getLogicalEdgeCenter", (struct std::array<double, 3> (bitpit::Octant::*)(unsigned char) const) &bitpit::Octant::getLogicalEdgeCenter, "C++: bitpit::Octant::getLogicalEdgeCenter(unsigned char) const --> struct std::array<double, 3>", pybind11::arg("iedge"));
		cl.def("getLogicalNodes", (void (bitpit::Octant::*)(class std::vector<struct std::array<unsigned int, 3>, class std::allocator<struct std::array<unsigned int, 3> > > &) const) &bitpit::Octant::getLogicalNodes, "C++: bitpit::Octant::getLogicalNodes(class std::vector<struct std::array<unsigned int, 3>, class std::allocator<struct std::array<unsigned int, 3> > > &) const --> void", pybind11::arg("nodes"));
		cl.def("getLogicalNodes", (class std::vector<struct std::array<unsigned int, 3>, class std::allocator<struct std::array<unsigned int, 3> > > (bitpit::Octant::*)() const) &bitpit::Octant::getLogicalNodes, "C++: bitpit::Octant::getLogicalNodes() const --> class std::vector<struct std::array<unsigned int, 3>, class std::allocator<struct std::array<unsigned int, 3> > >");
		cl.def("computeMorton", (unsigned long (bitpit::Octant::*)() const) &bitpit::Octant::computeMorton, "C++: bitpit::Octant::computeMorton() const --> unsigned long");
		cl.def("computeNodeMorton", (unsigned long (bitpit::Octant::*)(unsigned char) const) &bitpit::Octant::computeNodeMorton, "C++: bitpit::Octant::computeNodeMorton(unsigned char) const --> unsigned long", pybind11::arg("inode"));
		cl.def("buildLastDesc", (class bitpit::Octant (bitpit::Octant::*)() const) &bitpit::Octant::buildLastDesc, "C++: bitpit::Octant::buildLastDesc() const --> class bitpit::Octant");
		cl.def("buildFather", (class bitpit::Octant (bitpit::Octant::*)() const) &bitpit::Octant::buildFather, "C++: bitpit::Octant::buildFather() const --> class bitpit::Octant");
		cl.def("countChildren", (unsigned char (bitpit::Octant::*)() const) &bitpit::Octant::countChildren, "C++: bitpit::Octant::countChildren() const --> unsigned char");
		cl.def("buildChildren", (class std::vector<class bitpit::Octant, class std::allocator<class bitpit::Octant> > (bitpit::Octant::*)() const) &bitpit::Octant::buildChildren, "C++: bitpit::Octant::buildChildren() const --> class std::vector<class bitpit::Octant, class std::allocator<class bitpit::Octant> >");
		cl.def("buildChildren", (void (bitpit::Octant::*)(class bitpit::Octant *) const) &bitpit::Octant::buildChildren, "C++: bitpit::Octant::buildChildren(class bitpit::Octant *) const --> void", pybind11::arg("children"));
		cl.def("assign", (class bitpit::Octant & (bitpit::Octant::*)(const class bitpit::Octant &)) &bitpit::Octant::operator=, "C++: bitpit::Octant::operator=(const class bitpit::Octant &) --> class bitpit::Octant &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B578_[bitpit::Intersection] ";
	{ // bitpit::Intersection file:Intersection.hpp line:62
		pybind11::class_<bitpit::Intersection, std::shared_ptr<bitpit::Intersection>> cl(M("bitpit"), "Intersection", "	\n\n	\n			16/dec/2015\n	\n\n		Edoardo Lombardi\n	\n\n		Marco Cisternino\n	\n\n		Copyright (C) 2014-2021 OPTIMAD engineering srl. All rights reserved.\n\n	\n Intersection class definition\n\n	The intersection is the face (edge in 2D) or portion of face shared by two octants.\n	An intersection is defined by :\n	- the owner octants, i.e. the octants sharing the intersection,\n	identified by a couple (array[2]) of indices;\n	- the index of the face, that contains the intersection, of the first owner;\n	- an identifier of the octant in the couple with higher\n	level of refinement (0/1) [if same level identifier =0];\n	- a flag stating if an owner is ghost;\n	- a flag to communicate if the intersection is new after a mesh refinement.\n\n ");
		cl.def( pybind11::init( [](){ return new bitpit::Intersection(); } ) );
		cl.def( pybind11::init( [](bitpit::Intersection const &o){ return new bitpit::Intersection(o); } ) );
		cl.def("assign", (class bitpit::Intersection & (bitpit::Intersection::*)(const class bitpit::Intersection &)) &bitpit::Intersection::operator=, "C++: bitpit::Intersection::operator=(const class bitpit::Intersection &) --> class bitpit::Intersection &", pybind11::return_value_policy::automatic, pybind11::arg("intersection"));
	}
	std::cout << "B579_[bitpit::LocalTree] ";
	{ // bitpit::LocalTree file:LocalTree.hpp line:71
		pybind11::class_<bitpit::LocalTree, std::shared_ptr<bitpit::LocalTree>> cl(M("bitpit"), "LocalTree", "LocalTree.hpp\n\n	\n\n	\n		15/dec/2015\n	\n\n	Edoardo Lombardi\n	\n\n	Marco Cisternino\n	\n\n		Copyright (C) 2015-2021 OPTIMAD engineering srl. All rights reserved.\n\n	\n Local octree portion for each process\n\n	Local tree consists mainly of two vectors with:\n	- actual octants stored on current process;\n	- ghost octants neighbours of the first ones.\n\n	The octants (and ghosts) are ordered following the Z-curve defined by the Morton index.\n\n	Optionally in local tree three vectors of intersections are stored:\n	- intersections located on the physical domain of the octree;\n	- intersections of process bord (i.e. between octants and ghosts);\n	- intersections completely located in the domain of the process (i.e. between actual octants).\n\n	Class LocalTree is built with a dimensional parameter int dim and it accepts only two values: dim=2 and dim=3, for 2D and 3D respectively.");
		cl.def( pybind11::init( [](bitpit::LocalTree const &o){ return new bitpit::LocalTree(o); } ) );
	}
	std::cout << "B580_[bitpit::Map] ";
	{ // bitpit::Map file:Map.hpp line:70
		pybind11::class_<bitpit::Map, std::shared_ptr<bitpit::Map>> cl(M("bitpit"), "Map", "	\n\n	\n			17/dec/2015\n	\n\n		Edoardo Lombardi\n	\n\n		Marco Cisternino\n	\n\n		Copyright (C) 2014-2021 OPTIMAD engineering srl. All rights reserved.\n\n	\n Transformation Mapper\n\n	Definition of the transformation from the logical domain to the physical reference domain.\n	It contains a default implementation of a scaling and translation mapper\n	of logical octree in the reference domain with origin in (0,0,0) and size 1.\n	Map has to be implemented and customized by the user for different applications as a derived\n	class of ParaTree (see PabloUniform for a basic example).");
	}
	std::cout << "B581_[bitpit::ParaTree] ";
	{ // bitpit::ParaTree file:ParaTree.hpp line:83
		pybind11::class_<bitpit::ParaTree, std::shared_ptr<bitpit::ParaTree>, PyCallBack_bitpit_ParaTree> cl(M("bitpit"), "ParaTree", "	\n\n	\n			17/dec/2015\n	\n\n		Marco Cisternino\n	\n\n		Edoardo Lombardi\n\n	\n Para Tree is the user interface class\n\n	The user should (read can...) work only with this class and its methods.\n	The sizes are intended in reference physical domain with limits [0,1]. The transformation from the logical\n	domain to the physical domain is defined by an internal mapping.\n\n	The partition of the octree is performed by following the Z-curve defined by the Morton\n	index of the octants. By default it is a balanced partition over the number of octants for each\n	process.\n\n	Class ParaTree has a dimensional parameter int dim and it accepts only two\n	 values: dim=2 and dim=3, for 2D and 3D respectively.");
		cl.def( pybind11::init( [](PyCallBack_bitpit_ParaTree const &o){ return new PyCallBack_bitpit_ParaTree(o); } ) );
		cl.def( pybind11::init( [](bitpit::ParaTree const &o){ return new bitpit::ParaTree(o); } ) );

		pybind11::enum_<bitpit::ParaTree::Operation>(cl, "Operation", pybind11::arithmetic(), "")
			.value("OP_NONE", bitpit::ParaTree::OP_NONE)
			.value("OP_INIT", bitpit::ParaTree::OP_INIT)
			.value("OP_PRE_ADAPT", bitpit::ParaTree::OP_PRE_ADAPT)
			.value("OP_ADAPT_MAPPED", bitpit::ParaTree::OP_ADAPT_MAPPED)
			.value("OP_ADAPT_UNMAPPED", bitpit::ParaTree::OP_ADAPT_UNMAPPED)
			.value("OP_LOADBALANCE_FIRST", bitpit::ParaTree::OP_LOADBALANCE_FIRST)
			.value("OP_LOADBALANCE", bitpit::ParaTree::OP_LOADBALANCE)
			.export_values();

		cl.def("reset", (void (bitpit::ParaTree::*)()) &bitpit::ParaTree::reset, "C++: bitpit::ParaTree::reset() --> void");
		cl.def("getDumpVersion", (int (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getDumpVersion, "C++: bitpit::ParaTree::getDumpVersion() const --> int");
		cl.def("dump", [](bitpit::ParaTree &o, class std::basic_ostream<char> & a0) -> void { return o.dump(a0); }, "", pybind11::arg("stream"));
		cl.def("dump", (void (bitpit::ParaTree::*)(std::ostream &, bool)) &bitpit::ParaTree::dump, "C++: bitpit::ParaTree::dump(std::ostream &, bool) --> void", pybind11::arg("stream"), pybind11::arg("full"));
		cl.def("restore", (void (bitpit::ParaTree::*)(std::istream &)) &bitpit::ParaTree::restore, "C++: bitpit::ParaTree::restore(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("printHeader", (void (bitpit::ParaTree::*)()) &bitpit::ParaTree::printHeader, "C++: bitpit::ParaTree::printHeader() --> void");
		cl.def("printFooter", (void (bitpit::ParaTree::*)()) &bitpit::ParaTree::printFooter, "C++: bitpit::ParaTree::printFooter() --> void");
		cl.def("getDim", (unsigned char (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getDim, "C++: bitpit::ParaTree::getDim() const --> unsigned char");
		cl.def("getGlobalNumOctants", (unsigned long (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getGlobalNumOctants, "C++: bitpit::ParaTree::getGlobalNumOctants() const --> unsigned long");
		cl.def("getSerial", (bool (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getSerial, "C++: bitpit::ParaTree::getSerial() const --> bool");
		cl.def("getParallel", (bool (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getParallel, "C++: bitpit::ParaTree::getParallel() const --> bool");
		cl.def("getRank", (int (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getRank, "C++: bitpit::ParaTree::getRank() const --> int");
		cl.def("getNproc", (int (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNproc, "C++: bitpit::ParaTree::getNproc() const --> int");
		cl.def("getLastOperation", (enum bitpit::ParaTree::Operation (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getLastOperation, "C++: bitpit::ParaTree::getLastOperation() const --> enum bitpit::ParaTree::Operation");
		cl.def("freeComm", (void (bitpit::ParaTree::*)()) &bitpit::ParaTree::freeComm, "C++: bitpit::ParaTree::freeComm() --> void");
		cl.def("isCommSet", (bool (bitpit::ParaTree::*)() const) &bitpit::ParaTree::isCommSet, "C++: bitpit::ParaTree::isCommSet() const --> bool");
		cl.def("getPartitionRangeGlobalIdx", (const class std::vector<unsigned long, class std::allocator<unsigned long> > & (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getPartitionRangeGlobalIdx, "C++: bitpit::ParaTree::getPartitionRangeGlobalIdx() const --> const class std::vector<unsigned long, class std::allocator<unsigned long> > &", pybind11::return_value_policy::automatic);
		cl.def("getPartitionFirstDesc", (const class std::vector<unsigned long, class std::allocator<unsigned long> > & (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getPartitionFirstDesc, "C++: bitpit::ParaTree::getPartitionFirstDesc() const --> const class std::vector<unsigned long, class std::allocator<unsigned long> > &", pybind11::return_value_policy::automatic);
		cl.def("getPartitionLastDesc", (const class std::vector<unsigned long, class std::allocator<unsigned long> > & (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getPartitionLastDesc, "C++: bitpit::ParaTree::getPartitionLastDesc() const --> const class std::vector<unsigned long, class std::allocator<unsigned long> > &", pybind11::return_value_policy::automatic);
		cl.def("getOrigin", (struct std::array<double, 3> (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getOrigin, "C++: bitpit::ParaTree::getOrigin() const --> struct std::array<double, 3>");
		cl.def("getX0", (double (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getX0, "C++: bitpit::ParaTree::getX0() const --> double");
		cl.def("getY0", (double (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getY0, "C++: bitpit::ParaTree::getY0() const --> double");
		cl.def("getZ0", (double (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getZ0, "C++: bitpit::ParaTree::getZ0() const --> double");
		cl.def("getL", (double (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getL, "C++: bitpit::ParaTree::getL() const --> double");
		cl.def("getMaxLevel", (int (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getMaxLevel, "C++: bitpit::ParaTree::getMaxLevel() const --> int");
		cl.def("getMaxLength", (unsigned int (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getMaxLength, "C++: bitpit::ParaTree::getMaxLength() const --> unsigned int");
		cl.def("getNnodes", (unsigned char (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNnodes, "C++: bitpit::ParaTree::getNnodes() const --> unsigned char");
		cl.def("getNfaces", (unsigned char (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNfaces, "C++: bitpit::ParaTree::getNfaces() const --> unsigned char");
		cl.def("getNedges", (unsigned char (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNedges, "C++: bitpit::ParaTree::getNedges() const --> unsigned char");
		cl.def("getNchildren", (unsigned char (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNchildren, "C++: bitpit::ParaTree::getNchildren() const --> unsigned char");
		cl.def("getNnodesperface", (unsigned char (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNnodesperface, "C++: bitpit::ParaTree::getNnodesperface() const --> unsigned char");
		cl.def("getOppface", (const unsigned char * (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getOppface, "C++: bitpit::ParaTree::getOppface() const --> const unsigned char *", pybind11::return_value_policy::automatic);
		cl.def("getPeriodic", (class std::vector<bool, class std::allocator<bool> > (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getPeriodic, "C++: bitpit::ParaTree::getPeriodic() const --> class std::vector<bool, class std::allocator<bool> >");
		cl.def("getTol", (double (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getTol, "C++: bitpit::ParaTree::getTol() const --> double");
		cl.def("getPeriodic", (bool (bitpit::ParaTree::*)(unsigned char) const) &bitpit::ParaTree::getPeriodic, "C++: bitpit::ParaTree::getPeriodic(unsigned char) const --> bool", pybind11::arg("i"));
		cl.def("setPeriodic", (void (bitpit::ParaTree::*)(unsigned char)) &bitpit::ParaTree::setPeriodic, "C++: bitpit::ParaTree::setPeriodic(unsigned char) --> void", pybind11::arg("i"));
		cl.def("setTol", [](bitpit::ParaTree &o) -> void { return o.setTol(); }, "");
		cl.def("setTol", (void (bitpit::ParaTree::*)(double)) &bitpit::ParaTree::setTol, "C++: bitpit::ParaTree::setTol(double) --> void", pybind11::arg("tol"));
		cl.def("getCoordinates", (struct std::array<double, 3> (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getCoordinates, "C++: bitpit::ParaTree::getCoordinates(unsigned int) const --> struct std::array<double, 3>", pybind11::arg("idx"));
		cl.def("getX", (double (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getX, "C++: bitpit::ParaTree::getX(unsigned int) const --> double", pybind11::arg("idx"));
		cl.def("getY", (double (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getY, "C++: bitpit::ParaTree::getY(unsigned int) const --> double", pybind11::arg("idx"));
		cl.def("getZ", (double (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getZ, "C++: bitpit::ParaTree::getZ(unsigned int) const --> double", pybind11::arg("idx"));
		cl.def("getSize", (double (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getSize, "C++: bitpit::ParaTree::getSize(unsigned int) const --> double", pybind11::arg("idx"));
		cl.def("getArea", (double (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getArea, "C++: bitpit::ParaTree::getArea(unsigned int) const --> double", pybind11::arg("idx"));
		cl.def("getVolume", (double (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getVolume, "C++: bitpit::ParaTree::getVolume(unsigned int) const --> double", pybind11::arg("idx"));
		cl.def("getCenter", (void (bitpit::ParaTree::*)(unsigned int, struct std::array<double, 3> &) const) &bitpit::ParaTree::getCenter, "C++: bitpit::ParaTree::getCenter(unsigned int, struct std::array<double, 3> &) const --> void", pybind11::arg("idx"), pybind11::arg("center"));
		cl.def("getCenter", (struct std::array<double, 3> (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getCenter, "C++: bitpit::ParaTree::getCenter(unsigned int) const --> struct std::array<double, 3>", pybind11::arg("idx"));
		cl.def("getFaceCenter", (struct std::array<double, 3> (bitpit::ParaTree::*)(unsigned int, unsigned char) const) &bitpit::ParaTree::getFaceCenter, "C++: bitpit::ParaTree::getFaceCenter(unsigned int, unsigned char) const --> struct std::array<double, 3>", pybind11::arg("idx"), pybind11::arg("iface"));
		cl.def("getFaceCenter", (void (bitpit::ParaTree::*)(unsigned int, unsigned char, struct std::array<double, 3> &) const) &bitpit::ParaTree::getFaceCenter, "C++: bitpit::ParaTree::getFaceCenter(unsigned int, unsigned char, struct std::array<double, 3> &) const --> void", pybind11::arg("idx"), pybind11::arg("iface"), pybind11::arg("center"));
		cl.def("getNode", (struct std::array<double, 3> (bitpit::ParaTree::*)(unsigned int, unsigned char) const) &bitpit::ParaTree::getNode, "C++: bitpit::ParaTree::getNode(unsigned int, unsigned char) const --> struct std::array<double, 3>", pybind11::arg("idx"), pybind11::arg("inode"));
		cl.def("getNode", (void (bitpit::ParaTree::*)(unsigned int, unsigned char, struct std::array<double, 3> &) const) &bitpit::ParaTree::getNode, "C++: bitpit::ParaTree::getNode(unsigned int, unsigned char, struct std::array<double, 3> &) const --> void", pybind11::arg("idx"), pybind11::arg("inode"), pybind11::arg("node"));
		cl.def("getNodes", (void (bitpit::ParaTree::*)(unsigned int, class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > > &) const) &bitpit::ParaTree::getNodes, "C++: bitpit::ParaTree::getNodes(unsigned int, class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > > &) const --> void", pybind11::arg("idx"), pybind11::arg("nodes"));
		cl.def("getNodes", (class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > > (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getNodes, "C++: bitpit::ParaTree::getNodes(unsigned int) const --> class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > >", pybind11::arg("idx"));
		cl.def("getNormal", (void (bitpit::ParaTree::*)(unsigned int, unsigned char, struct std::array<double, 3> &) const) &bitpit::ParaTree::getNormal, "C++: bitpit::ParaTree::getNormal(unsigned int, unsigned char, struct std::array<double, 3> &) const --> void", pybind11::arg("idx"), pybind11::arg("iface"), pybind11::arg("normal"));
		cl.def("getNormal", (struct std::array<double, 3> (bitpit::ParaTree::*)(unsigned int, unsigned char) const) &bitpit::ParaTree::getNormal, "C++: bitpit::ParaTree::getNormal(unsigned int, unsigned char) const --> struct std::array<double, 3>", pybind11::arg("idx"), pybind11::arg("iface"));
		cl.def("getMarker", (signed char (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getMarker, "C++: bitpit::ParaTree::getMarker(unsigned int) const --> signed char", pybind11::arg("idx"));
		cl.def("getLevel", (unsigned char (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getLevel, "C++: bitpit::ParaTree::getLevel(unsigned int) const --> unsigned char", pybind11::arg("idx"));
		cl.def("getMorton", (unsigned long (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getMorton, "C++: bitpit::ParaTree::getMorton(unsigned int) const --> unsigned long", pybind11::arg("idx"));
		cl.def("getNodeMorton", (unsigned long (bitpit::ParaTree::*)(unsigned int, unsigned char) const) &bitpit::ParaTree::getNodeMorton, "C++: bitpit::ParaTree::getNodeMorton(unsigned int, unsigned char) const --> unsigned long", pybind11::arg("idx"), pybind11::arg("inode"));
		cl.def("getBalance", (bool (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getBalance, "C++: bitpit::ParaTree::getBalance(unsigned int) const --> bool", pybind11::arg("idx"));
		cl.def("getBound", (bool (bitpit::ParaTree::*)(unsigned int, unsigned char) const) &bitpit::ParaTree::getBound, "C++: bitpit::ParaTree::getBound(unsigned int, unsigned char) const --> bool", pybind11::arg("idx"), pybind11::arg("iface"));
		cl.def("getBound", (bool (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getBound, "C++: bitpit::ParaTree::getBound(unsigned int) const --> bool", pybind11::arg("idx"));
		cl.def("getPbound", (bool (bitpit::ParaTree::*)(unsigned int, unsigned char) const) &bitpit::ParaTree::getPbound, "C++: bitpit::ParaTree::getPbound(unsigned int, unsigned char) const --> bool", pybind11::arg("idx"), pybind11::arg("iface"));
		cl.def("getPbound", (bool (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getPbound, "C++: bitpit::ParaTree::getPbound(unsigned int) const --> bool", pybind11::arg("idx"));
		cl.def("getIsNewR", (bool (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getIsNewR, "C++: bitpit::ParaTree::getIsNewR(unsigned int) const --> bool", pybind11::arg("idx"));
		cl.def("getIsNewC", (bool (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getIsNewC, "C++: bitpit::ParaTree::getIsNewC(unsigned int) const --> bool", pybind11::arg("idx"));
		cl.def("getGlobalIdx", (unsigned long (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getGlobalIdx, "C++: bitpit::ParaTree::getGlobalIdx(unsigned int) const --> unsigned long", pybind11::arg("idx"));
		cl.def("getGhostGlobalIdx", (unsigned long (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getGhostGlobalIdx, "C++: bitpit::ParaTree::getGhostGlobalIdx(unsigned int) const --> unsigned long", pybind11::arg("idx"));
		cl.def("getLocalIdx", (unsigned int (bitpit::ParaTree::*)(unsigned long) const) &bitpit::ParaTree::getLocalIdx, "C++: bitpit::ParaTree::getLocalIdx(unsigned long) const --> unsigned int", pybind11::arg("gidx"));
		cl.def("getLocalIdx", (unsigned int (bitpit::ParaTree::*)(unsigned long, int) const) &bitpit::ParaTree::getLocalIdx, "C++: bitpit::ParaTree::getLocalIdx(unsigned long, int) const --> unsigned int", pybind11::arg("gidx"), pybind11::arg("rank"));
		cl.def("getLocalIdx", (void (bitpit::ParaTree::*)(unsigned long, unsigned int &, int &) const) &bitpit::ParaTree::getLocalIdx, "C++: bitpit::ParaTree::getLocalIdx(unsigned long, unsigned int &, int &) const --> void", pybind11::arg("gidx"), pybind11::arg("lidx"), pybind11::arg("rank"));
		cl.def("getGhostLocalIdx", (unsigned int (bitpit::ParaTree::*)(unsigned long) const) &bitpit::ParaTree::getGhostLocalIdx, "C++: bitpit::ParaTree::getGhostLocalIdx(unsigned long) const --> unsigned int", pybind11::arg("gidx"));
		cl.def("getPreMarker", (signed char (bitpit::ParaTree::*)(unsigned int)) &bitpit::ParaTree::getPreMarker, "C++: bitpit::ParaTree::getPreMarker(unsigned int) --> signed char", pybind11::arg("idx"));
		cl.def("setMarker", (void (bitpit::ParaTree::*)(unsigned int, signed char)) &bitpit::ParaTree::setMarker, "C++: bitpit::ParaTree::setMarker(unsigned int, signed char) --> void", pybind11::arg("idx"), pybind11::arg("marker"));
		cl.def("setBalance", (void (bitpit::ParaTree::*)(unsigned int, bool)) &bitpit::ParaTree::setBalance, "C++: bitpit::ParaTree::setBalance(unsigned int, bool) --> void", pybind11::arg("idx"), pybind11::arg("balance"));
		cl.def("getCoordinates", (struct std::array<double, 3> (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getCoordinates, "C++: bitpit::ParaTree::getCoordinates(const class bitpit::Octant *) const --> struct std::array<double, 3>", pybind11::arg("oct"));
		cl.def("getX", (double (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getX, "C++: bitpit::ParaTree::getX(const class bitpit::Octant *) const --> double", pybind11::arg("oct"));
		cl.def("getY", (double (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getY, "C++: bitpit::ParaTree::getY(const class bitpit::Octant *) const --> double", pybind11::arg("oct"));
		cl.def("getZ", (double (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getZ, "C++: bitpit::ParaTree::getZ(const class bitpit::Octant *) const --> double", pybind11::arg("oct"));
		cl.def("getSize", (double (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getSize, "C++: bitpit::ParaTree::getSize(const class bitpit::Octant *) const --> double", pybind11::arg("oct"));
		cl.def("getArea", (double (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getArea, "C++: bitpit::ParaTree::getArea(const class bitpit::Octant *) const --> double", pybind11::arg("oct"));
		cl.def("getVolume", (double (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getVolume, "C++: bitpit::ParaTree::getVolume(const class bitpit::Octant *) const --> double", pybind11::arg("oct"));
		cl.def("getCenter", (void (bitpit::ParaTree::*)(const class bitpit::Octant *, struct std::array<double, 3> &) const) &bitpit::ParaTree::getCenter, "C++: bitpit::ParaTree::getCenter(const class bitpit::Octant *, struct std::array<double, 3> &) const --> void", pybind11::arg("oct"), pybind11::arg("center"));
		cl.def("getCenter", (struct std::array<double, 3> (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getCenter, "C++: bitpit::ParaTree::getCenter(const class bitpit::Octant *) const --> struct std::array<double, 3>", pybind11::arg("oct"));
		cl.def("getFaceCenter", (struct std::array<double, 3> (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char) const) &bitpit::ParaTree::getFaceCenter, "C++: bitpit::ParaTree::getFaceCenter(const class bitpit::Octant *, unsigned char) const --> struct std::array<double, 3>", pybind11::arg("oct"), pybind11::arg("iface"));
		cl.def("getFaceCenter", (void (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char, struct std::array<double, 3> &) const) &bitpit::ParaTree::getFaceCenter, "C++: bitpit::ParaTree::getFaceCenter(const class bitpit::Octant *, unsigned char, struct std::array<double, 3> &) const --> void", pybind11::arg("oct"), pybind11::arg("iface"), pybind11::arg("center"));
		cl.def("getNode", (struct std::array<double, 3> (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char) const) &bitpit::ParaTree::getNode, "C++: bitpit::ParaTree::getNode(const class bitpit::Octant *, unsigned char) const --> struct std::array<double, 3>", pybind11::arg("oct"), pybind11::arg("inode"));
		cl.def("getNode", (void (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char, struct std::array<double, 3> &) const) &bitpit::ParaTree::getNode, "C++: bitpit::ParaTree::getNode(const class bitpit::Octant *, unsigned char, struct std::array<double, 3> &) const --> void", pybind11::arg("oct"), pybind11::arg("inode"), pybind11::arg("node"));
		cl.def("getNodes", (void (bitpit::ParaTree::*)(const class bitpit::Octant *, class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > > &) const) &bitpit::ParaTree::getNodes, "C++: bitpit::ParaTree::getNodes(const class bitpit::Octant *, class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > > &) const --> void", pybind11::arg("oct"), pybind11::arg("nodes"));
		cl.def("getNodes", (class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > > (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getNodes, "C++: bitpit::ParaTree::getNodes(const class bitpit::Octant *) const --> class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > >", pybind11::arg("oct"));
		cl.def("getNormal", (void (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char, struct std::array<double, 3> &) const) &bitpit::ParaTree::getNormal, "C++: bitpit::ParaTree::getNormal(const class bitpit::Octant *, unsigned char, struct std::array<double, 3> &) const --> void", pybind11::arg("oct"), pybind11::arg("iface"), pybind11::arg("normal"));
		cl.def("getNormal", (struct std::array<double, 3> (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char) const) &bitpit::ParaTree::getNormal, "C++: bitpit::ParaTree::getNormal(const class bitpit::Octant *, unsigned char) const --> struct std::array<double, 3>", pybind11::arg("oct"), pybind11::arg("iface"));
		cl.def("getMarker", (signed char (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getMarker, "C++: bitpit::ParaTree::getMarker(const class bitpit::Octant *) const --> signed char", pybind11::arg("oct"));
		cl.def("getLevel", (unsigned char (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getLevel, "C++: bitpit::ParaTree::getLevel(const class bitpit::Octant *) const --> unsigned char", pybind11::arg("oct"));
		cl.def("getMorton", (unsigned long (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getMorton, "C++: bitpit::ParaTree::getMorton(const class bitpit::Octant *) const --> unsigned long", pybind11::arg("oct"));
		cl.def("getLastDescMorton", (unsigned long (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getLastDescMorton, "C++: bitpit::ParaTree::getLastDescMorton(const class bitpit::Octant *) const --> unsigned long", pybind11::arg("oct"));
		cl.def("getNodeMorton", (unsigned long (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char) const) &bitpit::ParaTree::getNodeMorton, "C++: bitpit::ParaTree::getNodeMorton(const class bitpit::Octant *, unsigned char) const --> unsigned long", pybind11::arg("oct"), pybind11::arg("inode"));
		cl.def("getBalance", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getBalance, "C++: bitpit::ParaTree::getBalance(const class bitpit::Octant *) const --> bool", pybind11::arg("oct"));
		cl.def("getBound", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char) const) &bitpit::ParaTree::getBound, "C++: bitpit::ParaTree::getBound(const class bitpit::Octant *, unsigned char) const --> bool", pybind11::arg("oct"), pybind11::arg("iface"));
		cl.def("getBound", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getBound, "C++: bitpit::ParaTree::getBound(const class bitpit::Octant *) const --> bool", pybind11::arg("oct"));
		cl.def("getPbound", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char) const) &bitpit::ParaTree::getPbound, "C++: bitpit::ParaTree::getPbound(const class bitpit::Octant *, unsigned char) const --> bool", pybind11::arg("oct"), pybind11::arg("iface"));
		cl.def("getPbound", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getPbound, "C++: bitpit::ParaTree::getPbound(const class bitpit::Octant *) const --> bool", pybind11::arg("oct"));
		cl.def("getIsNewR", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getIsNewR, "C++: bitpit::ParaTree::getIsNewR(const class bitpit::Octant *) const --> bool", pybind11::arg("oct"));
		cl.def("getIsNewC", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getIsNewC, "C++: bitpit::ParaTree::getIsNewC(const class bitpit::Octant *) const --> bool", pybind11::arg("oct"));
		cl.def("getIdx", (unsigned int (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getIdx, "C++: bitpit::ParaTree::getIdx(const class bitpit::Octant *) const --> unsigned int", pybind11::arg("oct"));
		cl.def("getGlobalIdx", (unsigned long (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getGlobalIdx, "C++: bitpit::ParaTree::getGlobalIdx(const class bitpit::Octant *) const --> unsigned long", pybind11::arg("oct"));
		cl.def("getPreMarker", (signed char (bitpit::ParaTree::*)(class bitpit::Octant *)) &bitpit::ParaTree::getPreMarker, "C++: bitpit::ParaTree::getPreMarker(class bitpit::Octant *) --> signed char", pybind11::arg("oct"));
		cl.def("setMarker", (void (bitpit::ParaTree::*)(class bitpit::Octant *, signed char)) &bitpit::ParaTree::setMarker, "C++: bitpit::ParaTree::setMarker(class bitpit::Octant *, signed char) --> void", pybind11::arg("oct"), pybind11::arg("marker"));
		cl.def("setBalance", (void (bitpit::ParaTree::*)(class bitpit::Octant *, bool)) &bitpit::ParaTree::setBalance, "C++: bitpit::ParaTree::setBalance(class bitpit::Octant *, bool) --> void", pybind11::arg("oct"), pybind11::arg("balance"));
		cl.def("getStatus", (unsigned long (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getStatus, "C++: bitpit::ParaTree::getStatus() const --> unsigned long");
		cl.def("getNumOctants", (unsigned int (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNumOctants, "C++: bitpit::ParaTree::getNumOctants() const --> unsigned int");
		cl.def("getNumGhosts", (unsigned int (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNumGhosts, "C++: bitpit::ParaTree::getNumGhosts() const --> unsigned int");
		cl.def("getNumNodes", (unsigned int (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNumNodes, "C++: bitpit::ParaTree::getNumNodes() const --> unsigned int");
		cl.def("getLocalMaxDepth", (unsigned char (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getLocalMaxDepth, "C++: bitpit::ParaTree::getLocalMaxDepth() const --> unsigned char");
		cl.def("getLocalMaxSize", (double (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getLocalMaxSize, "C++: bitpit::ParaTree::getLocalMaxSize() const --> double");
		cl.def("getLocalMinSize", (double (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getLocalMinSize, "C++: bitpit::ParaTree::getLocalMinSize() const --> double");
		cl.def("getBalanceCodimension", (unsigned char (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getBalanceCodimension, "C++: bitpit::ParaTree::getBalanceCodimension() const --> unsigned char");
		cl.def("getFirstDescMorton", (unsigned long (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getFirstDescMorton, "C++: bitpit::ParaTree::getFirstDescMorton() const --> unsigned long");
		cl.def("getLastDescMorton", (unsigned long (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getLastDescMorton, "C++: bitpit::ParaTree::getLastDescMorton() const --> unsigned long");
		cl.def("getLastDescMorton", (unsigned long (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getLastDescMorton, "C++: bitpit::ParaTree::getLastDescMorton(unsigned int) const --> unsigned long", pybind11::arg("idx"));
		cl.def("getInternalOctantsBegin", (class __gnu_cxx::__normal_iterator<class bitpit::Octant **, class std::vector<class bitpit::Octant *, class std::allocator<class bitpit::Octant *> > > (bitpit::ParaTree::*)()) &bitpit::ParaTree::getInternalOctantsBegin, "C++: bitpit::ParaTree::getInternalOctantsBegin() --> class __gnu_cxx::__normal_iterator<class bitpit::Octant **, class std::vector<class bitpit::Octant *, class std::allocator<class bitpit::Octant *> > >");
		cl.def("getInternalOctantsEnd", (class __gnu_cxx::__normal_iterator<class bitpit::Octant **, class std::vector<class bitpit::Octant *, class std::allocator<class bitpit::Octant *> > > (bitpit::ParaTree::*)()) &bitpit::ParaTree::getInternalOctantsEnd, "C++: bitpit::ParaTree::getInternalOctantsEnd() --> class __gnu_cxx::__normal_iterator<class bitpit::Octant **, class std::vector<class bitpit::Octant *, class std::allocator<class bitpit::Octant *> > >");
		cl.def("getPboundOctantsBegin", (class __gnu_cxx::__normal_iterator<class bitpit::Octant **, class std::vector<class bitpit::Octant *, class std::allocator<class bitpit::Octant *> > > (bitpit::ParaTree::*)()) &bitpit::ParaTree::getPboundOctantsBegin, "C++: bitpit::ParaTree::getPboundOctantsBegin() --> class __gnu_cxx::__normal_iterator<class bitpit::Octant **, class std::vector<class bitpit::Octant *, class std::allocator<class bitpit::Octant *> > >");
		cl.def("getPboundOctantsEnd", (class __gnu_cxx::__normal_iterator<class bitpit::Octant **, class std::vector<class bitpit::Octant *, class std::allocator<class bitpit::Octant *> > > (bitpit::ParaTree::*)()) &bitpit::ParaTree::getPboundOctantsEnd, "C++: bitpit::ParaTree::getPboundOctantsEnd() --> class __gnu_cxx::__normal_iterator<class bitpit::Octant **, class std::vector<class bitpit::Octant *, class std::allocator<class bitpit::Octant *> > >");
		cl.def("setBalanceCodimension", (void (bitpit::ParaTree::*)(unsigned char)) &bitpit::ParaTree::setBalanceCodimension, "C++: bitpit::ParaTree::setBalanceCodimension(unsigned char) --> void", pybind11::arg("b21codim"));
		cl.def("getNumIntersections", (unsigned int (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNumIntersections, "C++: bitpit::ParaTree::getNumIntersections() const --> unsigned int");
		cl.def("getIntersection", (class bitpit::Intersection * (bitpit::ParaTree::*)(unsigned int)) &bitpit::ParaTree::getIntersection, "C++: bitpit::ParaTree::getIntersection(unsigned int) --> class bitpit::Intersection *", pybind11::return_value_policy::automatic, pybind11::arg("idx"));
		cl.def("getLevel", (unsigned char (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getLevel, "C++: bitpit::ParaTree::getLevel(const class bitpit::Intersection *) const --> unsigned char", pybind11::arg("inter"));
		cl.def("getFiner", (bool (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getFiner, "C++: bitpit::ParaTree::getFiner(const class bitpit::Intersection *) const --> bool", pybind11::arg("inter"));
		cl.def("getBound", (bool (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getBound, "C++: bitpit::ParaTree::getBound(const class bitpit::Intersection *) const --> bool", pybind11::arg("inter"));
		cl.def("getIsGhost", (bool (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getIsGhost, "C++: bitpit::ParaTree::getIsGhost(const class bitpit::Intersection *) const --> bool", pybind11::arg("inter"));
		cl.def("getPbound", (bool (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getPbound, "C++: bitpit::ParaTree::getPbound(const class bitpit::Intersection *) const --> bool", pybind11::arg("inter"));
		cl.def("getFace", (unsigned char (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getFace, "C++: bitpit::ParaTree::getFace(const class bitpit::Intersection *) const --> unsigned char", pybind11::arg("inter"));
		cl.def("getOwners", (class std::vector<unsigned int, class std::allocator<unsigned int> > (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getOwners, "C++: bitpit::ParaTree::getOwners(const class bitpit::Intersection *) const --> class std::vector<unsigned int, class std::allocator<unsigned int> >", pybind11::arg("inter"));
		cl.def("getIn", (unsigned int (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getIn, "C++: bitpit::ParaTree::getIn(const class bitpit::Intersection *) const --> unsigned int", pybind11::arg("inter"));
		cl.def("getOut", (unsigned int (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getOut, "C++: bitpit::ParaTree::getOut(const class bitpit::Intersection *) const --> unsigned int", pybind11::arg("inter"));
		cl.def("getOutIsGhost", (bool (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getOutIsGhost, "C++: bitpit::ParaTree::getOutIsGhost(const class bitpit::Intersection *) const --> bool", pybind11::arg("inter"));
		cl.def("getSize", (double (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getSize, "C++: bitpit::ParaTree::getSize(const class bitpit::Intersection *) const --> double", pybind11::arg("inter"));
		cl.def("getArea", (double (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getArea, "C++: bitpit::ParaTree::getArea(const class bitpit::Intersection *) const --> double", pybind11::arg("inter"));
		cl.def("getCenter", (struct std::array<double, 3> (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getCenter, "C++: bitpit::ParaTree::getCenter(const class bitpit::Intersection *) const --> struct std::array<double, 3>", pybind11::arg("inter"));
		cl.def("getNodes", (class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > > (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getNodes, "C++: bitpit::ParaTree::getNodes(const class bitpit::Intersection *) const --> class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > >", pybind11::arg("inter"));
		cl.def("getNormal", (struct std::array<double, 3> (bitpit::ParaTree::*)(const class bitpit::Intersection *) const) &bitpit::ParaTree::getNormal, "C++: bitpit::ParaTree::getNormal(const class bitpit::Intersection *) const --> struct std::array<double, 3>", pybind11::arg("inter"));
		cl.def("getOctant", (class bitpit::Octant * (bitpit::ParaTree::*)(unsigned int)) &bitpit::ParaTree::getOctant, "C++: bitpit::ParaTree::getOctant(unsigned int) --> class bitpit::Octant *", pybind11::return_value_policy::automatic, pybind11::arg("idx"));
		cl.def("getGhostOctant", (class bitpit::Octant * (bitpit::ParaTree::*)(unsigned int)) &bitpit::ParaTree::getGhostOctant, "C++: bitpit::ParaTree::getGhostOctant(unsigned int) --> class bitpit::Octant *", pybind11::return_value_policy::automatic, pybind11::arg("idx"));
		cl.def("getIsGhost", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getIsGhost, "C++: bitpit::ParaTree::getIsGhost(const class bitpit::Octant *) const --> bool", pybind11::arg("oct"));
		cl.def("getGhostLayer", (int (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getGhostLayer, "C++: bitpit::ParaTree::getGhostLayer(const class bitpit::Octant *) const --> int", pybind11::arg("oct"));
		cl.def("getLoadBalanceRanges", (const struct bitpit::ParaTree::LoadBalanceRanges & (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getLoadBalanceRanges, "C++: bitpit::ParaTree::getLoadBalanceRanges() const --> const struct bitpit::ParaTree::LoadBalanceRanges &", pybind11::return_value_policy::automatic);
		cl.def("getNofGhostLayers", (unsigned long (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNofGhostLayers, "C++: bitpit::ParaTree::getNofGhostLayers() const --> unsigned long");
		cl.def("setNofGhostLayers", (void (bitpit::ParaTree::*)(unsigned long)) &bitpit::ParaTree::setNofGhostLayers, "C++: bitpit::ParaTree::setNofGhostLayers(unsigned long) --> void", pybind11::arg("nofGhostLayers"));
		cl.def("getBordersPerProc", (const class std::map<int, class std::vector<unsigned int, class std::allocator<unsigned int> >, struct std::less<int>, class std::allocator<struct std::pair<const int, class std::vector<unsigned int, class std::allocator<unsigned int> > > > > & (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getBordersPerProc, "C++: bitpit::ParaTree::getBordersPerProc() const --> const class std::map<int, class std::vector<unsigned int, class std::allocator<unsigned int> >, struct std::less<int>, class std::allocator<struct std::pair<const int, class std::vector<unsigned int, class std::allocator<unsigned int> > > > > &", pybind11::return_value_policy::automatic);
		cl.def("findNeighbours", (void (bitpit::ParaTree::*)(unsigned int, unsigned char, unsigned char, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const) &bitpit::ParaTree::findNeighbours, "C++: bitpit::ParaTree::findNeighbours(unsigned int, unsigned char, unsigned char, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const --> void", pybind11::arg("idx"), pybind11::arg("iface"), pybind11::arg("codim"), pybind11::arg("neighbours"), pybind11::arg("isghost"));
		cl.def("findNeighbours", (void (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char, unsigned char, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const) &bitpit::ParaTree::findNeighbours, "C++: bitpit::ParaTree::findNeighbours(const class bitpit::Octant *, unsigned char, unsigned char, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const --> void", pybind11::arg("oct"), pybind11::arg("iface"), pybind11::arg("codim"), pybind11::arg("neighbours"), pybind11::arg("isghost"));
		cl.def("findGhostNeighbours", (void (bitpit::ParaTree::*)(unsigned int, unsigned char, unsigned char, class std::vector<unsigned int, class std::allocator<unsigned int> > &) const) &bitpit::ParaTree::findGhostNeighbours, "C++: bitpit::ParaTree::findGhostNeighbours(unsigned int, unsigned char, unsigned char, class std::vector<unsigned int, class std::allocator<unsigned int> > &) const --> void", pybind11::arg("idx"), pybind11::arg("iface"), pybind11::arg("codim"), pybind11::arg("neighbours"));
		cl.def("findGhostNeighbours", (void (bitpit::ParaTree::*)(unsigned int, unsigned char, unsigned char, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const) &bitpit::ParaTree::findGhostNeighbours, "C++: bitpit::ParaTree::findGhostNeighbours(unsigned int, unsigned char, unsigned char, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const --> void", pybind11::arg("idx"), pybind11::arg("iface"), pybind11::arg("codim"), pybind11::arg("neighbours"), pybind11::arg("isghost"));
		cl.def("findGhostNeighbours", (void (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char, unsigned char, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const) &bitpit::ParaTree::findGhostNeighbours, "C++: bitpit::ParaTree::findGhostNeighbours(const class bitpit::Octant *, unsigned char, unsigned char, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const --> void", pybind11::arg("oct"), pybind11::arg("iface"), pybind11::arg("codim"), pybind11::arg("neighbours"), pybind11::arg("isghost"));
		cl.def("findAllNodeNeighbours", (void (bitpit::ParaTree::*)(unsigned int, unsigned int, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &)) &bitpit::ParaTree::findAllNodeNeighbours, "C++: bitpit::ParaTree::findAllNodeNeighbours(unsigned int, unsigned int, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) --> void", pybind11::arg("idx"), pybind11::arg("inode"), pybind11::arg("neighbours"), pybind11::arg("isghost"));
		cl.def("findAllNodeNeighbours", (void (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned int, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const) &bitpit::ParaTree::findAllNodeNeighbours, "C++: bitpit::ParaTree::findAllNodeNeighbours(const class bitpit::Octant *, unsigned int, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const --> void", pybind11::arg("oct"), pybind11::arg("inode"), pybind11::arg("neighbours"), pybind11::arg("isghost"));
		cl.def("getPointOwner", (class bitpit::Octant * (bitpit::ParaTree::*)(const class std::vector<double, class std::allocator<double> > &)) &bitpit::ParaTree::getPointOwner, "C++: bitpit::ParaTree::getPointOwner(const class std::vector<double, class std::allocator<double> > &) --> class bitpit::Octant *", pybind11::return_value_policy::automatic, pybind11::arg("point"));
		cl.def("getPointOwner", (class bitpit::Octant * (bitpit::ParaTree::*)(const class std::vector<double, class std::allocator<double> > &, bool &)) &bitpit::ParaTree::getPointOwner, "C++: bitpit::ParaTree::getPointOwner(const class std::vector<double, class std::allocator<double> > &, bool &) --> class bitpit::Octant *", pybind11::return_value_policy::automatic, pybind11::arg("point"), pybind11::arg("isghost"));
		cl.def("getPointOwner", (class bitpit::Octant * (bitpit::ParaTree::*)(const struct std::array<double, 3> &)) &bitpit::ParaTree::getPointOwner, "C++: bitpit::ParaTree::getPointOwner(const struct std::array<double, 3> &) --> class bitpit::Octant *", pybind11::return_value_policy::automatic, pybind11::arg("point"));
		cl.def("getPointOwner", (class bitpit::Octant * (bitpit::ParaTree::*)(const struct std::array<double, 3> &, bool &)) &bitpit::ParaTree::getPointOwner, "C++: bitpit::ParaTree::getPointOwner(const struct std::array<double, 3> &, bool &) --> class bitpit::Octant *", pybind11::return_value_policy::automatic, pybind11::arg("point"), pybind11::arg("isghost"));
		cl.def("getPointOwnerIdx", (unsigned int (bitpit::ParaTree::*)(const double *) const) &bitpit::ParaTree::getPointOwnerIdx, "C++: bitpit::ParaTree::getPointOwnerIdx(const double *) const --> unsigned int", pybind11::arg("point"));
		cl.def("getPointOwnerIdx", (unsigned int (bitpit::ParaTree::*)(const double *, bool &) const) &bitpit::ParaTree::getPointOwnerIdx, "C++: bitpit::ParaTree::getPointOwnerIdx(const double *, bool &) const --> unsigned int", pybind11::arg("point"), pybind11::arg("isghost"));
		cl.def("getPointOwnerIdx", (unsigned int (bitpit::ParaTree::*)(const class std::vector<double, class std::allocator<double> > &) const) &bitpit::ParaTree::getPointOwnerIdx, "C++: bitpit::ParaTree::getPointOwnerIdx(const class std::vector<double, class std::allocator<double> > &) const --> unsigned int", pybind11::arg("point"));
		cl.def("getPointOwnerIdx", (unsigned int (bitpit::ParaTree::*)(const class std::vector<double, class std::allocator<double> > &, bool &) const) &bitpit::ParaTree::getPointOwnerIdx, "C++: bitpit::ParaTree::getPointOwnerIdx(const class std::vector<double, class std::allocator<double> > &, bool &) const --> unsigned int", pybind11::arg("point"), pybind11::arg("isghost"));
		cl.def("getPointOwnerIdx", (unsigned int (bitpit::ParaTree::*)(const struct std::array<double, 3> &) const) &bitpit::ParaTree::getPointOwnerIdx, "C++: bitpit::ParaTree::getPointOwnerIdx(const struct std::array<double, 3> &) const --> unsigned int", pybind11::arg("point"));
		cl.def("getPointOwnerIdx", (unsigned int (bitpit::ParaTree::*)(const struct std::array<double, 3> &, bool &) const) &bitpit::ParaTree::getPointOwnerIdx, "C++: bitpit::ParaTree::getPointOwnerIdx(const struct std::array<double, 3> &, bool &) const --> unsigned int", pybind11::arg("point"), pybind11::arg("isghost"));
		cl.def("findAllCodimensionNeighbours", (void (bitpit::ParaTree::*)(unsigned int, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &)) &bitpit::ParaTree::findAllCodimensionNeighbours, "C++: bitpit::ParaTree::findAllCodimensionNeighbours(unsigned int, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) --> void", pybind11::arg("idx"), pybind11::arg("neighbours"), pybind11::arg("isghost"));
		cl.def("findAllCodimensionNeighbours", (void (bitpit::ParaTree::*)(class bitpit::Octant *, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &)) &bitpit::ParaTree::findAllCodimensionNeighbours, "C++: bitpit::ParaTree::findAllCodimensionNeighbours(class bitpit::Octant *, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) --> void", pybind11::arg("oct"), pybind11::arg("neighbours"), pybind11::arg("isghost"));
		cl.def("findGhostAllCodimensionNeighbours", (void (bitpit::ParaTree::*)(unsigned int, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &)) &bitpit::ParaTree::findGhostAllCodimensionNeighbours, "C++: bitpit::ParaTree::findGhostAllCodimensionNeighbours(unsigned int, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) --> void", pybind11::arg("idx"), pybind11::arg("neighbours"), pybind11::arg("isghost"));
		cl.def("findGhostAllCodimensionNeighbours", (void (bitpit::ParaTree::*)(class bitpit::Octant *, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &)) &bitpit::ParaTree::findGhostAllCodimensionNeighbours, "C++: bitpit::ParaTree::findGhostAllCodimensionNeighbours(class bitpit::Octant *, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) --> void", pybind11::arg("oct"), pybind11::arg("neighbours"), pybind11::arg("isghost"));
		cl.def("getMapping", (void (bitpit::ParaTree::*)(unsigned int &, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const) &bitpit::ParaTree::getMapping, "C++: bitpit::ParaTree::getMapping(unsigned int &, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &) const --> void", pybind11::arg("idx"), pybind11::arg("mapper"), pybind11::arg("isghost"));
		cl.def("getMapping", (void (bitpit::ParaTree::*)(unsigned int &, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &, class std::vector<int, class std::allocator<int> > &) const) &bitpit::ParaTree::getMapping, "C++: bitpit::ParaTree::getMapping(unsigned int &, class std::vector<unsigned int, class std::allocator<unsigned int> > &, class std::vector<bool, class std::allocator<bool> > &, class std::vector<int, class std::allocator<int> > &) const --> void", pybind11::arg("idx"), pybind11::arg("mapper"), pybind11::arg("isghost"), pybind11::arg("rank"));
		cl.def("isNodeOnOctant", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char, const class bitpit::Octant *) const) &bitpit::ParaTree::isNodeOnOctant, "C++: bitpit::ParaTree::isNodeOnOctant(const class bitpit::Octant *, unsigned char, const class bitpit::Octant *) const --> bool", pybind11::arg("nodeOctant"), pybind11::arg("nodeIndex"), pybind11::arg("octant"));
		cl.def("isEdgeOnOctant", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char, const class bitpit::Octant *) const) &bitpit::ParaTree::isEdgeOnOctant, "C++: bitpit::ParaTree::isEdgeOnOctant(const class bitpit::Octant *, unsigned char, const class bitpit::Octant *) const --> bool", pybind11::arg("edgeOctant"), pybind11::arg("edgeIndex"), pybind11::arg("octant"));
		cl.def("isFaceOnOctant", (bool (bitpit::ParaTree::*)(const class bitpit::Octant *, unsigned char, const class bitpit::Octant *) const) &bitpit::ParaTree::isFaceOnOctant, "C++: bitpit::ParaTree::isFaceOnOctant(const class bitpit::Octant *, unsigned char, const class bitpit::Octant *) const --> bool", pybind11::arg("faceOctant"), pybind11::arg("faceIndex"), pybind11::arg("octant"));
		cl.def("getPointOwnerRank", (int (bitpit::ParaTree::*)(struct std::array<double, 3>)) &bitpit::ParaTree::getPointOwnerRank, "C++: bitpit::ParaTree::getPointOwnerRank(struct std::array<double, 3>) --> int", pybind11::arg("point"));
		cl.def("getFamilySplittingNode", (unsigned char (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getFamilySplittingNode, "C++: bitpit::ParaTree::getFamilySplittingNode(const class bitpit::Octant *) const --> unsigned char", pybind11::arg(""));
		cl.def("expectedOctantAdapt", (void (bitpit::ParaTree::*)(const class bitpit::Octant *, signed char, class std::vector<class bitpit::Octant, class std::allocator<class bitpit::Octant> > *) const) &bitpit::ParaTree::expectedOctantAdapt, "C++: bitpit::ParaTree::expectedOctantAdapt(const class bitpit::Octant *, signed char, class std::vector<class bitpit::Octant, class std::allocator<class bitpit::Octant> > *) const --> void", pybind11::arg("octant"), pybind11::arg("marker"), pybind11::arg("result"));
		cl.def("getMaxDepth", (signed char (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getMaxDepth, "C++: bitpit::ParaTree::getMaxDepth() const --> signed char");
		cl.def("findOwner", (int (bitpit::ParaTree::*)(unsigned long) const) &bitpit::ParaTree::findOwner, "C++: bitpit::ParaTree::findOwner(unsigned long) const --> int", pybind11::arg("morton"));
		cl.def("getOwnerRank", (int (bitpit::ParaTree::*)(unsigned long) const) &bitpit::ParaTree::getOwnerRank, "C++: bitpit::ParaTree::getOwnerRank(unsigned long) const --> int", pybind11::arg("globalIdx"));
		cl.def("settleMarkers", (void (bitpit::ParaTree::*)()) &bitpit::ParaTree::settleMarkers, "C++: bitpit::ParaTree::settleMarkers() --> void");
		cl.def("preadapt", (void (bitpit::ParaTree::*)()) &bitpit::ParaTree::preadapt, "C++: bitpit::ParaTree::preadapt() --> void");
		cl.def("checkToAdapt", (bool (bitpit::ParaTree::*)()) &bitpit::ParaTree::checkToAdapt, "C++: bitpit::ParaTree::checkToAdapt() --> bool");
		cl.def("adapt", [](bitpit::ParaTree &o) -> bool { return o.adapt(); }, "");
		cl.def("adapt", (bool (bitpit::ParaTree::*)(bool)) &bitpit::ParaTree::adapt, "C++: bitpit::ParaTree::adapt(bool) --> bool", pybind11::arg("mapper_flag"));
		cl.def("adaptGlobalRefine", [](bitpit::ParaTree &o) -> bool { return o.adaptGlobalRefine(); }, "");
		cl.def("adaptGlobalRefine", (bool (bitpit::ParaTree::*)(bool)) &bitpit::ParaTree::adaptGlobalRefine, "C++: bitpit::ParaTree::adaptGlobalRefine(bool) --> bool", pybind11::arg("mapper_flag"));
		cl.def("adaptGlobalCoarse", [](bitpit::ParaTree &o) -> bool { return o.adaptGlobalCoarse(); }, "");
		cl.def("adaptGlobalCoarse", (bool (bitpit::ParaTree::*)(bool)) &bitpit::ParaTree::adaptGlobalCoarse, "C++: bitpit::ParaTree::adaptGlobalCoarse(bool) --> bool", pybind11::arg("mapper_flag"));
		cl.def("computeConnectivity", (void (bitpit::ParaTree::*)()) &bitpit::ParaTree::computeConnectivity, "C++: bitpit::ParaTree::computeConnectivity() --> void");
		cl.def("clearConnectivity", (void (bitpit::ParaTree::*)()) &bitpit::ParaTree::clearConnectivity, "C++: bitpit::ParaTree::clearConnectivity() --> void");
		cl.def("updateConnectivity", (void (bitpit::ParaTree::*)()) &bitpit::ParaTree::updateConnectivity, "C++: bitpit::ParaTree::updateConnectivity() --> void");
		cl.def("getConnectivity", (const class std::vector<class std::vector<unsigned int, class std::allocator<unsigned int> >, class std::allocator<class std::vector<unsigned int, class std::allocator<unsigned int> > > > & (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getConnectivity, "C++: bitpit::ParaTree::getConnectivity() const --> const class std::vector<class std::vector<unsigned int, class std::allocator<unsigned int> >, class std::allocator<class std::vector<unsigned int, class std::allocator<unsigned int> > > > &", pybind11::return_value_policy::automatic);
		cl.def("getConnectivity", (const class std::vector<unsigned int, class std::allocator<unsigned int> > & (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getConnectivity, "C++: bitpit::ParaTree::getConnectivity(unsigned int) const --> const class std::vector<unsigned int, class std::allocator<unsigned int> > &", pybind11::return_value_policy::automatic, pybind11::arg("idx"));
		cl.def("getConnectivity", (const class std::vector<unsigned int, class std::allocator<unsigned int> > & (bitpit::ParaTree::*)(class bitpit::Octant *) const) &bitpit::ParaTree::getConnectivity, "C++: bitpit::ParaTree::getConnectivity(class bitpit::Octant *) const --> const class std::vector<unsigned int, class std::allocator<unsigned int> > &", pybind11::return_value_policy::automatic, pybind11::arg("oct"));
		cl.def("getNodes", (const class std::vector<struct std::array<unsigned int, 3>, class std::allocator<struct std::array<unsigned int, 3> > > & (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getNodes, "C++: bitpit::ParaTree::getNodes() const --> const class std::vector<struct std::array<unsigned int, 3>, class std::allocator<struct std::array<unsigned int, 3> > > &", pybind11::return_value_policy::automatic);
		cl.def("getNodeCoordinates", (struct std::array<double, 3> (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getNodeCoordinates, "C++: bitpit::ParaTree::getNodeCoordinates(unsigned int) const --> struct std::array<double, 3>", pybind11::arg("inode"));
		cl.def("getGhostConnectivity", (const class std::vector<class std::vector<unsigned int, class std::allocator<unsigned int> >, class std::allocator<class std::vector<unsigned int, class std::allocator<unsigned int> > > > & (bitpit::ParaTree::*)() const) &bitpit::ParaTree::getGhostConnectivity, "C++: bitpit::ParaTree::getGhostConnectivity() const --> const class std::vector<class std::vector<unsigned int, class std::allocator<unsigned int> >, class std::allocator<class std::vector<unsigned int, class std::allocator<unsigned int> > > > &", pybind11::return_value_policy::automatic);
		cl.def("getGhostConnectivity", (const class std::vector<unsigned int, class std::allocator<unsigned int> > & (bitpit::ParaTree::*)(unsigned int) const) &bitpit::ParaTree::getGhostConnectivity, "C++: bitpit::ParaTree::getGhostConnectivity(unsigned int) const --> const class std::vector<unsigned int, class std::allocator<unsigned int> > &", pybind11::return_value_policy::automatic, pybind11::arg("idx"));
		cl.def("getGhostConnectivity", (const class std::vector<unsigned int, class std::allocator<unsigned int> > & (bitpit::ParaTree::*)(const class bitpit::Octant *) const) &bitpit::ParaTree::getGhostConnectivity, "C++: bitpit::ParaTree::getGhostConnectivity(const class bitpit::Octant *) const --> const class std::vector<unsigned int, class std::allocator<unsigned int> > &", pybind11::return_value_policy::automatic, pybind11::arg("oct"));
		cl.def("check21Balance", (bool (bitpit::ParaTree::*)()) &bitpit::ParaTree::check21Balance, "C++: bitpit::ParaTree::check21Balance() --> bool");
		cl.def("loadBalance", [](bitpit::ParaTree &o) -> void { return o.loadBalance(); }, "");
		cl.def("loadBalance", (void (bitpit::ParaTree::*)(class std::vector<double, class std::allocator<double> > *)) &bitpit::ParaTree::loadBalance, "C++: bitpit::ParaTree::loadBalance(class std::vector<double, class std::allocator<double> > *) --> void", pybind11::arg("weight"));
		cl.def("loadBalance", [](bitpit::ParaTree &o, unsigned char & a0) -> void { return o.loadBalance(a0); }, "", pybind11::arg("level"));
		cl.def("loadBalance", (void (bitpit::ParaTree::*)(unsigned char &, class std::vector<double, class std::allocator<double> > *)) &bitpit::ParaTree::loadBalance, "C++: bitpit::ParaTree::loadBalance(unsigned char &, class std::vector<double, class std::allocator<double> > *) --> void", pybind11::arg("level"), pybind11::arg("weight"));
		cl.def("evalLoadBalanceRanges", (struct bitpit::ParaTree::LoadBalanceRanges (bitpit::ParaTree::*)(class std::vector<double, class std::allocator<double> > *)) &bitpit::ParaTree::evalLoadBalanceRanges, "C++: bitpit::ParaTree::evalLoadBalanceRanges(class std::vector<double, class std::allocator<double> > *) --> struct bitpit::ParaTree::LoadBalanceRanges", pybind11::arg("weights"));
		cl.def("evalLoadBalanceRanges", (struct bitpit::ParaTree::LoadBalanceRanges (bitpit::ParaTree::*)(unsigned char, class std::vector<double, class std::allocator<double> > *)) &bitpit::ParaTree::evalLoadBalanceRanges, "C++: bitpit::ParaTree::evalLoadBalanceRanges(unsigned char, class std::vector<double, class std::allocator<double> > *) --> struct bitpit::ParaTree::LoadBalanceRanges", pybind11::arg("level"), pybind11::arg("weights"));
		cl.def("levelToSize", (double (bitpit::ParaTree::*)(unsigned char &)) &bitpit::ParaTree::levelToSize, "C++: bitpit::ParaTree::levelToSize(unsigned char &) --> double", pybind11::arg("level"));
		cl.def("computeIntersections", (void (bitpit::ParaTree::*)()) &bitpit::ParaTree::computeIntersections, "C++: bitpit::ParaTree::computeIntersections() --> void");
		cl.def("write", (void (bitpit::ParaTree::*)(const std::string &)) &bitpit::ParaTree::write, "C++: bitpit::ParaTree::write(const std::string &) --> void", pybind11::arg("filename"));
		cl.def("writeTest", (void (bitpit::ParaTree::*)(const std::string &, class std::vector<double, class std::allocator<double> >)) &bitpit::ParaTree::writeTest, "C++: bitpit::ParaTree::writeTest(const std::string &, class std::vector<double, class std::allocator<double> >) --> void", pybind11::arg("filename"), pybind11::arg("data"));

		{ // bitpit::ParaTree::LoadBalanceRanges file:ParaTree.hpp line:103
			auto & enclosing_class = cl;
			pybind11::class_<bitpit::ParaTree::LoadBalanceRanges, std::shared_ptr<bitpit::ParaTree::LoadBalanceRanges>> cl(enclosing_class, "LoadBalanceRanges", "");
			cl.def( pybind11::init( [](){ return new bitpit::ParaTree::LoadBalanceRanges(); } ) );
			cl.def( pybind11::init<bool, const class std::unordered_map<int, struct std::array<unsigned int, 2>, struct std::hash<int>, struct std::equal_to<int>, class std::allocator<struct std::pair<const int, struct std::array<unsigned int, 2> > > > &, const class std::unordered_map<int, struct std::array<unsigned int, 2>, struct std::hash<int>, struct std::equal_to<int>, class std::allocator<struct std::pair<const int, struct std::array<unsigned int, 2> > > > &>(), pybind11::arg("serial"), pybind11::arg("_sendRanges"), pybind11::arg("_recvRanges") );

			cl.def( pybind11::init( [](bitpit::ParaTree::LoadBalanceRanges const &o){ return new bitpit::ParaTree::LoadBalanceRanges(o); } ) );

			pybind11::enum_<bitpit::ParaTree::LoadBalanceRanges::ExchangeAction>(cl, "ExchangeAction", pybind11::arithmetic(), "")
				.value("ACTION_UNDEFINED", bitpit::ParaTree::LoadBalanceRanges::ACTION_UNDEFINED)
				.value("ACTION_NONE", bitpit::ParaTree::LoadBalanceRanges::ACTION_NONE)
				.value("ACTION_SEND", bitpit::ParaTree::LoadBalanceRanges::ACTION_SEND)
				.value("ACTION_DELETE", bitpit::ParaTree::LoadBalanceRanges::ACTION_DELETE)
				.value("ACTION_RECEIVE", bitpit::ParaTree::LoadBalanceRanges::ACTION_RECEIVE)
				.export_values();

			cl.def_readwrite("sendAction", &bitpit::ParaTree::LoadBalanceRanges::sendAction);
			cl.def_readwrite("sendRanges", &bitpit::ParaTree::LoadBalanceRanges::sendRanges);
			cl.def_readwrite("recvAction", &bitpit::ParaTree::LoadBalanceRanges::recvAction);
			cl.def_readwrite("recvRanges", &bitpit::ParaTree::LoadBalanceRanges::recvRanges);
			cl.def("clear", (void (bitpit::ParaTree::LoadBalanceRanges::*)()) &bitpit::ParaTree::LoadBalanceRanges::clear, "C++: bitpit::ParaTree::LoadBalanceRanges::clear() --> void");
			cl.def("assign", (struct bitpit::ParaTree::LoadBalanceRanges & (bitpit::ParaTree::LoadBalanceRanges::*)(const struct bitpit::ParaTree::LoadBalanceRanges &)) &bitpit::ParaTree::LoadBalanceRanges::operator=, "C++: bitpit::ParaTree::LoadBalanceRanges::operator=(const struct bitpit::ParaTree::LoadBalanceRanges &) --> struct bitpit::ParaTree::LoadBalanceRanges &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		}

	}
}
#include <STL.hpp>
#include <array>
#include <communications_tags.hpp>
#include <ios>
#include <iostream>
#include <iterator>
#include <memory>
#include <ostream>
#include <sstream> // __str__
#include <streambuf>
#include <string>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_STL(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B565_[bitpit::STLBase] ";
	{ // bitpit::STLBase file:STL.hpp line:39
		pybind11::class_<bitpit::STLBase, std::shared_ptr<bitpit::STLBase>> cl(M("bitpit"), "STLBase", "");
		cl.def( pybind11::init( [](bitpit::STLBase const &o){ return new bitpit::STLBase(o); } ) );

		pybind11::enum_<bitpit::STLBase::Format>(cl, "Format", pybind11::arithmetic(), "")
			.value("FormatUnknown", bitpit::STLBase::FormatUnknown)
			.value("FormatASCII", bitpit::STLBase::FormatASCII)
			.value("FormatBinary", bitpit::STLBase::FormatBinary)
			.export_values();

		cl.def("getFilename", (const std::string & (bitpit::STLBase::*)() const) &bitpit::STLBase::getFilename, "C++: bitpit::STLBase::getFilename() const --> const std::string &", pybind11::return_value_policy::automatic);
		cl.def("getFormat", (enum bitpit::STLBase::Format (bitpit::STLBase::*)() const) &bitpit::STLBase::getFormat, "C++: bitpit::STLBase::getFormat() const --> enum bitpit::STLBase::Format");
		cl.def("assign", (class bitpit::STLBase & (bitpit::STLBase::*)(const class bitpit::STLBase &)) &bitpit::STLBase::operator=, "C++: bitpit::STLBase::operator=(const class bitpit::STLBase &) --> class bitpit::STLBase &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B566_[bitpit::STLReader] ";
	{ // bitpit::STLReader file:STL.hpp line:82
		pybind11::class_<bitpit::STLReader, std::shared_ptr<bitpit::STLReader>, bitpit::STLBase> cl(M("bitpit"), "STLReader", "");
		cl.def( pybind11::init( [](const class std::__cxx11::basic_string<char> & a0){ return new bitpit::STLReader(a0); } ), "doc" , pybind11::arg("filename"));

		cl.def_static("detectFormat", (enum bitpit::STLBase::Format (*)(const std::string &)) &bitpit::STLReader::detectFormat, "C++: bitpit::STLReader::detectFormat(const std::string &) --> enum bitpit::STLBase::Format", pybind11::arg("filename"));
		cl.def("inspect", (int (bitpit::STLReader::*)(struct bitpit::STLReader::InspectionInfo *)) &bitpit::STLReader::inspect, "C++: bitpit::STLReader::inspect(struct bitpit::STLReader::InspectionInfo *) --> int", pybind11::arg("info"));
		cl.def("displayInspectionInfo", (void (bitpit::STLReader::*)(const struct bitpit::STLReader::InspectionInfo &, std::ostream &) const) &bitpit::STLReader::displayInspectionInfo, "C++: bitpit::STLReader::displayInspectionInfo(const struct bitpit::STLReader::InspectionInfo &, std::ostream &) const --> void", pybind11::arg("info"), pybind11::arg("out"));
		cl.def("readBegin", (int (bitpit::STLReader::*)()) &bitpit::STLReader::readBegin, "C++: bitpit::STLReader::readBegin() --> int");
		cl.def("readEnd", (int (bitpit::STLReader::*)()) &bitpit::STLReader::readEnd, "C++: bitpit::STLReader::readEnd() --> int");
		cl.def("readHeader", (int (bitpit::STLReader::*)(std::string *, unsigned long *)) &bitpit::STLReader::readHeader, "C++: bitpit::STLReader::readHeader(std::string *, unsigned long *) --> int", pybind11::arg("name"), pybind11::arg("nT"));
		cl.def("readHeader", (int (bitpit::STLReader::*)(const std::string &, std::string *, unsigned long *)) &bitpit::STLReader::readHeader, "C++: bitpit::STLReader::readHeader(const std::string &, std::string *, unsigned long *) --> int", pybind11::arg("solid"), pybind11::arg("name"), pybind11::arg("nT"));
		cl.def("readFooter", (int (bitpit::STLReader::*)()) &bitpit::STLReader::readFooter, "C++: bitpit::STLReader::readFooter() --> int");
		cl.def("readFooter", (int (bitpit::STLReader::*)(const std::string &)) &bitpit::STLReader::readFooter, "C++: bitpit::STLReader::readFooter(const std::string &) --> int", pybind11::arg("solid"));
		cl.def("readFacet", (int (bitpit::STLReader::*)(struct std::array<double, 3> *, struct std::array<double, 3> *, struct std::array<double, 3> *, struct std::array<double, 3> *)) &bitpit::STLReader::readFacet, "C++: bitpit::STLReader::readFacet(struct std::array<double, 3> *, struct std::array<double, 3> *, struct std::array<double, 3> *, struct std::array<double, 3> *) --> int", pybind11::arg("V0"), pybind11::arg("V1"), pybind11::arg("V2"), pybind11::arg("N"));

		{ // bitpit::STLReader::InspectionInfo file:STL.hpp line:90
			auto & enclosing_class = cl;
			pybind11::class_<bitpit::STLReader::InspectionInfo, std::shared_ptr<bitpit::STLReader::InspectionInfo>> cl(enclosing_class, "InspectionInfo", "Structure holding inspection information");
			cl.def( pybind11::init( [](){ return new bitpit::STLReader::InspectionInfo(); } ) );
			cl.def( pybind11::init( [](bitpit::STLReader::InspectionInfo const &o){ return new bitpit::STLReader::InspectionInfo(o); } ) );
			cl.def_readwrite("nSolids", &bitpit::STLReader::InspectionInfo::nSolids);
			cl.def_readwrite("solidValid", &bitpit::STLReader::InspectionInfo::solidValid);
			cl.def_readwrite("solidErrors", &bitpit::STLReader::InspectionInfo::solidErrors);
			cl.def_readwrite("solidNames", &bitpit::STLReader::InspectionInfo::solidNames);
			cl.def_readwrite("solidFacetCount", &bitpit::STLReader::InspectionInfo::solidFacetCount);
			cl.def_readwrite("solidVertexCount", &bitpit::STLReader::InspectionInfo::solidVertexCount);
		}

	}
	std::cout << "B567_[bitpit::STLWriter] ";
	{ // bitpit::STLWriter file:STL.hpp line:155
		pybind11::class_<bitpit::STLWriter, std::shared_ptr<bitpit::STLWriter>, bitpit::STLBase> cl(M("bitpit"), "STLWriter", "");


		pybind11::enum_<bitpit::STLWriter::WriteMode>(cl, "WriteMode", pybind11::arithmetic(), "")
			.value("WriteOverwrite", bitpit::STLWriter::WriteOverwrite)
			.value("WriteAppend", bitpit::STLWriter::WriteAppend)
			.export_values();

		cl.def("writeBegin", [](bitpit::STLWriter &o, enum bitpit::STLWriter::WriteMode const & a0) -> int { return o.writeBegin(a0); }, "", pybind11::arg("writeMode"));
		cl.def("writeBegin", (int (bitpit::STLWriter::*)(enum bitpit::STLWriter::WriteMode, bool)) &bitpit::STLWriter::writeBegin, "C++: bitpit::STLWriter::writeBegin(enum bitpit::STLWriter::WriteMode, bool) --> int", pybind11::arg("writeMode"), pybind11::arg("partialWrite"));
		cl.def("writeEnd", (int (bitpit::STLWriter::*)()) &bitpit::STLWriter::writeEnd, "C++: bitpit::STLWriter::writeEnd() --> int");
		cl.def("writeHeader", (int (bitpit::STLWriter::*)(const std::string &, unsigned long)) &bitpit::STLWriter::writeHeader, "C++: bitpit::STLWriter::writeHeader(const std::string &, unsigned long) --> int", pybind11::arg("name"), pybind11::arg("nT"));
		cl.def("writeFooter", (int (bitpit::STLWriter::*)(const std::string &)) &bitpit::STLWriter::writeFooter, "C++: bitpit::STLWriter::writeFooter(const std::string &) --> int", pybind11::arg("name"));
		cl.def("writeFacet", (int (bitpit::STLWriter::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> &, const struct std::array<double, 3> &, const struct std::array<double, 3> &)) &bitpit::STLWriter::writeFacet, "C++: bitpit::STLWriter::writeFacet(const struct std::array<double, 3> &, const struct std::array<double, 3> &, const struct std::array<double, 3> &, const struct std::array<double, 3> &) --> int", pybind11::arg("V0"), pybind11::arg("V1"), pybind11::arg("V2"), pybind11::arg("N"));
	}
	std::cout << "B568_[bitpit::CommunicationTags] ";
	{ // bitpit::CommunicationTags file:communications_tags.hpp line:37
		pybind11::class_<bitpit::CommunicationTags, std::shared_ptr<bitpit::CommunicationTags>, bitpit::IndexGenerator<int>> cl(M("bitpit"), "CommunicationTags", "");
		cl.def_static("instance", (class bitpit::CommunicationTags & (*)()) &bitpit::CommunicationTags::instance, "C++: bitpit::CommunicationTags::instance() --> class bitpit::CommunicationTags &", pybind11::return_value_policy::automatic);
	}
}
#include <BoundaryConditions.hpp>
#include <Grid.hpp>
#include <Laplacian.hpp>
#include <LaplacianFactory.hpp>
#include <NeosPiercedVector.hpp>
#include <PabloUniform.hpp>
#include <STLGeometry.hpp>
#include <adaption.hpp>
#include <array>
#include <cell.hpp>
#include <common.hpp>
#include <element_type.hpp>
#include <functional>
#include <iostream>
#include <istream>
#include <iterator>
#include <map>
#include <memory>
#include <ostream>
#include <piercedVector.hpp>
#include <piercedVectorKernel.hpp>
#include <piercedVectorStorage.hpp>
#include <sstream> // __str__
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// neos::STLGeometry file:STLGeometry.hpp line:32
struct PyCallBack_neos_STLGeometry : public neos::STLGeometry {
	using neos::STLGeometry::STLGeometry;

	double getLevelSet(const struct std::array<double, 3> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::STLGeometry *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return STLGeometry::getLevelSet(a0);
	}
	double getLevelSet(const long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::STLGeometry *>(this), "getLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return STLGeometry::getLevelSet(a0);
	}
	using _binder_ret_0 = class neos::PiercedVector<double, long> &;
	_binder_ret_0 getPhi() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::STLGeometry *>(this), "getPhi");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return STLGeometry::getPhi();
	}
	void computeLevelSet(class neos::Grid * a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const neos::STLGeometry *>(this), "computeLevelSet");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return STLGeometry::computeLevelSet(a0);
	}
};

void bind_STLGeometry(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B628_[neos::STLGeometry] ";
	{ // neos::STLGeometry file:STLGeometry.hpp line:32
		pybind11::class_<neos::STLGeometry, std::shared_ptr<neos::STLGeometry>, PyCallBack_neos_STLGeometry, neos::Geometry> cl(M("neos"), "STLGeometry", "");
		cl.def( pybind11::init( [](){ return new neos::STLGeometry(); }, [](){ return new PyCallBack_neos_STLGeometry(); } ) );

		cl.def("addSTL", (void (neos::STLGeometry::*)(const std::string &, class neos::Grid *)) &neos::STLGeometry::addSTL, "C++: neos::STLGeometry::addSTL(const std::string &, class neos::Grid *) --> void", pybind11::arg("file"), pybind11::arg("grid"));
		cl.def("getLevelSet", (double (neos::STLGeometry::*)(const struct std::array<double, 3> &)) &neos::STLGeometry::getLevelSet, "C++: neos::STLGeometry::getLevelSet(const struct std::array<double, 3> &) --> double", pybind11::arg("point"));
		cl.def("getLevelSet", (double (neos::STLGeometry::*)(const long)) &neos::STLGeometry::getLevelSet, "C++: neos::STLGeometry::getLevelSet(const long) --> double", pybind11::arg("id"));
		cl.def("getPhi", (class neos::PiercedVector<double, long> & (neos::STLGeometry::*)()) &neos::STLGeometry::getPhi, "C++: neos::STLGeometry::getPhi() --> class neos::PiercedVector<double, long> &", pybind11::return_value_policy::automatic);
		cl.def("computeLevelSet", (void (neos::STLGeometry::*)(class neos::Grid *)) &neos::STLGeometry::computeLevelSet, "C++: neos::STLGeometry::computeLevelSet(class neos::Grid *) --> void", pybind11::arg("grid"));
	}
	std::cout << "B629_[neos::Stencil_weights] ";
	std::cout << "B630_[neos::Stencil] ";
	std::cout << "B631_[neos::StencilBuilder] ";
	std::cout << "B632_[neos::Laplacian] ";
	{ // neos::Laplacian file:Laplacian.hpp line:18
		pybind11::class_<neos::Laplacian, std::shared_ptr<neos::Laplacian>> cl(M("neos"), "Laplacian", "");
		cl.def("setMatrixValue", (void (neos::Laplacian::*)(int, int, double)) &neos::Laplacian::setMatrixValue, "C++: neos::Laplacian::setMatrixValue(int, int, double) --> void", pybind11::arg("row"), pybind11::arg("col"), pybind11::arg("val"));
		cl.def("addMatrixValue", (void (neos::Laplacian::*)(int, int, double)) &neos::Laplacian::addMatrixValue, "C++: neos::Laplacian::addMatrixValue(int, int, double) --> void", pybind11::arg("row"), pybind11::arg("col"), pybind11::arg("val"));
		cl.def("zeroMatrix", (void (neos::Laplacian::*)()) &neos::Laplacian::zeroMatrix, "C++: neos::Laplacian::zeroMatrix() --> void");
		cl.def("setRHSValue", (void (neos::Laplacian::*)(int, double)) &neos::Laplacian::setRHSValue, "C++: neos::Laplacian::setRHSValue(int, double) --> void", pybind11::arg("row"), pybind11::arg("val"));
		cl.def("addRHSValue", (void (neos::Laplacian::*)(int, double)) &neos::Laplacian::addRHSValue, "C++: neos::Laplacian::addRHSValue(int, double) --> void", pybind11::arg("row"), pybind11::arg("val"));
		cl.def("getRHSValue", (double (neos::Laplacian::*)(const int)) &neos::Laplacian::getRHSValue, "C++: neos::Laplacian::getRHSValue(const int) --> double", pybind11::arg("row"));
		cl.def("zeroRHS", (void (neos::Laplacian::*)()) &neos::Laplacian::zeroRHS, "C++: neos::Laplacian::zeroRHS() --> void");
		cl.def("solveLaplacian", (double (neos::Laplacian::*)()) &neos::Laplacian::solveLaplacian, "C++: neos::Laplacian::solveLaplacian() --> double");
		cl.def("getSolution", (class std::vector<double, class std::allocator<double> > (neos::Laplacian::*)()) &neos::Laplacian::getSolution, "C++: neos::Laplacian::getSolution() --> class std::vector<double, class std::allocator<double> >");
		cl.def("error", (class std::vector<double, class std::allocator<double> > (neos::Laplacian::*)()) &neos::Laplacian::error, "C++: neos::Laplacian::error() --> class std::vector<double, class std::allocator<double> >");
		cl.def("addSolutionToVTK", (void (neos::Laplacian::*)(std::string)) &neos::Laplacian::addSolutionToVTK, "C++: neos::Laplacian::addSolutionToVTK(std::string) --> void", pybind11::arg("tag"));
		cl.def("buildMatrix", (void (neos::Laplacian::*)()) &neos::Laplacian::buildMatrix, "C++: neos::Laplacian::buildMatrix() --> void");
		cl.def("buildFVMatrix", (void (neos::Laplacian::*)(class neos::PiercedVector<double, long> &, class neos::PiercedVector<double, long> &, double, enum neos::Var)) &neos::Laplacian::buildFVMatrix, "C++: neos::Laplacian::buildFVMatrix(class neos::PiercedVector<double, long> &, class neos::PiercedVector<double, long> &, double, enum neos::Var) --> void", pybind11::arg("kappaCC"), pybind11::arg("kappaFC"), pybind11::arg("t"), pybind11::arg("type"));
		cl.def("toggleNeumann", (void (neos::Laplacian::*)(bool)) &neos::Laplacian::toggleNeumann, "C++: neos::Laplacian::toggleNeumann(bool) --> void", pybind11::arg("neumann"));
		cl.def("computeRHSHeat2D", (void (neos::Laplacian::*)()) &neos::Laplacian::computeRHSHeat2D, "C++: neos::Laplacian::computeRHSHeat2D() --> void");
		cl.def("setLambda", (void (neos::Laplacian::*)(class std::vector<double, class std::allocator<double> > *)) &neos::Laplacian::setLambda, "C++: neos::Laplacian::setLambda(class std::vector<double, class std::allocator<double> > *) --> void", pybind11::arg("lambda"));
		cl.def("setDirichletCondition", [](neos::Laplacian &o) -> void { return o.setDirichletCondition(); }, "");
		cl.def("setDirichletCondition", (void (neos::Laplacian::*)(double)) &neos::Laplacian::setDirichletCondition, "C++: neos::Laplacian::setDirichletCondition(double) --> void", pybind11::arg("pos"));
		cl.def("setDirichletConditionRHS", [](neos::Laplacian &o) -> void { return o.setDirichletConditionRHS(); }, "");
		cl.def("setDirichletConditionRHS", (void (neos::Laplacian::*)(double)) &neos::Laplacian::setDirichletConditionRHS, "C++: neos::Laplacian::setDirichletConditionRHS(double) --> void", pybind11::arg("t"));
		cl.def("setRHS", (void (neos::Laplacian::*)(const class neos::PiercedVector<double, long> &)) &neos::Laplacian::setRHS, "C++: neos::Laplacian::setRHS(const class neos::PiercedVector<double, long> &) --> void", pybind11::arg("rhs"));
		cl.def("setRHS", (void (neos::Laplacian::*)(const class std::vector<double, class std::allocator<double> > &)) &neos::Laplacian::setRHS, "C++: neos::Laplacian::setRHS(const class std::vector<double, class std::allocator<double> > &) --> void", pybind11::arg("rhs"));
		cl.def("addRHS", (void (neos::Laplacian::*)(const class neos::PiercedVector<double, long> &)) &neos::Laplacian::addRHS, "C++: neos::Laplacian::addRHS(const class neos::PiercedVector<double, long> &) --> void", pybind11::arg("rhs"));
		cl.def("addRHS", (void (neos::Laplacian::*)(const class std::vector<double, class std::allocator<double> > &)) &neos::Laplacian::addRHS, "C++: neos::Laplacian::addRHS(const class std::vector<double, class std::allocator<double> > &) --> void", pybind11::arg("rhs"));
		cl.def("penalize", (void (neos::Laplacian::*)(const class std::vector<double, class std::allocator<double> > &)) &neos::Laplacian::penalize, "C++: neos::Laplacian::penalize(const class std::vector<double, class std::allocator<double> > &) --> void", pybind11::arg("phi"));
		cl.def("penalizeAtOrder1", (void (neos::Laplacian::*)(const class neos::PiercedVector<double, long> &, double)) &neos::Laplacian::penalizeAtOrder1, "C++: neos::Laplacian::penalizeAtOrder1(const class neos::PiercedVector<double, long> &, double) --> void", pybind11::arg("fctInd"), pybind11::arg("coeffPen"));
		cl.def("getSolutionPV", (class neos::PiercedVector<double, long> (neos::Laplacian::*)()) &neos::Laplacian::getSolutionPV, "C++: neos::Laplacian::getSolutionPV() --> class neos::PiercedVector<double, long>");
		cl.def("getGradientFromSolution", (class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > > (neos::Laplacian::*)()) &neos::Laplacian::getGradientFromSolution, "C++: neos::Laplacian::getGradientFromSolution() --> class std::vector<struct std::array<double, 3>, class std::allocator<struct std::array<double, 3> > >");
		cl.def("setRHS", (void (neos::Laplacian::*)()) &neos::Laplacian::setRHS, "C++: neos::Laplacian::setRHS() --> void");
		cl.def("solve", (class neos::PiercedVector<double, long> (neos::Laplacian::*)()) &neos::Laplacian::solve, "Solve the LaplacianPetsc\n\nC++: neos::Laplacian::solve() --> class neos::PiercedVector<double, long>");
		cl.def("setBCPositionCoef", (void (neos::Laplacian::*)(double)) &neos::Laplacian::setBCPositionCoef, "Set the BCPosition\n\n \n[in] bcPos Enum to set the BCPosition on the interface or outer the cell\n\nC++: neos::Laplacian::setBCPositionCoef(double) --> void", pybind11::arg("bcPos"));
		cl.def("assign", (class neos::Laplacian & (neos::Laplacian::*)(const class neos::Laplacian &)) &neos::Laplacian::operator=, "C++: neos::Laplacian::operator=(const class neos::Laplacian &) --> class neos::Laplacian &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B633_[neos::LaplacianFactory] ";
	{ // neos::LaplacianFactory file:LaplacianFactory.hpp line:26
		pybind11::class_<neos::LaplacianFactory, std::shared_ptr<neos::LaplacianFactory>> cl(M("neos"), "LaplacianFactory", "Laplacian algo class\n\n This class contains Florian algo for laplacian");
		cl.def( pybind11::init( [](){ return new neos::LaplacianFactory(); } ) );
		cl.def_static("get", (class neos::Laplacian * (*)(enum neos::lapType, enum neos::solverType, class neos::Grid *)) &neos::LaplacianFactory::get, "Return the good type of laplacian class\n\n \n lap_type enum to select the kind of laplacian to create\n \n\n Pointer on the grid\n \n\n a Laplacian class with the good laplacian instance\n\nC++: neos::LaplacianFactory::get(enum neos::lapType, enum neos::solverType, class neos::Grid *) --> class neos::Laplacian *", pybind11::return_value_policy::automatic, pybind11::arg("ltype"), pybind11::arg("lap"), pybind11::arg("grid"));
	}
}
#include <VTK.hpp>
#include <array>
#include <fstream>
#include <ios>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream> // __str__
#include <string>
#include <type_traits>
#include <typeinfo>
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// bitpit::VTKBaseStreamer file:VTK.hpp line:205
struct PyCallBack_bitpit_VTKBaseStreamer : public bitpit::VTKBaseStreamer {
	using bitpit::VTKBaseStreamer::VTKBaseStreamer;

};

// bitpit::VTKNativeStreamer file:VTK.hpp line:216
struct PyCallBack_bitpit_VTKNativeStreamer : public bitpit::VTKNativeStreamer {
	using bitpit::VTKNativeStreamer::VTKNativeStreamer;

};

// bitpit::VTKUnstructuredGrid file:VTK.hpp line:395
struct PyCallBack_bitpit_VTKUnstructuredGrid : public bitpit::VTKUnstructuredGrid {
	using bitpit::VTKUnstructuredGrid::VTKUnstructuredGrid;

};

// bitpit::VTKRectilinearGrid file:VTK.hpp line:455
struct PyCallBack_bitpit_VTKRectilinearGrid : public bitpit::VTKRectilinearGrid {
	using bitpit::VTKRectilinearGrid::VTKRectilinearGrid;

};

void bind_VTK(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B536_[bitpit::VTKWriteMode] ";
	// bitpit::VTKWriteMode file:VTK.hpp line:49
	pybind11::enum_<bitpit::VTKWriteMode>(M("bitpit"), "VTKWriteMode", "Enum class defining different modes for writing VTK files")
		.value("DEFAULT", bitpit::VTKWriteMode::DEFAULT)
		.value("NO_INCREMENT", bitpit::VTKWriteMode::NO_INCREMENT)
		.value("NO_SERIES", bitpit::VTKWriteMode::NO_SERIES);

;

	std::cout << "B537_[bitpit::VTKFieldType] ";
	// bitpit::VTKFieldType file:VTK.hpp line:59
	pybind11::enum_<bitpit::VTKFieldType>(M("bitpit"), "VTKFieldType", "Enum class defining types of fields whic may be written through class VTK")
		.value("UNDEFINED", bitpit::VTKFieldType::UNDEFINED)
		.value("SCALAR", bitpit::VTKFieldType::SCALAR)
		.value("VECTOR", bitpit::VTKFieldType::VECTOR)
		.value("KNOWN_BY_CLASS", bitpit::VTKFieldType::KNOWN_BY_CLASS);

;

	std::cout << "B538_[bitpit::VTKDataType] ";
	// bitpit::VTKDataType file:VTK.hpp line:70
	pybind11::enum_<bitpit::VTKDataType>(M("bitpit"), "VTKDataType", "Enum class defining basic data types of fields which may be written through VTK")
		.value("UNDEFINED", bitpit::VTKDataType::UNDEFINED)
		.value("Int8", bitpit::VTKDataType::Int8)
		.value("Int16", bitpit::VTKDataType::Int16)
		.value("Int32", bitpit::VTKDataType::Int32)
		.value("Int64", bitpit::VTKDataType::Int64)
		.value("UInt8", bitpit::VTKDataType::UInt8)
		.value("UInt16", bitpit::VTKDataType::UInt16)
		.value("UInt32", bitpit::VTKDataType::UInt32)
		.value("UInt64", bitpit::VTKDataType::UInt64)
		.value("Float32", bitpit::VTKDataType::Float32)
		.value("Float64", bitpit::VTKDataType::Float64);

;

	std::cout << "B539_[bitpit::VTKFormat] ";
	// bitpit::VTKFormat file:VTK.hpp line:88
	pybind11::enum_<bitpit::VTKFormat>(M("bitpit"), "VTKFormat", "Enum class defining the VTK format to be used for writing fields ")
		.value("UNDEFINED", bitpit::VTKFormat::UNDEFINED)
		.value("ASCII", bitpit::VTKFormat::ASCII)
		.value("APPENDED", bitpit::VTKFormat::APPENDED);

;

	std::cout << "B540_[bitpit::VTKLocation] ";
	// bitpit::VTKLocation file:VTK.hpp line:98
	pybind11::enum_<bitpit::VTKLocation>(M("bitpit"), "VTKLocation", "Enum class defining wheather data is stored at cells or nodes")
		.value("UNDEFINED", bitpit::VTKLocation::UNDEFINED)
		.value("CELL", bitpit::VTKLocation::CELL)
		.value("POINT", bitpit::VTKLocation::POINT);

;

	std::cout << "B541_[bitpit::VTKElementType] ";
	// bitpit::VTKElementType file:VTK.hpp line:108
	pybind11::enum_<bitpit::VTKElementType>(M("bitpit"), "VTKElementType", "Enum class listing different element types supported by VTKUnstructuredGrid")
		.value("UNDEFINED", bitpit::VTKElementType::UNDEFINED)
		.value("VERTEX", bitpit::VTKElementType::VERTEX)
		.value("LINE", bitpit::VTKElementType::LINE)
		.value("TRIANGLE", bitpit::VTKElementType::TRIANGLE)
		.value("POLYGON", bitpit::VTKElementType::POLYGON)
		.value("PIXEL", bitpit::VTKElementType::PIXEL)
		.value("QUAD", bitpit::VTKElementType::QUAD)
		.value("TETRA", bitpit::VTKElementType::TETRA)
		.value("VOXEL", bitpit::VTKElementType::VOXEL)
		.value("HEXAHEDRON", bitpit::VTKElementType::HEXAHEDRON)
		.value("WEDGE", bitpit::VTKElementType::WEDGE)
		.value("PYRAMID", bitpit::VTKElementType::PYRAMID)
		.value("QUADRATIC_EDGE", bitpit::VTKElementType::QUADRATIC_EDGE)
		.value("QUADRATIC_TRIANGLE", bitpit::VTKElementType::QUADRATIC_TRIANGLE)
		.value("QUADRATIC_QUAD", bitpit::VTKElementType::QUADRATIC_QUAD)
		.value("QUADRATIC_TETRA", bitpit::VTKElementType::QUADRATIC_TETRA)
		.value("QUADRATIC_HEXAHEDRON", bitpit::VTKElementType::QUADRATIC_HEXAHEDRON)
		.value("POLYHEDRON", bitpit::VTKElementType::POLYHEDRON);

;

	std::cout << "B542_[bitpit::VTKUnstructuredField] ";
	// bitpit::VTKUnstructuredField file:VTK.hpp line:133
	pybind11::enum_<bitpit::VTKUnstructuredField>(M("bitpit"), "VTKUnstructuredField", "Enum class listing different geometry fields used by VTKUnstructuredGrid")
		.value("POINTS", bitpit::VTKUnstructuredField::POINTS)
		.value("OFFSETS", bitpit::VTKUnstructuredField::OFFSETS)
		.value("TYPES", bitpit::VTKUnstructuredField::TYPES)
		.value("CONNECTIVITY", bitpit::VTKUnstructuredField::CONNECTIVITY)
		.value("FACE_STREAMS", bitpit::VTKUnstructuredField::FACE_STREAMS)
		.value("FACE_OFFSETS", bitpit::VTKUnstructuredField::FACE_OFFSETS);

;

	std::cout << "B543_[bitpit::VTKRectilinearField] ";
	// bitpit::VTKRectilinearField file:VTK.hpp line:146
	pybind11::enum_<bitpit::VTKRectilinearField>(M("bitpit"), "VTKRectilinearField", "Enum class listing different geometry fields used by VTKRectilinearGrid")
		.value("X_COORDS", bitpit::VTKRectilinearField::X_COORDS)
		.value("Y_COORDS", bitpit::VTKRectilinearField::Y_COORDS)
		.value("Z_COORDS", bitpit::VTKRectilinearField::Z_COORDS);

;

	std::cout << "B544_[bitpit::VTKTypes] ";
	{ // bitpit::VTKTypes file:VTK.hpp line:152
		pybind11::class_<bitpit::VTKTypes, std::shared_ptr<bitpit::VTKTypes>> cl(M("bitpit"), "VTKTypes", "");
		cl.def( pybind11::init( [](){ return new bitpit::VTKTypes(); } ) );
	}
	std::cout << "B545_[bitpit::VTKBaseContainer] ";
	{ // bitpit::VTKBaseContainer file:VTK.hpp line:176
		pybind11::class_<bitpit::VTKBaseContainer, std::shared_ptr<bitpit::VTKBaseContainer>> cl(M("bitpit"), "VTKBaseContainer", "");
	}
	std::cout << "B546_[bitpit::VTKVectorContainer<double>] ";
	std::cout << "B547_[bitpit::VTKVectorContainer<std::array<double, 3>>] ";
	std::cout << "B548_[bitpit::VTKBaseStreamer] ";
	{ // bitpit::VTKBaseStreamer file:VTK.hpp line:205
		pybind11::class_<bitpit::VTKBaseStreamer, std::shared_ptr<bitpit::VTKBaseStreamer>, PyCallBack_bitpit_VTKBaseStreamer> cl(M("bitpit"), "VTKBaseStreamer", "");
		cl.def( pybind11::init( [](){ return new bitpit::VTKBaseStreamer(); }, [](){ return new PyCallBack_bitpit_VTKBaseStreamer(); } ) );
	}
	std::cout << "B549_[bitpit::VTKNativeStreamer] ";
	{ // bitpit::VTKNativeStreamer file:VTK.hpp line:216
		pybind11::class_<bitpit::VTKNativeStreamer, std::shared_ptr<bitpit::VTKNativeStreamer>, PyCallBack_bitpit_VTKNativeStreamer, bitpit::VTKBaseStreamer> cl(M("bitpit"), "VTKNativeStreamer", "");
	}
	std::cout << "B550_[bitpit::VTKField] ";
	{ // bitpit::VTKField file:VTK.hpp line:232
		pybind11::class_<bitpit::VTKField, std::shared_ptr<bitpit::VTKField>> cl(M("bitpit"), "VTKField", "");
	}
	std::cout << "B551_[bitpit::VTK] ";
	{ // bitpit::VTK file:VTK.hpp line:280
		pybind11::class_<bitpit::VTK, std::shared_ptr<bitpit::VTK>> cl(M("bitpit"), "VTK", "");
	}
	std::cout << "B552_[bitpit::VTKUnstructuredGrid] ";
	{ // bitpit::VTKUnstructuredGrid file:VTK.hpp line:395
		pybind11::class_<bitpit::VTKUnstructuredGrid, std::shared_ptr<bitpit::VTKUnstructuredGrid>, PyCallBack_bitpit_VTKUnstructuredGrid, bitpit::VTK> cl(M("bitpit"), "VTKUnstructuredGrid", "");
	}
	std::cout << "B553_[bitpit::VTKRectilinearGrid] ";
	{ // bitpit::VTKRectilinearGrid file:VTK.hpp line:455
		pybind11::class_<bitpit::VTKRectilinearGrid, std::shared_ptr<bitpit::VTKRectilinearGrid>, PyCallBack_bitpit_VTKRectilinearGrid, bitpit::VTK> cl(M("bitpit"), "VTKRectilinearGrid", "");
	}
}
#include <VTK.hpp>
#include <array>
#include <fstream>
#include <ios>
#include <iostream>
#include <iterator>
#include <memory>
#include <string>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_VTK_1(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B554_[unsigned char bitpit::vtk::getElementNodeCount(enum bitpit::VTKElementType)] ";
	// bitpit::vtk::getElementNodeCount(enum bitpit::VTKElementType) file:VTK.hpp line:511
	M("bitpit::vtk").def("getElementNodeCount", (unsigned char (*)(enum bitpit::VTKElementType)) &bitpit::vtk::getElementNodeCount, "C++: bitpit::vtk::getElementNodeCount(enum bitpit::VTKElementType) --> unsigned char", pybind11::arg(""));

	std::cout << "B555_[class std::__cxx11::basic_string<char> bitpit::vtk::convertDataArrayToString(const class bitpit::VTKField &)] ";
	// bitpit::vtk::convertDataArrayToString(const class bitpit::VTKField &) file:VTK.hpp line:513
	M("bitpit::vtk").def("convertDataArrayToString", (std::string (*)(const class bitpit::VTKField &)) &bitpit::vtk::convertDataArrayToString, "C++: bitpit::vtk::convertDataArrayToString(const class bitpit::VTKField &) --> std::string", pybind11::arg(""));

	std::cout << "B556_[class std::__cxx11::basic_string<char> bitpit::vtk::convertPDataArrayToString(const class bitpit::VTKField &)] ";
	// bitpit::vtk::convertPDataArrayToString(const class bitpit::VTKField &) file:VTK.hpp line:514
	M("bitpit::vtk").def("convertPDataArrayToString", (std::string (*)(const class bitpit::VTKField &)) &bitpit::vtk::convertPDataArrayToString, "C++: bitpit::vtk::convertPDataArrayToString(const class bitpit::VTKField &) --> std::string", pybind11::arg(""));

	std::cout << "B557_[bool bitpit::vtk::convertStringToDataArray(const std::string &, class bitpit::VTKField &)] ";
	// bitpit::vtk::convertStringToDataArray(const std::string &, class bitpit::VTKField &) file:VTK.hpp line:516
	M("bitpit::vtk").def("convertStringToDataArray", (bool (*)(const std::string &, class bitpit::VTKField &)) &bitpit::vtk::convertStringToDataArray, "C++: bitpit::vtk::convertStringToDataArray(const std::string &, class bitpit::VTKField &) --> bool", pybind11::arg(""), pybind11::arg(""));

	std::cout << "B558_[class std::__cxx11::basic_string<char> bitpit::vtk::convertEnumToString(enum bitpit::VTKLocation)] ";
	// bitpit::vtk::convertEnumToString(enum bitpit::VTKLocation) file:VTK.hpp line:518
	M("bitpit::vtk").def("convertEnumToString", (std::string (*)(enum bitpit::VTKLocation)) &bitpit::vtk::convertEnumToString, "C++: bitpit::vtk::convertEnumToString(enum bitpit::VTKLocation) --> std::string", pybind11::arg(""));

	std::cout << "B559_[class std::__cxx11::basic_string<char> bitpit::vtk::convertEnumToString(enum bitpit::VTKFormat)] ";
	// bitpit::vtk::convertEnumToString(enum bitpit::VTKFormat) file:VTK.hpp line:519
	M("bitpit::vtk").def("convertEnumToString", (std::string (*)(enum bitpit::VTKFormat)) &bitpit::vtk::convertEnumToString, "C++: bitpit::vtk::convertEnumToString(enum bitpit::VTKFormat) --> std::string", pybind11::arg(""));

	std::cout << "B560_[class std::__cxx11::basic_string<char> bitpit::vtk::convertEnumToString(enum bitpit::VTKDataType)] ";
	// bitpit::vtk::convertEnumToString(enum bitpit::VTKDataType) file:VTK.hpp line:520
	M("bitpit::vtk").def("convertEnumToString", (std::string (*)(enum bitpit::VTKDataType)) &bitpit::vtk::convertEnumToString, "C++: bitpit::vtk::convertEnumToString(enum bitpit::VTKDataType) --> std::string", pybind11::arg(""));

	std::cout << "B561_[bool bitpit::vtk::convertStringToEnum(const std::string &, enum bitpit::VTKLocation &)] ";
	// bitpit::vtk::convertStringToEnum(const std::string &, enum bitpit::VTKLocation &) file:VTK.hpp line:522
	M("bitpit::vtk").def("convertStringToEnum", (bool (*)(const std::string &, enum bitpit::VTKLocation &)) &bitpit::vtk::convertStringToEnum, "C++: bitpit::vtk::convertStringToEnum(const std::string &, enum bitpit::VTKLocation &) --> bool", pybind11::arg(""), pybind11::arg(""));

	std::cout << "B562_[bool bitpit::vtk::convertStringToEnum(const std::string &, enum bitpit::VTKFormat &)] ";
	// bitpit::vtk::convertStringToEnum(const std::string &, enum bitpit::VTKFormat &) file:VTK.hpp line:523
	M("bitpit::vtk").def("convertStringToEnum", (bool (*)(const std::string &, enum bitpit::VTKFormat &)) &bitpit::vtk::convertStringToEnum, "C++: bitpit::vtk::convertStringToEnum(const std::string &, enum bitpit::VTKFormat &) --> bool", pybind11::arg(""), pybind11::arg(""));

	std::cout << "B563_[bool bitpit::vtk::convertStringToEnum(const std::string &, enum bitpit::VTKDataType &)] ";
	// bitpit::vtk::convertStringToEnum(const std::string &, enum bitpit::VTKDataType &) file:VTK.hpp line:524
	M("bitpit::vtk").def("convertStringToEnum", (bool (*)(const std::string &, enum bitpit::VTKDataType &)) &bitpit::vtk::convertStringToEnum, "C++: bitpit::vtk::convertStringToEnum(const std::string &, enum bitpit::VTKDataType &) --> bool", pybind11::arg(""), pybind11::arg(""));

	std::cout << "B564_[void bitpit::vtk::allocate<std::array<double, 3>>(struct std::array<double, 3> &, int)] ";
	// bitpit::vtk::allocate(struct std::array<double, 3> &, int) file:VTK.hpp line:530
	M("bitpit::vtk").def("allocate", (void (*)(struct std::array<double, 3> &, int)) &bitpit::vtk::allocate<std::array<double, 3>>, "C++: bitpit::vtk::allocate(struct std::array<double, 3> &, int) --> void", pybind11::arg("data"), pybind11::arg("n"));

}
#include <adaption.hpp>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream> // __str__
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_adaption(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B584_[bitpit::adaption::Marker] ";
	// bitpit::adaption::Marker file:adaption.hpp line:36
	pybind11::enum_<bitpit::adaption::Marker>(M("bitpit::adaption"), "Marker", pybind11::arithmetic(), "")
		.value("MARKER_REFINE", bitpit::adaption::MARKER_REFINE)
		.value("MARKER_NONE", bitpit::adaption::MARKER_NONE)
		.value("MARKER_COARSEN", bitpit::adaption::MARKER_COARSEN)
		.value("MARKER_UNDEFINED", bitpit::adaption::MARKER_UNDEFINED)
		.export_values();

;

	std::cout << "B585_[bitpit::adaption::Type] ";
	// bitpit::adaption::Type file:adaption.hpp line:43
	pybind11::enum_<bitpit::adaption::Type>(M("bitpit::adaption"), "Type", pybind11::arithmetic(), "")
		.value("TYPE_UNKNOWN", bitpit::adaption::TYPE_UNKNOWN)
		.value("TYPE_NONE", bitpit::adaption::TYPE_NONE)
		.value("TYPE_CREATION", bitpit::adaption::TYPE_CREATION)
		.value("TYPE_DELETION", bitpit::adaption::TYPE_DELETION)
		.value("TYPE_REFINEMENT", bitpit::adaption::TYPE_REFINEMENT)
		.value("TYPE_COARSENING", bitpit::adaption::TYPE_COARSENING)
		.value("TYPE_RENUMBERING", bitpit::adaption::TYPE_RENUMBERING)
		.value("TYPE_PARTITION_SEND", bitpit::adaption::TYPE_PARTITION_SEND)
		.value("TYPE_PARTITION_RECV", bitpit::adaption::TYPE_PARTITION_RECV)
		.value("TYPE_PARTITION_NOTICE", bitpit::adaption::TYPE_PARTITION_NOTICE)
		.export_values();

;

	std::cout << "B586_[bitpit::adaption::Entity] ";
	// bitpit::adaption::Entity file:adaption.hpp line:56
	pybind11::enum_<bitpit::adaption::Entity>(M("bitpit::adaption"), "Entity", pybind11::arithmetic(), "")
		.value("ENTITY_UNKNOWN", bitpit::adaption::ENTITY_UNKNOWN)
		.value("ENTITY_CELL", bitpit::adaption::ENTITY_CELL)
		.value("ENTITY_INTERFACE", bitpit::adaption::ENTITY_INTERFACE)
		.export_values();

;

	std::cout << "B587_[bitpit::adaption::Info] ";
	{ // bitpit::adaption::Info file:adaption.hpp line:62
		pybind11::class_<bitpit::adaption::Info, std::shared_ptr<bitpit::adaption::Info>> cl(M("bitpit::adaption"), "Info", "");
		cl.def( pybind11::init( [](){ return new bitpit::adaption::Info(); } ) );
		cl.def( pybind11::init( [](enum bitpit::adaption::Type const & a0, enum bitpit::adaption::Entity const & a1){ return new bitpit::adaption::Info(a0, a1); } ), "doc" , pybind11::arg("user_type"), pybind11::arg("user_entity"));
		cl.def( pybind11::init<enum bitpit::adaption::Type, enum bitpit::adaption::Entity, int>(), pybind11::arg("user_type"), pybind11::arg("user_entity"), pybind11::arg("user_rank") );

		cl.def( pybind11::init( [](bitpit::adaption::Info const &o){ return new bitpit::adaption::Info(o); } ) );
		cl.def_readwrite("type", &bitpit::adaption::Info::type);
		cl.def_readwrite("entity", &bitpit::adaption::Info::entity);
		cl.def_readwrite("rank", &bitpit::adaption::Info::rank);
		cl.def_readwrite("previous", &bitpit::adaption::Info::previous);
		cl.def_readwrite("current", &bitpit::adaption::Info::current);
		cl.def("assign", (struct bitpit::adaption::Info & (bitpit::adaption::Info::*)(const struct bitpit::adaption::Info &)) &bitpit::adaption::Info::operator=, "C++: bitpit::adaption::Info::operator=(const struct bitpit::adaption::Info &) --> struct bitpit::adaption::Info &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B588_[bitpit::adaption::InfoCollection] ";
	{ // bitpit::adaption::InfoCollection file:adaption.hpp line:81
		pybind11::class_<bitpit::adaption::InfoCollection, std::shared_ptr<bitpit::adaption::InfoCollection>> cl(M("bitpit::adaption"), "InfoCollection", "");
		cl.def( pybind11::init( [](){ return new bitpit::adaption::InfoCollection(); } ) );
		cl.def( pybind11::init( [](bitpit::adaption::InfoCollection const &o){ return new bitpit::adaption::InfoCollection(o); } ) );
		cl.def("create", (unsigned long (bitpit::adaption::InfoCollection::*)()) &bitpit::adaption::InfoCollection::create, "C++: bitpit::adaption::InfoCollection::create() --> unsigned long");
		cl.def("create", [](bitpit::adaption::InfoCollection &o, enum bitpit::adaption::Type const & a0, enum bitpit::adaption::Entity const & a1) -> unsigned long { return o.create(a0, a1); }, "", pybind11::arg("type"), pybind11::arg("entity"));
		cl.def("create", (unsigned long (bitpit::adaption::InfoCollection::*)(enum bitpit::adaption::Type, enum bitpit::adaption::Entity, int)) &bitpit::adaption::InfoCollection::create, "C++: bitpit::adaption::InfoCollection::create(enum bitpit::adaption::Type, enum bitpit::adaption::Entity, int) --> unsigned long", pybind11::arg("type"), pybind11::arg("entity"), pybind11::arg("rank"));
		cl.def("at", (struct bitpit::adaption::Info & (bitpit::adaption::InfoCollection::*)(unsigned long)) &bitpit::adaption::InfoCollection::at, "C++: bitpit::adaption::InfoCollection::at(unsigned long) --> struct bitpit::adaption::Info &", pybind11::return_value_policy::automatic, pybind11::arg("id"));
		cl.def("data", (class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > & (bitpit::adaption::InfoCollection::*)()) &bitpit::adaption::InfoCollection::data, "C++: bitpit::adaption::InfoCollection::data() --> class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &", pybind11::return_value_policy::automatic);
		cl.def("__getitem__", (struct bitpit::adaption::Info & (bitpit::adaption::InfoCollection::*)(unsigned long)) &bitpit::adaption::InfoCollection::operator[], "C++: bitpit::adaption::InfoCollection::operator[](unsigned long) --> struct bitpit::adaption::Info &", pybind11::return_value_policy::automatic, pybind11::arg("id"));
		cl.def("dump", (class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > (bitpit::adaption::InfoCollection::*)()) &bitpit::adaption::InfoCollection::dump, "C++: bitpit::adaption::InfoCollection::dump() --> class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> >");
		cl.def("assign", (class bitpit::adaption::InfoCollection & (bitpit::adaption::InfoCollection::*)(const class bitpit::adaption::InfoCollection &)) &bitpit::adaption::InfoCollection::operator=, "C++: bitpit::adaption::InfoCollection::operator=(const class bitpit::adaption::InfoCollection &) --> class bitpit::adaption::InfoCollection &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
}
#include <VTK.hpp>
#include <adaption.hpp>
#include <array>
#include <cell.hpp>
#include <element.hpp>
#include <element_type.hpp>
#include <fstream>
#include <functional>
#include <interface.hpp>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <memory>
#include <ostream>
#include <patch_kernel.hpp>
#include <piercedStorageIterator.hpp>
#include <piercedSync.hpp>
#include <piercedVector.hpp>
#include <piercedVectorKernel.hpp>
#include <piercedVectorStorage.hpp>
#include <set>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
#include <vertex.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// bitpit::FlatMapping file:adaption.hpp line:112
struct PyCallBack_bitpit_FlatMapping : public bitpit::FlatMapping {
	using bitpit::FlatMapping::FlatMapping;

	void update(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::FlatMapping *>(this), "update");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"FlatMapping::update\"");
	}
};

// bitpit::CellFlatMapping file:adaption.hpp line:134
struct PyCallBack_bitpit_CellFlatMapping : public bitpit::CellFlatMapping {
	using bitpit::CellFlatMapping::CellFlatMapping;

	void update(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::CellFlatMapping *>(this), "update");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return CellFlatMapping::update(a0);
	}
};

// bitpit::PiercedVector file:vertex.hpp line:106
struct PyCallBack_bitpit_PiercedVector_bitpit_Vertex_long_t : public bitpit::PiercedVector<bitpit::Vertex,long> {
	using bitpit::PiercedVector<bitpit::Vertex,long>::PiercedVector;

};

void bind_adaption_1(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B589_[bitpit::FlatMapping] ";
	{ // bitpit::FlatMapping file:adaption.hpp line:112
		pybind11::class_<bitpit::FlatMapping, std::shared_ptr<bitpit::FlatMapping>, PyCallBack_bitpit_FlatMapping> cl(M("bitpit"), "FlatMapping", "");
		cl.def( pybind11::init( [](){ return new PyCallBack_bitpit_FlatMapping(); } ) );
		cl.def(pybind11::init<PyCallBack_bitpit_FlatMapping const &>());
		cl.def("update", (void (bitpit::FlatMapping::*)(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &)) &bitpit::FlatMapping::update, "C++: bitpit::FlatMapping::update(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &) --> void", pybind11::arg("adaptionData"));
		cl.def("getNumbering", (const class std::vector<long, class std::allocator<long> > & (bitpit::FlatMapping::*)() const) &bitpit::FlatMapping::getNumbering, "C++: bitpit::FlatMapping::getNumbering() const --> const class std::vector<long, class std::allocator<long> > &", pybind11::return_value_policy::automatic);
		cl.def("getMapping", (const class std::vector<long, class std::allocator<long> > & (bitpit::FlatMapping::*)() const) &bitpit::FlatMapping::getMapping, "C++: bitpit::FlatMapping::getMapping() const --> const class std::vector<long, class std::allocator<long> > &", pybind11::return_value_policy::automatic);
		cl.def("assign", (class bitpit::FlatMapping & (bitpit::FlatMapping::*)(const class bitpit::FlatMapping &)) &bitpit::FlatMapping::operator=, "C++: bitpit::FlatMapping::operator=(const class bitpit::FlatMapping &) --> class bitpit::FlatMapping &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B590_[bitpit::CellFlatMapping] ";
	{ // bitpit::CellFlatMapping file:adaption.hpp line:134
		pybind11::class_<bitpit::CellFlatMapping, std::shared_ptr<bitpit::CellFlatMapping>, PyCallBack_bitpit_CellFlatMapping, bitpit::FlatMapping> cl(M("bitpit"), "CellFlatMapping", "");
		cl.def( pybind11::init( [](){ return new bitpit::CellFlatMapping(); }, [](){ return new PyCallBack_bitpit_CellFlatMapping(); } ) );
		cl.def( pybind11::init( [](PyCallBack_bitpit_CellFlatMapping const &o){ return new PyCallBack_bitpit_CellFlatMapping(o); } ) );
		cl.def( pybind11::init( [](bitpit::CellFlatMapping const &o){ return new bitpit::CellFlatMapping(o); } ) );
		cl.def("update", (void (bitpit::CellFlatMapping::*)(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &)) &bitpit::CellFlatMapping::update, "C++: bitpit::CellFlatMapping::update(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &) --> void", pybind11::arg("adaptionData"));
		cl.def("assign", (class bitpit::CellFlatMapping & (bitpit::CellFlatMapping::*)(const class bitpit::CellFlatMapping &)) &bitpit::CellFlatMapping::operator=, "C++: bitpit::CellFlatMapping::operator=(const class bitpit::CellFlatMapping &) --> class bitpit::CellFlatMapping &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B591_[bitpit::Interface] ";
	{ // bitpit::Interface file:interface.hpp line:37
		pybind11::class_<bitpit::Interface, std::shared_ptr<bitpit::Interface>, bitpit::Element> cl(M("bitpit"), "Interface", "");
	}
	std::cout << "B592_[bitpit::PiercedVector<bitpit::Interface,long>] ";
	std::cout << "B593_[bitpit::QualifiedInterfaceHalfEdge<bitpit::Interface>] ";
	std::cout << "B594_[bitpit::QualifiedInterfaceHalfEdge<const bitpit::Interface>] ";
	std::cout << "B595_[bitpit::Vertex] ";
	{ // bitpit::Vertex file:vertex.hpp line:43
		pybind11::class_<bitpit::Vertex, std::shared_ptr<bitpit::Vertex>> cl(M("bitpit"), "Vertex", "");
		cl.def( pybind11::init( [](){ return new bitpit::Vertex(); } ) );
		cl.def( pybind11::init( [](long const & a0){ return new bitpit::Vertex(a0); } ), "doc" , pybind11::arg("id"));
		cl.def( pybind11::init<long, bool>(), pybind11::arg("id"), pybind11::arg("interior") );

		cl.def( pybind11::init( [](long const & a0, const struct std::array<double, 3> & a1){ return new bitpit::Vertex(a0, a1); } ), "doc" , pybind11::arg("id"), pybind11::arg("coords"));
		cl.def( pybind11::init<long, const struct std::array<double, 3> &, bool>(), pybind11::arg("id"), pybind11::arg("coords"), pybind11::arg("interior") );

		cl.def( pybind11::init( [](bitpit::Vertex const &o){ return new bitpit::Vertex(o); } ) );

		pybind11::enum_<bitpit::Vertex::Coordinate>(cl, "Coordinate", pybind11::arithmetic(), "")
			.value("COORD_X", bitpit::Vertex::COORD_X)
			.value("COORD_Y", bitpit::Vertex::COORD_Y)
			.value("COORD_Z", bitpit::Vertex::COORD_Z)
			.export_values();

		cl.def("assign", (class bitpit::Vertex & (bitpit::Vertex::*)(const class bitpit::Vertex &)) &bitpit::Vertex::operator=, "C++: bitpit::Vertex::operator=(const class bitpit::Vertex &) --> class bitpit::Vertex &", pybind11::return_value_policy::automatic, pybind11::arg("other"));
		cl.def("swap", (void (bitpit::Vertex::*)(class bitpit::Vertex &)) &bitpit::Vertex::swap, "C++: bitpit::Vertex::swap(class bitpit::Vertex &) --> void", pybind11::arg("other"));
		cl.def("initialize", (void (bitpit::Vertex::*)(long, const struct std::array<double, 3> &, bool)) &bitpit::Vertex::initialize, "C++: bitpit::Vertex::initialize(long, const struct std::array<double, 3> &, bool) --> void", pybind11::arg("id"), pybind11::arg("coords"), pybind11::arg("interior"));
		cl.def("isInterior", (bool (bitpit::Vertex::*)() const) &bitpit::Vertex::isInterior, "C++: bitpit::Vertex::isInterior() const --> bool");
		cl.def("__eq__", (bool (bitpit::Vertex::*)(const class bitpit::Vertex &) const) &bitpit::Vertex::operator==, "C++: bitpit::Vertex::operator==(const class bitpit::Vertex &) const --> bool", pybind11::arg("other"));
		cl.def("__getitem__", (double & (bitpit::Vertex::*)(int)) &bitpit::Vertex::operator[], "C++: bitpit::Vertex::operator[](int) --> double &", pybind11::return_value_policy::automatic, pybind11::arg("coord_id"));
		cl.def("setId", (void (bitpit::Vertex::*)(long)) &bitpit::Vertex::setId, "C++: bitpit::Vertex::setId(long) --> void", pybind11::arg("id"));
		cl.def("getId", (long (bitpit::Vertex::*)() const) &bitpit::Vertex::getId, "C++: bitpit::Vertex::getId() const --> long");
		cl.def("setCoords", (void (bitpit::Vertex::*)(const struct std::array<double, 3> &)) &bitpit::Vertex::setCoords, "C++: bitpit::Vertex::setCoords(const struct std::array<double, 3> &) --> void", pybind11::arg("coords"));
		cl.def("getCoords", (const struct std::array<double, 3> & (bitpit::Vertex::*)() const) &bitpit::Vertex::getCoords, "C++: bitpit::Vertex::getCoords() const --> const struct std::array<double, 3> &", pybind11::return_value_policy::automatic);
		cl.def("translate", (void (bitpit::Vertex::*)(const struct std::array<double, 3> &)) &bitpit::Vertex::translate, "C++: bitpit::Vertex::translate(const struct std::array<double, 3> &) --> void", pybind11::arg("translation"));
		cl.def("translate", (void (bitpit::Vertex::*)(double, double, double)) &bitpit::Vertex::translate, "C++: bitpit::Vertex::translate(double, double, double) --> void", pybind11::arg("sx"), pybind11::arg("sy"), pybind11::arg("sz"));
		cl.def("scale", (void (bitpit::Vertex::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> &)) &bitpit::Vertex::scale, "C++: bitpit::Vertex::scale(const struct std::array<double, 3> &, const struct std::array<double, 3> &) --> void", pybind11::arg("scaling"), pybind11::arg("center"));
		cl.def("scale", (void (bitpit::Vertex::*)(double, double, double, double, double, double)) &bitpit::Vertex::scale, "C++: bitpit::Vertex::scale(double, double, double, double, double, double) --> void", pybind11::arg("sx"), pybind11::arg("sy"), pybind11::arg("sz"), pybind11::arg("cx"), pybind11::arg("cy"), pybind11::arg("cz"));
		cl.def("getBinarySize", (unsigned int (bitpit::Vertex::*)() const) &bitpit::Vertex::getBinarySize, "C++: bitpit::Vertex::getBinarySize() const --> unsigned int");
		cl.def("display", (void (bitpit::Vertex::*)(std::ostream &, unsigned short) const) &bitpit::Vertex::display, "C++: bitpit::Vertex::display(std::ostream &, unsigned short) const --> void", pybind11::arg("out"), pybind11::arg("indent"));
	}
	std::cout << "B596_[bitpit::PiercedVector<bitpit::Vertex,long>] ";
	{ // bitpit::PiercedVector file:vertex.hpp line:106
		pybind11::class_<bitpit::PiercedVector<bitpit::Vertex,long>, std::shared_ptr<bitpit::PiercedVector<bitpit::Vertex,long>>, PyCallBack_bitpit_PiercedVector_bitpit_Vertex_long_t, bitpit::BasePiercedVector, bitpit::PiercedVectorKernel<long>> cl(M("bitpit"), "PiercedVector_bitpit_Vertex_long_t", "");
		cl.def( pybind11::init( [](){ return new bitpit::PiercedVector<bitpit::Vertex,long>(); }, [](){ return new PyCallBack_bitpit_PiercedVector_bitpit_Vertex_long_t(); } ) );
		cl.def( pybind11::init<unsigned long>(), pybind11::arg("n") );

		cl.def( pybind11::init( [](PyCallBack_bitpit_PiercedVector_bitpit_Vertex_long_t const &o){ return new PyCallBack_bitpit_PiercedVector_bitpit_Vertex_long_t(o); } ) );
		cl.def( pybind11::init( [](bitpit::PiercedVector<bitpit::Vertex,long> const &o){ return new bitpit::PiercedVector<bitpit::Vertex,long>(o); } ) );
		cl.def("assign", (class bitpit::PiercedVector<class bitpit::Vertex> & (bitpit::PiercedVector<bitpit::Vertex,long>::*)(class bitpit::PiercedVector<class bitpit::Vertex>)) &bitpit::PiercedVector<bitpit::Vertex, long>::operator=, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::operator=(class bitpit::PiercedVector<class bitpit::Vertex>) --> class bitpit::PiercedVector<class bitpit::Vertex> &", pybind11::return_value_policy::automatic, pybind11::arg("x"));
		cl.def("popBack", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)()) &bitpit::PiercedVector<bitpit::Vertex, long>::popBack, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::popBack() --> void");
		cl.def("swap", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)(long, long)) &bitpit::PiercedVector<bitpit::Vertex, long>::swap, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::swap(long, long) --> void", pybind11::arg("id_first"), pybind11::arg("id_second"));
		cl.def("clear", [](bitpit::PiercedVector<bitpit::Vertex,long> &o) -> void { return o.clear(); }, "");
		cl.def("clear", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)(bool)) &bitpit::PiercedVector<bitpit::Vertex, long>::clear, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::clear(bool) --> void", pybind11::arg("release"));
		cl.def("reserve", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)(unsigned long)) &bitpit::PiercedVector<bitpit::Vertex, long>::reserve, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::reserve(unsigned long) --> void", pybind11::arg("n"));
		cl.def("resize", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)(unsigned long)) &bitpit::PiercedVector<bitpit::Vertex, long>::resize, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::resize(unsigned long) --> void", pybind11::arg("n"));
		cl.def("sort", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)()) &bitpit::PiercedVector<bitpit::Vertex, long>::sort, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::sort() --> void");
		cl.def("sortAfter", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)(long, bool)) &bitpit::PiercedVector<bitpit::Vertex, long>::sortAfter, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::sortAfter(long, bool) --> void", pybind11::arg("referenceId"), pybind11::arg("inclusive"));
		cl.def("sortBefore", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)(long, bool)) &bitpit::PiercedVector<bitpit::Vertex, long>::sortBefore, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::sortBefore(long, bool) --> void", pybind11::arg("referenceId"), pybind11::arg("inclusive"));
		cl.def("squeeze", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)()) &bitpit::PiercedVector<bitpit::Vertex, long>::squeeze, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::squeeze() --> void");
		cl.def("shrinkToFit", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)()) &bitpit::PiercedVector<bitpit::Vertex, long>::shrinkToFit, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::shrinkToFit() --> void");
		cl.def("swap", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)(class bitpit::PiercedVector<class bitpit::Vertex> &)) &bitpit::PiercedVector<bitpit::Vertex, long>::swap, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::swap(class bitpit::PiercedVector<class bitpit::Vertex> &) --> void", pybind11::arg("x"));
		cl.def("getKernel", (const class bitpit::PiercedVectorKernel<long> & (bitpit::PiercedVector<bitpit::Vertex,long>::*)() const) &bitpit::PiercedVector<bitpit::Vertex, long>::getKernel, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::getKernel() const --> const class bitpit::PiercedVectorKernel<long> &", pybind11::return_value_policy::automatic);
		cl.def("dump", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)() const) &bitpit::PiercedVector<bitpit::Vertex, long>::dump, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::dump() const --> void");
		cl.def("restoreKernel", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)(std::istream &)) &bitpit::PiercedVector<bitpit::Vertex, long>::restoreKernel, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::restoreKernel(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dumpKernel", (void (bitpit::PiercedVector<bitpit::Vertex,long>::*)(std::ostream &) const) &bitpit::PiercedVector<bitpit::Vertex, long>::dumpKernel, "C++: bitpit::PiercedVector<bitpit::Vertex, long>::dumpKernel(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("assign", (class bitpit::BasePiercedVector & (bitpit::BasePiercedVector::*)(const class bitpit::BasePiercedVector &)) &bitpit::BasePiercedVector::operator=, "C++: bitpit::BasePiercedVector::operator=(const class bitpit::BasePiercedVector &) --> class bitpit::BasePiercedVector &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("dump", [](bitpit::PiercedVectorKernel<long> const &o, class std::basic_ostream<char> & a0) -> void { return o.dump(a0); }, "", pybind11::arg("stream"));
		cl.def("dump", [](bitpit::PiercedVectorKernel<long> const &o) -> void { return o.dump(); }, "");
		cl.def("restore", [](bitpit::PiercedVectorKernel<long> &o, class std::basic_istream<char> & a0) -> void { return o.restore(a0); }, "", pybind11::arg("stream"));
		cl.def("rawIndex", (unsigned long (bitpit::PiercedVectorKernel<long>::*)(long) const) &bitpit::PiercedVectorKernel<long>::rawIndex, "C++: bitpit::PiercedVectorKernel<long>::rawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("exists", (bool (bitpit::PiercedVectorKernel<long>::*)(long) const) &bitpit::PiercedVectorKernel<long>::exists, "C++: bitpit::PiercedVectorKernel<long>::exists(long) const --> bool", pybind11::arg("id"));
		cl.def("assign", (class bitpit::PiercedVectorKernel<long> & (bitpit::PiercedVectorKernel<long>::*)(const class bitpit::PiercedVectorKernel<long> &)) &bitpit::PiercedVectorKernel<long>::operator=, "C++: bitpit::PiercedVectorKernel<long>::operator=(const class bitpit::PiercedVectorKernel<long> &) --> class bitpit::PiercedVectorKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("assign", (class bitpit::BasePiercedVectorKernel & (bitpit::BasePiercedVectorKernel::*)(const class bitpit::BasePiercedVectorKernel &)) &bitpit::BasePiercedVectorKernel::operator=, "C++: bitpit::BasePiercedVectorKernel::operator=(const class bitpit::BasePiercedVectorKernel &) --> class bitpit::BasePiercedVectorKernel &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("sync", [](bitpit::PiercedKernel<long> &o) -> void { return o.sync(); }, "");
		cl.def("isSynced", [](bitpit::PiercedKernel<long> const &o) -> bool { return o.isSynced(); }, "");
		cl.def("clear", [](bitpit::PiercedKernel<long> &o) -> bitpit::PiercedKernel<long>::ClearAction { return o.clear(); }, "");
		cl.def("clear", (class bitpit::PiercedKernel<long>::ClearAction (bitpit::PiercedKernel<long>::*)(bool)) &bitpit::PiercedKernel<long>::clear, "C++: bitpit::PiercedKernel<long>::clear(bool) --> class bitpit::PiercedKernel<long>::ClearAction", pybind11::arg("release"));
		cl.def("swap", (void (bitpit::PiercedKernel<long>::*)(class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::swap, "C++: bitpit::PiercedKernel<long>::swap(class bitpit::PiercedKernel<long> &) --> void", pybind11::arg("x"));
		cl.def("flush", (void (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::flush, "C++: bitpit::PiercedKernel<long>::flush() --> void");
		cl.def("contiguous", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::contiguous, "C++: bitpit::PiercedKernel<long>::contiguous() const --> bool");
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump() const --> void");
		cl.def("empty", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::empty, "C++: bitpit::PiercedKernel<long>::empty() const --> bool");
		cl.def("isIteratorSlow", (bool (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::isIteratorSlow, "C++: bitpit::PiercedKernel<long>::isIteratorSlow() --> bool");
		cl.def("maxSize", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::maxSize, "C++: bitpit::PiercedKernel<long>::maxSize() const --> unsigned long");
		cl.def("size", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::size, "C++: bitpit::PiercedKernel<long>::size() const --> unsigned long");
		cl.def("capacity", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::capacity, "C++: bitpit::PiercedKernel<long>::capacity() const --> unsigned long");
		cl.def("contains", (bool (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::contains, "C++: bitpit::PiercedKernel<long>::contains(long) const --> bool", pybind11::arg("id"));
		cl.def("count", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::count, "C++: bitpit::PiercedKernel<long>::count(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getRawIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::getRawIndex, "C++: bitpit::PiercedKernel<long>::getRawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("evalFlatIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::evalFlatIndex, "C++: bitpit::PiercedKernel<long>::evalFlatIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getIds", [](bitpit::PiercedKernel<long> const &o) -> std::vector<long, class std::allocator<long> > { return o.getIds(); }, "");
		cl.def("getIds", (class std::vector<long, class std::allocator<long> > (bitpit::PiercedKernel<long>::*)(bool) const) &bitpit::PiercedKernel<long>::getIds, "C++: bitpit::PiercedKernel<long>::getIds(bool) const --> class std::vector<long, class std::allocator<long> >", pybind11::arg("ordered"));
		cl.def("getSizeMarker", [](bitpit::PiercedKernel<long> &o, unsigned long const & a0) -> long { return o.getSizeMarker(a0); }, "", pybind11::arg("targetSize"));
		cl.def("getSizeMarker", (long (bitpit::PiercedKernel<long>::*)(unsigned long, const long &)) &bitpit::PiercedKernel<long>::getSizeMarker, "C++: bitpit::PiercedKernel<long>::getSizeMarker(unsigned long, const long &) --> long", pybind11::arg("targetSize"), pybind11::arg("fallback"));
		cl.def("find", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(const long &) const) &bitpit::PiercedKernel<long>::find, "C++: bitpit::PiercedKernel<long>::find(const long &) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("id"));
		cl.def("rawFind", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(unsigned long) const) &bitpit::PiercedKernel<long>::rawFind, "C++: bitpit::PiercedKernel<long>::rawFind(unsigned long) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("pos"));
		cl.def("begin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::begin, "C++: bitpit::PiercedKernel<long>::begin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("end", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::end, "C++: bitpit::PiercedKernel<long>::end() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cbegin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cbegin, "C++: bitpit::PiercedKernel<long>::cbegin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cend", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cend, "C++: bitpit::PiercedKernel<long>::cend() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("checkIntegrity", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::checkIntegrity, "C++: bitpit::PiercedKernel<long>::checkIntegrity() const --> void");
		cl.def("updateId", (void (bitpit::PiercedKernel<long>::*)(const long &, const long &)) &bitpit::PiercedKernel<long>::updateId, "C++: bitpit::PiercedKernel<long>::updateId(const long &, const long &) --> void", pybind11::arg("currentId"), pybind11::arg("updatedId"));
		cl.def("back", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::back, "C++: bitpit::PiercedKernel<long>::back() const --> unsigned long");
		cl.def("front", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::front, "C++: bitpit::PiercedKernel<long>::front() const --> unsigned long");
		cl.def("restore", (void (bitpit::PiercedKernel<long>::*)(std::istream &)) &bitpit::PiercedKernel<long>::restore, "C++: bitpit::PiercedKernel<long>::restore(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)(std::ostream &) const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("assign", (class bitpit::PiercedKernel<long> & (bitpit::PiercedKernel<long>::*)(const class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::operator=, "C++: bitpit::PiercedKernel<long>::operator=(const class bitpit::PiercedKernel<long> &) --> class bitpit::PiercedKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
}
#include <array>
#include <binary_stream.hpp>
#include <element_reference.hpp>
#include <element_type.hpp>
#include <flatVector2D.hpp>
#include <ios>
#include <iostream>
#include <iterator>
#include <memory>
#include <proxyVector.hpp>
#include <sstream> // __str__
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// bitpit::BinaryStream file:binary_stream.hpp line:65
struct PyCallBack_bitpit_BinaryStream : public bitpit::BinaryStream {
	using bitpit::BinaryStream::BinaryStream;

	void setSize(unsigned long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::BinaryStream *>(this), "setSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return BinaryStream::setSize(a0);
	}
};

// bitpit::IBinaryStream file:binary_stream.hpp line:107
struct PyCallBack_bitpit_IBinaryStream : public bitpit::IBinaryStream {
	using bitpit::IBinaryStream::IBinaryStream;

	void setSize(unsigned long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::IBinaryStream *>(this), "setSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return BinaryStream::setSize(a0);
	}
};

// bitpit::OBinaryStream file:binary_stream.hpp line:125
struct PyCallBack_bitpit_OBinaryStream : public bitpit::OBinaryStream {
	using bitpit::OBinaryStream::OBinaryStream;

	void setSize(unsigned long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::OBinaryStream *>(this), "setSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return OBinaryStream::setSize(a0);
	}
};

// bitpit::ReferenceElementInfo file:element_reference.hpp line:37
struct PyCallBack_bitpit_ReferenceElementInfo : public bitpit::ReferenceElementInfo {
	using bitpit::ReferenceElementInfo::ReferenceElementInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceElementInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalSize\"");
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceElementInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalPointProjection\"");
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceElementInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalPointDistance\"");
	}
};

// bitpit::Reference3DElementInfo file:element_reference.hpp line:78
struct PyCallBack_bitpit_Reference3DElementInfo : public bitpit::Reference3DElementInfo {
	using bitpit::Reference3DElementInfo::Reference3DElementInfo;

	double evalVolume(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference3DElementInfo *>(this), "evalVolume");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Reference3DElementInfo::evalVolume\"");
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference3DElementInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return Reference3DElementInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference3DElementInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return Reference3DElementInfo::evalPointDistance(a0, a1);
	}
	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference3DElementInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalSize\"");
	}
};

// bitpit::ReferenceTetraInfo file:element_reference.hpp line:93
struct PyCallBack_bitpit_ReferenceTetraInfo : public bitpit::ReferenceTetraInfo {
	using bitpit::ReferenceTetraInfo::ReferenceTetraInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceTetraInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceTetraInfo::evalSize(a0);
	}
	double evalVolume(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceTetraInfo *>(this), "evalVolume");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceTetraInfo::evalVolume(a0);
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceTetraInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return Reference3DElementInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceTetraInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return Reference3DElementInfo::evalPointDistance(a0, a1);
	}
};

// bitpit::ReferenceVoxelInfo file:element_reference.hpp line:112
struct PyCallBack_bitpit_ReferenceVoxelInfo : public bitpit::ReferenceVoxelInfo {
	using bitpit::ReferenceVoxelInfo::ReferenceVoxelInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceVoxelInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceVoxelInfo::evalSize(a0);
	}
	double evalVolume(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceVoxelInfo *>(this), "evalVolume");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceVoxelInfo::evalVolume(a0);
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceVoxelInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return Reference3DElementInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceVoxelInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return Reference3DElementInfo::evalPointDistance(a0, a1);
	}
};

void bind_binary_stream(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B379_[bitpit::BinaryStream] ";
	{ // bitpit::BinaryStream file:binary_stream.hpp line:65
		pybind11::class_<bitpit::BinaryStream, std::shared_ptr<bitpit::BinaryStream>, PyCallBack_bitpit_BinaryStream> cl(M("bitpit"), "BinaryStream", "");
		cl.def( pybind11::init( [](PyCallBack_bitpit_BinaryStream const &o){ return new PyCallBack_bitpit_BinaryStream(o); } ) );
		cl.def( pybind11::init( [](bitpit::BinaryStream const &o){ return new bitpit::BinaryStream(o); } ) );
		cl.def("eof", (bool (bitpit::BinaryStream::*)() const) &bitpit::BinaryStream::eof, "C++: bitpit::BinaryStream::eof() const --> bool");
		cl.def("seekg", (bool (bitpit::BinaryStream::*)(unsigned long)) &bitpit::BinaryStream::seekg, "C++: bitpit::BinaryStream::seekg(unsigned long) --> bool", pybind11::arg("pos"));
		cl.def("seekg", (bool (bitpit::BinaryStream::*)(long, enum std::_Ios_Seekdir)) &bitpit::BinaryStream::seekg, "C++: bitpit::BinaryStream::seekg(long, enum std::_Ios_Seekdir) --> bool", pybind11::arg("offset"), pybind11::arg("way"));
		cl.def("data", (char * (bitpit::BinaryStream::*)()) &bitpit::BinaryStream::data, "C++: bitpit::BinaryStream::data() --> char *", pybind11::return_value_policy::automatic);
		cl.def("setSize", (void (bitpit::BinaryStream::*)(unsigned long)) &bitpit::BinaryStream::setSize, "C++: bitpit::BinaryStream::setSize(unsigned long) --> void", pybind11::arg("size"));
		cl.def("getSize", (unsigned long (bitpit::BinaryStream::*)() const) &bitpit::BinaryStream::getSize, "C++: bitpit::BinaryStream::getSize() const --> unsigned long");
		cl.def("getCapacity", (unsigned long (bitpit::BinaryStream::*)() const) &bitpit::BinaryStream::getCapacity, "C++: bitpit::BinaryStream::getCapacity() const --> unsigned long");
		cl.def("getChunkSize", (int (bitpit::BinaryStream::*)()) &bitpit::BinaryStream::getChunkSize, "C++: bitpit::BinaryStream::getChunkSize() --> int");
		cl.def("getChunkCount", (int (bitpit::BinaryStream::*)()) &bitpit::BinaryStream::getChunkCount, "C++: bitpit::BinaryStream::getChunkCount() --> int");
		cl.def("assign", (class bitpit::BinaryStream & (bitpit::BinaryStream::*)(const class bitpit::BinaryStream &)) &bitpit::BinaryStream::operator=, "C++: bitpit::BinaryStream::operator=(const class bitpit::BinaryStream &) --> class bitpit::BinaryStream &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B380_[bitpit::IBinaryStream] ";
	{ // bitpit::IBinaryStream file:binary_stream.hpp line:107
		pybind11::class_<bitpit::IBinaryStream, std::shared_ptr<bitpit::IBinaryStream>, PyCallBack_bitpit_IBinaryStream, bitpit::BinaryStream> cl(M("bitpit"), "IBinaryStream", "");
		cl.def( pybind11::init( [](){ return new bitpit::IBinaryStream(); }, [](){ return new PyCallBack_bitpit_IBinaryStream(); } ) );
		cl.def( pybind11::init<unsigned long>(), pybind11::arg("size") );



		cl.def( pybind11::init( [](PyCallBack_bitpit_IBinaryStream const &o){ return new PyCallBack_bitpit_IBinaryStream(o); } ) );
		cl.def( pybind11::init( [](bitpit::IBinaryStream const &o){ return new bitpit::IBinaryStream(o); } ) );
		cl.def("open", (void (bitpit::IBinaryStream::*)(const char *, unsigned long)) &bitpit::IBinaryStream::open, "C++: bitpit::IBinaryStream::open(const char *, unsigned long) --> void", pybind11::arg("buffer"), pybind11::arg("size"));
		cl.def("open", (void (bitpit::IBinaryStream::*)(unsigned long)) &bitpit::IBinaryStream::open, "C++: bitpit::IBinaryStream::open(unsigned long) --> void", pybind11::arg("size"));
		cl.def("read", (void (bitpit::IBinaryStream::*)(char *, unsigned long)) &bitpit::IBinaryStream::read, "C++: bitpit::IBinaryStream::read(char *, unsigned long) --> void", pybind11::arg("data"), pybind11::arg("size"));
		cl.def("assign", (class bitpit::IBinaryStream & (bitpit::IBinaryStream::*)(const class bitpit::IBinaryStream &)) &bitpit::IBinaryStream::operator=, "C++: bitpit::IBinaryStream::operator=(const class bitpit::IBinaryStream &) --> class bitpit::IBinaryStream &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B381_[bitpit::OBinaryStream] ";
	{ // bitpit::OBinaryStream file:binary_stream.hpp line:125
		pybind11::class_<bitpit::OBinaryStream, std::shared_ptr<bitpit::OBinaryStream>, PyCallBack_bitpit_OBinaryStream, bitpit::BinaryStream> cl(M("bitpit"), "OBinaryStream", "");
		cl.def( pybind11::init( [](){ return new bitpit::OBinaryStream(); }, [](){ return new PyCallBack_bitpit_OBinaryStream(); } ) );
		cl.def( pybind11::init<unsigned long>(), pybind11::arg("size") );

		cl.def( pybind11::init( [](PyCallBack_bitpit_OBinaryStream const &o){ return new PyCallBack_bitpit_OBinaryStream(o); } ) );
		cl.def( pybind11::init( [](bitpit::OBinaryStream const &o){ return new bitpit::OBinaryStream(o); } ) );
		cl.def("open", (void (bitpit::OBinaryStream::*)(unsigned long)) &bitpit::OBinaryStream::open, "C++: bitpit::OBinaryStream::open(unsigned long) --> void", pybind11::arg("size"));
		cl.def("setSize", (void (bitpit::OBinaryStream::*)(unsigned long)) &bitpit::OBinaryStream::setSize, "C++: bitpit::OBinaryStream::setSize(unsigned long) --> void", pybind11::arg("size"));
		cl.def("squeeze", (void (bitpit::OBinaryStream::*)()) &bitpit::OBinaryStream::squeeze, "C++: bitpit::OBinaryStream::squeeze() --> void");
		cl.def("write", (void (bitpit::OBinaryStream::*)(const char *, unsigned long)) &bitpit::OBinaryStream::write, "C++: bitpit::OBinaryStream::write(const char *, unsigned long) --> void", pybind11::arg("data"), pybind11::arg("size"));
		cl.def("assign", (class bitpit::OBinaryStream & (bitpit::OBinaryStream::*)(const class bitpit::OBinaryStream &)) &bitpit::OBinaryStream::operator=, "C++: bitpit::OBinaryStream::operator=(const class bitpit::OBinaryStream &) --> class bitpit::OBinaryStream &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B382_[bitpit::FlatVector2D<long>] ";
	std::cout << "B383_[bitpit::ProxyVector<const long>] ";
	{ // bitpit::ProxyVector file:proxyVector.hpp line:134
		pybind11::class_<bitpit::ProxyVector<const long>, std::shared_ptr<bitpit::ProxyVector<const long>>> cl(M("bitpit"), "ProxyVector_const_long_t", "");
		cl.def( pybind11::init( [](){ return new bitpit::ProxyVector<const long>(); } ) );

		cl.def( pybind11::init( [](bitpit::ProxyVector<const long> const &o){ return new bitpit::ProxyVector<const long>(o); } ) );
		cl.def("assign", (class bitpit::ProxyVector<const long> & (bitpit::ProxyVector<const long>::*)(const class bitpit::ProxyVector<const long> &)) &bitpit::ProxyVector<const long>::operator=, "C++: bitpit::ProxyVector<const long>::operator=(const class bitpit::ProxyVector<const long> &) --> class bitpit::ProxyVector<const long> &", pybind11::return_value_policy::automatic, pybind11::arg("other"));
		cl.def("set", (void (bitpit::ProxyVector<const long>::*)(const long *, unsigned long)) &bitpit::ProxyVector<const long>::set, "C++: bitpit::ProxyVector<const long>::set(const long *, unsigned long) --> void", pybind11::arg("data"), pybind11::arg("size"));
		cl.def("clear", (void (bitpit::ProxyVector<const long>::*)()) &bitpit::ProxyVector<const long>::clear, "C++: bitpit::ProxyVector<const long>::clear() --> void");
		cl.def("swap", (void (bitpit::ProxyVector<const long>::*)(class bitpit::ProxyVector<const long> &)) &bitpit::ProxyVector<const long>::swap, "C++: bitpit::ProxyVector<const long>::swap(class bitpit::ProxyVector<const long> &) --> void", pybind11::arg("other"));
		cl.def("empty", (bool (bitpit::ProxyVector<const long>::*)() const) &bitpit::ProxyVector<const long>::empty, "C++: bitpit::ProxyVector<const long>::empty() const --> bool");
		cl.def("size", (unsigned long (bitpit::ProxyVector<const long>::*)() const) &bitpit::ProxyVector<const long>::size, "C++: bitpit::ProxyVector<const long>::size() const --> unsigned long");
		cl.def("__eq__", (bool (bitpit::ProxyVector<const long>::*)(const class bitpit::ProxyVector<const long> &) const) &bitpit::ProxyVector<const long>::operator==, "C++: bitpit::ProxyVector<const long>::operator==(const class bitpit::ProxyVector<const long> &) const --> bool", pybind11::arg("rhs"));
		cl.def("data", (const long * (bitpit::ProxyVector<const long>::*)()) &bitpit::ProxyVector<const long>::data, "C++: bitpit::ProxyVector<const long>::data() --> const long *", pybind11::return_value_policy::automatic);
		cl.def("__getitem__", (const long & (bitpit::ProxyVector<const long>::*)(unsigned long)) &bitpit::ProxyVector<const long>::operator[], "C++: bitpit::ProxyVector<const long>::operator[](unsigned long) --> const long &", pybind11::return_value_policy::automatic, pybind11::arg("n"));
		cl.def("at", (const long & (bitpit::ProxyVector<const long>::*)(unsigned long)) &bitpit::ProxyVector<const long>::at, "C++: bitpit::ProxyVector<const long>::at(unsigned long) --> const long &", pybind11::return_value_policy::automatic, pybind11::arg("n"));
		cl.def("front", (const long & (bitpit::ProxyVector<const long>::*)()) &bitpit::ProxyVector<const long>::front, "C++: bitpit::ProxyVector<const long>::front() --> const long &", pybind11::return_value_policy::automatic);
		cl.def("back", (const long & (bitpit::ProxyVector<const long>::*)()) &bitpit::ProxyVector<const long>::back, "C++: bitpit::ProxyVector<const long>::back() --> const long &", pybind11::return_value_policy::automatic);
	}
	std::cout << "B384_[bitpit::ProxyVector<int>] ";
	std::cout << "B385_[bitpit::ProxyVector<long>] ";
	std::cout << "B386_[bitpit::ProxyVector<double>] ";
	std::cout << "B387_[bitpit::ElementType] ";
	// bitpit::ElementType file:element_type.hpp line:30
	pybind11::enum_<bitpit::ElementType>(M("bitpit"), "ElementType", pybind11::arithmetic(), "")
		.value("UNDEFINED", bitpit::UNDEFINED)
		.value("VERTEX", bitpit::VERTEX)
		.value("LINE", bitpit::LINE)
		.value("TRIANGLE", bitpit::TRIANGLE)
		.value("POLYGON", bitpit::POLYGON)
		.value("PIXEL", bitpit::PIXEL)
		.value("QUAD", bitpit::QUAD)
		.value("TETRA", bitpit::TETRA)
		.value("VOXEL", bitpit::VOXEL)
		.value("HEXAHEDRON", bitpit::HEXAHEDRON)
		.value("WEDGE", bitpit::WEDGE)
		.value("PYRAMID", bitpit::PYRAMID)
		.value("POLYHEDRON", bitpit::POLYHEDRON)
		.export_values();

;

	std::cout << "B388_[bitpit::ReferenceElementInfo] ";
	{ // bitpit::ReferenceElementInfo file:element_reference.hpp line:37
		pybind11::class_<bitpit::ReferenceElementInfo, std::shared_ptr<bitpit::ReferenceElementInfo>, PyCallBack_bitpit_ReferenceElementInfo> cl(M("bitpit"), "ReferenceElementInfo", "");
		cl.def_readwrite("dimension", &bitpit::ReferenceElementInfo::dimension);
		cl.def_readwrite("type", &bitpit::ReferenceElementInfo::type);
		cl.def_readwrite("nVertices", &bitpit::ReferenceElementInfo::nVertices);
		cl.def_readwrite("nFaces", &bitpit::ReferenceElementInfo::nFaces);
		cl.def_readwrite("nEdges", &bitpit::ReferenceElementInfo::nEdges);
		cl.def_readwrite("faceTypeStorage", &bitpit::ReferenceElementInfo::faceTypeStorage);
		cl.def_readwrite("faceConnectStorage", &bitpit::ReferenceElementInfo::faceConnectStorage);
		cl.def_readwrite("faceEdgeStorage", &bitpit::ReferenceElementInfo::faceEdgeStorage);
		cl.def_readwrite("edgeTypeStorage", &bitpit::ReferenceElementInfo::edgeTypeStorage);
		cl.def_readwrite("edgeConnectStorage", &bitpit::ReferenceElementInfo::edgeConnectStorage);
		cl.def("evalSize", (double (bitpit::ReferenceElementInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceElementInfo::evalSize, "C++: bitpit::ReferenceElementInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalPointProjection", (void (bitpit::ReferenceElementInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const) &bitpit::ReferenceElementInfo::evalPointProjection, "C++: bitpit::ReferenceElementInfo::evalPointProjection(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const --> void", pybind11::arg("point"), pybind11::arg("vertexCoords"), pybind11::arg("projection"), pybind11::arg("distance"));
		cl.def("evalPointDistance", (double (bitpit::ReferenceElementInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const) &bitpit::ReferenceElementInfo::evalPointDistance, "C++: bitpit::ReferenceElementInfo::evalPointDistance(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const --> double", pybind11::arg("point"), pybind11::arg("vertexCoords"));
	}
	std::cout << "B389_[bitpit::Reference3DElementInfo] ";
	{ // bitpit::Reference3DElementInfo file:element_reference.hpp line:78
		pybind11::class_<bitpit::Reference3DElementInfo, std::shared_ptr<bitpit::Reference3DElementInfo>, PyCallBack_bitpit_Reference3DElementInfo, bitpit::ReferenceElementInfo> cl(M("bitpit"), "Reference3DElementInfo", "");
		cl.def("evalVolume", (double (bitpit::Reference3DElementInfo::*)(const struct std::array<double, 3> *) const) &bitpit::Reference3DElementInfo::evalVolume, "C++: bitpit::Reference3DElementInfo::evalVolume(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalSurfaceArea", (double (bitpit::Reference3DElementInfo::*)(const struct std::array<double, 3> *) const) &bitpit::Reference3DElementInfo::evalSurfaceArea, "C++: bitpit::Reference3DElementInfo::evalSurfaceArea(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalEdgePerimeter", (double (bitpit::Reference3DElementInfo::*)(const struct std::array<double, 3> *) const) &bitpit::Reference3DElementInfo::evalEdgePerimeter, "C++: bitpit::Reference3DElementInfo::evalEdgePerimeter(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalPointProjection", (void (bitpit::Reference3DElementInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const) &bitpit::Reference3DElementInfo::evalPointProjection, "C++: bitpit::Reference3DElementInfo::evalPointProjection(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const --> void", pybind11::arg("point"), pybind11::arg("vertexCoords"), pybind11::arg("projection"), pybind11::arg("distance"));
		cl.def("evalPointDistance", (double (bitpit::Reference3DElementInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const) &bitpit::Reference3DElementInfo::evalPointDistance, "C++: bitpit::Reference3DElementInfo::evalPointDistance(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const --> double", pybind11::arg("point"), pybind11::arg("vertexCoords"));
	}
	std::cout << "B390_[bitpit::ReferenceTetraInfo] ";
	{ // bitpit::ReferenceTetraInfo file:element_reference.hpp line:93
		pybind11::class_<bitpit::ReferenceTetraInfo, std::shared_ptr<bitpit::ReferenceTetraInfo>, PyCallBack_bitpit_ReferenceTetraInfo, bitpit::Reference3DElementInfo> cl(M("bitpit"), "ReferenceTetraInfo", "");
		cl.def("evalSize", (double (bitpit::ReferenceTetraInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceTetraInfo::evalSize, "C++: bitpit::ReferenceTetraInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalVolume", (double (bitpit::ReferenceTetraInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceTetraInfo::evalVolume, "C++: bitpit::ReferenceTetraInfo::evalVolume(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
	}
	std::cout << "B391_[bitpit::ReferenceVoxelInfo] ";
	{ // bitpit::ReferenceVoxelInfo file:element_reference.hpp line:112
		pybind11::class_<bitpit::ReferenceVoxelInfo, std::shared_ptr<bitpit::ReferenceVoxelInfo>, PyCallBack_bitpit_ReferenceVoxelInfo, bitpit::Reference3DElementInfo> cl(M("bitpit"), "ReferenceVoxelInfo", "");
		cl.def("evalSize", (double (bitpit::ReferenceVoxelInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceVoxelInfo::evalSize, "C++: bitpit::ReferenceVoxelInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalVolume", (double (bitpit::ReferenceVoxelInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceVoxelInfo::evalVolume, "C++: bitpit::ReferenceVoxelInfo::evalVolume(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
	}
}
#include <cell.hpp>
#include <element.hpp>
#include <element_type.hpp>
#include <ios>
#include <iostream>
#include <istream>
#include <ostream>
#include <piercedSync.hpp>
#include <piercedVector.hpp>
#include <piercedVectorKernel.hpp>
#include <piercedVectorStorage.hpp>
#include <sstream> // __str__
#include <streambuf>
#include <string>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// bitpit::PiercedVector file:cell.hpp line:152
struct PyCallBack_bitpit_PiercedVector_bitpit_Cell_long_t : public bitpit::PiercedVector<bitpit::Cell,long> {
	using bitpit::PiercedVector<bitpit::Cell,long>::PiercedVector;

};

void bind_cell(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B416_[bitpit::PiercedVector<bitpit::Cell,long>] ";
	{ // bitpit::PiercedVector file:cell.hpp line:152
		pybind11::class_<bitpit::PiercedVector<bitpit::Cell,long>, std::shared_ptr<bitpit::PiercedVector<bitpit::Cell,long>>, PyCallBack_bitpit_PiercedVector_bitpit_Cell_long_t, bitpit::BasePiercedVector, bitpit::PiercedVectorKernel<long>> cl(M("bitpit"), "PiercedVector_bitpit_Cell_long_t", "");
		cl.def( pybind11::init( [](){ return new bitpit::PiercedVector<bitpit::Cell,long>(); }, [](){ return new PyCallBack_bitpit_PiercedVector_bitpit_Cell_long_t(); } ) );
		cl.def( pybind11::init<unsigned long>(), pybind11::arg("n") );

		cl.def( pybind11::init( [](PyCallBack_bitpit_PiercedVector_bitpit_Cell_long_t const &o){ return new PyCallBack_bitpit_PiercedVector_bitpit_Cell_long_t(o); } ) );
		cl.def( pybind11::init( [](bitpit::PiercedVector<bitpit::Cell,long> const &o){ return new bitpit::PiercedVector<bitpit::Cell,long>(o); } ) );
		cl.def("assign", (class bitpit::PiercedVector<class bitpit::Cell> & (bitpit::PiercedVector<bitpit::Cell,long>::*)(class bitpit::PiercedVector<class bitpit::Cell>)) &bitpit::PiercedVector<bitpit::Cell, long>::operator=, "C++: bitpit::PiercedVector<bitpit::Cell, long>::operator=(class bitpit::PiercedVector<class bitpit::Cell>) --> class bitpit::PiercedVector<class bitpit::Cell> &", pybind11::return_value_policy::automatic, pybind11::arg("x"));
		cl.def("popBack", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)()) &bitpit::PiercedVector<bitpit::Cell, long>::popBack, "C++: bitpit::PiercedVector<bitpit::Cell, long>::popBack() --> void");
		cl.def("swap", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)(long, long)) &bitpit::PiercedVector<bitpit::Cell, long>::swap, "C++: bitpit::PiercedVector<bitpit::Cell, long>::swap(long, long) --> void", pybind11::arg("id_first"), pybind11::arg("id_second"));
		cl.def("clear", [](bitpit::PiercedVector<bitpit::Cell,long> &o) -> void { return o.clear(); }, "");
		cl.def("clear", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)(bool)) &bitpit::PiercedVector<bitpit::Cell, long>::clear, "C++: bitpit::PiercedVector<bitpit::Cell, long>::clear(bool) --> void", pybind11::arg("release"));
		cl.def("reserve", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)(unsigned long)) &bitpit::PiercedVector<bitpit::Cell, long>::reserve, "C++: bitpit::PiercedVector<bitpit::Cell, long>::reserve(unsigned long) --> void", pybind11::arg("n"));
		cl.def("resize", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)(unsigned long)) &bitpit::PiercedVector<bitpit::Cell, long>::resize, "C++: bitpit::PiercedVector<bitpit::Cell, long>::resize(unsigned long) --> void", pybind11::arg("n"));
		cl.def("sort", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)()) &bitpit::PiercedVector<bitpit::Cell, long>::sort, "C++: bitpit::PiercedVector<bitpit::Cell, long>::sort() --> void");
		cl.def("sortAfter", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)(long, bool)) &bitpit::PiercedVector<bitpit::Cell, long>::sortAfter, "C++: bitpit::PiercedVector<bitpit::Cell, long>::sortAfter(long, bool) --> void", pybind11::arg("referenceId"), pybind11::arg("inclusive"));
		cl.def("sortBefore", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)(long, bool)) &bitpit::PiercedVector<bitpit::Cell, long>::sortBefore, "C++: bitpit::PiercedVector<bitpit::Cell, long>::sortBefore(long, bool) --> void", pybind11::arg("referenceId"), pybind11::arg("inclusive"));
		cl.def("squeeze", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)()) &bitpit::PiercedVector<bitpit::Cell, long>::squeeze, "C++: bitpit::PiercedVector<bitpit::Cell, long>::squeeze() --> void");
		cl.def("shrinkToFit", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)()) &bitpit::PiercedVector<bitpit::Cell, long>::shrinkToFit, "C++: bitpit::PiercedVector<bitpit::Cell, long>::shrinkToFit() --> void");
		cl.def("swap", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)(class bitpit::PiercedVector<class bitpit::Cell> &)) &bitpit::PiercedVector<bitpit::Cell, long>::swap, "C++: bitpit::PiercedVector<bitpit::Cell, long>::swap(class bitpit::PiercedVector<class bitpit::Cell> &) --> void", pybind11::arg("x"));
		cl.def("getKernel", (const class bitpit::PiercedVectorKernel<long> & (bitpit::PiercedVector<bitpit::Cell,long>::*)() const) &bitpit::PiercedVector<bitpit::Cell, long>::getKernel, "C++: bitpit::PiercedVector<bitpit::Cell, long>::getKernel() const --> const class bitpit::PiercedVectorKernel<long> &", pybind11::return_value_policy::automatic);
		cl.def("dump", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)() const) &bitpit::PiercedVector<bitpit::Cell, long>::dump, "C++: bitpit::PiercedVector<bitpit::Cell, long>::dump() const --> void");
		cl.def("restoreKernel", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)(std::istream &)) &bitpit::PiercedVector<bitpit::Cell, long>::restoreKernel, "C++: bitpit::PiercedVector<bitpit::Cell, long>::restoreKernel(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dumpKernel", (void (bitpit::PiercedVector<bitpit::Cell,long>::*)(std::ostream &) const) &bitpit::PiercedVector<bitpit::Cell, long>::dumpKernel, "C++: bitpit::PiercedVector<bitpit::Cell, long>::dumpKernel(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("assign", (class bitpit::BasePiercedVector & (bitpit::BasePiercedVector::*)(const class bitpit::BasePiercedVector &)) &bitpit::BasePiercedVector::operator=, "C++: bitpit::BasePiercedVector::operator=(const class bitpit::BasePiercedVector &) --> class bitpit::BasePiercedVector &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("dump", [](bitpit::PiercedVectorKernel<long> const &o, class std::basic_ostream<char> & a0) -> void { return o.dump(a0); }, "", pybind11::arg("stream"));
		cl.def("dump", [](bitpit::PiercedVectorKernel<long> const &o) -> void { return o.dump(); }, "");
		cl.def("restore", [](bitpit::PiercedVectorKernel<long> &o, class std::basic_istream<char> & a0) -> void { return o.restore(a0); }, "", pybind11::arg("stream"));
		cl.def("rawIndex", (unsigned long (bitpit::PiercedVectorKernel<long>::*)(long) const) &bitpit::PiercedVectorKernel<long>::rawIndex, "C++: bitpit::PiercedVectorKernel<long>::rawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("exists", (bool (bitpit::PiercedVectorKernel<long>::*)(long) const) &bitpit::PiercedVectorKernel<long>::exists, "C++: bitpit::PiercedVectorKernel<long>::exists(long) const --> bool", pybind11::arg("id"));
		cl.def("assign", (class bitpit::PiercedVectorKernel<long> & (bitpit::PiercedVectorKernel<long>::*)(const class bitpit::PiercedVectorKernel<long> &)) &bitpit::PiercedVectorKernel<long>::operator=, "C++: bitpit::PiercedVectorKernel<long>::operator=(const class bitpit::PiercedVectorKernel<long> &) --> class bitpit::PiercedVectorKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("assign", (class bitpit::BasePiercedVectorKernel & (bitpit::BasePiercedVectorKernel::*)(const class bitpit::BasePiercedVectorKernel &)) &bitpit::BasePiercedVectorKernel::operator=, "C++: bitpit::BasePiercedVectorKernel::operator=(const class bitpit::BasePiercedVectorKernel &) --> class bitpit::BasePiercedVectorKernel &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("sync", [](bitpit::PiercedKernel<long> &o) -> void { return o.sync(); }, "");
		cl.def("isSynced", [](bitpit::PiercedKernel<long> const &o) -> bool { return o.isSynced(); }, "");
		cl.def("clear", [](bitpit::PiercedKernel<long> &o) -> bitpit::PiercedKernel<long>::ClearAction { return o.clear(); }, "");
		cl.def("clear", (class bitpit::PiercedKernel<long>::ClearAction (bitpit::PiercedKernel<long>::*)(bool)) &bitpit::PiercedKernel<long>::clear, "C++: bitpit::PiercedKernel<long>::clear(bool) --> class bitpit::PiercedKernel<long>::ClearAction", pybind11::arg("release"));
		cl.def("swap", (void (bitpit::PiercedKernel<long>::*)(class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::swap, "C++: bitpit::PiercedKernel<long>::swap(class bitpit::PiercedKernel<long> &) --> void", pybind11::arg("x"));
		cl.def("flush", (void (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::flush, "C++: bitpit::PiercedKernel<long>::flush() --> void");
		cl.def("contiguous", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::contiguous, "C++: bitpit::PiercedKernel<long>::contiguous() const --> bool");
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump() const --> void");
		cl.def("empty", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::empty, "C++: bitpit::PiercedKernel<long>::empty() const --> bool");
		cl.def("isIteratorSlow", (bool (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::isIteratorSlow, "C++: bitpit::PiercedKernel<long>::isIteratorSlow() --> bool");
		cl.def("maxSize", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::maxSize, "C++: bitpit::PiercedKernel<long>::maxSize() const --> unsigned long");
		cl.def("size", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::size, "C++: bitpit::PiercedKernel<long>::size() const --> unsigned long");
		cl.def("capacity", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::capacity, "C++: bitpit::PiercedKernel<long>::capacity() const --> unsigned long");
		cl.def("contains", (bool (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::contains, "C++: bitpit::PiercedKernel<long>::contains(long) const --> bool", pybind11::arg("id"));
		cl.def("count", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::count, "C++: bitpit::PiercedKernel<long>::count(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getRawIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::getRawIndex, "C++: bitpit::PiercedKernel<long>::getRawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("evalFlatIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::evalFlatIndex, "C++: bitpit::PiercedKernel<long>::evalFlatIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getIds", [](bitpit::PiercedKernel<long> const &o) -> std::vector<long, class std::allocator<long> > { return o.getIds(); }, "");
		cl.def("getIds", (class std::vector<long, class std::allocator<long> > (bitpit::PiercedKernel<long>::*)(bool) const) &bitpit::PiercedKernel<long>::getIds, "C++: bitpit::PiercedKernel<long>::getIds(bool) const --> class std::vector<long, class std::allocator<long> >", pybind11::arg("ordered"));
		cl.def("getSizeMarker", [](bitpit::PiercedKernel<long> &o, unsigned long const & a0) -> long { return o.getSizeMarker(a0); }, "", pybind11::arg("targetSize"));
		cl.def("getSizeMarker", (long (bitpit::PiercedKernel<long>::*)(unsigned long, const long &)) &bitpit::PiercedKernel<long>::getSizeMarker, "C++: bitpit::PiercedKernel<long>::getSizeMarker(unsigned long, const long &) --> long", pybind11::arg("targetSize"), pybind11::arg("fallback"));
		cl.def("find", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(const long &) const) &bitpit::PiercedKernel<long>::find, "C++: bitpit::PiercedKernel<long>::find(const long &) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("id"));
		cl.def("rawFind", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(unsigned long) const) &bitpit::PiercedKernel<long>::rawFind, "C++: bitpit::PiercedKernel<long>::rawFind(unsigned long) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("pos"));
		cl.def("begin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::begin, "C++: bitpit::PiercedKernel<long>::begin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("end", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::end, "C++: bitpit::PiercedKernel<long>::end() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cbegin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cbegin, "C++: bitpit::PiercedKernel<long>::cbegin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cend", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cend, "C++: bitpit::PiercedKernel<long>::cend() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("checkIntegrity", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::checkIntegrity, "C++: bitpit::PiercedKernel<long>::checkIntegrity() const --> void");
		cl.def("updateId", (void (bitpit::PiercedKernel<long>::*)(const long &, const long &)) &bitpit::PiercedKernel<long>::updateId, "C++: bitpit::PiercedKernel<long>::updateId(const long &, const long &) --> void", pybind11::arg("currentId"), pybind11::arg("updatedId"));
		cl.def("back", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::back, "C++: bitpit::PiercedKernel<long>::back() const --> unsigned long");
		cl.def("front", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::front, "C++: bitpit::PiercedKernel<long>::front() const --> unsigned long");
		cl.def("restore", (void (bitpit::PiercedKernel<long>::*)(std::istream &)) &bitpit::PiercedKernel<long>::restore, "C++: bitpit::PiercedKernel<long>::restore(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)(std::ostream &) const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("assign", (class bitpit::PiercedKernel<long> & (bitpit::PiercedKernel<long>::*)(const class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::operator=, "C++: bitpit::PiercedKernel<long>::operator=(const class bitpit::PiercedKernel<long> &) --> class bitpit::PiercedKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B417_[bitpit::QualifiedCellHalfEdge<bitpit::Cell>] ";
	std::cout << "B418_[bitpit::QualifiedCellHalfEdge<const bitpit::Cell>] ";
	std::cout << "B419_[bitpit::QualifiedCellHalfFace<bitpit::Cell>] ";
	std::cout << "B420_[bitpit::QualifiedCellHalfFace<const bitpit::Cell>] ";
}
#include <common.hpp>
#include <iostream>
#include <sstream> // __str__

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_common(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B373_[neos::lapType] ";
	// neos::lapType file:common.hpp line:33
	pybind11::enum_<neos::lapType>(M("neos"), "lapType", pybind11::arithmetic(), "enum class for laplacian algo type ")
		.value("FINITEVOLUME", neos::FINITEVOLUME)
		.value("FINITEDIFF", neos::FINITEDIFF)
		.export_values();

;

	std::cout << "B374_[neos::solverType] ";
	// neos::solverType file:common.hpp line:39
	pybind11::enum_<neos::solverType>(M("neos"), "solverType", pybind11::arithmetic(), "enum class for solver selection (for laplacian) ")
		.value("PETSC", neos::PETSC)
		.value("OTHER", neos::OTHER)
		.export_values();

;

	std::cout << "B375_[neos::BCPosition] ";
	{ // neos::BCPosition file:common.hpp line:49
		pybind11::class_<neos::BCPosition, std::shared_ptr<neos::BCPosition>> cl(M("neos"), "BCPosition", "");
		cl.def( pybind11::init( [](){ return new neos::BCPosition(); } ) );
	}
	std::cout << "B376_[neos::interpoType] ";
	// neos::interpoType file:common.hpp line:56
	pybind11::enum_<neos::interpoType>(M("neos"), "interpoType", pybind11::arithmetic(), "enum class for Interpolation algo type ")
		.value("DISTANCEWEIGHTED", neos::DISTANCEWEIGHTED)
		.value("POLY2D", neos::POLY2D)
		.value("POLY3D", neos::POLY3D)
		.value("RBF", neos::RBF)
		.value("MLS", neos::MLS)
		.export_values();

;

	std::cout << "B377_[neos::Var] ";
	// neos::Var file:common.hpp line:64
	pybind11::enum_<neos::Var>(M("neos"), "Var", pybind11::arithmetic(), "")
		.value("Ux", neos::Ux)
		.value("Uy", neos::Uy)
		.value("Uz", neos::Uz)
		.value("P", neos::P)
		.value("LS", neos::LS)
		.export_values();

;

	std::cout << "B378_[neos::RHSOpType] ";
	// neos::RHSOpType file:common.hpp line:72
	pybind11::enum_<neos::RHSOpType>(M("neos"), "RHSOpType", pybind11::arithmetic(), "")
		.value("ADD", neos::ADD)
		.value("SET", neos::SET)
		.export_values();

;

}
#include <communications_tags.hpp>
#include <iostream>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_communications_tags(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B569_[class bitpit::CommunicationTags & bitpit::communications::tags()] ";
	// bitpit::communications::tags() file:communications_tags.hpp line:70
	M("bitpit::communications").def("tags", (class bitpit::CommunicationTags & (*)()) &bitpit::communications::tags, "C++: bitpit::communications::tags() --> class bitpit::CommunicationTags &", pybind11::return_value_policy::automatic);

}
#include <configuration.hpp>
#include <configuration_config.hpp>
#include <configuration_config.tpp>
#include <ios>
#include <iostream>
#include <iterator>
#include <memory>
#include <ostream>
#include <sstream> // __str__
#include <streambuf>
#include <string>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_configuration_config(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B530_[bitpit::Config] ";
	{ // bitpit::Config file:configuration_config.hpp line:34
		pybind11::class_<bitpit::Config, std::shared_ptr<bitpit::Config>> cl(M("bitpit"), "Config", "");
		cl.def( pybind11::init( [](){ return new bitpit::Config(); } ), "doc" );
		cl.def( pybind11::init<bool>(), pybind11::arg("multiSections") );

		cl.def( pybind11::init( [](bitpit::Config const &o){ return new bitpit::Config(o); } ) );
		cl.def("set", (void (bitpit::Config::*)(const std::string &, const std::string &)) &bitpit::Config::set<std::string>, "C++: bitpit::Config::set(const std::string &, const std::string &) --> void", pybind11::arg("key"), pybind11::arg("value"));
		cl.def("assign", (class bitpit::Config & (bitpit::Config::*)(class bitpit::Config)) &bitpit::Config::operator=, "C++: bitpit::Config::operator=(class bitpit::Config) --> class bitpit::Config &", pybind11::return_value_policy::automatic, pybind11::arg("other"));
		cl.def("swap", (void (bitpit::Config::*)(class bitpit::Config &)) &bitpit::Config::swap, "C++: bitpit::Config::swap(class bitpit::Config &) --> void", pybind11::arg("other"));
		cl.def("isMultiSectionsEnabled", (bool (bitpit::Config::*)() const) &bitpit::Config::isMultiSectionsEnabled, "C++: bitpit::Config::isMultiSectionsEnabled() const --> bool");
		cl.def("getOptionCount", (int (bitpit::Config::*)() const) &bitpit::Config::getOptionCount, "C++: bitpit::Config::getOptionCount() const --> int");
		cl.def("hasOption", (bool (bitpit::Config::*)(const std::string &) const) &bitpit::Config::hasOption, "C++: bitpit::Config::hasOption(const std::string &) const --> bool", pybind11::arg("key"));
		cl.def("get", (std::string (bitpit::Config::*)(const std::string &) const) &bitpit::Config::get, "C++: bitpit::Config::get(const std::string &) const --> std::string", pybind11::arg("key"));
		cl.def("get", (std::string (bitpit::Config::*)(const std::string &, const std::string &) const) &bitpit::Config::get, "C++: bitpit::Config::get(const std::string &, const std::string &) const --> std::string", pybind11::arg("key"), pybind11::arg("fallback"));
		cl.def("set", (void (bitpit::Config::*)(const std::string &, const std::string &)) &bitpit::Config::set, "C++: bitpit::Config::set(const std::string &, const std::string &) --> void", pybind11::arg("key"), pybind11::arg("value"));
		cl.def("removeOption", (bool (bitpit::Config::*)(const std::string &)) &bitpit::Config::removeOption, "C++: bitpit::Config::removeOption(const std::string &) --> bool", pybind11::arg("key"));
		cl.def("getSectionCount", (int (bitpit::Config::*)() const) &bitpit::Config::getSectionCount, "C++: bitpit::Config::getSectionCount() const --> int");
		cl.def("getSectionCount", (int (bitpit::Config::*)(const std::string &) const) &bitpit::Config::getSectionCount, "C++: bitpit::Config::getSectionCount(const std::string &) const --> int", pybind11::arg("key"));
		cl.def("hasSection", (bool (bitpit::Config::*)(const std::string &) const) &bitpit::Config::hasSection, "C++: bitpit::Config::hasSection(const std::string &) const --> bool", pybind11::arg("key"));
		cl.def("getSection", (class bitpit::Config & (bitpit::Config::*)(const std::string &)) &bitpit::Config::getSection, "C++: bitpit::Config::getSection(const std::string &) --> class bitpit::Config &", pybind11::return_value_policy::automatic, pybind11::arg("key"));
		cl.def("addSection", (class bitpit::Config & (bitpit::Config::*)(const std::string &)) &bitpit::Config::addSection, "C++: bitpit::Config::addSection(const std::string &) --> class bitpit::Config &", pybind11::return_value_policy::automatic, pybind11::arg("key"));
		cl.def("removeSection", (bool (bitpit::Config::*)(const std::string &)) &bitpit::Config::removeSection, "C++: bitpit::Config::removeSection(const std::string &) --> bool", pybind11::arg("key"));
		cl.def("clear", (void (bitpit::Config::*)()) &bitpit::Config::clear, "C++: bitpit::Config::clear() --> void");
		cl.def("dump", [](bitpit::Config const &o, class std::basic_ostream<char> & a0) -> void { return o.dump(a0); }, "", pybind11::arg("out"));
		cl.def("dump", (void (bitpit::Config::*)(std::ostream &, int) const) &bitpit::Config::dump, "C++: bitpit::Config::dump(std::ostream &, int) const --> void", pybind11::arg("out"), pybind11::arg("indentLevel"));
		cl.def("__getitem__", (class bitpit::Config & (bitpit::Config::*)(const std::string &)) &bitpit::Config::operator[], "C++: bitpit::Config::operator[](const std::string &) --> class bitpit::Config &", pybind11::return_value_policy::automatic, pybind11::arg("key"));
	}
	std::cout << "B531_[bitpit::ConfigParser] ";
	{ // bitpit::ConfigParser file:configuration.hpp line:35
		pybind11::class_<bitpit::ConfigParser, std::shared_ptr<bitpit::ConfigParser>, bitpit::Config> cl(M("bitpit"), "ConfigParser", "");




		cl.def( pybind11::init( [](bitpit::ConfigParser const &o){ return new bitpit::ConfigParser(o); } ) );
		cl.def("reset", (void (bitpit::ConfigParser::*)(const std::string &)) &bitpit::ConfigParser::reset, "C++: bitpit::ConfigParser::reset(const std::string &) --> void", pybind11::arg("root"));
		cl.def("reset", (void (bitpit::ConfigParser::*)(const std::string &, int)) &bitpit::ConfigParser::reset, "C++: bitpit::ConfigParser::reset(const std::string &, int) --> void", pybind11::arg("root"), pybind11::arg("version"));
		cl.def("read", [](bitpit::ConfigParser &o, const class std::__cxx11::basic_string<char> & a0) -> void { return o.read(a0); }, "", pybind11::arg("filename"));
		cl.def("read", (void (bitpit::ConfigParser::*)(const std::string &, bool)) &bitpit::ConfigParser::read, "C++: bitpit::ConfigParser::read(const std::string &, bool) --> void", pybind11::arg("filename"), pybind11::arg("append"));
		cl.def("write", (void (bitpit::ConfigParser::*)(const std::string &) const) &bitpit::ConfigParser::write, "C++: bitpit::ConfigParser::write(const std::string &) const --> void", pybind11::arg("filename"));
		cl.def("assign", (class bitpit::ConfigParser & (bitpit::ConfigParser::*)(const class bitpit::ConfigParser &)) &bitpit::ConfigParser::operator=, "C++: bitpit::ConfigParser::operator=(const class bitpit::ConfigParser &) --> class bitpit::ConfigParser &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B532_[bitpit::GlobalConfigParser] ";
	{ // bitpit::GlobalConfigParser file:configuration.hpp line:61
		pybind11::class_<bitpit::GlobalConfigParser, std::shared_ptr<bitpit::GlobalConfigParser>, bitpit::ConfigParser> cl(M("bitpit"), "GlobalConfigParser", "");
		cl.def_static("parser", (class bitpit::GlobalConfigParser & (*)()) &bitpit::GlobalConfigParser::parser, "C++: bitpit::GlobalConfigParser::parser() --> class bitpit::GlobalConfigParser &", pybind11::return_value_policy::automatic);
	}
}
#include <array>
#include <cell.hpp>
#include <element.hpp>
#include <element_reference.hpp>
#include <element_type.hpp>
#include <functional>
#include <interface.hpp>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <memory>
#include <ostream>
#include <piercedKernel.hpp>
#include <piercedStorage.hpp>
#include <piercedSync.hpp>
#include <piercedVector.hpp>
#include <piercedVectorKernel.hpp>
#include <piercedVectorStorage.hpp>
#include <proxyVector.hpp>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_element(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B403_[bitpit::Element] ";
	{ // bitpit::Element file:element.hpp line:47
		pybind11::class_<bitpit::Element, std::shared_ptr<bitpit::Element>> cl(M("bitpit"), "Element", "");
		cl.def( pybind11::init( [](){ return new bitpit::Element(); } ) );
		cl.def( pybind11::init( [](long const & a0, enum bitpit::ElementType const & a1){ return new bitpit::Element(a0, a1); } ), "doc" , pybind11::arg("id"), pybind11::arg("type"));
		cl.def( pybind11::init<long, enum bitpit::ElementType, int>(), pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("connectSize") );

		cl.def( pybind11::init( [](bitpit::Element const &o){ return new bitpit::Element(o); } ) );
		cl.def("assign", (class bitpit::Element & (bitpit::Element::*)(const class bitpit::Element &)) &bitpit::Element::operator=, "C++: bitpit::Element::operator=(const class bitpit::Element &) --> class bitpit::Element &", pybind11::return_value_policy::automatic, pybind11::arg("other"));
		cl.def("swap", (void (bitpit::Element::*)(class bitpit::Element &)) &bitpit::Element::swap, "C++: bitpit::Element::swap(class bitpit::Element &) --> void", pybind11::arg("other"));
		cl.def("initialize", [](bitpit::Element &o, long const & a0, enum bitpit::ElementType const & a1) -> void { return o.initialize(a0, a1); }, "", pybind11::arg("id"), pybind11::arg("type"));
		cl.def("initialize", (void (bitpit::Element::*)(long, enum bitpit::ElementType, int)) &bitpit::Element::initialize, "C++: bitpit::Element::initialize(long, enum bitpit::ElementType, int) --> void", pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("connectSize"));
		cl.def("hasInfo", (bool (bitpit::Element::*)() const) &bitpit::Element::hasInfo, "C++: bitpit::Element::hasInfo() const --> bool");
		cl.def("getInfo", (const class bitpit::ReferenceElementInfo & (bitpit::Element::*)() const) &bitpit::Element::getInfo, "C++: bitpit::Element::getInfo() const --> const class bitpit::ReferenceElementInfo &", pybind11::return_value_policy::automatic);
		cl.def("setId", (void (bitpit::Element::*)(long)) &bitpit::Element::setId, "C++: bitpit::Element::setId(long) --> void", pybind11::arg("id"));
		cl.def("getId", (long (bitpit::Element::*)() const) &bitpit::Element::getId, "C++: bitpit::Element::getId() const --> long");
		cl.def("setType", (void (bitpit::Element::*)(enum bitpit::ElementType)) &bitpit::Element::setType, "C++: bitpit::Element::setType(enum bitpit::ElementType) --> void", pybind11::arg("type"));
		cl.def("getType", (enum bitpit::ElementType (bitpit::Element::*)() const) &bitpit::Element::getType, "C++: bitpit::Element::getType() const --> enum bitpit::ElementType");
		cl.def("setPID", (void (bitpit::Element::*)(int)) &bitpit::Element::setPID, "C++: bitpit::Element::setPID(int) --> void", pybind11::arg("pid"));
		cl.def("getPID", (int (bitpit::Element::*)() const) &bitpit::Element::getPID, "C++: bitpit::Element::getPID() const --> int");
		cl.def("unsetConnect", (void (bitpit::Element::*)()) &bitpit::Element::unsetConnect, "C++: bitpit::Element::unsetConnect() --> void");
		cl.def("getConnectSize", (int (bitpit::Element::*)() const) &bitpit::Element::getConnectSize, "C++: bitpit::Element::getConnectSize() const --> int");
		cl.def("getConnect", (long * (bitpit::Element::*)()) &bitpit::Element::getConnect, "C++: bitpit::Element::getConnect() --> long *", pybind11::return_value_policy::automatic);
		cl.def("hasSameConnect", (bool (bitpit::Element::*)(const class bitpit::Element &) const) &bitpit::Element::hasSameConnect, "C++: bitpit::Element::hasSameConnect(const class bitpit::Element &) const --> bool", pybind11::arg("other"));
		cl.def("getFaceCount", (int (bitpit::Element::*)() const) &bitpit::Element::getFaceCount, "C++: bitpit::Element::getFaceCount() const --> int");
		cl.def("getFaceType", (enum bitpit::ElementType (bitpit::Element::*)(int) const) &bitpit::Element::getFaceType, "C++: bitpit::Element::getFaceType(int) const --> enum bitpit::ElementType", pybind11::arg("face"));
		cl.def("getFaceVertexCount", (int (bitpit::Element::*)(int) const) &bitpit::Element::getFaceVertexCount, "C++: bitpit::Element::getFaceVertexCount(int) const --> int", pybind11::arg("face"));
		cl.def("getFaceConnect", (class bitpit::ProxyVector<const long> (bitpit::Element::*)(int) const) &bitpit::Element::getFaceConnect, "C++: bitpit::Element::getFaceConnect(int) const --> class bitpit::ProxyVector<const long>", pybind11::arg("face"));
		cl.def("getFaceVertexIds", (class bitpit::ProxyVector<const long> (bitpit::Element::*)(int) const) &bitpit::Element::getFaceVertexIds, "C++: bitpit::Element::getFaceVertexIds(int) const --> class bitpit::ProxyVector<const long>", pybind11::arg("face"));
		cl.def("getFaceVertexId", (long (bitpit::Element::*)(int, int) const) &bitpit::Element::getFaceVertexId, "C++: bitpit::Element::getFaceVertexId(int, int) const --> long", pybind11::arg("face"), pybind11::arg("vertex"));
		cl.def("getEdgeCount", (int (bitpit::Element::*)() const) &bitpit::Element::getEdgeCount, "C++: bitpit::Element::getEdgeCount() const --> int");
		cl.def("getEdgeType", (enum bitpit::ElementType (bitpit::Element::*)(int) const) &bitpit::Element::getEdgeType, "C++: bitpit::Element::getEdgeType(int) const --> enum bitpit::ElementType", pybind11::arg("edge"));
		cl.def("getEdgeVertexCount", (int (bitpit::Element::*)(int) const) &bitpit::Element::getEdgeVertexCount, "C++: bitpit::Element::getEdgeVertexCount(int) const --> int", pybind11::arg("edge"));
		cl.def("getEdgeConnect", (class bitpit::ProxyVector<const long> (bitpit::Element::*)(int) const) &bitpit::Element::getEdgeConnect, "C++: bitpit::Element::getEdgeConnect(int) const --> class bitpit::ProxyVector<const long>", pybind11::arg("edge"));
		cl.def("getEdgeVertexIds", (class bitpit::ProxyVector<const long> (bitpit::Element::*)(int) const) &bitpit::Element::getEdgeVertexIds, "C++: bitpit::Element::getEdgeVertexIds(int) const --> class bitpit::ProxyVector<const long>", pybind11::arg("edge"));
		cl.def("getEdgeVertexId", (long (bitpit::Element::*)(int, int) const) &bitpit::Element::getEdgeVertexId, "C++: bitpit::Element::getEdgeVertexId(int, int) const --> long", pybind11::arg("edge"), pybind11::arg("vertex"));
		cl.def("getVertexCount", (int (bitpit::Element::*)() const) &bitpit::Element::getVertexCount, "C++: bitpit::Element::getVertexCount() const --> int");
		cl.def("renumberVertices", (void (bitpit::Element::*)(const class std::unordered_map<long, long, struct std::hash<long>, struct std::equal_to<long>, class std::allocator<struct std::pair<const long, long> > > &)) &bitpit::Element::renumberVertices, "C++: bitpit::Element::renumberVertices(const class std::unordered_map<long, long, struct std::hash<long>, struct std::equal_to<long>, class std::allocator<struct std::pair<const long, long> > > &) --> void", pybind11::arg("map"));
		cl.def("getVertexId", (long (bitpit::Element::*)(int) const) &bitpit::Element::getVertexId, "C++: bitpit::Element::getVertexId(int) const --> long", pybind11::arg("vertex"));
		cl.def("findVertex", (int (bitpit::Element::*)(long) const) &bitpit::Element::findVertex, "C++: bitpit::Element::findVertex(long) const --> int", pybind11::arg("vertexId"));
		cl.def("getFaceStreamSize", (int (bitpit::Element::*)() const) &bitpit::Element::getFaceStreamSize, "C++: bitpit::Element::getFaceStreamSize() const --> int");
		cl.def("getFaceStream", (class std::vector<long, class std::allocator<long> > (bitpit::Element::*)() const) &bitpit::Element::getFaceStream, "C++: bitpit::Element::getFaceStream() const --> class std::vector<long, class std::allocator<long> >");
		cl.def("evalCentroid", (struct std::array<double, 3> (bitpit::Element::*)(const struct std::array<double, 3> *) const) &bitpit::Element::evalCentroid, "C++: bitpit::Element::evalCentroid(const struct std::array<double, 3> *) const --> struct std::array<double, 3>", pybind11::arg("coordinates"));
		cl.def("evalSize", (double (bitpit::Element::*)(const struct std::array<double, 3> *) const) &bitpit::Element::evalSize, "C++: bitpit::Element::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("coordinates"));
		cl.def("evalVolume", (double (bitpit::Element::*)(const struct std::array<double, 3> *) const) &bitpit::Element::evalVolume, "C++: bitpit::Element::evalVolume(const struct std::array<double, 3> *) const --> double", pybind11::arg("coordinates"));
		cl.def("evalArea", (double (bitpit::Element::*)(const struct std::array<double, 3> *) const) &bitpit::Element::evalArea, "C++: bitpit::Element::evalArea(const struct std::array<double, 3> *) const --> double", pybind11::arg("coordinates"));
		cl.def("evalLength", (double (bitpit::Element::*)(const struct std::array<double, 3> *) const) &bitpit::Element::evalLength, "C++: bitpit::Element::evalLength(const struct std::array<double, 3> *) const --> double", pybind11::arg("coordinates"));
		cl.def("evalNormal", [](bitpit::Element const &o, const struct std::array<double, 3> * a0) -> std::array<double, 3> { return o.evalNormal(a0); }, "", pybind11::arg("coordinates"));
		cl.def("evalNormal", [](bitpit::Element const &o, const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1) -> std::array<double, 3> { return o.evalNormal(a0, a1); }, "", pybind11::arg("coordinates"), pybind11::arg("orientation"));
		cl.def("evalNormal", (struct std::array<double, 3> (bitpit::Element::*)(const struct std::array<double, 3> *, const struct std::array<double, 3> &, const struct std::array<double, 3> &) const) &bitpit::Element::evalNormal, "C++: bitpit::Element::evalNormal(const struct std::array<double, 3> *, const struct std::array<double, 3> &, const struct std::array<double, 3> &) const --> struct std::array<double, 3>", pybind11::arg("coordinates"), pybind11::arg("orientation"), pybind11::arg("point"));
		cl.def("evalPointProjection", (void (bitpit::Element::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const) &bitpit::Element::evalPointProjection, "C++: bitpit::Element::evalPointProjection(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const --> void", pybind11::arg("point"), pybind11::arg("coordinates"), pybind11::arg("projection"), pybind11::arg("distance"));
		cl.def("evalPointDistance", (double (bitpit::Element::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const) &bitpit::Element::evalPointDistance, "C++: bitpit::Element::evalPointDistance(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const --> double", pybind11::arg("point"), pybind11::arg("coordinates"));
		cl.def("getBinarySize", (unsigned int (bitpit::Element::*)() const) &bitpit::Element::getBinarySize, "C++: bitpit::Element::getBinarySize() const --> unsigned int");

		{ // bitpit::Element::IdHasher file:element.hpp line:69
			auto & enclosing_class = cl;
			pybind11::class_<bitpit::Element::IdHasher, std::shared_ptr<bitpit::Element::IdHasher>> cl(enclosing_class, "IdHasher", "		Hasher for the ids.\n\n		Since ids are unique, the hasher can be a function that\n		takes an id and cast it to a size_t.\n\n		The hasher is defined as a struct, because a struct can be\n		passed as an object into metafunctions (meaning that the type\n		deduction for the template paramenters can take place, and\n		also meaning that inlining is easier for the compiler). A bare\n		function would have to be passed as a function pointer.\n		To transform a function template into a function pointer,\n		the template would have to be manually instantiated (with a\n		perhaps unknown type argument).\n\n	");
			cl.def( pybind11::init( [](){ return new bitpit::Element::IdHasher(); } ) );
			cl.def("__call__", (unsigned long (bitpit::Element::IdHasher::*)(const long &) const) &bitpit::Element::IdHasher::operator()<const long &>, "C++: bitpit::Element::IdHasher::operator()(const long &) const --> unsigned long", pybind11::arg("value"));
		}

	}
	std::cout << "B404_[bitpit::ElementHalfItem<bitpit::Cell>] ";
	std::cout << "B405_[bitpit::ElementHalfItem<const bitpit::Cell>] ";
	std::cout << "B406_[bitpit::ElementHalfItem<bitpit::Interface>] ";
	std::cout << "B407_[bitpit::ElementHalfItem<const bitpit::Interface>] ";
	std::cout << "B408_[bitpit::ElementHalfEdge<bitpit::Cell>] ";
	std::cout << "B409_[bitpit::ElementHalfEdge<const bitpit::Cell>] ";
	std::cout << "B410_[bitpit::ElementHalfFace<bitpit::Cell>] ";
	std::cout << "B411_[bitpit::ElementHalfFace<const bitpit::Cell>] ";
	std::cout << "B412_[bitpit::ElementHalfFace<bitpit::Interface>] ";
	std::cout << "B413_[bitpit::ElementHalfFace<const bitpit::Interface>] ";
	std::cout << "B414_[bitpit::PiercedVector<bitpit::Element,long>] ";
	std::cout << "B415_[bitpit::Cell] ";
	{ // bitpit::Cell file:cell.hpp line:44
		pybind11::class_<bitpit::Cell, std::shared_ptr<bitpit::Cell>, bitpit::Element> cl(M("bitpit"), "Cell", "");
		cl.def( pybind11::init( [](){ return new bitpit::Cell(); } ) );
		cl.def( pybind11::init( [](long const & a0, enum bitpit::ElementType const & a1){ return new bitpit::Cell(a0, a1); } ), "doc" , pybind11::arg("id"), pybind11::arg("type"));
		cl.def( pybind11::init( [](long const & a0, enum bitpit::ElementType const & a1, bool const & a2){ return new bitpit::Cell(a0, a1, a2); } ), "doc" , pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("interior"));
		cl.def( pybind11::init<long, enum bitpit::ElementType, bool, bool>(), pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("interior"), pybind11::arg("storeNeighbourhood") );

		cl.def( pybind11::init( [](long const & a0, enum bitpit::ElementType const & a1, int const & a2){ return new bitpit::Cell(a0, a1, a2); } ), "doc" , pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("connectSize"));
		cl.def( pybind11::init( [](long const & a0, enum bitpit::ElementType const & a1, int const & a2, bool const & a3){ return new bitpit::Cell(a0, a1, a2, a3); } ), "doc" , pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("connectSize"), pybind11::arg("interior"));
		cl.def( pybind11::init<long, enum bitpit::ElementType, int, bool, bool>(), pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("connectSize"), pybind11::arg("interior"), pybind11::arg("storeNeighbourhood") );

		cl.def( pybind11::init( [](bitpit::Cell const &o){ return new bitpit::Cell(o); } ) );
		cl.def("assign", (class bitpit::Cell & (bitpit::Cell::*)(const class bitpit::Cell &)) &bitpit::Cell::operator=, "C++: bitpit::Cell::operator=(const class bitpit::Cell &) --> class bitpit::Cell &", pybind11::return_value_policy::automatic, pybind11::arg("other"));
		cl.def("swap", (void (bitpit::Cell::*)(class bitpit::Cell &)) &bitpit::Cell::swap, "C++: bitpit::Cell::swap(class bitpit::Cell &) --> void", pybind11::arg("other"));
		cl.def("initialize", [](bitpit::Cell &o, long const & a0, enum bitpit::ElementType const & a1, bool const & a2) -> void { return o.initialize(a0, a1, a2); }, "", pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("interior"));
		cl.def("initialize", (void (bitpit::Cell::*)(long, enum bitpit::ElementType, bool, bool)) &bitpit::Cell::initialize, "C++: bitpit::Cell::initialize(long, enum bitpit::ElementType, bool, bool) --> void", pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("interior"), pybind11::arg("storeNeighbourhood"));
		cl.def("initialize", [](bitpit::Cell &o, long const & a0, enum bitpit::ElementType const & a1, int const & a2, bool const & a3) -> void { return o.initialize(a0, a1, a2, a3); }, "", pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("connectSize"), pybind11::arg("interior"));
		cl.def("initialize", (void (bitpit::Cell::*)(long, enum bitpit::ElementType, int, bool, bool)) &bitpit::Cell::initialize, "C++: bitpit::Cell::initialize(long, enum bitpit::ElementType, int, bool, bool) --> void", pybind11::arg("id"), pybind11::arg("type"), pybind11::arg("connectSize"), pybind11::arg("interior"), pybind11::arg("storeNeighbourhood"));
		cl.def("isInterior", (bool (bitpit::Cell::*)() const) &bitpit::Cell::isInterior, "C++: bitpit::Cell::isInterior() const --> bool");
		cl.def("deleteInterfaces", (void (bitpit::Cell::*)()) &bitpit::Cell::deleteInterfaces, "C++: bitpit::Cell::deleteInterfaces() --> void");
		cl.def("resetInterfaces", [](bitpit::Cell &o) -> void { return o.resetInterfaces(); }, "");
		cl.def("resetInterfaces", (void (bitpit::Cell::*)(bool)) &bitpit::Cell::resetInterfaces, "C++: bitpit::Cell::resetInterfaces(bool) --> void", pybind11::arg("storeInterfaces"));
		cl.def("setInterface", (void (bitpit::Cell::*)(int, int, long)) &bitpit::Cell::setInterface, "C++: bitpit::Cell::setInterface(int, int, long) --> void", pybind11::arg("face"), pybind11::arg("index"), pybind11::arg("interface"));
		cl.def("pushInterface", (void (bitpit::Cell::*)(int, long)) &bitpit::Cell::pushInterface, "C++: bitpit::Cell::pushInterface(int, long) --> void", pybind11::arg("face"), pybind11::arg("interface"));
		cl.def("deleteInterface", (void (bitpit::Cell::*)(int, int)) &bitpit::Cell::deleteInterface, "C++: bitpit::Cell::deleteInterface(int, int) --> void", pybind11::arg("face"), pybind11::arg("i"));
		cl.def("getInterfaceCount", (int (bitpit::Cell::*)() const) &bitpit::Cell::getInterfaceCount, "C++: bitpit::Cell::getInterfaceCount() const --> int");
		cl.def("getInterfaceCount", (int (bitpit::Cell::*)(int) const) &bitpit::Cell::getInterfaceCount, "C++: bitpit::Cell::getInterfaceCount(int) const --> int", pybind11::arg("face"));
		cl.def("getInterface", [](bitpit::Cell const &o, int const & a0) -> long { return o.getInterface(a0); }, "", pybind11::arg("face"));
		cl.def("getInterface", (long (bitpit::Cell::*)(int, int) const) &bitpit::Cell::getInterface, "C++: bitpit::Cell::getInterface(int, int) const --> long", pybind11::arg("face"), pybind11::arg("index"));
		cl.def("getInterfaces", (long * (bitpit::Cell::*)()) &bitpit::Cell::getInterfaces, "C++: bitpit::Cell::getInterfaces() --> long *", pybind11::return_value_policy::automatic);
		cl.def("getInterfaces", (long * (bitpit::Cell::*)(int)) &bitpit::Cell::getInterfaces, "C++: bitpit::Cell::getInterfaces(int) --> long *", pybind11::return_value_policy::automatic, pybind11::arg("face"));
		cl.def("findInterface", (int (bitpit::Cell::*)(int, int)) &bitpit::Cell::findInterface, "C++: bitpit::Cell::findInterface(int, int) --> int", pybind11::arg("face"), pybind11::arg("interface"));
		cl.def("findInterface", (int (bitpit::Cell::*)(int)) &bitpit::Cell::findInterface, "C++: bitpit::Cell::findInterface(int) --> int", pybind11::arg("interface"));
		cl.def("deleteAdjacencies", (void (bitpit::Cell::*)()) &bitpit::Cell::deleteAdjacencies, "C++: bitpit::Cell::deleteAdjacencies() --> void");
		cl.def("resetAdjacencies", [](bitpit::Cell &o) -> void { return o.resetAdjacencies(); }, "");
		cl.def("resetAdjacencies", (void (bitpit::Cell::*)(bool)) &bitpit::Cell::resetAdjacencies, "C++: bitpit::Cell::resetAdjacencies(bool) --> void", pybind11::arg("storeAdjacencies"));
		cl.def("setAdjacency", (void (bitpit::Cell::*)(int, int, long)) &bitpit::Cell::setAdjacency, "C++: bitpit::Cell::setAdjacency(int, int, long) --> void", pybind11::arg("face"), pybind11::arg("index"), pybind11::arg("adjacencies"));
		cl.def("pushAdjacency", (void (bitpit::Cell::*)(int, long)) &bitpit::Cell::pushAdjacency, "C++: bitpit::Cell::pushAdjacency(int, long) --> void", pybind11::arg("face"), pybind11::arg("adjacency"));
		cl.def("deleteAdjacency", (void (bitpit::Cell::*)(int, int)) &bitpit::Cell::deleteAdjacency, "C++: bitpit::Cell::deleteAdjacency(int, int) --> void", pybind11::arg("face"), pybind11::arg("i"));
		cl.def("getAdjacencyCount", (int (bitpit::Cell::*)() const) &bitpit::Cell::getAdjacencyCount, "C++: bitpit::Cell::getAdjacencyCount() const --> int");
		cl.def("getAdjacencyCount", (int (bitpit::Cell::*)(int) const) &bitpit::Cell::getAdjacencyCount, "C++: bitpit::Cell::getAdjacencyCount(int) const --> int", pybind11::arg("face"));
		cl.def("getAdjacency", [](bitpit::Cell const &o, int const & a0) -> long { return o.getAdjacency(a0); }, "", pybind11::arg("face"));
		cl.def("getAdjacency", (long (bitpit::Cell::*)(int, int) const) &bitpit::Cell::getAdjacency, "C++: bitpit::Cell::getAdjacency(int, int) const --> long", pybind11::arg("face"), pybind11::arg("index"));
		cl.def("getAdjacencies", (long * (bitpit::Cell::*)()) &bitpit::Cell::getAdjacencies, "C++: bitpit::Cell::getAdjacencies() --> long *", pybind11::return_value_policy::automatic);
		cl.def("getAdjacencies", (long * (bitpit::Cell::*)(int)) &bitpit::Cell::getAdjacencies, "C++: bitpit::Cell::getAdjacencies(int) --> long *", pybind11::return_value_policy::automatic, pybind11::arg("face"));
		cl.def("findAdjacency", (int (bitpit::Cell::*)(int, int)) &bitpit::Cell::findAdjacency, "C++: bitpit::Cell::findAdjacency(int, int) --> int", pybind11::arg("face"), pybind11::arg("adjacency"));
		cl.def("findAdjacency", (int (bitpit::Cell::*)(int)) &bitpit::Cell::findAdjacency, "C++: bitpit::Cell::findAdjacency(int) --> int", pybind11::arg("adjacency"));
		cl.def("isFaceBorder", (bool (bitpit::Cell::*)(int) const) &bitpit::Cell::isFaceBorder, "C++: bitpit::Cell::isFaceBorder(int) const --> bool", pybind11::arg("face"));
		cl.def("display", (void (bitpit::Cell::*)(std::ostream &, unsigned short) const) &bitpit::Cell::display, "C++: bitpit::Cell::display(std::ostream &, unsigned short) const --> void", pybind11::arg("out"), pybind11::arg("indent"));
		cl.def("getBinarySize", (unsigned int (bitpit::Cell::*)() const) &bitpit::Cell::getBinarySize, "C++: bitpit::Cell::getBinarySize() const --> unsigned int");
	}
}
#include <array>
#include <element_reference.hpp>
#include <iostream>
#include <sstream> // __str__

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// bitpit::ReferenceHexahedronInfo file:element_reference.hpp line:131
struct PyCallBack_bitpit_ReferenceHexahedronInfo : public bitpit::ReferenceHexahedronInfo {
	using bitpit::ReferenceHexahedronInfo::ReferenceHexahedronInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceHexahedronInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceHexahedronInfo::evalSize(a0);
	}
	double evalVolume(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceHexahedronInfo *>(this), "evalVolume");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceHexahedronInfo::evalVolume(a0);
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceHexahedronInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return Reference3DElementInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceHexahedronInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return Reference3DElementInfo::evalPointDistance(a0, a1);
	}
};

// bitpit::ReferencePyramidInfo file:element_reference.hpp line:150
struct PyCallBack_bitpit_ReferencePyramidInfo : public bitpit::ReferencePyramidInfo {
	using bitpit::ReferencePyramidInfo::ReferencePyramidInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferencePyramidInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferencePyramidInfo::evalSize(a0);
	}
	double evalVolume(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferencePyramidInfo *>(this), "evalVolume");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferencePyramidInfo::evalVolume(a0);
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferencePyramidInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return Reference3DElementInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferencePyramidInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return Reference3DElementInfo::evalPointDistance(a0, a1);
	}
};

// bitpit::ReferenceWedgeInfo file:element_reference.hpp line:169
struct PyCallBack_bitpit_ReferenceWedgeInfo : public bitpit::ReferenceWedgeInfo {
	using bitpit::ReferenceWedgeInfo::ReferenceWedgeInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceWedgeInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceWedgeInfo::evalSize(a0);
	}
	double evalVolume(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceWedgeInfo *>(this), "evalVolume");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceWedgeInfo::evalVolume(a0);
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceWedgeInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return Reference3DElementInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceWedgeInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return Reference3DElementInfo::evalPointDistance(a0, a1);
	}
};

// bitpit::Reference2DElementInfo file:element_reference.hpp line:188
struct PyCallBack_bitpit_Reference2DElementInfo : public bitpit::Reference2DElementInfo {
	using bitpit::Reference2DElementInfo::Reference2DElementInfo;

	double evalArea(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference2DElementInfo *>(this), "evalArea");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Reference2DElementInfo::evalArea\"");
	}
	using _binder_ret_0 = struct std::array<double, 3>;
	_binder_ret_0 evalNormal(const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference2DElementInfo *>(this), "evalNormal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Reference2DElementInfo::evalNormal\"");
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference2DElementInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return Reference2DElementInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference2DElementInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return Reference2DElementInfo::evalPointDistance(a0, a1);
	}
	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference2DElementInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalSize\"");
	}
};

// bitpit::ReferenceTriangleInfo file:element_reference.hpp line:206
struct PyCallBack_bitpit_ReferenceTriangleInfo : public bitpit::ReferenceTriangleInfo {
	using bitpit::ReferenceTriangleInfo::ReferenceTriangleInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceTriangleInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceTriangleInfo::evalSize(a0);
	}
	double evalArea(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceTriangleInfo *>(this), "evalArea");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceTriangleInfo::evalArea(a0);
	}
	using _binder_ret_0 = struct std::array<double, 3>;
	_binder_ret_0 evalNormal(const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceTriangleInfo *>(this), "evalNormal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return ReferenceTriangleInfo::evalNormal(a0, a1);
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceTriangleInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ReferenceTriangleInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceTriangleInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceTriangleInfo::evalPointDistance(a0, a1);
	}
};

// bitpit::ReferencePixelInfo file:element_reference.hpp line:233
struct PyCallBack_bitpit_ReferencePixelInfo : public bitpit::ReferencePixelInfo {
	using bitpit::ReferencePixelInfo::ReferencePixelInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferencePixelInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferencePixelInfo::evalSize(a0);
	}
	double evalArea(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferencePixelInfo *>(this), "evalArea");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferencePixelInfo::evalArea(a0);
	}
	using _binder_ret_0 = struct std::array<double, 3>;
	_binder_ret_0 evalNormal(const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferencePixelInfo *>(this), "evalNormal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return ReferencePixelInfo::evalNormal(a0, a1);
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferencePixelInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return Reference2DElementInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferencePixelInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return Reference2DElementInfo::evalPointDistance(a0, a1);
	}
};

// bitpit::ReferenceQuadInfo file:element_reference.hpp line:257
struct PyCallBack_bitpit_ReferenceQuadInfo : public bitpit::ReferenceQuadInfo {
	using bitpit::ReferenceQuadInfo::ReferenceQuadInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceQuadInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceQuadInfo::evalSize(a0);
	}
	double evalArea(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceQuadInfo *>(this), "evalArea");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceQuadInfo::evalArea(a0);
	}
	using _binder_ret_0 = struct std::array<double, 3>;
	_binder_ret_0 evalNormal(const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceQuadInfo *>(this), "evalNormal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return ReferenceQuadInfo::evalNormal(a0, a1);
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceQuadInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return Reference2DElementInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceQuadInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return Reference2DElementInfo::evalPointDistance(a0, a1);
	}
};

// bitpit::Reference1DElementInfo file:element_reference.hpp line:281
struct PyCallBack_bitpit_Reference1DElementInfo : public bitpit::Reference1DElementInfo {
	using bitpit::Reference1DElementInfo::Reference1DElementInfo;

	double evalLength(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference1DElementInfo *>(this), "evalLength");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Reference1DElementInfo::evalLength\"");
	}
	using _binder_ret_0 = struct std::array<double, 3>;
	_binder_ret_0 evalNormal(const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1, const struct std::array<double, 3> & a2) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference1DElementInfo *>(this), "evalNormal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Reference1DElementInfo::evalNormal\"");
	}
	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference1DElementInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalSize\"");
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference1DElementInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalPointProjection\"");
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference1DElementInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalPointDistance\"");
	}
};

// bitpit::ReferenceLineInfo file:element_reference.hpp line:293
struct PyCallBack_bitpit_ReferenceLineInfo : public bitpit::ReferenceLineInfo {
	using bitpit::ReferenceLineInfo::ReferenceLineInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceLineInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceLineInfo::evalSize(a0);
	}
	double evalLength(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceLineInfo *>(this), "evalLength");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceLineInfo::evalLength(a0);
	}
	using _binder_ret_0 = struct std::array<double, 3>;
	_binder_ret_0 evalNormal(const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1, const struct std::array<double, 3> & a2) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceLineInfo *>(this), "evalNormal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return ReferenceLineInfo::evalNormal(a0, a1, a2);
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceLineInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ReferenceLineInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceLineInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceLineInfo::evalPointDistance(a0, a1);
	}
};

// bitpit::Reference0DElementInfo file:element_reference.hpp line:325
struct PyCallBack_bitpit_Reference0DElementInfo : public bitpit::Reference0DElementInfo {
	using bitpit::Reference0DElementInfo::Reference0DElementInfo;

	using _binder_ret_0 = struct std::array<double, 3>;
	_binder_ret_0 evalNormal(const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference0DElementInfo *>(this), "evalNormal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"Reference0DElementInfo::evalNormal\"");
	}
	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference0DElementInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalSize\"");
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference0DElementInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalPointProjection\"");
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::Reference0DElementInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"ReferenceElementInfo::evalPointDistance\"");
	}
};

// bitpit::ReferenceVertexInfo file:element_reference.hpp line:335
struct PyCallBack_bitpit_ReferenceVertexInfo : public bitpit::ReferenceVertexInfo {
	using bitpit::ReferenceVertexInfo::ReferenceVertexInfo;

	double evalSize(const struct std::array<double, 3> * a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceVertexInfo *>(this), "evalSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceVertexInfo::evalSize(a0);
	}
	using _binder_ret_0 = struct std::array<double, 3>;
	_binder_ret_0 evalNormal(const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceVertexInfo *>(this), "evalNormal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return ReferenceVertexInfo::evalNormal(a0, a1);
	}
	void evalPointProjection(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1, struct std::array<double, 3> * a2, double * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceVertexInfo *>(this), "evalPointProjection");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return ReferenceVertexInfo::evalPointProjection(a0, a1, a2, a3);
	}
	double evalPointDistance(const struct std::array<double, 3> & a0, const struct std::array<double, 3> * a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::ReferenceVertexInfo *>(this), "evalPointDistance");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return ReferenceVertexInfo::evalPointDistance(a0, a1);
	}
};

void bind_element_reference(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B392_[bitpit::ReferenceHexahedronInfo] ";
	{ // bitpit::ReferenceHexahedronInfo file:element_reference.hpp line:131
		pybind11::class_<bitpit::ReferenceHexahedronInfo, std::shared_ptr<bitpit::ReferenceHexahedronInfo>, PyCallBack_bitpit_ReferenceHexahedronInfo, bitpit::Reference3DElementInfo> cl(M("bitpit"), "ReferenceHexahedronInfo", "");
		cl.def("evalSize", (double (bitpit::ReferenceHexahedronInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceHexahedronInfo::evalSize, "C++: bitpit::ReferenceHexahedronInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalVolume", (double (bitpit::ReferenceHexahedronInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceHexahedronInfo::evalVolume, "C++: bitpit::ReferenceHexahedronInfo::evalVolume(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
	}
	std::cout << "B393_[bitpit::ReferencePyramidInfo] ";
	{ // bitpit::ReferencePyramidInfo file:element_reference.hpp line:150
		pybind11::class_<bitpit::ReferencePyramidInfo, std::shared_ptr<bitpit::ReferencePyramidInfo>, PyCallBack_bitpit_ReferencePyramidInfo, bitpit::Reference3DElementInfo> cl(M("bitpit"), "ReferencePyramidInfo", "");
		cl.def("evalSize", (double (bitpit::ReferencePyramidInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferencePyramidInfo::evalSize, "C++: bitpit::ReferencePyramidInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalVolume", (double (bitpit::ReferencePyramidInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferencePyramidInfo::evalVolume, "C++: bitpit::ReferencePyramidInfo::evalVolume(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
	}
	std::cout << "B394_[bitpit::ReferenceWedgeInfo] ";
	{ // bitpit::ReferenceWedgeInfo file:element_reference.hpp line:169
		pybind11::class_<bitpit::ReferenceWedgeInfo, std::shared_ptr<bitpit::ReferenceWedgeInfo>, PyCallBack_bitpit_ReferenceWedgeInfo, bitpit::Reference3DElementInfo> cl(M("bitpit"), "ReferenceWedgeInfo", "");
		cl.def("evalSize", (double (bitpit::ReferenceWedgeInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceWedgeInfo::evalSize, "C++: bitpit::ReferenceWedgeInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalVolume", (double (bitpit::ReferenceWedgeInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceWedgeInfo::evalVolume, "C++: bitpit::ReferenceWedgeInfo::evalVolume(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
	}
	std::cout << "B395_[bitpit::Reference2DElementInfo] ";
	{ // bitpit::Reference2DElementInfo file:element_reference.hpp line:188
		pybind11::class_<bitpit::Reference2DElementInfo, std::shared_ptr<bitpit::Reference2DElementInfo>, PyCallBack_bitpit_Reference2DElementInfo, bitpit::ReferenceElementInfo> cl(M("bitpit"), "Reference2DElementInfo", "");
		cl.def("evalArea", (double (bitpit::Reference2DElementInfo::*)(const struct std::array<double, 3> *) const) &bitpit::Reference2DElementInfo::evalArea, "C++: bitpit::Reference2DElementInfo::evalArea(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalPerimeter", (double (bitpit::Reference2DElementInfo::*)(const struct std::array<double, 3> *) const) &bitpit::Reference2DElementInfo::evalPerimeter, "C++: bitpit::Reference2DElementInfo::evalPerimeter(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", [](bitpit::Reference2DElementInfo const &o, const struct std::array<double, 3> * a0) -> std::array<double, 3> { return o.evalNormal(a0); }, "", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", (struct std::array<double, 3> (bitpit::Reference2DElementInfo::*)(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const) &bitpit::Reference2DElementInfo::evalNormal, "C++: bitpit::Reference2DElementInfo::evalNormal(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const --> struct std::array<double, 3>", pybind11::arg("vertexCoords"), pybind11::arg("point"));
		cl.def("evalPointProjection", (void (bitpit::Reference2DElementInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const) &bitpit::Reference2DElementInfo::evalPointProjection, "C++: bitpit::Reference2DElementInfo::evalPointProjection(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const --> void", pybind11::arg("point"), pybind11::arg("vertexCoords"), pybind11::arg("projection"), pybind11::arg("distance"));
		cl.def("evalPointDistance", (double (bitpit::Reference2DElementInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const) &bitpit::Reference2DElementInfo::evalPointDistance, "C++: bitpit::Reference2DElementInfo::evalPointDistance(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const --> double", pybind11::arg("point"), pybind11::arg("vertexCoords"));
	}
	std::cout << "B396_[bitpit::ReferenceTriangleInfo] ";
	{ // bitpit::ReferenceTriangleInfo file:element_reference.hpp line:206
		pybind11::class_<bitpit::ReferenceTriangleInfo, std::shared_ptr<bitpit::ReferenceTriangleInfo>, PyCallBack_bitpit_ReferenceTriangleInfo, bitpit::Reference2DElementInfo> cl(M("bitpit"), "ReferenceTriangleInfo", "");
		cl.def("evalSize", (double (bitpit::ReferenceTriangleInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceTriangleInfo::evalSize, "C++: bitpit::ReferenceTriangleInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalArea", (double (bitpit::ReferenceTriangleInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceTriangleInfo::evalArea, "C++: bitpit::ReferenceTriangleInfo::evalArea(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", [](bitpit::ReferenceTriangleInfo const &o, const struct std::array<double, 3> * a0) -> std::array<double, 3> { return o.evalNormal(a0); }, "", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", (struct std::array<double, 3> (bitpit::ReferenceTriangleInfo::*)(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const) &bitpit::ReferenceTriangleInfo::evalNormal, "C++: bitpit::ReferenceTriangleInfo::evalNormal(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const --> struct std::array<double, 3>", pybind11::arg("vertexCoords"), pybind11::arg("point"));
		cl.def("evalPointProjection", (void (bitpit::ReferenceTriangleInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const) &bitpit::ReferenceTriangleInfo::evalPointProjection, "C++: bitpit::ReferenceTriangleInfo::evalPointProjection(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const --> void", pybind11::arg("point"), pybind11::arg("vertexCoords"), pybind11::arg("projection"), pybind11::arg("distance"));
		cl.def("evalPointDistance", (double (bitpit::ReferenceTriangleInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const) &bitpit::ReferenceTriangleInfo::evalPointDistance, "C++: bitpit::ReferenceTriangleInfo::evalPointDistance(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const --> double", pybind11::arg("point"), pybind11::arg("vertexCoords"));
	}
	std::cout << "B397_[bitpit::ReferencePixelInfo] ";
	{ // bitpit::ReferencePixelInfo file:element_reference.hpp line:233
		pybind11::class_<bitpit::ReferencePixelInfo, std::shared_ptr<bitpit::ReferencePixelInfo>, PyCallBack_bitpit_ReferencePixelInfo, bitpit::Reference2DElementInfo> cl(M("bitpit"), "ReferencePixelInfo", "");
		cl.def("evalSize", (double (bitpit::ReferencePixelInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferencePixelInfo::evalSize, "C++: bitpit::ReferencePixelInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalArea", (double (bitpit::ReferencePixelInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferencePixelInfo::evalArea, "C++: bitpit::ReferencePixelInfo::evalArea(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", [](bitpit::ReferencePixelInfo const &o, const struct std::array<double, 3> * a0) -> std::array<double, 3> { return o.evalNormal(a0); }, "", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", (struct std::array<double, 3> (bitpit::ReferencePixelInfo::*)(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const) &bitpit::ReferencePixelInfo::evalNormal, "C++: bitpit::ReferencePixelInfo::evalNormal(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const --> struct std::array<double, 3>", pybind11::arg("vertexCoords"), pybind11::arg("point"));
	}
	std::cout << "B398_[bitpit::ReferenceQuadInfo] ";
	{ // bitpit::ReferenceQuadInfo file:element_reference.hpp line:257
		pybind11::class_<bitpit::ReferenceQuadInfo, std::shared_ptr<bitpit::ReferenceQuadInfo>, PyCallBack_bitpit_ReferenceQuadInfo, bitpit::Reference2DElementInfo> cl(M("bitpit"), "ReferenceQuadInfo", "");
		cl.def("evalSize", (double (bitpit::ReferenceQuadInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceQuadInfo::evalSize, "C++: bitpit::ReferenceQuadInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalArea", (double (bitpit::ReferenceQuadInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceQuadInfo::evalArea, "C++: bitpit::ReferenceQuadInfo::evalArea(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", [](bitpit::ReferenceQuadInfo const &o, const struct std::array<double, 3> * a0) -> std::array<double, 3> { return o.evalNormal(a0); }, "", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", (struct std::array<double, 3> (bitpit::ReferenceQuadInfo::*)(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const) &bitpit::ReferenceQuadInfo::evalNormal, "C++: bitpit::ReferenceQuadInfo::evalNormal(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const --> struct std::array<double, 3>", pybind11::arg("vertexCoords"), pybind11::arg("point"));
	}
	std::cout << "B399_[bitpit::Reference1DElementInfo] ";
	{ // bitpit::Reference1DElementInfo file:element_reference.hpp line:281
		pybind11::class_<bitpit::Reference1DElementInfo, std::shared_ptr<bitpit::Reference1DElementInfo>, PyCallBack_bitpit_Reference1DElementInfo, bitpit::ReferenceElementInfo> cl(M("bitpit"), "Reference1DElementInfo", "");
		cl.def("evalLength", (double (bitpit::Reference1DElementInfo::*)(const struct std::array<double, 3> *) const) &bitpit::Reference1DElementInfo::evalLength, "C++: bitpit::Reference1DElementInfo::evalLength(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", [](bitpit::Reference1DElementInfo const &o, const struct std::array<double, 3> * a0) -> std::array<double, 3> { return o.evalNormal(a0); }, "", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", [](bitpit::Reference1DElementInfo const &o, const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1) -> std::array<double, 3> { return o.evalNormal(a0, a1); }, "", pybind11::arg("vertexCoords"), pybind11::arg("orientation"));
		cl.def("evalNormal", (struct std::array<double, 3> (bitpit::Reference1DElementInfo::*)(const struct std::array<double, 3> *, const struct std::array<double, 3> &, const struct std::array<double, 3> &) const) &bitpit::Reference1DElementInfo::evalNormal, "C++: bitpit::Reference1DElementInfo::evalNormal(const struct std::array<double, 3> *, const struct std::array<double, 3> &, const struct std::array<double, 3> &) const --> struct std::array<double, 3>", pybind11::arg("vertexCoords"), pybind11::arg("orientation"), pybind11::arg("point"));
	}
	std::cout << "B400_[bitpit::ReferenceLineInfo] ";
	{ // bitpit::ReferenceLineInfo file:element_reference.hpp line:293
		pybind11::class_<bitpit::ReferenceLineInfo, std::shared_ptr<bitpit::ReferenceLineInfo>, PyCallBack_bitpit_ReferenceLineInfo, bitpit::Reference1DElementInfo> cl(M("bitpit"), "ReferenceLineInfo", "");
		cl.def("evalSize", (double (bitpit::ReferenceLineInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceLineInfo::evalSize, "C++: bitpit::ReferenceLineInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalLength", (double (bitpit::ReferenceLineInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceLineInfo::evalLength, "C++: bitpit::ReferenceLineInfo::evalLength(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", [](bitpit::ReferenceLineInfo const &o, const struct std::array<double, 3> * a0) -> std::array<double, 3> { return o.evalNormal(a0); }, "", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", [](bitpit::ReferenceLineInfo const &o, const struct std::array<double, 3> * a0, const struct std::array<double, 3> & a1) -> std::array<double, 3> { return o.evalNormal(a0, a1); }, "", pybind11::arg("vertexCoords"), pybind11::arg("orientation"));
		cl.def("evalNormal", (struct std::array<double, 3> (bitpit::ReferenceLineInfo::*)(const struct std::array<double, 3> *, const struct std::array<double, 3> &, const struct std::array<double, 3> &) const) &bitpit::ReferenceLineInfo::evalNormal, "C++: bitpit::ReferenceLineInfo::evalNormal(const struct std::array<double, 3> *, const struct std::array<double, 3> &, const struct std::array<double, 3> &) const --> struct std::array<double, 3>", pybind11::arg("vertexCoords"), pybind11::arg("orientation"), pybind11::arg("point"));
		cl.def("evalPointProjection", (void (bitpit::ReferenceLineInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const) &bitpit::ReferenceLineInfo::evalPointProjection, "C++: bitpit::ReferenceLineInfo::evalPointProjection(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const --> void", pybind11::arg("point"), pybind11::arg("vertexCoords"), pybind11::arg("projection"), pybind11::arg("distance"));
		cl.def("evalPointDistance", (double (bitpit::ReferenceLineInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const) &bitpit::ReferenceLineInfo::evalPointDistance, "C++: bitpit::ReferenceLineInfo::evalPointDistance(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const --> double", pybind11::arg("point"), pybind11::arg("vertexCoords"));
	}
	std::cout << "B401_[bitpit::Reference0DElementInfo] ";
	{ // bitpit::Reference0DElementInfo file:element_reference.hpp line:325
		pybind11::class_<bitpit::Reference0DElementInfo, std::shared_ptr<bitpit::Reference0DElementInfo>, PyCallBack_bitpit_Reference0DElementInfo, bitpit::ReferenceElementInfo> cl(M("bitpit"), "Reference0DElementInfo", "");
		cl.def("evalNormal", [](bitpit::Reference0DElementInfo const &o, const struct std::array<double, 3> * a0) -> std::array<double, 3> { return o.evalNormal(a0); }, "", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", (struct std::array<double, 3> (bitpit::Reference0DElementInfo::*)(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const) &bitpit::Reference0DElementInfo::evalNormal, "C++: bitpit::Reference0DElementInfo::evalNormal(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const --> struct std::array<double, 3>", pybind11::arg("vertexCoords"), pybind11::arg("orientation"));
	}
	std::cout << "B402_[bitpit::ReferenceVertexInfo] ";
	{ // bitpit::ReferenceVertexInfo file:element_reference.hpp line:335
		pybind11::class_<bitpit::ReferenceVertexInfo, std::shared_ptr<bitpit::ReferenceVertexInfo>, PyCallBack_bitpit_ReferenceVertexInfo, bitpit::Reference0DElementInfo> cl(M("bitpit"), "ReferenceVertexInfo", "");
		cl.def("evalSize", (double (bitpit::ReferenceVertexInfo::*)(const struct std::array<double, 3> *) const) &bitpit::ReferenceVertexInfo::evalSize, "C++: bitpit::ReferenceVertexInfo::evalSize(const struct std::array<double, 3> *) const --> double", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", [](bitpit::ReferenceVertexInfo const &o, const struct std::array<double, 3> * a0) -> std::array<double, 3> { return o.evalNormal(a0); }, "", pybind11::arg("vertexCoords"));
		cl.def("evalNormal", (struct std::array<double, 3> (bitpit::ReferenceVertexInfo::*)(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const) &bitpit::ReferenceVertexInfo::evalNormal, "C++: bitpit::ReferenceVertexInfo::evalNormal(const struct std::array<double, 3> *, const struct std::array<double, 3> &) const --> struct std::array<double, 3>", pybind11::arg("vertexCoords"), pybind11::arg("orientation"));
		cl.def("evalPointProjection", (void (bitpit::ReferenceVertexInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const) &bitpit::ReferenceVertexInfo::evalPointProjection, "C++: bitpit::ReferenceVertexInfo::evalPointProjection(const struct std::array<double, 3> &, const struct std::array<double, 3> *, struct std::array<double, 3> *, double *) const --> void", pybind11::arg("point"), pybind11::arg("vertexCoords"), pybind11::arg("projection"), pybind11::arg("distance"));
		cl.def("evalPointDistance", (double (bitpit::ReferenceVertexInfo::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const) &bitpit::ReferenceVertexInfo::evalPointDistance, "C++: bitpit::ReferenceVertexInfo::evalPointDistance(const struct std::array<double, 3> &, const struct std::array<double, 3> *) const --> double", pybind11::arg("point"), pybind11::arg("vertexCoords"));
	}
}
#include <index_generator.hpp>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <memory>
#include <ostream>
#include <sstream> // __str__
#include <streambuf>
#include <string>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_index_generator(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B533_[bitpit::IndexGenerator<int>] ";
	{ // bitpit::IndexGenerator file:index_generator.hpp line:79
		pybind11::class_<bitpit::IndexGenerator<int>, std::shared_ptr<bitpit::IndexGenerator<int>>> cl(M("bitpit"), "IndexGenerator_int_t", "");
		cl.def( pybind11::init( [](){ return new bitpit::IndexGenerator<int>(); } ) );
		cl.def( pybind11::init( [](bitpit::IndexGenerator<int> const &o){ return new bitpit::IndexGenerator<int>(o); } ) );
		cl.def("generate", (int (bitpit::IndexGenerator<int>::*)()) &bitpit::IndexGenerator<int>::generate, "C++: bitpit::IndexGenerator<int>::generate() --> int");
		cl.def("isAssigned", (bool (bitpit::IndexGenerator<int>::*)(int)) &bitpit::IndexGenerator<int>::isAssigned, "C++: bitpit::IndexGenerator<int>::isAssigned(int) --> bool", pybind11::arg("id"));
		cl.def("setAssigned", (void (bitpit::IndexGenerator<int>::*)(int)) &bitpit::IndexGenerator<int>::setAssigned, "C++: bitpit::IndexGenerator<int>::setAssigned(int) --> void", pybind11::arg("id"));
		cl.def("trash", (void (bitpit::IndexGenerator<int>::*)(int)) &bitpit::IndexGenerator<int>::trash, "C++: bitpit::IndexGenerator<int>::trash(int) --> void", pybind11::arg("id"));
		cl.def("getLatest", (int (bitpit::IndexGenerator<int>::*)()) &bitpit::IndexGenerator<int>::getLatest, "C++: bitpit::IndexGenerator<int>::getLatest() --> int");
		cl.def("getHighest", (int (bitpit::IndexGenerator<int>::*)()) &bitpit::IndexGenerator<int>::getHighest, "C++: bitpit::IndexGenerator<int>::getHighest() --> int");
		cl.def("reset", (void (bitpit::IndexGenerator<int>::*)()) &bitpit::IndexGenerator<int>::reset, "C++: bitpit::IndexGenerator<int>::reset() --> void");
		cl.def("dump", (void (bitpit::IndexGenerator<int>::*)(std::ostream &) const) &bitpit::IndexGenerator<int>::dump, "C++: bitpit::IndexGenerator<int>::dump(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("restore", (void (bitpit::IndexGenerator<int>::*)(std::istream &)) &bitpit::IndexGenerator<int>::restore, "C++: bitpit::IndexGenerator<int>::restore(std::istream &) --> void", pybind11::arg("stream"));
	}
	std::cout << "B534_[bitpit::IndexGenerator<long>] ";
	std::cout << "B535_[bitpit::FileHandler] ";
}
#include <Octant.hpp>
#include <PabloUniform.hpp>
#include <adaption.hpp>
#include <array>
#include <cell.hpp>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <levelSet.hpp>
#include <levelSetCommon.hpp>
#include <memory>
#include <ostream>
#include <patch_kernel.hpp>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <surface_kernel.hpp>
#include <vector>
#include <vertex.hpp>
#include <voloctree.hpp>
#include <volume_kernel.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_levelSetCommon(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B622_[bitpit::LevelSetInfo] ";
	{ // bitpit::LevelSetInfo file:levelSetCommon.hpp line:50
		pybind11::class_<bitpit::LevelSetInfo, std::shared_ptr<bitpit::LevelSetInfo>> cl(M("bitpit"), "LevelSetInfo", "");
		cl.def( pybind11::init( [](){ return new bitpit::LevelSetInfo(); } ) );
		cl.def( pybind11::init<double, const struct std::array<double, 3> &>(), pybind11::arg(""), pybind11::arg("") );

		cl.def_readwrite("value", &bitpit::LevelSetInfo::value);
		cl.def_readwrite("gradient", &bitpit::LevelSetInfo::gradient);
	}
	std::cout << "B623_[bitpit::LevelSetBooleanOperation] ";
	// bitpit::LevelSetBooleanOperation file:levelSetCommon.hpp line:62
	pybind11::enum_<bitpit::LevelSetBooleanOperation>(M("bitpit"), "LevelSetBooleanOperation", "Enum class defining different boolean operations")
		.value("UNION", bitpit::LevelSetBooleanOperation::UNION)
		.value("INTERSECTION", bitpit::LevelSetBooleanOperation::INTERSECTION)
		.value("SUBTRACTION", bitpit::LevelSetBooleanOperation::SUBTRACTION);

;

	std::cout << "B624_[bitpit::LevelSetIntersectionStatus] ";
	// bitpit::LevelSetIntersectionStatus file:levelSetCommon.hpp line:73
	pybind11::enum_<bitpit::LevelSetIntersectionStatus>(M("bitpit"), "LevelSetIntersectionStatus", "Enum class defining the intersection between a cell and the zero levelset.\n For details see LevelSetObject::intersectSurface()")
		.value("FALSE", bitpit::LevelSetIntersectionStatus::FALSE)
		.value("TRUE", bitpit::LevelSetIntersectionStatus::TRUE)
		.value("CLOSE", bitpit::LevelSetIntersectionStatus::CLOSE);

;

	std::cout << "B625_[bitpit::LevelSetIntersectionMode] ";
	// bitpit::LevelSetIntersectionMode file:levelSetCommon.hpp line:84
	pybind11::enum_<bitpit::LevelSetIntersectionMode>(M("bitpit"), "LevelSetIntersectionMode", "Enum class describing the how the intersection between cell and zero levelset \n should be computed")
		.value("FAST_FUZZY", bitpit::LevelSetIntersectionMode::FAST_FUZZY)
		.value("FAST_GUARANTEE_TRUE", bitpit::LevelSetIntersectionMode::FAST_GUARANTEE_TRUE)
		.value("FAST_GUARANTEE_FALSE", bitpit::LevelSetIntersectionMode::FAST_GUARANTEE_FALSE)
		.value("ACCURATE", bitpit::LevelSetIntersectionMode::ACCURATE);

;

	std::cout << "B626_[bitpit::LevelSetWriteField] ";
	// bitpit::LevelSetWriteField file:levelSetCommon.hpp line:95
	pybind11::enum_<bitpit::LevelSetWriteField>(M("bitpit"), "LevelSetWriteField", "Enum class containing the possible fields to be added to the VTK file")
		.value("VALUE", bitpit::LevelSetWriteField::VALUE)
		.value("GRADIENT", bitpit::LevelSetWriteField::GRADIENT)
		.value("NORMAL", bitpit::LevelSetWriteField::NORMAL)
		.value("PART", bitpit::LevelSetWriteField::PART)
		.value("ALL", bitpit::LevelSetWriteField::ALL)
		.value("DEFAULT", bitpit::LevelSetWriteField::DEFAULT);

;

	std::cout << "B627_[bitpit::LevelSet] ";
	{ // bitpit::LevelSet file:levelSet.hpp line:51
		pybind11::class_<bitpit::LevelSet, std::shared_ptr<bitpit::LevelSet>> cl(M("bitpit"), "LevelSet", "");
		cl.def( pybind11::init( [](){ return new bitpit::LevelSet(); } ) );
		cl.def("clear", (void (bitpit::LevelSet::*)()) &bitpit::LevelSet::clear, "C++: bitpit::LevelSet::clear() --> void");
		cl.def("setMesh", (void (bitpit::LevelSet::*)(class bitpit::VolumeKernel *)) &bitpit::LevelSet::setMesh, "C++: bitpit::LevelSet::setMesh(class bitpit::VolumeKernel *) --> void", pybind11::arg(""));
		cl.def("setMesh", (void (bitpit::LevelSet::*)(class bitpit::VolOctree *)) &bitpit::LevelSet::setMesh, "C++: bitpit::LevelSet::setMesh(class bitpit::VolOctree *) --> void", pybind11::arg(""));
		cl.def("addObject", [](bitpit::LevelSet &o, enum bitpit::LevelSetBooleanOperation const & a0, int const & a1, int const & a2) -> int { return o.addObject(a0, a1, a2); }, "", pybind11::arg(""), pybind11::arg(""), pybind11::arg(""));
		cl.def("addObject", (int (bitpit::LevelSet::*)(enum bitpit::LevelSetBooleanOperation, int, int, int)) &bitpit::LevelSet::addObject, "C++: bitpit::LevelSet::addObject(enum bitpit::LevelSetBooleanOperation, int, int, int) --> int", pybind11::arg(""), pybind11::arg(""), pybind11::arg(""), pybind11::arg("id"));
		cl.def("addObject", [](bitpit::LevelSet &o, enum bitpit::LevelSetBooleanOperation const & a0, const class std::vector<int, class std::allocator<int> > & a1) -> int { return o.addObject(a0, a1); }, "", pybind11::arg(""), pybind11::arg(""));
		cl.def("addObject", (int (bitpit::LevelSet::*)(enum bitpit::LevelSetBooleanOperation, const class std::vector<int, class std::allocator<int> > &, int)) &bitpit::LevelSet::addObject, "C++: bitpit::LevelSet::addObject(enum bitpit::LevelSetBooleanOperation, const class std::vector<int, class std::allocator<int> > &, int) --> int", pybind11::arg(""), pybind11::arg(""), pybind11::arg("id"));
		cl.def("addObject", [](bitpit::LevelSet &o, const class std::vector<long, class std::allocator<long> > & a0, long const & a1, bool const & a2) -> int { return o.addObject(a0, a1, a2); }, "", pybind11::arg(""), pybind11::arg(""), pybind11::arg(""));
		cl.def("addObject", (int (bitpit::LevelSet::*)(const class std::vector<long, class std::allocator<long> > &, long, bool, int)) &bitpit::LevelSet::addObject, "C++: bitpit::LevelSet::addObject(const class std::vector<long, class std::allocator<long> > &, long, bool, int) --> int", pybind11::arg(""), pybind11::arg(""), pybind11::arg(""), pybind11::arg("id"));
		cl.def("removeObjects", (void (bitpit::LevelSet::*)()) &bitpit::LevelSet::removeObjects, "C++: bitpit::LevelSet::removeObjects() --> void");
		cl.def("removeObject", (bool (bitpit::LevelSet::*)(int)) &bitpit::LevelSet::removeObject, "C++: bitpit::LevelSet::removeObject(int) --> bool", pybind11::arg(""));
		cl.def("getObjectCount", (int (bitpit::LevelSet::*)() const) &bitpit::LevelSet::getObjectCount, "C++: bitpit::LevelSet::getObjectCount() const --> int");
		cl.def("getObjectIds", (class std::vector<int, class std::allocator<int> > (bitpit::LevelSet::*)() const) &bitpit::LevelSet::getObjectIds, "C++: bitpit::LevelSet::getObjectIds() const --> class std::vector<int, class std::allocator<int> >");
		cl.def("setSizeNarrowBand", (void (bitpit::LevelSet::*)(double)) &bitpit::LevelSet::setSizeNarrowBand, "C++: bitpit::LevelSet::setSizeNarrowBand(double) --> void", pybind11::arg(""));
		cl.def("getSizeNarrowBand", (double (bitpit::LevelSet::*)() const) &bitpit::LevelSet::getSizeNarrowBand, "C++: bitpit::LevelSet::getSizeNarrowBand() const --> double");
		cl.def("setSign", (void (bitpit::LevelSet::*)(bool)) &bitpit::LevelSet::setSign, "C++: bitpit::LevelSet::setSign(bool) --> void", pybind11::arg(""));
		cl.def("setPropagateSign", (void (bitpit::LevelSet::*)(bool)) &bitpit::LevelSet::setPropagateSign, "C++: bitpit::LevelSet::setPropagateSign(bool) --> void", pybind11::arg(""));
		cl.def("dump", (void (bitpit::LevelSet::*)(std::ostream &)) &bitpit::LevelSet::dump, "C++: bitpit::LevelSet::dump(std::ostream &) --> void", pybind11::arg(""));
		cl.def("restore", (void (bitpit::LevelSet::*)(std::istream &)) &bitpit::LevelSet::restore, "C++: bitpit::LevelSet::restore(std::istream &) --> void", pybind11::arg(""));
		cl.def("compute", (void (bitpit::LevelSet::*)()) &bitpit::LevelSet::compute, "C++: bitpit::LevelSet::compute() --> void");
		cl.def("update", (void (bitpit::LevelSet::*)(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &)) &bitpit::LevelSet::update, "C++: bitpit::LevelSet::update(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &) --> void", pybind11::arg(""));
		cl.def("partition", (void (bitpit::LevelSet::*)(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &)) &bitpit::LevelSet::partition, "C++: bitpit::LevelSet::partition(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &) --> void", pybind11::arg(""));
	}
}
#include <iostream>
#include <morton.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_morton(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B571_[unsigned long bitpit::PABLO::splitBy3(unsigned int)] ";
	// bitpit::PABLO::splitBy3(unsigned int) file:morton.hpp line:45
	M("bitpit::PABLO").def("splitBy3", (unsigned long (*)(unsigned int)) &bitpit::PABLO::splitBy3, "Seperate bits from a given integer 3 positions apart.\n\n The function comes from libmorton (see https://github.com/Forceflow/libmorton).\n\n \n is an integer position\n \n\n Separated bits.\n\nC++: bitpit::PABLO::splitBy3(unsigned int) --> unsigned long", pybind11::arg("a"));

	std::cout << "B572_[unsigned long bitpit::PABLO::splitBy2(unsigned int)] ";
	// bitpit::PABLO::splitBy2(unsigned int) file:morton.hpp line:65
	M("bitpit::PABLO").def("splitBy2", (unsigned long (*)(unsigned int)) &bitpit::PABLO::splitBy2, "Seperate bits from a given integer 2 positions apart.\n\n The function comes from libmorton (see https://github.com/Forceflow/libmorton).\n\n \n is an integer position\n \n\n Separated bits.\n\nC++: bitpit::PABLO::splitBy2(unsigned int) --> unsigned long", pybind11::arg("a"));

	std::cout << "B573_[unsigned long bitpit::PABLO::computeMorton(unsigned int, unsigned int, unsigned int)] ";
	// bitpit::PABLO::computeMorton(unsigned int, unsigned int, unsigned int) file:morton.hpp line:88
	M("bitpit::PABLO").def("computeMorton", (unsigned long (*)(unsigned int, unsigned int, unsigned int)) &bitpit::PABLO::computeMorton, "Compute the Morton number of the given set of coordinates.\n\n The function uses the \"magic bits\" algorithm of the libmorton library\n (see https://github.com/Forceflow/libmorton).\n\n \n is the integer x position\n \n\n is the integer y position\n \n\n is the integer z position\n \n\n The Morton number.\n\nC++: bitpit::PABLO::computeMorton(unsigned int, unsigned int, unsigned int) --> unsigned long", pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"));

	std::cout << "B574_[unsigned long bitpit::PABLO::computeMorton(unsigned int, unsigned int)] ";
	// bitpit::PABLO::computeMorton(unsigned int, unsigned int) file:morton.hpp line:105
	M("bitpit::PABLO").def("computeMorton", (unsigned long (*)(unsigned int, unsigned int)) &bitpit::PABLO::computeMorton, "Compute the Morton number of the given set of coordinates.\n\n The function uses the \"magic bits\" algorithm of the libmorton library\n (see https://github.com/Forceflow/libmorton).\n\n \n is the integer x position\n \n\n is the integer y position\n \n\n The Morton number.\n\nC++: bitpit::PABLO::computeMorton(unsigned int, unsigned int) --> unsigned long", pybind11::arg("x"), pybind11::arg("y"));

	std::cout << "B575_[unsigned long bitpit::PABLO::computeXYZKey(unsigned long, unsigned long, unsigned long, signed char)] ";
	// bitpit::PABLO::computeXYZKey(unsigned long, unsigned long, unsigned long, signed char) file:morton.hpp line:121
	M("bitpit::PABLO").def("computeXYZKey", (unsigned long (*)(unsigned long, unsigned long, unsigned long, signed char)) &bitpit::PABLO::computeXYZKey, "Compute the XYZ key of the given set of coordinates.\n\n \n is the integer x position\n \n\n is the integer y position\n \n\n is the integer z position\n \n\n is the Maximum allowed refinement level of octree\n \n\n The Morton number.\n\nC++: bitpit::PABLO::computeXYZKey(unsigned long, unsigned long, unsigned long, signed char) --> unsigned long", pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"), pybind11::arg("maxLevel"));

	std::cout << "B576_[unsigned long bitpit::PABLO::computeXYZKey(unsigned long, unsigned long, signed char)] ";
	// bitpit::PABLO::computeXYZKey(unsigned long, unsigned long, signed char) file:morton.hpp line:136
	M("bitpit::PABLO").def("computeXYZKey", (unsigned long (*)(unsigned long, unsigned long, signed char)) &bitpit::PABLO::computeXYZKey, "Compute the XYZ key number of the given set of coordinates.\n\n \n is the integer x position\n \n\n is the integer y position\n \n\n is the Maximum allowed refinement level of octree\n \n\n The Morton number.\n\nC++: bitpit::PABLO::computeXYZKey(unsigned long, unsigned long, signed char) --> unsigned long", pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("maxLevel"));

}
#include <VTK.hpp>
#include <adaption.hpp>
#include <array>
#include <cell.hpp>
#include <element.hpp>
#include <element_type.hpp>
#include <fstream>
#include <functional>
#include <interface.hpp>
#include <iostream>
#include <istream>
#include <iterator>
#include <memory>
#include <ostream>
#include <patch_info.hpp>
#include <patch_kernel.hpp>
#include <piercedStorageIterator.hpp>
#include <set>
#include <sstream> // __str__
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
#include <vertex.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// bitpit::PatchInfo file:patch_info.hpp line:35
struct PyCallBack_bitpit_PatchInfo : public bitpit::PatchInfo {
	using bitpit::PatchInfo::PatchInfo;

	void _init() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::PatchInfo *>(this), "_init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"PatchInfo::_init\"");
	}
	void _reset() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::PatchInfo *>(this), "_reset");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"PatchInfo::_reset\"");
	}
	void _extract() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::PatchInfo *>(this), "_extract");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"PatchInfo::_extract\"");
	}
};

// bitpit::PatchNumberingInfo file:patch_info.hpp line:60
struct PyCallBack_bitpit_PatchNumberingInfo : public bitpit::PatchNumberingInfo {
	using bitpit::PatchNumberingInfo::PatchNumberingInfo;

	void _init() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::PatchNumberingInfo *>(this), "_init");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchNumberingInfo::_init();
	}
	void _reset() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::PatchNumberingInfo *>(this), "_reset");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchNumberingInfo::_reset();
	}
	void _extract() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::PatchNumberingInfo *>(this), "_extract");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchNumberingInfo::_extract();
	}
};

void bind_patch_info(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B582_[bitpit::PatchInfo] ";
	{ // bitpit::PatchInfo file:patch_info.hpp line:35
		pybind11::class_<bitpit::PatchInfo, std::shared_ptr<bitpit::PatchInfo>, PyCallBack_bitpit_PatchInfo> cl(M("bitpit"), "PatchInfo", "");
		cl.def(pybind11::init<PyCallBack_bitpit_PatchInfo const &>());
		cl.def("reset", (void (bitpit::PatchInfo::*)()) &bitpit::PatchInfo::reset, "C++: bitpit::PatchInfo::reset() --> void");
		cl.def("extract", (void (bitpit::PatchInfo::*)()) &bitpit::PatchInfo::extract, "C++: bitpit::PatchInfo::extract() --> void");
		cl.def("update", (void (bitpit::PatchInfo::*)()) &bitpit::PatchInfo::update, "C++: bitpit::PatchInfo::update() --> void");
		cl.def("assign", (class bitpit::PatchInfo & (bitpit::PatchInfo::*)(const class bitpit::PatchInfo &)) &bitpit::PatchInfo::operator=, "C++: bitpit::PatchInfo::operator=(const class bitpit::PatchInfo &) --> class bitpit::PatchInfo &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B583_[bitpit::PatchNumberingInfo] ";
	{ // bitpit::PatchNumberingInfo file:patch_info.hpp line:60
		pybind11::class_<bitpit::PatchNumberingInfo, std::shared_ptr<bitpit::PatchNumberingInfo>, PyCallBack_bitpit_PatchNumberingInfo, bitpit::PatchInfo> cl(M("bitpit"), "PatchNumberingInfo", "");
		cl.def( pybind11::init( [](PyCallBack_bitpit_PatchNumberingInfo const &o){ return new PyCallBack_bitpit_PatchNumberingInfo(o); } ) );
		cl.def( pybind11::init( [](bitpit::PatchNumberingInfo const &o){ return new bitpit::PatchNumberingInfo(o); } ) );
		cl.def("getCellConsecutiveOffset", (long (bitpit::PatchNumberingInfo::*)() const) &bitpit::PatchNumberingInfo::getCellConsecutiveOffset, "C++: bitpit::PatchNumberingInfo::getCellConsecutiveOffset() const --> long");
		cl.def("getCellConsecutiveId", (long (bitpit::PatchNumberingInfo::*)(long) const) &bitpit::PatchNumberingInfo::getCellConsecutiveId, "C++: bitpit::PatchNumberingInfo::getCellConsecutiveId(long) const --> long", pybind11::arg("id"));
		cl.def("getCellConsecutiveMap", (const class std::unordered_map<long, long, struct std::hash<long>, struct std::equal_to<long>, class std::allocator<struct std::pair<const long, long> > > & (bitpit::PatchNumberingInfo::*)() const) &bitpit::PatchNumberingInfo::getCellConsecutiveMap, "C++: bitpit::PatchNumberingInfo::getCellConsecutiveMap() const --> const class std::unordered_map<long, long, struct std::hash<long>, struct std::equal_to<long>, class std::allocator<struct std::pair<const long, long> > > &", pybind11::return_value_policy::automatic);
		cl.def("getCellGlobalCount", (long (bitpit::PatchNumberingInfo::*)() const) &bitpit::PatchNumberingInfo::getCellGlobalCount, "C++: bitpit::PatchNumberingInfo::getCellGlobalCount() const --> long");
		cl.def("getCellGlobalCountOffset", (long (bitpit::PatchNumberingInfo::*)() const) &bitpit::PatchNumberingInfo::getCellGlobalCountOffset, "C++: bitpit::PatchNumberingInfo::getCellGlobalCountOffset() const --> long");
		cl.def("getCellGlobalCountOffset", (long (bitpit::PatchNumberingInfo::*)(int) const) &bitpit::PatchNumberingInfo::getCellGlobalCountOffset, "C++: bitpit::PatchNumberingInfo::getCellGlobalCountOffset(int) const --> long", pybind11::arg("rank"));
		cl.def("getCellGlobalId", (long (bitpit::PatchNumberingInfo::*)(long) const) &bitpit::PatchNumberingInfo::getCellGlobalId, "C++: bitpit::PatchNumberingInfo::getCellGlobalId(long) const --> long", pybind11::arg("id"));
		cl.def("getCellGlobalMap", (const class std::unordered_map<long, long, struct std::hash<long>, struct std::equal_to<long>, class std::allocator<struct std::pair<const long, long> > > & (bitpit::PatchNumberingInfo::*)() const) &bitpit::PatchNumberingInfo::getCellGlobalMap, "C++: bitpit::PatchNumberingInfo::getCellGlobalMap() const --> const class std::unordered_map<long, long, struct std::hash<long>, struct std::equal_to<long>, class std::allocator<struct std::pair<const long, long> > > &", pybind11::return_value_policy::automatic);
		cl.def("getCellRankFromLocal", (int (bitpit::PatchNumberingInfo::*)(long) const) &bitpit::PatchNumberingInfo::getCellRankFromLocal, "C++: bitpit::PatchNumberingInfo::getCellRankFromLocal(long) const --> int", pybind11::arg("id"));
		cl.def("getCellRankFromConsecutive", (int (bitpit::PatchNumberingInfo::*)(long) const) &bitpit::PatchNumberingInfo::getCellRankFromConsecutive, "C++: bitpit::PatchNumberingInfo::getCellRankFromConsecutive(long) const --> int", pybind11::arg("id"));
		cl.def("getCellRankFromGlobal", (int (bitpit::PatchNumberingInfo::*)(long) const) &bitpit::PatchNumberingInfo::getCellRankFromGlobal, "C++: bitpit::PatchNumberingInfo::getCellRankFromGlobal(long) const --> int", pybind11::arg("id"));
		cl.def("assign", (class bitpit::PatchNumberingInfo & (bitpit::PatchNumberingInfo::*)(const class bitpit::PatchNumberingInfo &)) &bitpit::PatchNumberingInfo::operator=, "C++: bitpit::PatchNumberingInfo::operator=(const class bitpit::PatchNumberingInfo &) --> class bitpit::PatchNumberingInfo &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
}
#include <VTK.hpp>
#include <adaption.hpp>
#include <array>
#include <cell.hpp>
#include <element.hpp>
#include <element_type.hpp>
#include <fstream>
#include <functional>
#include <interface.hpp>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <memory>
#include <ostream>
#include <patch_kernel.hpp>
#include <patch_manager.hpp>
#include <piercedStorageIterator.hpp>
#include <set>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
#include <vertex.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_patch_manager(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B597_[bitpit::PatchManager] ";
	{ // bitpit::PatchManager file:patch_manager.hpp line:36
		pybind11::class_<bitpit::PatchManager, std::shared_ptr<bitpit::PatchManager>> cl(M("bitpit"), "PatchManager", "");
		cl.def_static("manager", (class bitpit::PatchManager & (*)()) &bitpit::PatchManager::manager, "C++: bitpit::PatchManager::manager() --> class bitpit::PatchManager &", pybind11::return_value_policy::automatic);
		cl.def("dump", (void (bitpit::PatchManager::*)(std::ostream &)) &bitpit::PatchManager::dump, "C++: bitpit::PatchManager::dump(std::ostream &) --> void", pybind11::arg("stream"));
		cl.def("restore", (void (bitpit::PatchManager::*)(std::istream &)) &bitpit::PatchManager::restore, "C++: bitpit::PatchManager::restore(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dumpAll", (void (bitpit::PatchManager::*)(std::ostream &)) &bitpit::PatchManager::dumpAll, "C++: bitpit::PatchManager::dumpAll(std::ostream &) --> void", pybind11::arg("stream"));
		cl.def("restoreAll", (void (bitpit::PatchManager::*)(std::istream &)) &bitpit::PatchManager::restoreAll, "C++: bitpit::PatchManager::restoreAll(std::istream &) --> void", pybind11::arg("stream"));
	}
}
#include <VTK.hpp>
#include <adaption.hpp>
#include <array>
#include <cell.hpp>
#include <element.hpp>
#include <element_type.hpp>
#include <fstream>
#include <functional>
#include <interface.hpp>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <memory>
#include <ostream>
#include <patch_kernel.hpp>
#include <patch_manager.hpp>
#include <piercedStorageIterator.hpp>
#include <set>
#include <streambuf>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
#include <vertex.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_patch_manager_1(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B598_[class bitpit::PatchManager & bitpit::patch::manager()] ";
	// bitpit::patch::manager() file:patch_manager.hpp line:79
	M("bitpit::patch").def("manager", (class bitpit::PatchManager & (*)()) &bitpit::patch::manager, "C++: bitpit::patch::manager() --> class bitpit::PatchManager &", pybind11::return_value_policy::automatic);

}
#include <VTK.hpp>
#include <adaption.hpp>
#include <array>
#include <cell.hpp>
#include <element.hpp>
#include <element_type.hpp>
#include <fstream>
#include <functional>
#include <interface.hpp>
#include <iostream>
#include <istream>
#include <iterator>
#include <memory>
#include <ostream>
#include <patch_kernel.hpp>
#include <patch_skd_tree.hpp>
#include <piercedStorageIterator.hpp>
#include <set>
#include <sstream> // __str__
#include <string>
#include <surface_kernel.hpp>
#include <surface_skd_tree.hpp>
#include <unordered_map>
#include <utility>
#include <vector>
#include <vertex.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_patch_skd_tree(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B599_[bitpit::SkdPatchInfo] ";
	{ // bitpit::SkdPatchInfo file:patch_skd_tree.hpp line:34
		pybind11::class_<bitpit::SkdPatchInfo, std::shared_ptr<bitpit::SkdPatchInfo>> cl(M("bitpit"), "SkdPatchInfo", "");
		cl.def("buildCache", (void (bitpit::SkdPatchInfo::*)()) &bitpit::SkdPatchInfo::buildCache, "C++: bitpit::SkdPatchInfo::buildCache() --> void");
		cl.def("destroyCache", (void (bitpit::SkdPatchInfo::*)()) &bitpit::SkdPatchInfo::destroyCache, "C++: bitpit::SkdPatchInfo::destroyCache() --> void");
		cl.def("getCellRawIds", (const class std::vector<unsigned long, class std::allocator<unsigned long> > & (bitpit::SkdPatchInfo::*)() const) &bitpit::SkdPatchInfo::getCellRawIds, "C++: bitpit::SkdPatchInfo::getCellRawIds() const --> const class std::vector<unsigned long, class std::allocator<unsigned long> > &", pybind11::return_value_policy::automatic);
		cl.def("getCellRawId", (unsigned long (bitpit::SkdPatchInfo::*)(unsigned long) const) &bitpit::SkdPatchInfo::getCellRawId, "C++: bitpit::SkdPatchInfo::getCellRawId(unsigned long) const --> unsigned long", pybind11::arg("n"));
		cl.def("getCachedCentroid", (const struct std::array<double, 3> & (bitpit::SkdPatchInfo::*)(unsigned long) const) &bitpit::SkdPatchInfo::getCachedCentroid, "C++: bitpit::SkdPatchInfo::getCachedCentroid(unsigned long) const --> const struct std::array<double, 3> &", pybind11::return_value_policy::automatic, pybind11::arg("rawId"));
		cl.def("getCachedBoxMin", (const struct std::array<double, 3> & (bitpit::SkdPatchInfo::*)(unsigned long) const) &bitpit::SkdPatchInfo::getCachedBoxMin, "C++: bitpit::SkdPatchInfo::getCachedBoxMin(unsigned long) const --> const struct std::array<double, 3> &", pybind11::return_value_policy::automatic, pybind11::arg("rawId"));
		cl.def("getCachedBoxMax", (const struct std::array<double, 3> & (bitpit::SkdPatchInfo::*)(unsigned long) const) &bitpit::SkdPatchInfo::getCachedBoxMax, "C++: bitpit::SkdPatchInfo::getCachedBoxMax(unsigned long) const --> const struct std::array<double, 3> &", pybind11::return_value_policy::automatic, pybind11::arg("rawId"));
		cl.def("evalCachedBoxMean", (struct std::array<double, 3> (bitpit::SkdPatchInfo::*)(unsigned long) const) &bitpit::SkdPatchInfo::evalCachedBoxMean, "C++: bitpit::SkdPatchInfo::evalCachedBoxMean(unsigned long) const --> struct std::array<double, 3>", pybind11::arg("rawId"));
	}
	std::cout << "B600_[bitpit::SkdBox] ";
	{ // bitpit::SkdBox file:patch_skd_tree.hpp line:67
		pybind11::class_<bitpit::SkdBox, std::shared_ptr<bitpit::SkdBox>> cl(M("bitpit"), "SkdBox", "");
		cl.def( pybind11::init( [](){ return new bitpit::SkdBox(); } ) );

		cl.def("getBoxMin", (const struct std::array<double, 3> & (bitpit::SkdBox::*)() const) &bitpit::SkdBox::getBoxMin, "C++: bitpit::SkdBox::getBoxMin() const --> const struct std::array<double, 3> &", pybind11::return_value_policy::automatic);
		cl.def("getBoxMax", (const struct std::array<double, 3> & (bitpit::SkdBox::*)() const) &bitpit::SkdBox::getBoxMax, "C++: bitpit::SkdBox::getBoxMax() const --> const struct std::array<double, 3> &", pybind11::return_value_policy::automatic);
		cl.def("evalPointMinDistance", (double (bitpit::SkdBox::*)(const struct std::array<double, 3> &) const) &bitpit::SkdBox::evalPointMinDistance, "C++: bitpit::SkdBox::evalPointMinDistance(const struct std::array<double, 3> &) const --> double", pybind11::arg("point"));
		cl.def("evalPointMaxDistance", (double (bitpit::SkdBox::*)(const struct std::array<double, 3> &) const) &bitpit::SkdBox::evalPointMaxDistance, "C++: bitpit::SkdBox::evalPointMaxDistance(const struct std::array<double, 3> &) const --> double", pybind11::arg("point"));
		cl.def("boxContainsPoint", (bool (bitpit::SkdBox::*)(const struct std::array<double, 3> &, double) const) &bitpit::SkdBox::boxContainsPoint, "C++: bitpit::SkdBox::boxContainsPoint(const struct std::array<double, 3> &, double) const --> bool", pybind11::arg("point"), pybind11::arg("offset"));
		cl.def("boxIntersectsSphere", (bool (bitpit::SkdBox::*)(const struct std::array<double, 3> &, double) const) &bitpit::SkdBox::boxIntersectsSphere, "C++: bitpit::SkdBox::boxIntersectsSphere(const struct std::array<double, 3> &, double) const --> bool", pybind11::arg("center"), pybind11::arg("radius"));
	}
	std::cout << "B601_[bitpit::SkdNode] ";
	{ // bitpit::SkdNode file:patch_skd_tree.hpp line:88
		pybind11::class_<bitpit::SkdNode, std::shared_ptr<bitpit::SkdNode>, bitpit::SkdBox> cl(M("bitpit"), "SkdNode", "");

		pybind11::enum_<bitpit::SkdNode::ChildLocation>(cl, "ChildLocation", pybind11::arithmetic(), "")
			.value("CHILD_LEFT", bitpit::SkdNode::CHILD_LEFT)
			.value("CHILD_RIGHT", bitpit::SkdNode::CHILD_RIGHT)
			.value("CHILD_BEGIN", bitpit::SkdNode::CHILD_BEGIN)
			.value("CHILD_END", bitpit::SkdNode::CHILD_END)
			.export_values();

		cl.def("getCellCount", (unsigned long (bitpit::SkdNode::*)() const) &bitpit::SkdNode::getCellCount, "C++: bitpit::SkdNode::getCellCount() const --> unsigned long");
		cl.def("getCells", (class std::vector<long, class std::allocator<long> > (bitpit::SkdNode::*)() const) &bitpit::SkdNode::getCells, "C++: bitpit::SkdNode::getCells() const --> class std::vector<long, class std::allocator<long> >");
		cl.def("getCell", (long (bitpit::SkdNode::*)(unsigned long) const) &bitpit::SkdNode::getCell, "C++: bitpit::SkdNode::getCell(unsigned long) const --> long", pybind11::arg("n"));
		cl.def("getBoundingBox", (const class bitpit::SkdBox & (bitpit::SkdNode::*)() const) &bitpit::SkdNode::getBoundingBox, "C++: bitpit::SkdNode::getBoundingBox() const --> const class bitpit::SkdBox &", pybind11::return_value_policy::automatic);
		cl.def("evalBoxWeightedMean", (struct std::array<double, 3> (bitpit::SkdNode::*)() const) &bitpit::SkdNode::evalBoxWeightedMean, "C++: bitpit::SkdNode::evalBoxWeightedMean() const --> struct std::array<double, 3>");
		cl.def("isLeaf", (bool (bitpit::SkdNode::*)() const) &bitpit::SkdNode::isLeaf, "C++: bitpit::SkdNode::isLeaf() const --> bool");
		cl.def("hasChild", (bool (bitpit::SkdNode::*)(enum bitpit::SkdNode::ChildLocation) const) &bitpit::SkdNode::hasChild, "C++: bitpit::SkdNode::hasChild(enum bitpit::SkdNode::ChildLocation) const --> bool", pybind11::arg("child"));
		cl.def("getChildId", (unsigned long (bitpit::SkdNode::*)(enum bitpit::SkdNode::ChildLocation) const) &bitpit::SkdNode::getChildId, "C++: bitpit::SkdNode::getChildId(enum bitpit::SkdNode::ChildLocation) const --> unsigned long", pybind11::arg("child"));
		cl.def("evalPointDistance", (double (bitpit::SkdNode::*)(const struct std::array<double, 3> &, bool) const) &bitpit::SkdNode::evalPointDistance, "C++: bitpit::SkdNode::evalPointDistance(const struct std::array<double, 3> &, bool) const --> double", pybind11::arg("point"), pybind11::arg("ignoreGhosts"));
		cl.def("findPointClosestCell", (void (bitpit::SkdNode::*)(const struct std::array<double, 3> &, bool, long *, double *) const) &bitpit::SkdNode::findPointClosestCell, "C++: bitpit::SkdNode::findPointClosestCell(const struct std::array<double, 3> &, bool, long *, double *) const --> void", pybind11::arg("point"), pybind11::arg("ignoreGhosts"), pybind11::arg("id"), pybind11::arg("distance"));
		cl.def("updatePointClosestCell", (void (bitpit::SkdNode::*)(const struct std::array<double, 3> &, bool, long *, double *) const) &bitpit::SkdNode::updatePointClosestCell, "C++: bitpit::SkdNode::updatePointClosestCell(const struct std::array<double, 3> &, bool, long *, double *) const --> void", pybind11::arg("point"), pybind11::arg("ignoreGhosts"), pybind11::arg("id"), pybind11::arg("distance"));
	}
	std::cout << "B602_[bitpit::SkdGlobalCellDistance] ";
	{ // bitpit::SkdGlobalCellDistance file:patch_skd_tree.hpp line:154
		pybind11::class_<bitpit::SkdGlobalCellDistance, std::shared_ptr<bitpit::SkdGlobalCellDistance>> cl(M("bitpit"), "SkdGlobalCellDistance", "");
		cl.def( pybind11::init( [](){ return new bitpit::SkdGlobalCellDistance(); } ) );
		cl.def("getRank", (int & (bitpit::SkdGlobalCellDistance::*)()) &bitpit::SkdGlobalCellDistance::getRank, "C++: bitpit::SkdGlobalCellDistance::getRank() --> int &", pybind11::return_value_policy::automatic);
		cl.def("getId", (long & (bitpit::SkdGlobalCellDistance::*)()) &bitpit::SkdGlobalCellDistance::getId, "C++: bitpit::SkdGlobalCellDistance::getId() --> long &", pybind11::return_value_policy::automatic);
		cl.def("getDistance", (double & (bitpit::SkdGlobalCellDistance::*)()) &bitpit::SkdGlobalCellDistance::getDistance, "C++: bitpit::SkdGlobalCellDistance::getDistance() --> double &", pybind11::return_value_policy::automatic);
		cl.def("exportData", (void (bitpit::SkdGlobalCellDistance::*)(int *, long *, double *) const) &bitpit::SkdGlobalCellDistance::exportData, "C++: bitpit::SkdGlobalCellDistance::exportData(int *, long *, double *) const --> void", pybind11::arg("rank"), pybind11::arg("id"), pybind11::arg("distance"));
	}
	std::cout << "B603_[bitpit::PatchSkdTree] ";
	{ // bitpit::PatchSkdTree file:patch_skd_tree.hpp line:181
		pybind11::class_<bitpit::PatchSkdTree, std::shared_ptr<bitpit::PatchSkdTree>> cl(M("bitpit"), "PatchSkdTree", "");
		cl.def("build", [](bitpit::PatchSkdTree &o) -> void { return o.build(); }, "");
		cl.def("build", [](bitpit::PatchSkdTree &o, unsigned long const & a0) -> void { return o.build(a0); }, "", pybind11::arg("leaftThreshold"));
		cl.def("build", (void (bitpit::PatchSkdTree::*)(unsigned long, bool)) &bitpit::PatchSkdTree::build, "C++: bitpit::PatchSkdTree::build(unsigned long, bool) --> void", pybind11::arg("leaftThreshold"), pybind11::arg("squeezeStorage"));
		cl.def("clear", [](bitpit::PatchSkdTree &o) -> void { return o.clear(); }, "");
		cl.def("clear", (void (bitpit::PatchSkdTree::*)(bool)) &bitpit::PatchSkdTree::clear, "C++: bitpit::PatchSkdTree::clear(bool) --> void", pybind11::arg("release"));
		cl.def("getLeafMinCellCount", (unsigned long (bitpit::PatchSkdTree::*)() const) &bitpit::PatchSkdTree::getLeafMinCellCount, "C++: bitpit::PatchSkdTree::getLeafMinCellCount() const --> unsigned long");
		cl.def("getLeafMaxCellCount", (unsigned long (bitpit::PatchSkdTree::*)() const) &bitpit::PatchSkdTree::getLeafMaxCellCount, "C++: bitpit::PatchSkdTree::getLeafMaxCellCount() const --> unsigned long");
		cl.def("getNodeCount", (unsigned long (bitpit::PatchSkdTree::*)() const) &bitpit::PatchSkdTree::getNodeCount, "C++: bitpit::PatchSkdTree::getNodeCount() const --> unsigned long");
		cl.def("getLeafCount", (unsigned long (bitpit::PatchSkdTree::*)() const) &bitpit::PatchSkdTree::getLeafCount, "C++: bitpit::PatchSkdTree::getLeafCount() const --> unsigned long");
		cl.def("getNode", (const class bitpit::SkdNode & (bitpit::PatchSkdTree::*)(unsigned long) const) &bitpit::PatchSkdTree::getNode, "C++: bitpit::PatchSkdTree::getNode(unsigned long) const --> const class bitpit::SkdNode &", pybind11::return_value_policy::automatic, pybind11::arg("nodeId"));
		cl.def("evalMaxDepth", [](bitpit::PatchSkdTree const &o) -> unsigned long { return o.evalMaxDepth(); }, "");
		cl.def("evalMaxDepth", (unsigned long (bitpit::PatchSkdTree::*)(unsigned long) const) &bitpit::PatchSkdTree::evalMaxDepth, "C++: bitpit::PatchSkdTree::evalMaxDepth(unsigned long) const --> unsigned long", pybind11::arg("rootId"));
		cl.def("getPartitionBox", (const class bitpit::SkdBox & (bitpit::PatchSkdTree::*)(int) const) &bitpit::PatchSkdTree::getPartitionBox, "C++: bitpit::PatchSkdTree::getPartitionBox(int) const --> const class bitpit::SkdBox &", pybind11::return_value_policy::automatic, pybind11::arg("rank"));
	}
	std::cout << "B604_[bitpit::SurfaceSkdTree] ";
	{ // bitpit::SurfaceSkdTree file:surface_skd_tree.hpp line:33
		pybind11::class_<bitpit::SurfaceSkdTree, std::shared_ptr<bitpit::SurfaceSkdTree>, bitpit::PatchSkdTree> cl(M("bitpit"), "SurfaceSkdTree", "");
		cl.def("clear", (void (bitpit::SurfaceSkdTree::*)(bool)) &bitpit::SurfaceSkdTree::clear, "C++: bitpit::SurfaceSkdTree::clear(bool) --> void", pybind11::arg("release"));
		cl.def("evalPointDistance", (double (bitpit::SurfaceSkdTree::*)(const struct std::array<double, 3> &) const) &bitpit::SurfaceSkdTree::evalPointDistance, "C++: bitpit::SurfaceSkdTree::evalPointDistance(const struct std::array<double, 3> &) const --> double", pybind11::arg("point"));
		cl.def("evalPointDistance", (double (bitpit::SurfaceSkdTree::*)(const struct std::array<double, 3> &, double) const) &bitpit::SurfaceSkdTree::evalPointDistance, "C++: bitpit::SurfaceSkdTree::evalPointDistance(const struct std::array<double, 3> &, double) const --> double", pybind11::arg("point"), pybind11::arg("maxDistance"));
		cl.def("evalPointDistance", (double (bitpit::SurfaceSkdTree::*)(const struct std::array<double, 3> &, double, bool) const) &bitpit::SurfaceSkdTree::evalPointDistance, "C++: bitpit::SurfaceSkdTree::evalPointDistance(const struct std::array<double, 3> &, double, bool) const --> double", pybind11::arg("point"), pybind11::arg("maxDistance"), pybind11::arg("interorOnly"));
		cl.def("evalPointDistance", (void (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, double *) const) &bitpit::SurfaceSkdTree::evalPointDistance, "C++: bitpit::SurfaceSkdTree::evalPointDistance(int, const struct std::array<double, 3> *, double *) const --> void", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("distances"));
		cl.def("evalPointDistance", (void (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, double, double *) const) &bitpit::SurfaceSkdTree::evalPointDistance, "C++: bitpit::SurfaceSkdTree::evalPointDistance(int, const struct std::array<double, 3> *, double, double *) const --> void", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("maxDistance"), pybind11::arg("distances"));
		cl.def("evalPointDistance", (void (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, const double *, double *) const) &bitpit::SurfaceSkdTree::evalPointDistance, "C++: bitpit::SurfaceSkdTree::evalPointDistance(int, const struct std::array<double, 3> *, const double *, double *) const --> void", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("maxDistances"), pybind11::arg("distances"));
		cl.def("evalPointDistance", (void (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, const double *, bool, double *) const) &bitpit::SurfaceSkdTree::evalPointDistance, "C++: bitpit::SurfaceSkdTree::evalPointDistance(int, const struct std::array<double, 3> *, const double *, bool, double *) const --> void", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("maxDistances"), pybind11::arg("interorOnly"), pybind11::arg("distances"));
		cl.def("evalPointGlobalDistance", (void (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, double *) const) &bitpit::SurfaceSkdTree::evalPointGlobalDistance, "C++: bitpit::SurfaceSkdTree::evalPointGlobalDistance(int, const struct std::array<double, 3> *, double *) const --> void", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("distances"));
		cl.def("evalPointGlobalDistance", (void (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, double, double *) const) &bitpit::SurfaceSkdTree::evalPointGlobalDistance, "C++: bitpit::SurfaceSkdTree::evalPointGlobalDistance(int, const struct std::array<double, 3> *, double, double *) const --> void", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("maxDistance"), pybind11::arg("distances"));
		cl.def("evalPointGlobalDistance", (void (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, const double *, double *) const) &bitpit::SurfaceSkdTree::evalPointGlobalDistance, "C++: bitpit::SurfaceSkdTree::evalPointGlobalDistance(int, const struct std::array<double, 3> *, const double *, double *) const --> void", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("maxDistances"), pybind11::arg("distances"));
		cl.def("findPointClosestCell", (long (bitpit::SurfaceSkdTree::*)(const struct std::array<double, 3> &, long *, double *) const) &bitpit::SurfaceSkdTree::findPointClosestCell, "C++: bitpit::SurfaceSkdTree::findPointClosestCell(const struct std::array<double, 3> &, long *, double *) const --> long", pybind11::arg("point"), pybind11::arg("id"), pybind11::arg("distance"));
		cl.def("findPointClosestCell", (long (bitpit::SurfaceSkdTree::*)(const struct std::array<double, 3> &, double, long *, double *) const) &bitpit::SurfaceSkdTree::findPointClosestCell, "C++: bitpit::SurfaceSkdTree::findPointClosestCell(const struct std::array<double, 3> &, double, long *, double *) const --> long", pybind11::arg("point"), pybind11::arg("maxDistance"), pybind11::arg("id"), pybind11::arg("distance"));
		cl.def("findPointClosestCell", (long (bitpit::SurfaceSkdTree::*)(const struct std::array<double, 3> &, double, bool, long *, double *) const) &bitpit::SurfaceSkdTree::findPointClosestCell, "C++: bitpit::SurfaceSkdTree::findPointClosestCell(const struct std::array<double, 3> &, double, bool, long *, double *) const --> long", pybind11::arg("point"), pybind11::arg("maxDistance"), pybind11::arg("interorOnly"), pybind11::arg("id"), pybind11::arg("distance"));
		cl.def("findPointClosestCell", (long (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, long *, double *) const) &bitpit::SurfaceSkdTree::findPointClosestCell, "C++: bitpit::SurfaceSkdTree::findPointClosestCell(int, const struct std::array<double, 3> *, long *, double *) const --> long", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("ids"), pybind11::arg("distances"));
		cl.def("findPointClosestCell", (long (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, double, long *, double *) const) &bitpit::SurfaceSkdTree::findPointClosestCell, "C++: bitpit::SurfaceSkdTree::findPointClosestCell(int, const struct std::array<double, 3> *, double, long *, double *) const --> long", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("maxDistance"), pybind11::arg("ids"), pybind11::arg("distances"));
		cl.def("findPointClosestCell", (long (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, const double *, long *, double *) const) &bitpit::SurfaceSkdTree::findPointClosestCell, "C++: bitpit::SurfaceSkdTree::findPointClosestCell(int, const struct std::array<double, 3> *, const double *, long *, double *) const --> long", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("maxDistances"), pybind11::arg("ids"), pybind11::arg("distances"));
		cl.def("findPointClosestCell", (long (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, const double *, bool, long *, double *) const) &bitpit::SurfaceSkdTree::findPointClosestCell, "C++: bitpit::SurfaceSkdTree::findPointClosestCell(int, const struct std::array<double, 3> *, const double *, bool, long *, double *) const --> long", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("maxDistances"), pybind11::arg("interorOnly"), pybind11::arg("ids"), pybind11::arg("distances"));
		cl.def("findPointClosestGlobalCell", (long (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, long *, int *, double *) const) &bitpit::SurfaceSkdTree::findPointClosestGlobalCell, "C++: bitpit::SurfaceSkdTree::findPointClosestGlobalCell(int, const struct std::array<double, 3> *, long *, int *, double *) const --> long", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("ids"), pybind11::arg("ranks"), pybind11::arg("distances"));
		cl.def("findPointClosestGlobalCell", (long (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, double, long *, int *, double *) const) &bitpit::SurfaceSkdTree::findPointClosestGlobalCell, "C++: bitpit::SurfaceSkdTree::findPointClosestGlobalCell(int, const struct std::array<double, 3> *, double, long *, int *, double *) const --> long", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("maxDistance"), pybind11::arg("ids"), pybind11::arg("ranks"), pybind11::arg("distances"));
		cl.def("findPointClosestGlobalCell", (long (bitpit::SurfaceSkdTree::*)(int, const struct std::array<double, 3> *, const double *, long *, int *, double *) const) &bitpit::SurfaceSkdTree::findPointClosestGlobalCell, "C++: bitpit::SurfaceSkdTree::findPointClosestGlobalCell(int, const struct std::array<double, 3> *, const double *, long *, int *, double *) const --> long", pybind11::arg("nPoints"), pybind11::arg("points"), pybind11::arg("maxDistances"), pybind11::arg("ids"), pybind11::arg("ranks"), pybind11::arg("distances"));
	}
}
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <memory>
#include <ostream>
#include <piercedKernel.hpp>
#include <piercedKernelIterator.hpp>
#include <piercedVectorKernel.hpp>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <vector>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_piercedKernel(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B356_[bitpit::PiercedKernel<long>] ";
	{ // bitpit::PiercedKernel file:piercedKernel.hpp line:103
		pybind11::class_<bitpit::PiercedKernel<long>, std::shared_ptr<bitpit::PiercedKernel<long>>> cl(M("bitpit"), "PiercedKernel_long_t", "");
		cl.def( pybind11::init( [](){ return new bitpit::PiercedKernel<long>(); } ) );
		cl.def( pybind11::init<unsigned long>(), pybind11::arg("n") );

		cl.def( pybind11::init( [](bitpit::PiercedKernel<long> const &o){ return new bitpit::PiercedKernel<long>(o); } ) );
		cl.def("sync", [](bitpit::PiercedKernel<long> &o) -> void { return o.sync(); }, "");
		cl.def("isSynced", [](bitpit::PiercedKernel<long> const &o) -> bool { return o.isSynced(); }, "");
		cl.def("clear", [](bitpit::PiercedKernel<long> &o) -> bitpit::PiercedKernel<long>::ClearAction { return o.clear(); }, "");
		cl.def("clear", (class bitpit::PiercedKernel<long>::ClearAction (bitpit::PiercedKernel<long>::*)(bool)) &bitpit::PiercedKernel<long>::clear, "C++: bitpit::PiercedKernel<long>::clear(bool) --> class bitpit::PiercedKernel<long>::ClearAction", pybind11::arg("release"));
		cl.def("swap", (void (bitpit::PiercedKernel<long>::*)(class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::swap, "C++: bitpit::PiercedKernel<long>::swap(class bitpit::PiercedKernel<long> &) --> void", pybind11::arg("x"));
		cl.def("flush", (void (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::flush, "C++: bitpit::PiercedKernel<long>::flush() --> void");
		cl.def("contiguous", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::contiguous, "C++: bitpit::PiercedKernel<long>::contiguous() const --> bool");
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump() const --> void");
		cl.def("empty", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::empty, "C++: bitpit::PiercedKernel<long>::empty() const --> bool");
		cl.def("isIteratorSlow", (bool (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::isIteratorSlow, "C++: bitpit::PiercedKernel<long>::isIteratorSlow() --> bool");
		cl.def("maxSize", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::maxSize, "C++: bitpit::PiercedKernel<long>::maxSize() const --> unsigned long");
		cl.def("size", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::size, "C++: bitpit::PiercedKernel<long>::size() const --> unsigned long");
		cl.def("capacity", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::capacity, "C++: bitpit::PiercedKernel<long>::capacity() const --> unsigned long");
		cl.def("contains", (bool (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::contains, "C++: bitpit::PiercedKernel<long>::contains(long) const --> bool", pybind11::arg("id"));
		cl.def("count", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::count, "C++: bitpit::PiercedKernel<long>::count(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getRawIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::getRawIndex, "C++: bitpit::PiercedKernel<long>::getRawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("evalFlatIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::evalFlatIndex, "C++: bitpit::PiercedKernel<long>::evalFlatIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getIds", [](bitpit::PiercedKernel<long> const &o) -> std::vector<long, class std::allocator<long> > { return o.getIds(); }, "");
		cl.def("getIds", (class std::vector<long, class std::allocator<long> > (bitpit::PiercedKernel<long>::*)(bool) const) &bitpit::PiercedKernel<long>::getIds, "C++: bitpit::PiercedKernel<long>::getIds(bool) const --> class std::vector<long, class std::allocator<long> >", pybind11::arg("ordered"));
		cl.def("getSizeMarker", [](bitpit::PiercedKernel<long> &o, unsigned long const & a0) -> long { return o.getSizeMarker(a0); }, "", pybind11::arg("targetSize"));
		cl.def("getSizeMarker", (long (bitpit::PiercedKernel<long>::*)(unsigned long, const long &)) &bitpit::PiercedKernel<long>::getSizeMarker, "C++: bitpit::PiercedKernel<long>::getSizeMarker(unsigned long, const long &) --> long", pybind11::arg("targetSize"), pybind11::arg("fallback"));
		cl.def("find", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(const long &) const) &bitpit::PiercedKernel<long>::find, "C++: bitpit::PiercedKernel<long>::find(const long &) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("id"));
		cl.def("rawFind", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(unsigned long) const) &bitpit::PiercedKernel<long>::rawFind, "C++: bitpit::PiercedKernel<long>::rawFind(unsigned long) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("pos"));
		cl.def("begin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::begin, "C++: bitpit::PiercedKernel<long>::begin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("end", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::end, "C++: bitpit::PiercedKernel<long>::end() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cbegin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cbegin, "C++: bitpit::PiercedKernel<long>::cbegin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cend", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cend, "C++: bitpit::PiercedKernel<long>::cend() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("checkIntegrity", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::checkIntegrity, "C++: bitpit::PiercedKernel<long>::checkIntegrity() const --> void");
		cl.def("updateId", (void (bitpit::PiercedKernel<long>::*)(const long &, const long &)) &bitpit::PiercedKernel<long>::updateId, "C++: bitpit::PiercedKernel<long>::updateId(const long &, const long &) --> void", pybind11::arg("currentId"), pybind11::arg("updatedId"));
		cl.def("back", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::back, "C++: bitpit::PiercedKernel<long>::back() const --> unsigned long");
		cl.def("front", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::front, "C++: bitpit::PiercedKernel<long>::front() const --> unsigned long");
		cl.def("restore", (void (bitpit::PiercedKernel<long>::*)(std::istream &)) &bitpit::PiercedKernel<long>::restore, "C++: bitpit::PiercedKernel<long>::restore(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)(std::ostream &) const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("assign", (class bitpit::PiercedKernel<long> & (bitpit::PiercedKernel<long>::*)(const class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::operator=, "C++: bitpit::PiercedKernel<long>::operator=(const class bitpit::PiercedKernel<long> &) --> class bitpit::PiercedKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));

		{ // bitpit::PiercedKernel<long>::ClearAction file:piercedKernel.hpp line:259
			auto & enclosing_class = cl;
			pybind11::class_<bitpit::PiercedKernel<long>::ClearAction, std::shared_ptr<bitpit::PiercedKernel<long>::ClearAction>> cl(enclosing_class, "ClearAction", "");
			cl.def( pybind11::init( [](){ return new bitpit::PiercedKernel<long>::ClearAction(); } ) );
			cl.def( pybind11::init( [](bitpit::PiercedKernel<long>::ClearAction const &o){ return new bitpit::PiercedKernel<long>::ClearAction(o); } ) );
		}

	}
	std::cout << "B357_[bitpit::PiercedKernelIterator<long>] ";
	{ // bitpit::PiercedKernelIterator file:piercedKernelIterator.hpp line:47
		pybind11::class_<bitpit::PiercedKernelIterator<long>, std::shared_ptr<bitpit::PiercedKernelIterator<long>>> cl(M("bitpit"), "PiercedKernelIterator_long_t", "");
		cl.def( pybind11::init( [](){ return new bitpit::PiercedKernelIterator<long>(); } ) );
		cl.def( pybind11::init( [](bitpit::PiercedKernelIterator<long> const &o){ return new bitpit::PiercedKernelIterator<long>(o); } ) );
		cl.def("swap", (void (bitpit::PiercedKernelIterator<long>::*)(class bitpit::PiercedKernelIterator<long> &)) &bitpit::PiercedKernelIterator<long>::swap, "C++: bitpit::PiercedKernelIterator<long>::swap(class bitpit::PiercedKernelIterator<long> &) --> void", pybind11::arg("other"));
		cl.def("getKernel", (const class bitpit::PiercedKernel<long> & (bitpit::PiercedKernelIterator<long>::*)() const) &bitpit::PiercedKernelIterator<long>::getKernel, "C++: bitpit::PiercedKernelIterator<long>::getKernel() const --> const class bitpit::PiercedKernel<long> &", pybind11::return_value_policy::automatic);
		cl.def("getId", [](bitpit::PiercedKernelIterator<long> const &o) -> long { return o.getId(); }, "");
		cl.def("getId", (long (bitpit::PiercedKernelIterator<long>::*)(const long &) const) &bitpit::PiercedKernelIterator<long>::getId, "C++: bitpit::PiercedKernelIterator<long>::getId(const long &) const --> long", pybind11::arg("fallback"));
		cl.def("getRawIndex", (unsigned long (bitpit::PiercedKernelIterator<long>::*)() const) &bitpit::PiercedKernelIterator<long>::getRawIndex, "C++: bitpit::PiercedKernelIterator<long>::getRawIndex() const --> unsigned long");
		cl.def("getPos", (unsigned long (bitpit::PiercedKernelIterator<long>::*)() const) &bitpit::PiercedKernelIterator<long>::getPos, "C++: bitpit::PiercedKernelIterator<long>::getPos() const --> unsigned long");
		cl.def("plus_plus", (class bitpit::PiercedKernelIterator<long> & (bitpit::PiercedKernelIterator<long>::*)()) &bitpit::PiercedKernelIterator<long>::operator++, "C++: bitpit::PiercedKernelIterator<long>::operator++() --> class bitpit::PiercedKernelIterator<long> &", pybind11::return_value_policy::automatic);
		cl.def("plus_plus", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernelIterator<long>::*)(int)) &bitpit::PiercedKernelIterator<long>::operator++, "C++: bitpit::PiercedKernelIterator<long>::operator++(int) --> class bitpit::PiercedKernelIterator<long>", pybind11::arg(""));
		cl.def("minus_minus", (class bitpit::PiercedKernelIterator<long> & (bitpit::PiercedKernelIterator<long>::*)()) &bitpit::PiercedKernelIterator<long>::operator--, "C++: bitpit::PiercedKernelIterator<long>::operator--() --> class bitpit::PiercedKernelIterator<long> &", pybind11::return_value_policy::automatic);
		cl.def("minus_minus", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernelIterator<long>::*)(int)) &bitpit::PiercedKernelIterator<long>::operator--, "C++: bitpit::PiercedKernelIterator<long>::operator--(int) --> class bitpit::PiercedKernelIterator<long>", pybind11::arg(""));
		cl.def("__mul__", (const long & (bitpit::PiercedKernelIterator<long>::*)() const) &bitpit::PiercedKernelIterator<long>::operator*, "C++: bitpit::PiercedKernelIterator<long>::operator*() const --> const long &", pybind11::return_value_policy::automatic);
		cl.def("__eq__", (bool (bitpit::PiercedKernelIterator<long>::*)(const class bitpit::PiercedKernelIterator<long> &) const) &bitpit::PiercedKernelIterator<long>::operator==, "C++: bitpit::PiercedKernelIterator<long>::operator==(const class bitpit::PiercedKernelIterator<long> &) const --> bool", pybind11::arg("rhs"));
		cl.def("__ne__", (bool (bitpit::PiercedKernelIterator<long>::*)(const class bitpit::PiercedKernelIterator<long> &) const) &bitpit::PiercedKernelIterator<long>::operator!=, "C++: bitpit::PiercedKernelIterator<long>::operator!=(const class bitpit::PiercedKernelIterator<long> &) const --> bool", pybind11::arg("rhs"));
	}
	std::cout << "B358_[bitpit::BasePiercedKernel] ";
	std::cout << "B359_[bitpit::BasePiercedVectorKernel] ";
	{ // bitpit::BasePiercedVectorKernel file:piercedVectorKernel.hpp line:34
		pybind11::class_<bitpit::BasePiercedVectorKernel, std::shared_ptr<bitpit::BasePiercedVectorKernel>> cl(M("bitpit"), "BasePiercedVectorKernel", "");
		cl.def( pybind11::init( [](bitpit::BasePiercedVectorKernel const &o){ return new bitpit::BasePiercedVectorKernel(o); } ) );
		cl.def("assign", (class bitpit::BasePiercedVectorKernel & (bitpit::BasePiercedVectorKernel::*)(const class bitpit::BasePiercedVectorKernel &)) &bitpit::BasePiercedVectorKernel::operator=, "C++: bitpit::BasePiercedVectorKernel::operator=(const class bitpit::BasePiercedVectorKernel &) --> class bitpit::BasePiercedVectorKernel &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B360_[bitpit::PiercedVectorKernel<long>] ";
	{ // bitpit::PiercedVectorKernel file:piercedVectorKernel.hpp line:50
		pybind11::class_<bitpit::PiercedVectorKernel<long>, std::shared_ptr<bitpit::PiercedVectorKernel<long>>, bitpit::BasePiercedVectorKernel, bitpit::PiercedKernel<long>> cl(M("bitpit"), "PiercedVectorKernel_long_t", "");
		cl.def( pybind11::init( [](){ return new bitpit::PiercedVectorKernel<long>(); } ) );
		cl.def( pybind11::init( [](bitpit::PiercedVectorKernel<long> const &o){ return new bitpit::PiercedVectorKernel<long>(o); } ) );
		cl.def("dump", [](bitpit::PiercedVectorKernel<long> const &o, class std::basic_ostream<char> & a0) -> void { return o.dump(a0); }, "", pybind11::arg("stream"));
		cl.def("dump", [](bitpit::PiercedVectorKernel<long> const &o) -> void { return o.dump(); }, "");
		cl.def("restore", [](bitpit::PiercedVectorKernel<long> &o, class std::basic_istream<char> & a0) -> void { return o.restore(a0); }, "", pybind11::arg("stream"));
		cl.def("rawIndex", (unsigned long (bitpit::PiercedVectorKernel<long>::*)(long) const) &bitpit::PiercedVectorKernel<long>::rawIndex, "C++: bitpit::PiercedVectorKernel<long>::rawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("exists", (bool (bitpit::PiercedVectorKernel<long>::*)(long) const) &bitpit::PiercedVectorKernel<long>::exists, "C++: bitpit::PiercedVectorKernel<long>::exists(long) const --> bool", pybind11::arg("id"));
		cl.def("assign", (class bitpit::PiercedVectorKernel<long> & (bitpit::PiercedVectorKernel<long>::*)(const class bitpit::PiercedVectorKernel<long> &)) &bitpit::PiercedVectorKernel<long>::operator=, "C++: bitpit::PiercedVectorKernel<long>::operator=(const class bitpit::PiercedVectorKernel<long> &) --> class bitpit::PiercedVectorKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("assign", (class bitpit::BasePiercedVectorKernel & (bitpit::BasePiercedVectorKernel::*)(const class bitpit::BasePiercedVectorKernel &)) &bitpit::BasePiercedVectorKernel::operator=, "C++: bitpit::BasePiercedVectorKernel::operator=(const class bitpit::BasePiercedVectorKernel &) --> class bitpit::BasePiercedVectorKernel &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("sync", [](bitpit::PiercedKernel<long> &o) -> void { return o.sync(); }, "");
		cl.def("isSynced", [](bitpit::PiercedKernel<long> const &o) -> bool { return o.isSynced(); }, "");
		cl.def("clear", [](bitpit::PiercedKernel<long> &o) -> bitpit::PiercedKernel<long>::ClearAction { return o.clear(); }, "");
		cl.def("clear", (class bitpit::PiercedKernel<long>::ClearAction (bitpit::PiercedKernel<long>::*)(bool)) &bitpit::PiercedKernel<long>::clear, "C++: bitpit::PiercedKernel<long>::clear(bool) --> class bitpit::PiercedKernel<long>::ClearAction", pybind11::arg("release"));
		cl.def("swap", (void (bitpit::PiercedKernel<long>::*)(class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::swap, "C++: bitpit::PiercedKernel<long>::swap(class bitpit::PiercedKernel<long> &) --> void", pybind11::arg("x"));
		cl.def("flush", (void (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::flush, "C++: bitpit::PiercedKernel<long>::flush() --> void");
		cl.def("contiguous", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::contiguous, "C++: bitpit::PiercedKernel<long>::contiguous() const --> bool");
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump() const --> void");
		cl.def("empty", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::empty, "C++: bitpit::PiercedKernel<long>::empty() const --> bool");
		cl.def("isIteratorSlow", (bool (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::isIteratorSlow, "C++: bitpit::PiercedKernel<long>::isIteratorSlow() --> bool");
		cl.def("maxSize", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::maxSize, "C++: bitpit::PiercedKernel<long>::maxSize() const --> unsigned long");
		cl.def("size", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::size, "C++: bitpit::PiercedKernel<long>::size() const --> unsigned long");
		cl.def("capacity", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::capacity, "C++: bitpit::PiercedKernel<long>::capacity() const --> unsigned long");
		cl.def("contains", (bool (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::contains, "C++: bitpit::PiercedKernel<long>::contains(long) const --> bool", pybind11::arg("id"));
		cl.def("count", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::count, "C++: bitpit::PiercedKernel<long>::count(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getRawIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::getRawIndex, "C++: bitpit::PiercedKernel<long>::getRawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("evalFlatIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::evalFlatIndex, "C++: bitpit::PiercedKernel<long>::evalFlatIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getIds", [](bitpit::PiercedKernel<long> const &o) -> std::vector<long, class std::allocator<long> > { return o.getIds(); }, "");
		cl.def("getIds", (class std::vector<long, class std::allocator<long> > (bitpit::PiercedKernel<long>::*)(bool) const) &bitpit::PiercedKernel<long>::getIds, "C++: bitpit::PiercedKernel<long>::getIds(bool) const --> class std::vector<long, class std::allocator<long> >", pybind11::arg("ordered"));
		cl.def("getSizeMarker", [](bitpit::PiercedKernel<long> &o, unsigned long const & a0) -> long { return o.getSizeMarker(a0); }, "", pybind11::arg("targetSize"));
		cl.def("getSizeMarker", (long (bitpit::PiercedKernel<long>::*)(unsigned long, const long &)) &bitpit::PiercedKernel<long>::getSizeMarker, "C++: bitpit::PiercedKernel<long>::getSizeMarker(unsigned long, const long &) --> long", pybind11::arg("targetSize"), pybind11::arg("fallback"));
		cl.def("find", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(const long &) const) &bitpit::PiercedKernel<long>::find, "C++: bitpit::PiercedKernel<long>::find(const long &) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("id"));
		cl.def("rawFind", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(unsigned long) const) &bitpit::PiercedKernel<long>::rawFind, "C++: bitpit::PiercedKernel<long>::rawFind(unsigned long) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("pos"));
		cl.def("begin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::begin, "C++: bitpit::PiercedKernel<long>::begin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("end", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::end, "C++: bitpit::PiercedKernel<long>::end() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cbegin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cbegin, "C++: bitpit::PiercedKernel<long>::cbegin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cend", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cend, "C++: bitpit::PiercedKernel<long>::cend() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("checkIntegrity", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::checkIntegrity, "C++: bitpit::PiercedKernel<long>::checkIntegrity() const --> void");
		cl.def("updateId", (void (bitpit::PiercedKernel<long>::*)(const long &, const long &)) &bitpit::PiercedKernel<long>::updateId, "C++: bitpit::PiercedKernel<long>::updateId(const long &, const long &) --> void", pybind11::arg("currentId"), pybind11::arg("updatedId"));
		cl.def("back", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::back, "C++: bitpit::PiercedKernel<long>::back() const --> unsigned long");
		cl.def("front", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::front, "C++: bitpit::PiercedKernel<long>::front() const --> unsigned long");
		cl.def("restore", (void (bitpit::PiercedKernel<long>::*)(std::istream &)) &bitpit::PiercedKernel<long>::restore, "C++: bitpit::PiercedKernel<long>::restore(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)(std::ostream &) const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("assign", (class bitpit::PiercedKernel<long> & (bitpit::PiercedKernel<long>::*)(const class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::operator=, "C++: bitpit::PiercedKernel<long>::operator=(const class bitpit::PiercedKernel<long> &) --> class bitpit::PiercedKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
}
#include <array>
#include <cell.hpp>
#include <element.hpp>
#include <element_reference.hpp>
#include <element_type.hpp>
#include <functional>
#include <interface.hpp>
#include <ios>
#include <iostream>
#include <istream>
#include <memory>
#include <ostream>
#include <piercedKernel.hpp>
#include <piercedKernelIterator.hpp>
#include <piercedStorage.hpp>
#include <piercedSync.hpp>
#include <piercedVector.hpp>
#include <piercedVectorKernel.hpp>
#include <piercedVectorStorage.hpp>
#include <proxyVector.hpp>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
#include <vertex.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// bitpit::PiercedVector file:piercedVector.hpp line:63
struct PyCallBack_bitpit_PiercedVector_double_long_t : public bitpit::PiercedVector<double,long> {
	using bitpit::PiercedVector<double,long>::PiercedVector;

};

void bind_piercedStorage(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B361_[bitpit::BasePiercedStorage] ";
	{ // bitpit::BasePiercedStorage file:piercedStorage.hpp line:42
		pybind11::class_<bitpit::BasePiercedStorage, std::shared_ptr<bitpit::BasePiercedStorage>> cl(M("bitpit"), "BasePiercedStorage", "");
		cl.def("assign", (class bitpit::BasePiercedStorage & (bitpit::BasePiercedStorage::*)(const class bitpit::BasePiercedStorage &)) &bitpit::BasePiercedStorage::operator=, "C++: bitpit::BasePiercedStorage::operator=(const class bitpit::BasePiercedStorage &) --> class bitpit::BasePiercedStorage &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B362_[bitpit::BasePiercedVectorStorage] ";
	{ // bitpit::BasePiercedVectorStorage file:piercedVectorStorage.hpp line:40
		pybind11::class_<bitpit::BasePiercedVectorStorage, std::shared_ptr<bitpit::BasePiercedVectorStorage>> cl(M("bitpit"), "BasePiercedVectorStorage", "");
		cl.def( pybind11::init( [](bitpit::BasePiercedVectorStorage const &o){ return new bitpit::BasePiercedVectorStorage(o); } ) );
		cl.def("assign", (class bitpit::BasePiercedVectorStorage & (bitpit::BasePiercedVectorStorage::*)(const class bitpit::BasePiercedVectorStorage &)) &bitpit::BasePiercedVectorStorage::operator=, "C++: bitpit::BasePiercedVectorStorage::operator=(const class bitpit::BasePiercedVectorStorage &) --> class bitpit::BasePiercedVectorStorage &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B363_[bitpit::PiercedVectorStorage<bitpit::Element,long>] ";
	std::cout << "B364_[bitpit::PiercedVectorStorage<bitpit::Cell,long>] ";
	std::cout << "B365_[bitpit::PiercedVectorStorage<bitpit::Interface,long>] ";
	std::cout << "B366_[bitpit::PiercedVectorStorage<bitpit::Vertex,long>] ";
	std::cout << "B367_[bitpit::PiercedVectorStorage<double,long>] ";
	std::cout << "B368_[bitpit::PiercedVectorStorage<neos::Stencil,long>] ";
	std::cout << "B369_[bitpit::BasePiercedVector] ";
	{ // bitpit::BasePiercedVector file:piercedVector.hpp line:40
		pybind11::class_<bitpit::BasePiercedVector, std::shared_ptr<bitpit::BasePiercedVector>> cl(M("bitpit"), "BasePiercedVector", "");
		cl.def( pybind11::init( [](bitpit::BasePiercedVector const &o){ return new bitpit::BasePiercedVector(o); } ) );
		cl.def("assign", (class bitpit::BasePiercedVector & (bitpit::BasePiercedVector::*)(const class bitpit::BasePiercedVector &)) &bitpit::BasePiercedVector::operator=, "C++: bitpit::BasePiercedVector::operator=(const class bitpit::BasePiercedVector &) --> class bitpit::BasePiercedVector &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	std::cout << "B370_[bitpit::PiercedVector<double,long>] ";
	{ // bitpit::PiercedVector file:piercedVector.hpp line:63
		pybind11::class_<bitpit::PiercedVector<double,long>, std::shared_ptr<bitpit::PiercedVector<double,long>>, PyCallBack_bitpit_PiercedVector_double_long_t, bitpit::BasePiercedVector, bitpit::PiercedVectorKernel<long>> cl(M("bitpit"), "PiercedVector_double_long_t", "");
		cl.def( pybind11::init( [](){ return new bitpit::PiercedVector<double,long>(); }, [](){ return new PyCallBack_bitpit_PiercedVector_double_long_t(); } ) );
		cl.def( pybind11::init<unsigned long>(), pybind11::arg("n") );

		cl.def( pybind11::init( [](PyCallBack_bitpit_PiercedVector_double_long_t const &o){ return new PyCallBack_bitpit_PiercedVector_double_long_t(o); } ) );
		cl.def( pybind11::init( [](bitpit::PiercedVector<double,long> const &o){ return new bitpit::PiercedVector<double,long>(o); } ) );
		cl.def("assign", (class bitpit::PiercedVector<double, long> & (bitpit::PiercedVector<double,long>::*)(class bitpit::PiercedVector<double, long>)) &bitpit::PiercedVector<double, long>::operator=, "C++: bitpit::PiercedVector<double, long>::operator=(class bitpit::PiercedVector<double, long>) --> class bitpit::PiercedVector<double, long> &", pybind11::return_value_policy::automatic, pybind11::arg("x"));
		cl.def("popBack", (void (bitpit::PiercedVector<double,long>::*)()) &bitpit::PiercedVector<double, long>::popBack, "C++: bitpit::PiercedVector<double, long>::popBack() --> void");
		cl.def("swap", (void (bitpit::PiercedVector<double,long>::*)(long, long)) &bitpit::PiercedVector<double, long>::swap, "C++: bitpit::PiercedVector<double, long>::swap(long, long) --> void", pybind11::arg("id_first"), pybind11::arg("id_second"));
		cl.def("clear", [](bitpit::PiercedVector<double,long> &o) -> void { return o.clear(); }, "");
		cl.def("clear", (void (bitpit::PiercedVector<double,long>::*)(bool)) &bitpit::PiercedVector<double, long>::clear, "C++: bitpit::PiercedVector<double, long>::clear(bool) --> void", pybind11::arg("release"));
		cl.def("reserve", (void (bitpit::PiercedVector<double,long>::*)(unsigned long)) &bitpit::PiercedVector<double, long>::reserve, "C++: bitpit::PiercedVector<double, long>::reserve(unsigned long) --> void", pybind11::arg("n"));
		cl.def("resize", (void (bitpit::PiercedVector<double,long>::*)(unsigned long)) &bitpit::PiercedVector<double, long>::resize, "C++: bitpit::PiercedVector<double, long>::resize(unsigned long) --> void", pybind11::arg("n"));
		cl.def("sort", (void (bitpit::PiercedVector<double,long>::*)()) &bitpit::PiercedVector<double, long>::sort, "C++: bitpit::PiercedVector<double, long>::sort() --> void");
		cl.def("sortAfter", (void (bitpit::PiercedVector<double,long>::*)(long, bool)) &bitpit::PiercedVector<double, long>::sortAfter, "C++: bitpit::PiercedVector<double, long>::sortAfter(long, bool) --> void", pybind11::arg("referenceId"), pybind11::arg("inclusive"));
		cl.def("sortBefore", (void (bitpit::PiercedVector<double,long>::*)(long, bool)) &bitpit::PiercedVector<double, long>::sortBefore, "C++: bitpit::PiercedVector<double, long>::sortBefore(long, bool) --> void", pybind11::arg("referenceId"), pybind11::arg("inclusive"));
		cl.def("squeeze", (void (bitpit::PiercedVector<double,long>::*)()) &bitpit::PiercedVector<double, long>::squeeze, "C++: bitpit::PiercedVector<double, long>::squeeze() --> void");
		cl.def("shrinkToFit", (void (bitpit::PiercedVector<double,long>::*)()) &bitpit::PiercedVector<double, long>::shrinkToFit, "C++: bitpit::PiercedVector<double, long>::shrinkToFit() --> void");
		cl.def("swap", (void (bitpit::PiercedVector<double,long>::*)(class bitpit::PiercedVector<double, long> &)) &bitpit::PiercedVector<double, long>::swap, "C++: bitpit::PiercedVector<double, long>::swap(class bitpit::PiercedVector<double, long> &) --> void", pybind11::arg("x"));
		cl.def("getKernel", (const class bitpit::PiercedVectorKernel<long> & (bitpit::PiercedVector<double,long>::*)() const) &bitpit::PiercedVector<double, long>::getKernel, "C++: bitpit::PiercedVector<double, long>::getKernel() const --> const class bitpit::PiercedVectorKernel<long> &", pybind11::return_value_policy::automatic);
		cl.def("dump", (void (bitpit::PiercedVector<double,long>::*)() const) &bitpit::PiercedVector<double, long>::dump, "C++: bitpit::PiercedVector<double, long>::dump() const --> void");
		cl.def("restoreKernel", (void (bitpit::PiercedVector<double,long>::*)(std::istream &)) &bitpit::PiercedVector<double, long>::restoreKernel, "C++: bitpit::PiercedVector<double, long>::restoreKernel(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dumpKernel", (void (bitpit::PiercedVector<double,long>::*)(std::ostream &) const) &bitpit::PiercedVector<double, long>::dumpKernel, "C++: bitpit::PiercedVector<double, long>::dumpKernel(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("assign", (class bitpit::BasePiercedVector & (bitpit::BasePiercedVector::*)(const class bitpit::BasePiercedVector &)) &bitpit::BasePiercedVector::operator=, "C++: bitpit::BasePiercedVector::operator=(const class bitpit::BasePiercedVector &) --> class bitpit::BasePiercedVector &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("dump", [](bitpit::PiercedVectorKernel<long> const &o, class std::basic_ostream<char> & a0) -> void { return o.dump(a0); }, "", pybind11::arg("stream"));
		cl.def("dump", [](bitpit::PiercedVectorKernel<long> const &o) -> void { return o.dump(); }, "");
		cl.def("restore", [](bitpit::PiercedVectorKernel<long> &o, class std::basic_istream<char> & a0) -> void { return o.restore(a0); }, "", pybind11::arg("stream"));
		cl.def("rawIndex", (unsigned long (bitpit::PiercedVectorKernel<long>::*)(long) const) &bitpit::PiercedVectorKernel<long>::rawIndex, "C++: bitpit::PiercedVectorKernel<long>::rawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("exists", (bool (bitpit::PiercedVectorKernel<long>::*)(long) const) &bitpit::PiercedVectorKernel<long>::exists, "C++: bitpit::PiercedVectorKernel<long>::exists(long) const --> bool", pybind11::arg("id"));
		cl.def("assign", (class bitpit::PiercedVectorKernel<long> & (bitpit::PiercedVectorKernel<long>::*)(const class bitpit::PiercedVectorKernel<long> &)) &bitpit::PiercedVectorKernel<long>::operator=, "C++: bitpit::PiercedVectorKernel<long>::operator=(const class bitpit::PiercedVectorKernel<long> &) --> class bitpit::PiercedVectorKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("assign", (class bitpit::BasePiercedVectorKernel & (bitpit::BasePiercedVectorKernel::*)(const class bitpit::BasePiercedVectorKernel &)) &bitpit::BasePiercedVectorKernel::operator=, "C++: bitpit::BasePiercedVectorKernel::operator=(const class bitpit::BasePiercedVectorKernel &) --> class bitpit::BasePiercedVectorKernel &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("sync", [](bitpit::PiercedKernel<long> &o) -> void { return o.sync(); }, "");
		cl.def("isSynced", [](bitpit::PiercedKernel<long> const &o) -> bool { return o.isSynced(); }, "");
		cl.def("clear", [](bitpit::PiercedKernel<long> &o) -> bitpit::PiercedKernel<long>::ClearAction { return o.clear(); }, "");
		cl.def("clear", (class bitpit::PiercedKernel<long>::ClearAction (bitpit::PiercedKernel<long>::*)(bool)) &bitpit::PiercedKernel<long>::clear, "C++: bitpit::PiercedKernel<long>::clear(bool) --> class bitpit::PiercedKernel<long>::ClearAction", pybind11::arg("release"));
		cl.def("swap", (void (bitpit::PiercedKernel<long>::*)(class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::swap, "C++: bitpit::PiercedKernel<long>::swap(class bitpit::PiercedKernel<long> &) --> void", pybind11::arg("x"));
		cl.def("flush", (void (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::flush, "C++: bitpit::PiercedKernel<long>::flush() --> void");
		cl.def("contiguous", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::contiguous, "C++: bitpit::PiercedKernel<long>::contiguous() const --> bool");
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump() const --> void");
		cl.def("empty", (bool (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::empty, "C++: bitpit::PiercedKernel<long>::empty() const --> bool");
		cl.def("isIteratorSlow", (bool (bitpit::PiercedKernel<long>::*)()) &bitpit::PiercedKernel<long>::isIteratorSlow, "C++: bitpit::PiercedKernel<long>::isIteratorSlow() --> bool");
		cl.def("maxSize", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::maxSize, "C++: bitpit::PiercedKernel<long>::maxSize() const --> unsigned long");
		cl.def("size", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::size, "C++: bitpit::PiercedKernel<long>::size() const --> unsigned long");
		cl.def("capacity", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::capacity, "C++: bitpit::PiercedKernel<long>::capacity() const --> unsigned long");
		cl.def("contains", (bool (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::contains, "C++: bitpit::PiercedKernel<long>::contains(long) const --> bool", pybind11::arg("id"));
		cl.def("count", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::count, "C++: bitpit::PiercedKernel<long>::count(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getRawIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::getRawIndex, "C++: bitpit::PiercedKernel<long>::getRawIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("evalFlatIndex", (unsigned long (bitpit::PiercedKernel<long>::*)(long) const) &bitpit::PiercedKernel<long>::evalFlatIndex, "C++: bitpit::PiercedKernel<long>::evalFlatIndex(long) const --> unsigned long", pybind11::arg("id"));
		cl.def("getIds", [](bitpit::PiercedKernel<long> const &o) -> std::vector<long, class std::allocator<long> > { return o.getIds(); }, "");
		cl.def("getIds", (class std::vector<long, class std::allocator<long> > (bitpit::PiercedKernel<long>::*)(bool) const) &bitpit::PiercedKernel<long>::getIds, "C++: bitpit::PiercedKernel<long>::getIds(bool) const --> class std::vector<long, class std::allocator<long> >", pybind11::arg("ordered"));
		cl.def("getSizeMarker", [](bitpit::PiercedKernel<long> &o, unsigned long const & a0) -> long { return o.getSizeMarker(a0); }, "", pybind11::arg("targetSize"));
		cl.def("getSizeMarker", (long (bitpit::PiercedKernel<long>::*)(unsigned long, const long &)) &bitpit::PiercedKernel<long>::getSizeMarker, "C++: bitpit::PiercedKernel<long>::getSizeMarker(unsigned long, const long &) --> long", pybind11::arg("targetSize"), pybind11::arg("fallback"));
		cl.def("find", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(const long &) const) &bitpit::PiercedKernel<long>::find, "C++: bitpit::PiercedKernel<long>::find(const long &) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("id"));
		cl.def("rawFind", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)(unsigned long) const) &bitpit::PiercedKernel<long>::rawFind, "C++: bitpit::PiercedKernel<long>::rawFind(unsigned long) const --> class bitpit::PiercedKernelIterator<long>", pybind11::arg("pos"));
		cl.def("begin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::begin, "C++: bitpit::PiercedKernel<long>::begin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("end", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::end, "C++: bitpit::PiercedKernel<long>::end() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cbegin", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cbegin, "C++: bitpit::PiercedKernel<long>::cbegin() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("cend", (class bitpit::PiercedKernelIterator<long> (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::cend, "C++: bitpit::PiercedKernel<long>::cend() const --> class bitpit::PiercedKernelIterator<long>");
		cl.def("checkIntegrity", (void (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::checkIntegrity, "C++: bitpit::PiercedKernel<long>::checkIntegrity() const --> void");
		cl.def("updateId", (void (bitpit::PiercedKernel<long>::*)(const long &, const long &)) &bitpit::PiercedKernel<long>::updateId, "C++: bitpit::PiercedKernel<long>::updateId(const long &, const long &) --> void", pybind11::arg("currentId"), pybind11::arg("updatedId"));
		cl.def("back", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::back, "C++: bitpit::PiercedKernel<long>::back() const --> unsigned long");
		cl.def("front", (unsigned long (bitpit::PiercedKernel<long>::*)() const) &bitpit::PiercedKernel<long>::front, "C++: bitpit::PiercedKernel<long>::front() const --> unsigned long");
		cl.def("restore", (void (bitpit::PiercedKernel<long>::*)(std::istream &)) &bitpit::PiercedKernel<long>::restore, "C++: bitpit::PiercedKernel<long>::restore(std::istream &) --> void", pybind11::arg("stream"));
		cl.def("dump", (void (bitpit::PiercedKernel<long>::*)(std::ostream &) const) &bitpit::PiercedKernel<long>::dump, "C++: bitpit::PiercedKernel<long>::dump(std::ostream &) const --> void", pybind11::arg("stream"));
		cl.def("assign", (class bitpit::PiercedKernel<long> & (bitpit::PiercedKernel<long>::*)(const class bitpit::PiercedKernel<long> &)) &bitpit::PiercedKernel<long>::operator=, "C++: bitpit::PiercedKernel<long>::operator=(const class bitpit::PiercedKernel<long> &) --> class bitpit::PiercedKernel<long> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
}
#include <map>
#include <memory>
#include <stdexcept>
#include <functional>
#include <string>

#include <pybind11/pybind11.h>

typedef std::function< pybind11::module & (std::string const &) > ModuleGetter;

void bind_piercedKernel(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_piercedStorage(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_NeosPiercedVector(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_common(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_binary_stream(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_element_reference(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_element(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_cell(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_DataLBInterface(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_configuration_config(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_index_generator(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_VTK(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_VTK_1(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_STL(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_communications_tags(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_tree_constants(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_morton(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_Octant(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_patch_info(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_adaption(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_adaption_1(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_patch_manager(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_patch_manager_1(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_patch_skd_tree(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_volume_kernel(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_volume_mapper(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_volume_mapper_1(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_BoundaryConditions(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_NeosLevelSet(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_levelSetCommon(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_STLGeometry(std::function< pybind11::module &(std::string const &namespace_) > &M);


PYBIND11_MODULE(pyneos, root_module) {
	root_module.doc() = "pyneos module";

	std::map <std::string, pybind11::module> modules;
	ModuleGetter M = [&](std::string const &namespace_) -> pybind11::module & {
		auto it = modules.find(namespace_);
		if( it == modules.end() ) throw std::runtime_error("Attempt to access pybind11::module for namespace " + namespace_ + " before it was created!!!");
		return it->second;
	};

	modules[""] = root_module;

	std::vector< std::pair<std::string, std::string> > sub_modules {
		{"", "bitpit"},
		{"bitpit", "PABLO"},
		{"bitpit", "adaption"},
		{"bitpit", "communications"},
		{"bitpit", "mapping"},
		{"bitpit", "patch"},
		{"bitpit", "vtk"},
		{"", "neos"},
		{"", "std"},
	};
	for(auto &p : sub_modules ) modules[p.first.size() ? p.first+"::"+p.second : p.second] = modules[p.first].def_submodule(p.second.c_str(), ("Bindings for " + p.first + "::" + p.second + " namespace").c_str() );

	//pybind11::class_<std::shared_ptr<void>>(M(""), "_encapsulated_data_");

	bind_piercedKernel(M);
	bind_piercedStorage(M);
	bind_NeosPiercedVector(M);
	bind_common(M);
	bind_binary_stream(M);
	bind_element_reference(M);
	bind_element(M);
	bind_cell(M);
	bind_DataLBInterface(M);
	bind_configuration_config(M);
	bind_index_generator(M);
	bind_VTK(M);
	bind_VTK_1(M);
	bind_STL(M);
	bind_communications_tags(M);
	bind_tree_constants(M);
	bind_morton(M);
	bind_Octant(M);
	bind_patch_info(M);
	bind_adaption(M);
	bind_adaption_1(M);
	bind_patch_manager(M);
	bind_patch_manager_1(M);
	bind_patch_skd_tree(M);
	bind_volume_kernel(M);
	bind_volume_mapper(M);
	bind_volume_mapper_1(M);
	bind_BoundaryConditions(M);
	bind_NeosLevelSet(M);
	bind_levelSetCommon(M);
	bind_STLGeometry(M);

}
#include <iostream>
#include <sstream> // __str__
#include <tree_constants.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_tree_constants(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B570_[bitpit::TreeConstants] ";
	{ // bitpit::TreeConstants file:tree_constants.hpp line:50
		pybind11::class_<bitpit::TreeConstants, std::shared_ptr<bitpit::TreeConstants>> cl(M("bitpit"), "TreeConstants", "	\n\n	\n			23/apr/2014\n	\n\n		Edoardo Lombardi\n	\n\n		Marco Cisternino\n	\n\n		Copyright (C) 2014-2021 OPTIMAD engineering srl. All rights reserved.\n\n	\n Global constants associated to a PABLO tree\n\n	Global constants are used in PABLO everywhere and they are public, i.e.\n	each global variable can be used by any external code.\n\n ");
		cl.def_readwrite("nChildren", &bitpit::TreeConstants::nChildren);
		cl.def_readwrite("nFaces", &bitpit::TreeConstants::nFaces);
		cl.def_readwrite("nEdges", &bitpit::TreeConstants::nEdges);
		cl.def_readwrite("nNodes", &bitpit::TreeConstants::nNodes);
		cl.def_readwrite("nNodesPerFace", &bitpit::TreeConstants::nNodesPerFace);
		cl.def_readwrite("lengths", &bitpit::TreeConstants::lengths);
		cl.def_readwrite("areas", &bitpit::TreeConstants::areas);
		cl.def_readwrite("volumes", &bitpit::TreeConstants::volumes);
		cl.def_static("instance", (const struct bitpit::TreeConstants & (*)(unsigned char)) &bitpit::TreeConstants::instance, "C++: bitpit::TreeConstants::instance(unsigned char) --> const struct bitpit::TreeConstants &", pybind11::return_value_policy::automatic, pybind11::arg("dim"));
	}
}
#include <array>
#include <iostream>
#include <sstream> // __str__
#include <volume_kernel.hpp>
#include <volume_skd_tree.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_volume_kernel(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B605_[bitpit::VolumeKernel] ";
	{ // bitpit::VolumeKernel file:volume_kernel.hpp line:32
		pybind11::class_<bitpit::VolumeKernel, std::shared_ptr<bitpit::VolumeKernel>> cl(M("bitpit"), "VolumeKernel", "");
		cl.def("isPointInside", (bool (bitpit::VolumeKernel::*)(double, double, double) const) &bitpit::VolumeKernel::isPointInside, "C++: bitpit::VolumeKernel::isPointInside(double, double, double) const --> bool", pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"));
		cl.def("isPointInside", (bool (bitpit::VolumeKernel::*)(const struct std::array<double, 3> &) const) &bitpit::VolumeKernel::isPointInside, "C++: bitpit::VolumeKernel::isPointInside(const struct std::array<double, 3> &) const --> bool", pybind11::arg("point"));
		cl.def("isPointInside", (bool (bitpit::VolumeKernel::*)(long, double, double, double) const) &bitpit::VolumeKernel::isPointInside, "C++: bitpit::VolumeKernel::isPointInside(long, double, double, double) const --> bool", pybind11::arg("id"), pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"));
		cl.def("isPointInside", (bool (bitpit::VolumeKernel::*)(long, const struct std::array<double, 3> &) const) &bitpit::VolumeKernel::isPointInside, "C++: bitpit::VolumeKernel::isPointInside(long, const struct std::array<double, 3> &) const --> bool", pybind11::arg("id"), pybind11::arg("point"));
		cl.def("evalCellVolume", (double (bitpit::VolumeKernel::*)(long) const) &bitpit::VolumeKernel::evalCellVolume, "C++: bitpit::VolumeKernel::evalCellVolume(long) const --> double", pybind11::arg("id"));
		cl.def("evalInterfaceArea", (double (bitpit::VolumeKernel::*)(long) const) &bitpit::VolumeKernel::evalInterfaceArea, "C++: bitpit::VolumeKernel::evalInterfaceArea(long) const --> double", pybind11::arg("id"));
		cl.def("evalInterfaceNormal", (struct std::array<double, 3> (bitpit::VolumeKernel::*)(long) const) &bitpit::VolumeKernel::evalInterfaceNormal, "C++: bitpit::VolumeKernel::evalInterfaceNormal(long) const --> struct std::array<double, 3>", pybind11::arg("id"));
	}
	std::cout << "B606_[bitpit::VolumeSkdTree] ";
	{ // bitpit::VolumeSkdTree file:volume_skd_tree.hpp line:33
		pybind11::class_<bitpit::VolumeSkdTree, std::shared_ptr<bitpit::VolumeSkdTree>, bitpit::PatchSkdTree> cl(M("bitpit"), "VolumeSkdTree", "");
		cl.def( pybind11::init( [](const class bitpit::VolumeKernel * a0){ return new bitpit::VolumeSkdTree(a0); } ), "doc" , pybind11::arg("patch"));

	}
}
#include <iostream>
#include <sstream> // __str__
#include <volume_mapper.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

void bind_volume_mapper(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B607_[bitpit::mapping::Type] ";
	// bitpit::mapping::Type file:volume_mapper.hpp line:49
	pybind11::enum_<bitpit::mapping::Type>(M("bitpit::mapping"), "Type", pybind11::arithmetic(), "Type of mapping relationship between elements.")
		.value("TYPE_UNKNOWN", bitpit::mapping::TYPE_UNKNOWN)
		.value("TYPE_REFINEMENT", bitpit::mapping::TYPE_REFINEMENT)
		.value("TYPE_COARSENING", bitpit::mapping::TYPE_COARSENING)
		.value("TYPE_RENUMBERING", bitpit::mapping::TYPE_RENUMBERING)
		.export_values();

;

	std::cout << "B608_[bitpit::mapping::Entity] ";
	// bitpit::mapping::Entity file:volume_mapper.hpp line:59
	pybind11::enum_<bitpit::mapping::Entity>(M("bitpit::mapping"), "Entity", pybind11::arithmetic(), "Entity involved in a mapping relationship.")
		.value("ENTITY_UNKNOWN", bitpit::mapping::ENTITY_UNKNOWN)
		.value("ENTITY_CELL", bitpit::mapping::ENTITY_CELL)
		.export_values();

;

	std::cout << "B609_[bitpit::mapping::Info] ";
	{ // bitpit::mapping::Info file:volume_mapper.hpp line:71
		pybind11::class_<bitpit::mapping::Info, std::shared_ptr<bitpit::mapping::Info>> cl(M("bitpit::mapping"), "Info", "The Info is the structure to store info about mapping between elements.\n\n ");
		cl.def( pybind11::init( [](){ return new bitpit::mapping::Info(); } ) );
		cl.def( pybind11::init<enum bitpit::mapping::Type, enum bitpit::mapping::Entity>(), pybind11::arg("user_type"), pybind11::arg("user_entity") );

		cl.def( pybind11::init( [](bitpit::mapping::Info const &o){ return new bitpit::mapping::Info(o); } ) );
		cl.def_readwrite("type", &bitpit::mapping::Info::type);
		cl.def_readwrite("entity", &bitpit::mapping::Info::entity);
		cl.def_readwrite("ids", &bitpit::mapping::Info::ids);
		cl.def_readwrite("ranks", &bitpit::mapping::Info::ranks);
	}
}
#include <Intersection.hpp>
#include <Octant.hpp>
#include <PabloUniform.hpp>
#include <VTK.hpp>
#include <adaption.hpp>
#include <array>
#include <cell.hpp>
#include <element_type.hpp>
#include <fstream>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <locale>
#include <memory>
#include <ostream>
#include <patch_kernel.hpp>
#include <piercedKernel.hpp>
#include <piercedStorage.hpp>
#include <piercedSync.hpp>
#include <piercedVector.hpp>
#include <piercedVectorKernel.hpp>
#include <piercedVectorStorage.hpp>
#include <sstream> // __str__
#include <streambuf>
#include <string>
#include <vector>
#include <vertex.hpp>
#include <voloctree.hpp>
#include <volume_mapper.hpp>

#include <pybind11/pybind11.h>
#include <functional>
#include <string>
#include <pybind11/stl.h>


#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
	PYBIND11_MAKE_OPAQUE(std::shared_ptr<void>);
#endif

// bitpit::VolOctree file:voloctree.hpp line:37
struct PyCallBack_bitpit_VolOctree : public bitpit::VolOctree {
	using bitpit::VolOctree::VolOctree;

	void reset() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "reset");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::reset();
	}
	void setDimension(int a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "setDimension");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::setDimension(a0);
	}
	void settleAdaptionMarkers() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "settleAdaptionMarkers");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::settleAdaptionMarkers();
	}
	double evalCellVolume(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "evalCellVolume");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return VolOctree::evalCellVolume(a0);
	}
	double evalCellSize(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "evalCellSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return VolOctree::evalCellSize(a0);
	}
	using _binder_ret_0 = struct std::array<double, 3>;
	_binder_ret_0 evalCellCentroid(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "evalCellCentroid");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_0>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_0> caster;
				return pybind11::detail::cast_ref<_binder_ret_0>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_0>(std::move(o));
		}
		return VolOctree::evalCellCentroid(a0);
	}
	void simulateCellUpdate(long a0, enum bitpit::adaption::Marker a1, class std::vector<class bitpit::Cell, class std::allocator<class bitpit::Cell> > * a2, class bitpit::PiercedVector<class bitpit::Vertex> * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "simulateCellUpdate");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::simulateCellUpdate(a0, a1, a2, a3);
	}
	void evalCellBoundingBox(long a0, struct std::array<double, 3> * a1, struct std::array<double, 3> * a2) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "evalCellBoundingBox");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::evalCellBoundingBox(a0, a1, a2);
	}
	double evalInterfaceArea(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "evalInterfaceArea");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<double>::value) {
				static pybind11::detail::override_caster_t<double> caster;
				return pybind11::detail::cast_ref<double>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<double>(std::move(o));
		}
		return VolOctree::evalInterfaceArea(a0);
	}
	using _binder_ret_1 = struct std::array<double, 3>;
	_binder_ret_1 evalInterfaceNormal(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "evalInterfaceNormal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_1>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_1> caster;
				return pybind11::detail::cast_ref<_binder_ret_1>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_1>(std::move(o));
		}
		return VolOctree::evalInterfaceNormal(a0);
	}
	bool isPointInside(const struct std::array<double, 3> & a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "isPointInside");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::isPointInside(a0);
	}
	bool isPointInside(long a0, const struct std::array<double, 3> & a1) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "isPointInside");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::isPointInside(a0, a1);
	}
	long locatePoint(const struct std::array<double, 3> & a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "locatePoint");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<long>::value) {
				static pybind11::detail::override_caster_t<long> caster;
				return pybind11::detail::cast_ref<long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<long>(std::move(o));
		}
		return VolOctree::locatePoint(a0);
	}
	void translate(const struct std::array<double, 3> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "translate");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::translate(a0);
	}
	void scale(const struct std::array<double, 3> & a0, const struct std::array<double, 3> & a1) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "scale");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::scale(a0, a1);
	}
	int getCellHaloLayer(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "getCellHaloLayer");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<int>::value) {
				static pybind11::detail::override_caster_t<int> caster;
				return pybind11::detail::cast_ref<int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<int>(std::move(o));
		}
		return VolOctree::getCellHaloLayer(a0);
	}
	using _binder_ret_2 = class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> >;
	_binder_ret_2 _spawn(bool a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_spawn");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_2>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_2> caster;
				return pybind11::detail::cast_ref<_binder_ret_2>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_2>(std::move(o));
		}
		return VolOctree::_spawn(a0);
	}
	void _updateAdjacencies() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_updateAdjacencies");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_updateAdjacencies();
	}
	using _binder_ret_3 = class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> >;
	_binder_ret_3 _adaptionPrepare(bool a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_adaptionPrepare");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_3>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_3> caster;
				return pybind11::detail::cast_ref<_binder_ret_3>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_3>(std::move(o));
		}
		return VolOctree::_adaptionPrepare(a0);
	}
	using _binder_ret_4 = class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> >;
	_binder_ret_4 _adaptionAlter(bool a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_adaptionAlter");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_4>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_4> caster;
				return pybind11::detail::cast_ref<_binder_ret_4>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_4>(std::move(o));
		}
		return VolOctree::_adaptionAlter(a0);
	}
	void _adaptionCleanup() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_adaptionCleanup");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_adaptionCleanup();
	}
	bool _markCellForRefinement(long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_markCellForRefinement");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::_markCellForRefinement(a0);
	}
	bool _markCellForCoarsening(long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_markCellForCoarsening");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::_markCellForCoarsening(a0);
	}
	bool _resetCellAdaptionMarker(long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_resetCellAdaptionMarker");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::_resetCellAdaptionMarker(a0);
	}
	enum bitpit::adaption::Marker _getCellAdaptionMarker(long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_getCellAdaptionMarker");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<enum bitpit::adaption::Marker>::value) {
				static pybind11::detail::override_caster_t<enum bitpit::adaption::Marker> caster;
				return pybind11::detail::cast_ref<enum bitpit::adaption::Marker>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<enum bitpit::adaption::Marker>(std::move(o));
		}
		return VolOctree::_getCellAdaptionMarker(a0);
	}
	bool _enableCellBalancing(long a0, bool a1) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_enableCellBalancing");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<bool>::value) {
				static pybind11::detail::override_caster_t<bool> caster;
				return pybind11::detail::cast_ref<bool>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<bool>(std::move(o));
		}
		return VolOctree::_enableCellBalancing(a0, a1);
	}
	void _setTol(double a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_setTol");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_setTol(a0);
	}
	void _resetTol() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_resetTol");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_resetTol();
	}
	int _getDumpVersion() const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_getDumpVersion");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<int>::value) {
				static pybind11::detail::override_caster_t<int> caster;
				return pybind11::detail::cast_ref<int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<int>(std::move(o));
		}
		return VolOctree::_getDumpVersion();
	}
	void _dump(class std::basic_ostream<char> & a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_dump");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_dump(a0);
	}
	void _restore(class std::basic_istream<char> & a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_restore");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_restore(a0);
	}
	long _getCellNativeIndex(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_getCellNativeIndex");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<long>::value) {
				static pybind11::detail::override_caster_t<long> caster;
				return pybind11::detail::cast_ref<long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<long>(std::move(o));
		}
		return VolOctree::_getCellNativeIndex(a0);
	}
	void _findCellNeighs(long a0, const class std::vector<long, class std::allocator<long> > & a1, class std::vector<long, class std::allocator<long> > * a2) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_findCellNeighs");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_findCellNeighs(a0, a1, a2);
	}
	void _findCellEdgeNeighs(long a0, int a1, const class std::vector<long, class std::allocator<long> > & a2, class std::vector<long, class std::allocator<long> > * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_findCellEdgeNeighs");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_findCellEdgeNeighs(a0, a1, a2, a3);
	}
	void _findCellVertexNeighs(long a0, int a1, const class std::vector<long, class std::allocator<long> > & a2, class std::vector<long, class std::allocator<long> > * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_findCellVertexNeighs");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_findCellVertexNeighs(a0, a1, a2, a3);
	}
	unsigned long _getMaxHaloSize() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_getMaxHaloSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned long>::value) {
				static pybind11::detail::override_caster_t<unsigned long> caster;
				return pybind11::detail::cast_ref<unsigned long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned long>(std::move(o));
		}
		return VolOctree::_getMaxHaloSize();
	}
	void _setHaloSize(unsigned long a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_setHaloSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_setHaloSize(a0);
	}
	using _binder_ret_5 = class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> >;
	_binder_ret_5 _partitioningAlter(bool a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_partitioningAlter");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_5>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_5> caster;
				return pybind11::detail::cast_ref<_binder_ret_5>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_5>(std::move(o));
		}
		return VolOctree::_partitioningAlter(a0);
	}
	void _partitioningCleanup() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_partitioningCleanup");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return VolOctree::_partitioningCleanup();
	}
	using _binder_ret_6 = class std::vector<long, class std::allocator<long> >;
	_binder_ret_6 _findGhostCellExchangeSources(int a0) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_findGhostCellExchangeSources");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_6>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_6> caster;
				return pybind11::detail::cast_ref<_binder_ret_6>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_6>(std::move(o));
		}
		return VolOctree::_findGhostCellExchangeSources(a0);
	}
	void resetVertices() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "resetVertices");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::resetVertices();
	}
	void resetCells() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "resetCells");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::resetCells();
	}
	void resetInterfaces() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "resetInterfaces");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::resetInterfaces();
	}
	long getVertexCount() const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "getVertexCount");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<long>::value) {
				static pybind11::detail::override_caster_t<long> caster;
				return pybind11::detail::cast_ref<long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<long>(std::move(o));
		}
		return PatchKernel::getVertexCount();
	}
	long getCellCount() const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "getCellCount");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<long>::value) {
				static pybind11::detail::override_caster_t<long> caster;
				return pybind11::detail::cast_ref<long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<long>(std::move(o));
		}
		return PatchKernel::getCellCount();
	}
	enum bitpit::ElementType getCellType(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "getCellType");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<enum bitpit::ElementType>::value) {
				static pybind11::detail::override_caster_t<enum bitpit::ElementType> caster;
				return pybind11::detail::cast_ref<enum bitpit::ElementType>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<enum bitpit::ElementType>(std::move(o));
		}
		return PatchKernel::getCellType(a0);
	}
	long getInterfaceCount() const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "getInterfaceCount");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<long>::value) {
				static pybind11::detail::override_caster_t<long> caster;
				return pybind11::detail::cast_ref<long>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<long>(std::move(o));
		}
		return PatchKernel::getInterfaceCount();
	}
	enum bitpit::ElementType getInterfaceType(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "getInterfaceType");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<enum bitpit::ElementType>::value) {
				static pybind11::detail::override_caster_t<enum bitpit::ElementType> caster;
				return pybind11::detail::cast_ref<enum bitpit::ElementType>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<enum bitpit::ElementType>(std::move(o));
		}
		return PatchKernel::getInterfaceType(a0);
	}
	using _binder_ret_7 = struct std::array<double, 3>;
	_binder_ret_7 evalInterfaceCentroid(long a0) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "evalInterfaceCentroid");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<_binder_ret_7>::value) {
				static pybind11::detail::override_caster_t<_binder_ret_7> caster;
				return pybind11::detail::cast_ref<_binder_ret_7>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<_binder_ret_7>(std::move(o));
		}
		return PatchKernel::evalInterfaceCentroid(a0);
	}
	void evalInterfaceBoundingBox(long a0, struct std::array<double, 3> * a1, struct std::array<double, 3> * a2) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "evalInterfaceBoundingBox");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::evalInterfaceBoundingBox(a0, a1, a2);
	}
	void flushData(class std::basic_fstream<char> & a0, const class std::__cxx11::basic_string<char> & a1, enum bitpit::VTKFormat a2) override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "flushData");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::flushData(a0, a1, a2);
	}
	void _resetAdjacencies() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_resetAdjacencies");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::_resetAdjacencies();
	}
	void _resetInterfaces() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_resetInterfaces");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::_resetInterfaces();
	}
	void _updateInterfaces() override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_updateInterfaces");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::_updateInterfaces();
	}
	void _findCellFaceNeighs(long a0, int a1, const class std::vector<long, class std::allocator<long> > & a2, class std::vector<long, class std::allocator<long> > * a3) const override {
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const bitpit::VolOctree *>(this), "_findCellFaceNeighs");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2, a3);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::override_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return PatchKernel::_findCellFaceNeighs(a0, a1, a2, a3);
	}
};

void bind_volume_mapper_1(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	std::cout << "B610_[bitpit::VolumeMapper] ";
	{ // bitpit::VolumeMapper file:volume_mapper.hpp line:102
		pybind11::class_<bitpit::VolumeMapper, std::shared_ptr<bitpit::VolumeMapper>> cl(M("bitpit"), "VolumeMapper", "");
		cl.def("clear", (void (bitpit::VolumeMapper::*)()) &bitpit::VolumeMapper::clear, "C++: bitpit::VolumeMapper::clear() --> void");
		cl.def("clearMapping", (void (bitpit::VolumeMapper::*)()) &bitpit::VolumeMapper::clearMapping, "C++: bitpit::VolumeMapper::clearMapping() --> void");
		cl.def("clearInverseMapping", (void (bitpit::VolumeMapper::*)()) &bitpit::VolumeMapper::clearInverseMapping, "C++: bitpit::VolumeMapper::clearInverseMapping() --> void");
		cl.def("initialize", [](bitpit::VolumeMapper &o) -> void { return o.initialize(); }, "");
		cl.def("initialize", (void (bitpit::VolumeMapper::*)(bool)) &bitpit::VolumeMapper::initialize, "C++: bitpit::VolumeMapper::initialize(bool) --> void", pybind11::arg("fillInv"));
		cl.def("adaptionPrepare", [](bitpit::VolumeMapper &o, const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > & a0) -> void { return o.adaptionPrepare(a0); }, "", pybind11::arg("infoAdapt"));
		cl.def("adaptionPrepare", (void (bitpit::VolumeMapper::*)(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &, bool)) &bitpit::VolumeMapper::adaptionPrepare, "C++: bitpit::VolumeMapper::adaptionPrepare(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &, bool) --> void", pybind11::arg("infoAdapt"), pybind11::arg("reference"));
		cl.def("adaptionAlter", [](bitpit::VolumeMapper &o, const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > & a0) -> void { return o.adaptionAlter(a0); }, "", pybind11::arg("infoAdapt"));
		cl.def("adaptionAlter", [](bitpit::VolumeMapper &o, const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > & a0, bool const & a1) -> void { return o.adaptionAlter(a0, a1); }, "", pybind11::arg("infoAdapt"), pybind11::arg("reference"));
		cl.def("adaptionAlter", (void (bitpit::VolumeMapper::*)(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &, bool, bool)) &bitpit::VolumeMapper::adaptionAlter, "C++: bitpit::VolumeMapper::adaptionAlter(const class std::vector<struct bitpit::adaption::Info, class std::allocator<struct bitpit::adaption::Info> > &, bool, bool) --> void", pybind11::arg("infoAdapt"), pybind11::arg("reference"), pybind11::arg("fillInv"));
		cl.def("adaptionCleanup", (void (bitpit::VolumeMapper::*)()) &bitpit::VolumeMapper::adaptionCleanup, "C++: bitpit::VolumeMapper::adaptionCleanup() --> void");
		cl.def("checkPartition", (bool (bitpit::VolumeMapper::*)()) &bitpit::VolumeMapper::checkPartition, "C++: bitpit::VolumeMapper::checkPartition() --> bool");
	}
	std::cout << "B611_[bitpit::VolOctree] ";
	{ // bitpit::VolOctree file:voloctree.hpp line:37
		pybind11::class_<bitpit::VolOctree, std::shared_ptr<bitpit::VolOctree>, PyCallBack_bitpit_VolOctree, bitpit::VolumeKernel> cl(M("bitpit"), "VolOctree", "");
		cl.def("isPointInside", [](bitpit::VolOctree const &o, long const & a0, double const & a1, double const & a2, double const & a3) -> bool { return o.isPointInside(a0, a1, a2, a3); }, "", pybind11::arg("id"), pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"));
		cl.def("isPointInside", [](bitpit::VolOctree const &o, double const & a0, double const & a1, double const & a2) -> bool { return o.isPointInside(a0, a1, a2); }, "", pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"));
		cl.def("locatePoint", [](bitpit::VolOctree const &o, double const & a0, double const & a1, double const & a2) -> long { return o.locatePoint(a0, a1, a2); }, "", pybind11::arg("x"), pybind11::arg("y"), pybind11::arg("z"));
		cl.def("reset", (void (bitpit::VolOctree::*)()) &bitpit::VolOctree::reset, "C++: bitpit::VolOctree::reset() --> void");
		cl.def("setDimension", (void (bitpit::VolOctree::*)(int)) &bitpit::VolOctree::setDimension, "C++: bitpit::VolOctree::setDimension(int) --> void", pybind11::arg("dimension"));
		cl.def("settleAdaptionMarkers", (void (bitpit::VolOctree::*)()) &bitpit::VolOctree::settleAdaptionMarkers, "C++: bitpit::VolOctree::settleAdaptionMarkers() --> void");
		cl.def("evalCellVolume", (double (bitpit::VolOctree::*)(long) const) &bitpit::VolOctree::evalCellVolume, "C++: bitpit::VolOctree::evalCellVolume(long) const --> double", pybind11::arg("id"));
		cl.def("evalCellSize", (double (bitpit::VolOctree::*)(long) const) &bitpit::VolOctree::evalCellSize, "C++: bitpit::VolOctree::evalCellSize(long) const --> double", pybind11::arg("id"));
		cl.def("evalCellCentroid", (struct std::array<double, 3> (bitpit::VolOctree::*)(long) const) &bitpit::VolOctree::evalCellCentroid, "C++: bitpit::VolOctree::evalCellCentroid(long) const --> struct std::array<double, 3>", pybind11::arg("id"));
		cl.def("simulateCellUpdate", (void (bitpit::VolOctree::*)(long, enum bitpit::adaption::Marker, class std::vector<class bitpit::Cell, class std::allocator<class bitpit::Cell> > *, class bitpit::PiercedVector<class bitpit::Vertex> *) const) &bitpit::VolOctree::simulateCellUpdate, "C++: bitpit::VolOctree::simulateCellUpdate(long, enum bitpit::adaption::Marker, class std::vector<class bitpit::Cell, class std::allocator<class bitpit::Cell> > *, class bitpit::PiercedVector<class bitpit::Vertex> *) const --> void", pybind11::arg("id"), pybind11::arg("marker"), pybind11::arg("virtualCells"), pybind11::arg("virtualVertices"));
		cl.def("evalCellBoundingBox", (void (bitpit::VolOctree::*)(long, struct std::array<double, 3> *, struct std::array<double, 3> *) const) &bitpit::VolOctree::evalCellBoundingBox, "C++: bitpit::VolOctree::evalCellBoundingBox(long, struct std::array<double, 3> *, struct std::array<double, 3> *) const --> void", pybind11::arg("id"), pybind11::arg("minPoint"), pybind11::arg("maxPoint"));
		cl.def("evalInterfaceArea", (double (bitpit::VolOctree::*)(long) const) &bitpit::VolOctree::evalInterfaceArea, "C++: bitpit::VolOctree::evalInterfaceArea(long) const --> double", pybind11::arg("id"));
		cl.def("evalInterfaceNormal", (struct std::array<double, 3> (bitpit::VolOctree::*)(long) const) &bitpit::VolOctree::evalInterfaceNormal, "C++: bitpit::VolOctree::evalInterfaceNormal(long) const --> struct std::array<double, 3>", pybind11::arg("id"));
		cl.def("getCellOctant", (struct bitpit::VolOctree::OctantInfo (bitpit::VolOctree::*)(long) const) &bitpit::VolOctree::getCellOctant, "C++: bitpit::VolOctree::getCellOctant(long) const --> struct bitpit::VolOctree::OctantInfo", pybind11::arg("id"));
		cl.def("getCellLevel", (int (bitpit::VolOctree::*)(long) const) &bitpit::VolOctree::getCellLevel, "C++: bitpit::VolOctree::getCellLevel(long) const --> int", pybind11::arg("id"));
		cl.def("getCellFamilySplitLocalVertex", (int (bitpit::VolOctree::*)(long) const) &bitpit::VolOctree::getCellFamilySplitLocalVertex, "C++: bitpit::VolOctree::getCellFamilySplitLocalVertex(long) const --> int", pybind11::arg("id"));
		cl.def("getOctantId", (long (bitpit::VolOctree::*)(const struct bitpit::VolOctree::OctantInfo &) const) &bitpit::VolOctree::getOctantId, "C++: bitpit::VolOctree::getOctantId(const struct bitpit::VolOctree::OctantInfo &) const --> long", pybind11::arg("octantInfo"));
		cl.def("getOctantPointer", (class bitpit::Octant * (bitpit::VolOctree::*)(const struct bitpit::VolOctree::OctantInfo &)) &bitpit::VolOctree::getOctantPointer, "C++: bitpit::VolOctree::getOctantPointer(const struct bitpit::VolOctree::OctantInfo &) --> class bitpit::Octant *", pybind11::return_value_policy::automatic, pybind11::arg("octantInfo"));
		cl.def("isPointInside", (bool (bitpit::VolOctree::*)(const struct std::array<double, 3> &) const) &bitpit::VolOctree::isPointInside, "C++: bitpit::VolOctree::isPointInside(const struct std::array<double, 3> &) const --> bool", pybind11::arg("point"));
		cl.def("isPointInside", (bool (bitpit::VolOctree::*)(long, const struct std::array<double, 3> &) const) &bitpit::VolOctree::isPointInside, "C++: bitpit::VolOctree::isPointInside(long, const struct std::array<double, 3> &) const --> bool", pybind11::arg("id"), pybind11::arg("point"));
		cl.def("locatePoint", (long (bitpit::VolOctree::*)(const struct std::array<double, 3> &) const) &bitpit::VolOctree::locatePoint, "C++: bitpit::VolOctree::locatePoint(const struct std::array<double, 3> &) const --> long", pybind11::arg("point"));
		cl.def("getOrigin", (struct std::array<double, 3> (bitpit::VolOctree::*)() const) &bitpit::VolOctree::getOrigin, "C++: bitpit::VolOctree::getOrigin() const --> struct std::array<double, 3>");
		cl.def("setOrigin", (void (bitpit::VolOctree::*)(const struct std::array<double, 3> &)) &bitpit::VolOctree::setOrigin, "C++: bitpit::VolOctree::setOrigin(const struct std::array<double, 3> &) --> void", pybind11::arg("origin"));
		cl.def("translate", (void (bitpit::VolOctree::*)(const struct std::array<double, 3> &)) &bitpit::VolOctree::translate, "C++: bitpit::VolOctree::translate(const struct std::array<double, 3> &) --> void", pybind11::arg("translation"));
		cl.def("getLength", (double (bitpit::VolOctree::*)() const) &bitpit::VolOctree::getLength, "C++: bitpit::VolOctree::getLength() const --> double");
		cl.def("setLength", (void (bitpit::VolOctree::*)(double)) &bitpit::VolOctree::setLength, "C++: bitpit::VolOctree::setLength(double) --> void", pybind11::arg("length"));
		cl.def("scale", (void (bitpit::VolOctree::*)(const struct std::array<double, 3> &, const struct std::array<double, 3> &)) &bitpit::VolOctree::scale, "C++: bitpit::VolOctree::scale(const struct std::array<double, 3> &, const struct std::array<double, 3> &) --> void", pybind11::arg("scaling"), pybind11::arg("center"));
		cl.def("getCellHaloLayer", (int (bitpit::VolOctree::*)(long) const) &bitpit::VolOctree::getCellHaloLayer, "C++: bitpit::VolOctree::getCellHaloLayer(long) const --> int", pybind11::arg("id"));

		{ // bitpit::VolOctree::OctantInfo file:voloctree.hpp line:43
			auto & enclosing_class = cl;
			pybind11::class_<bitpit::VolOctree::OctantInfo, std::shared_ptr<bitpit::VolOctree::OctantInfo>> cl(enclosing_class, "OctantInfo", "");
			cl.def( pybind11::init( [](){ return new bitpit::VolOctree::OctantInfo(); } ) );
			cl.def( pybind11::init<unsigned int, bool>(), pybind11::arg("_id"), pybind11::arg("_internal") );

			cl.def_readwrite("id", &bitpit::VolOctree::OctantInfo::id);
			cl.def_readwrite("internal", &bitpit::VolOctree::OctantInfo::internal);
			cl.def("__eq__", (bool (bitpit::VolOctree::OctantInfo::*)(const struct bitpit::VolOctree::OctantInfo &) const) &bitpit::VolOctree::OctantInfo::operator==, "C++: bitpit::VolOctree::OctantInfo::operator==(const struct bitpit::VolOctree::OctantInfo &) const --> bool", pybind11::arg("other"));
		}

		{ // bitpit::VolOctree::OctantInfoHasher file:voloctree.hpp line:56
			auto & enclosing_class = cl;
			pybind11::class_<bitpit::VolOctree::OctantInfoHasher, std::shared_ptr<bitpit::VolOctree::OctantInfoHasher>> cl(enclosing_class, "OctantInfoHasher", "");
			cl.def( pybind11::init( [](){ return new bitpit::VolOctree::OctantInfoHasher(); } ) );
			cl.def("__call__", (unsigned long (bitpit::VolOctree::OctantInfoHasher::*)(const struct bitpit::VolOctree::OctantInfo &) const) &bitpit::VolOctree::OctantInfoHasher::operator(), "C++: bitpit::VolOctree::OctantInfoHasher::operator()(const struct bitpit::VolOctree::OctantInfo &) const --> unsigned long", pybind11::arg("k"));
		}

	}
}
