#include <iostream>
#include <string>
#include "Neos.hpp"

using namespace neos;

int main() {

  Neos_Init();

  int dim  = GRID_3D;
  Grid        *grid = new Grid(-20, -10, -5, 20.0, 0.5, dim);
  ASphere     *geo  = new ASphere(10, 15, 10, 2.0, dim);
  ASphere     *geo2 = new ASphere(5, 7, 13, 2.0, dim);
  ASphere     *geo3 = new ASphere(7, 7, 3, 2.0, dim);

  LevelSet *ls = new LevelSet(grid);

  geo->computeLevelSet(grid);
  geo2->computeLevelSet(grid);
  geo3->computeLevelSet(grid);

  ls->addGeometry(geo, "TAG");
  ls->addGeometry(geo2, "TAG");
  ls->addGeometry(geo3);

  std::vector<double> phi = ls->getLevelSet("TAG");

  grid->setExportName("levelset");
  grid->addData("phi", phi);
  grid->write();
  delete geo;
  delete geo2;
  delete geo3;
  delete grid;

  Neos_Finalize();

  return 0;
}
