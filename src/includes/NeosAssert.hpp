/* -------------------------------------------------------------------------*\
 *
 *  NEOS
 *
 *  -------------------------------------------------------------------------
 *  License
 *  This file is part of Neos.
 *
 *  Neos is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License v3 (LGPL)
 *  as published by the Free Software Foundation.
 *
 *  Neos is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Neos. If not, see <http://www.gnu.org/licenses/>.
 *
\*---------------------------------------------------------------------------*/

/**
 * @file   NeosAssert.hpp
 * @author Antoine Gerard <antoine.gerard@inria.fr>
 * @date   Fri Sep  6 10:11:46 2019
 *
 * @brief  This file contains assertion macros
 *
 *
 */

#ifndef __NEOSASSERT_HPP__
#define __NEOSASSERT_HPP__

#define ASSERT(condition, message)              \
  do { \
    if (!(condition)) { \
      std::cerr << std::endl << "\033[1m\033[31mFatal error:\033[0m\n assertion `" #condition "` failed in " \
                << __FILE__ << " line " << __LINE__ << ": " << message << std::endl; \
      std::exit(EXIT_FAILURE); \
    } \
  } while (false)

#endif
